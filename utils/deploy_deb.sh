#!/bin/bash
#
# This script is used to deploy SisIYA debian packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
error()
{
	echo "$0: Error: $1"
	exit 1
}

deploy()
{
	ubuntu_code_name="$1"

	echo "$0: Creating source packages in $output_dir for $ubuntu_code_name ..."

	# clean up
	if [ -d "$output_dir/deb" ]; then
		rm -rf "$output_dir/deb/*${version_str}*" || error "rm -f $output_dir/deb/*${version_str}*"
	fi

	cd "$git_dir" || error "cd $git_dir"

	"$git_dir/utils/create_source_packages.sh" "$output_dir" "$ubuntu_code_name"

	echo "$0: Building DEB packages for $ubuntu_code_name ..."
	cd "$output_dir/deb" || error "cd $output_dir/deb"

	for file in *"${version_str}.orig.tar.gz" ; do
 		f=${file/"_${version_str}.orig.tar.gz"}
		echo "$0: Building source files from ${f}_${version_str}.orig.tar.gz for $ubuntu_code_name ..."
		tar xfz "${f}_${version_str}.orig.tar.gz" || error "tar xfz ${f}_${version_str}.orig.tar.gz"
	
		cd "${f}-${version_str}" || error "cd ${f}-${version_str}"

		# build only source and sign it (-S == --build=source)
		debuild -S -k059E695F8ED833FC729C43B688F79F8032671810 || error "debuild -S -k059E695F8ED833FC729C43B688F79F8032671810"

		cd .. || error "cd .."
		
	done

	for f in *"${version_str}"*.changes
	do
		echo "$0: Uploading $f to launchpad.net for $ubuntu_code_name ..."
		dput  ppa:erdalmutlu/sisiya "$f"
	done
}
###########################################################################
if [ $# -ne 3 ]; then
	echo "Usage  : sisiya_git_directory output_directory version_str "
	echo "Example: $HOME/git/sisiya $HOME/tmp_sisiya_output 0.6.78"
	exit 1
fi

git_dir="$1"
output_dir="$2"
version_str="$3"
ubuntu_code_names="jammy focal bionic xenial trusty"

if [ ! -d "$git_dir" ]; then
	error "SisIYA git directory $git_dir does not exist!"
fi

echo "$0: Creating output directory $output_dir ..."
mkdir -p "$output_dir" || error "mkdir -p $output_dir"

for v in $ubuntu_code_names
do
	deploy "$v"
done
