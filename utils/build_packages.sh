#!/usr/bin/env bash
#
# This script is used for building SisIYA packages.
#
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
### defaults
project_name="sisiya"
git_url="https://gitlab.com/erdalmutlu/sisiya.git"
git_dir="$HOME/git"
git_branch="master"
clean_git_dir="true"
project_dir="$git_dir/$project_name"
build_status_file="$HOME/build_status_${project_name}.txt"
deb_build_prog="$project_dir/utils/build_deb.sh"
rpm_build_prog="$project_dir/utils/build_rpm.sh"
pacman_build_prog="$project_dir/utils/build_pacman.sh"
log_dir="$HOME"
tmp_output_dir=$HOME/tmp_sisiya_dir
### end of defaults
#################################################################################
error() {
	echo "$0: $1"
	exit 1
}

error_without_exit() {
	echo "$0: $1"
}

cleanup() {
        rm -rf "$log_file"
}

echo_log() {
	date_str=$(date '+%Y%m%d%H%M%S')
	echo "$date_str : $1" >> "$log_file"
}

record_build_status() {
	tmp_file=$(mktemp)
	if [[ -f "$build_status_file" ]]; then
		grep -v "$1" "$build_status_file" > "$tmp_file"
		cp -f "$tmp_file" "$build_status_file"
	else
		touch "$build_status_file"
	fi
	# mv does not work across file systems
	rm -f "$tmp_file"
	echo "${1}:$2" >> "$build_status_file"
}

check_build_status() {
	echo "$0: Checking last build status for $1..."
	if [[ ! -f "$build_status_file" ]]; then
		touch "$build_status_file"
		return
	fi

	str=$(grep "$1" "$build_status_file" | grep "OK")
	if [[ -n "$str" ]]; then
		echo "$0: Packages for $project_name $1 are already built."
		echo "$0: If you would like to rebuild, please remove the following line from $build_status_file file"
		echo "$1"
		exit 0
	fi
}

check_prerequisites() {
	return
#	for f in $HOME/.git-credentials
#	do
#		if [[ ! -f $f ]]; then
#			error "File $f does not exist"
#		fi
#	done
}

get_version()
{
	version_str=$(git describe | tr "-" ".")
	if [[ -z "$version_str" ]]; then
		echo "$0: git describe returned epmty string. No tags found!"
		exit 1
	fi
}

set_build_prog() {
#############################################
# ls_release -a command's output
# on Arch linux system
#LSB Version:	1.4
#Distributor ID:	Arch
#Description:	Arch Linux
#Release:	rolling
#Codename:	n/a
# on a CentOS system
#Distributor ID:	CentOS
#Description:	CentOS release 6.10 (Final)
#Release:	6.10
#Codename:	Final
# on Ubuntu system
#No LSB modules are available.
#Distributor ID:	Ubuntu
#Description:	Ubuntu 12.04.5 LTS
#Release:	12.04
#Codename:	precise
#############################################
	build_prog="unknown distribution"
	if which lsb_release >/dev/null 2>&1 ; then
		if ! distro_str=$(lsb_release --id 2>/dev/null | awk '{print $3}') ; then
			error "Error occured during lsb_release -a"
		fi
	else
		# AlmaLinux and RockyLinux distributions does not have lsb_release command,
		# which comes with redhat-lsb-core package.
		distro_str=$(grep ^ID= /etc/os-release | cut -d "=" -f 2 | cut -d '"' -f 2)
	fi
	case "$distro_str" in
		"Arch")
			build_prog=$pacman_build_prog
			;;
		"AlmaLinux" | "almalinux" | "CentOS" | "RockyLinux" | "rocky")
			build_prog=$rpm_build_prog
			;;
		"Ubuntu")
			build_prog=$deb_build_prog
			;;
	esac
}
#################################################################################
trap cleanup EXIT
log_file=$(mktemp)


if [[ $# -eq 1 ]]; then
	conf_file="$1"
	if [[ -f "$conf_file" ]]; then
		### source the conf file
		# shellcheck disable=SC1090
		source "$conf_file"
	fi
else
	conf_file="build_packages.conf"
	if [[ -f "$conf_file" ]]; then
		### source the conf file
		# shellcheck disable=SC1090
		source "$conf_file"
	fi
fi

check_prerequisites

mkdir -p "$git_dir"

cd "$git_dir" || error "Could not change to directory $git_dir"

# configure credential store
# git config --global credential.helper store

if [[ "$clean_git_dir" == "true" ]]; then
	echo_log "Removing $git_dir/$project_name ..."
	rm -rf "${git_dir:?}/$project_name"
	echo_log "Cloning $git_url ..." >> "$log_file"
	#git clone --depth 1 --single-branch $git_url >> $log_file 2>&1
	#str=`git clone --depth 1 --single-branch --branch $git_branch $git_url 2>&1`
	if ! git clone --branch "$git_branch" "$git_url" 2>&1 ; then
		echo_log "$str"
		error "Could not clone git: git clone --branch $git_branch $git_url"
	fi
	echo_log "$str"
else
	echo_log "Skipping rm -rf $git_dir/$project_name"
fi

cd "$project_dir" || error "Could not change to directory $project_dir"
last_commit_hash=$(git rev-parse  HEAD)
#echo "Last commit : $last_commit_hash log_file: $log_file"

get_version

check_build_status "$last_commit_hash"

echo "$0: Building..."
set_build_prog
if [[ ! -f "$build_prog" ]]; then
	error "$0: No such file $build_prog"
fi

mkdir -p "$tmp_output_dir" || error "Could not create tmp output directory $tmp_output_dir"

echo_log "Started to build using $build_prog ..."
if "$build_prog" "$project_dir" "$tmp_output_dir" "$version_str" >> "$log_file" 2>&1 ; then
	# archive
	record_build_status "$last_commit_hash" "OK"
	echo_log "Successfully built $project_name $last_commit_hash"
	cp -f "$log_file" "$log_dir/${project_name}_${last_commit_hash}.log"
	echo "$0: Suceessfuly built. For details have a look at log file: $log_dir/${project_name}_${last_commit_hash}.log"
else
	record_build_status "$last_commit_hash" "FAILED"
	echo_log "Could not build $project_name $last_commit_hash"
	cp -f "$log_file" "$log_dir/${project_name}_${last_commit_hash}.log"
	echo "$0: Failed to build! Please have a look at log file: $log_dir/${project_name}_${last_commit_hash}.log"
	exit 1
fi
