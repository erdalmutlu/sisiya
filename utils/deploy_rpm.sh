#!/bin/bash
#
# This script is used to deploy SisIYA RPM packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
error()
{
	echo "$0: Error: $1"
	exit 1
}

###########################################################################
if [ $# -ne 3 ]; then
	echo "Usage  : sisiya_git_directory output_directory version_str "
	echo "Example: $HOME/git/sisiya $HOME/tmp_sisiya_output 0.6.78"
	exit 1
fi

git_dir="$1"
output_dir="$2"
version_str="$3"
osc_dir="$HOME/prj"

if [ ! -d "$git_dir" ]; then
	error "SisIYA git directory $git_dir does not exist!"
fi

echo "$0: Creating output directory $output_dir ..."
mkdir -p "$output_dir" || error "mkdir -p $output_dir"

cd "$git_dir" || error "cd $git_dir"

echo "$0: Creating source packages in $output_dir ..."
"$git_dir/utils/create_source_packages.sh" "$output_dir"

echo "$0: Deploying RPM packages..."
cd "$output_dir/rpm" || error "cd $output_dir/rpm"

if [ ! -e "$osc_dir" ]; then
	error "Directory $osc_dir does not exist! Please use osc to checkout the repository. And link it as $osc_dir"
fi

package_list=$(ls *${version_str}.tar.gz | sed -e "s/-${version_str}.tar.gz//")
for f in $package_list ; do
# for file in *"${version_str}.tar.gz" ; do
#	f=${file/"-${version_str}.tar.gz"}
	echo "$0: Deploying ${f}-${version_str}.tar.gz ..."
	cd "$osc_dir/$f" || error "cd $osc_dir/$f"
	# remove old files
	for x in *.tar.gz; do
		[ -e "$x" ] || continue
		osc remove "$x"	|| error "osc remove $x"
	done
	cp -f "$output_dir/rpm/${f}-${version_str}.tar.gz" .	|| error "cp -f $output_dir/rpm/${f}-${version_str}.tar.gz ."
	tar xfz "${f}-${version_str}.tar.gz"			|| error "tar xfz ${f}-${version_str}.tar.gz"
	cp "${f}-${version_str}/${f}.spec" .			|| error "cp ${f}-${version_str}/${f}.spec ."
	rm -rf "${f}-${version_str}"				|| error "rm -rf ${f}-${version_str}"
	osc add "${f}-${version_str}.tar.gz"			|| error "osc add ${f}-${version_str}.tar.gz"
	osc commit -m "auto deploy"				|| error "osc commit"
done
