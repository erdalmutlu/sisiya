#!/bin/bash
#
# This script is used to build SisIYA debian packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
error() {
	echo "$0: Error: $1"
	exit 1
}

get_ubuntu_code_name() {
	ubuntu_code_name=$(lsb_release -a 2>/dev/null | grep "Codename:" | awk '{print $2}')
}
###########################################################################
if [[ $# -ne 3 ]]; then
	echo "Usage  : sisiya_git_directory output_directory version_str "
	echo "Example: $HOME/git/sisiya $HOME/tmp_sisiya_output 0.6.78"
	exit 1
fi

git_dir="$1"
output_dir="$2"
version_str="$3"

if [[ ! -d $git_dir ]]; then
	error "SisIYA git directory $git_dir does not exist!"
fi

echo "$0: Creating output directory $output_dir ..."
mkdir -p "$output_dir" || error "mkdir -p $output_dir"

cd "$git_dir" || error "cd $git_dir"

echo "$0: Creating source packages in $output_dir ..."
"$git_dir/utils/create_source_packages.sh" "$output_dir" "$ubuntu_code_name"

echo "$0: Building DEB packages..."
cd "$output_dir/deb" || error "cd $output_dir/deb"

# shellcheck disable=SC2010
package_list=$(ls ./*orig.tar.gz | grep "$version_str" | sed -e "s/_${version_str}.orig.tar.gz//")
for f in $package_list ; do
	echo "$0: Building ${f}_${version_str}.orig.tar.gz ..."
	tar xfz "${f}_${version_str}.orig.tar.gz" || error "tar xfz ${f}_${version_str}.orig.tar.gz"

	# build binary package without signing
	cd "${f}-${version_str}" || error "cd ${f}-${version_str}"
	# -b     Equivalent to --build=binary or --build=any,all
	debuild -b -us -uc || error "debuild -b -us -uc"

	# build only source and sign it (-S == --build=source)
	# cd ${f}-${version_str} && debuild -S -k7F640C1A
	# cd ${f}-${version_str} && debuild --build=source -k7F640C1A

	cd ..
done
