#!/bin/bash
#
# This script is used to build RPM packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
error() {
	echo "$0: Error: $1"
	exit 1
}
###########################################################################
if [[ $# -ne 3 ]]; then
	echo "Usage  : sisiya_git_directory output_directory version_str "
	echo "Example: $HOME/git/sisiya $HOME/tmp_sisiya_output 0.6.78"
	exit 1
fi

git_dir="$1"
output_dir="$2"
version_str="$3"

if [[ ! -d "$git_dir" ]]; then
	error "SisIYA git directory $git_dir does not exist!"
fi

echo "$0: Creating output directory $output_dir ..."
mkdir -p "$output_dir" || error "mkdir -p $output_dir"

cd "$git_dir" || error "cd $git_dir"

echo "$0: Creating source packages in $output_dir ..."
"$git_dir/utils/create_source_packages.sh" "$output_dir"

echo "$0: Building RPMs..."
cd "$output_dir/rpm" || error "cd $output_dir/rpm"
for f in *"${version_str}.tar.gz" ; do
	# str=$(echo "$f" | grep -E 'sisiya-edbc-libs|sisiyad')
	# if [[ -n "$str" ]]; then
	# 	# we cannot build this package any more
	# 	echo "$0: Skipping package $f..."
	# 	continue
	# fi
	echo "$0: Building package $f ..."
	if ! rpmbuild -ta "$f" ; then
		error "Could not build package $f"
	fi
done
