#!/bin/bash
#
# This script is used to generate SisIYA source packages for all components.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
#################################################################################
check_git_porcelain()
{
	str=`git status --porcelain`
	if [ -n "$str" ]; then
		echo "$0: The working tree is not clean!"
		git status --porcelain
		error "Please commit your changes or disgard them and try againi!"
	fi
}

cleanup()
{
	rm -f $tmp_file
}

error()
{
	echo "$0: $1"
	exit 1
}


get_version()
{
	# version_str=0.6.78.31.g8bc1c38
	version_str=`git describe | tr "-" "."`
	if [ -z "$version_str" ]; then
		echo "$0: git describe returned epmty string. No tags found!"
		exit 1
	fi
	# version_major_str=`echo $version_str | cut -d "." -f 1,2`
	# version_minor_str=`echo $version_str | cut -d "." -f 3`
	release_str="1"
}

change_libdir()
{
	# I could not find a way in Ubuntu to pass LIBDIR to Makefile.am
	# for now I use this workaround
	sed -i "s~\$(LIBDIR)~/usr/lib~" $1/Makefile.am
}

create_webui_images()
{

	package_str="sisiya-webui-images"
	#package_name="${package_str}-${version_str}-$release_str"
	package_name="${package_str}-${version_str}"
	package_dir="$base_dir/tmp/${package_name}"

	### common package directory for all package types (rpm, deb, pacman ...)
	rm -rf $package_dir
	mkdir -p $package_dir
	cp -a ${source_dir}/$package_str/* $package_dir/
	echo "${version_str}-$release_str" > $package_dir/version.txt
	cat ${source_dir}/$package_str/debian/copyright | sed -e "s/__YEAR__/${year_str}/"  > $package_dir/debian/copyright
	cat ${source_dir}/$package_str/debian/changelog | sed -e "s/__DISTRO_NAME__/${distro_name}/"  > $package_dir/debian/changelog
	update_changelog
	#############################
	### create RPM source package
	#############################
	rpm_root_dir="$base_dir/rpm/$package_name"
	echo -n "Creating ${rpm_root_dir}.tar.gz ..."
	rm -rf $rpm_root_dir
	cp -a $package_dir $rpm_root_dir
	rm -rf $rpm_root_dir/debian
	cat $source_dir/packaging/rpmspec/${package_str}.spec 	| sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/"  > $rpm_root_dir/${package_str}.spec 
	(cd $base_dir/rpm ; tar -cz -f ${package_name}.tar.gz $package_name)
	rm -rf $rpm_root_dir
	echo "OK"
	################################
	### create Debian source package
	################################
	deb_root_dir="$base_dir/deb/$package_name"
	echo -n "Creating $base_dir/deb/${package_str}_${version_str}.orig.tar.gz ..."
	rm -rf $deb_root_dir 
	mkdir -p $deb_root_dir/var/lib/${package_str} 
	for f in  debian version.txt
	do
		cp -a $package_dir/$f $deb_root_dir
	done
	cp -a $package_dir/*.png $deb_root_dir/var/lib/${package_str}
	(cd $base_dir/deb ; tar cfz ${package_str}_${version_str}.orig.tar.gz $package_name) 
	rm -rf $deb_root_dir 
	echo "OK"
	###############################################
	### create directory structure for Arch systems
	###############################################
	pacman_root_dir="$base_dir/pacman/$package_name"
	echo -n "Creating ${pacman_root_dir}.tar.gz ..."
	rm -rf $pacman_root_dir 
	cp -a $package_dir $pacman_root_dir
	rm -rf $pacman_root_dir/debian
	(cd $base_dir/pacman ; tar cfz ${package_name}.tar.gz $package_name )
	md5sum_str=`md5sum $base_dir/pacman/${package_name}.tar.gz | cut -d " " -f 1`
	cat $source_dir/packaging/pacman/PKGBUILD-${package_str} | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/" -e "s/__MD5SUM__/${md5sum_str}/" > $base_dir/pacman/PKGBUILD-$package_name
	cp $source_dir/packaging/pacman/${package_str}.install $base_dir/pacman/
	rm -rf $pacman_root_dir &&
	echo "OK"
}

create_webui_php()
{
	package_str="sisiya-webui-php"
	#package_name="${package_str}-${version_str}-$release_str"
	package_name="${package_str}-${version_str}"
	package_dir="$base_dir/tmp/${package_name}"

	### common package directory for all package types (rpm, deb, pacman ...)
	rm -rf $package_dir
	mkdir -p $package_dir
	cp -a ${source_dir}/$package_str/* $package_dir/
	echo "${version_str}-$release_str" > $package_dir/version.txt
	mkdir -p $package_dir/etc/cron.d
	for f in "sisiya-alerts" "sisiya-archive" "sisiya-check-expired" "sisiya-rss"
	do
		cp -a ${source_dir}/etc/cron.d/$f $package_dir/etc/cron.d/
	done
	mkdir -p $package_dir/etc/sisiya
	cp -a $source_dir/etc/sisiya/$package_str $package_dir/etc/sisiya
	cp -a ${source_dir}/sisiya_ui/XMPPHP $package_dir/
	cat ${source_dir}/$package_str/debian/copyright | sed -e "s/__YEAR__/${year_str}/"  > $package_dir/debian/copyright
	cat ${source_dir}/$package_str/debian/changelog | sed -e "s/__DISTRO_NAME__/${distro_name}/"  > $package_dir/debian/changelog
	update_changelog 
	cat $source_dir/etc/sisiya/$package_str/sisiya_common_conf.php | sed -e "s/__VERSION__/${version_str}/" -e "s/__YEAR__/${year_str}/"  > $package_dir/etc/sisiya/$package_str/sisiya_common_conf.php 
	find $package_dir -name "*.php" -exec sed -i "s/__YEAR__/$year_str/g" {} \;
	#############################
	### create RPM source package
	#############################
	rpm_root_dir="$base_dir/rpm/$package_name"
	echo -n "Creating ${rpm_root_dir}.tar.gz ..."
	rm -rf $rpm_root_dir
	cp -a $package_dir $rpm_root_dir
	cat $source_dir/packaging/rpmspec/${package_str}.spec 	| sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/"  > $rpm_root_dir/${package_str}.spec 
	(cd $base_dir/rpm ; tar -cz -f ${package_name}.tar.gz $package_name)
	rm -rf $rpm_root_dir
	echo "OK"
	################################
	### create Debian source package
	################################
	deb_root_dir="$base_dir/deb/$package_name"
	echo -n "Creating $base_dir/deb/${package_str}_${version_str}.orig.tar.gz ..."
	rm -rf $deb_root_dir 
	mkdir -p $deb_root_dir/usr/share/${package_str} 
	for f in etc debian version.txt
	do
		cp -a $package_dir/$f $deb_root_dir
	done

	cp -a $package_dir/* $deb_root_dir/usr/share/${package_str}/
	rm -rf $deb_root_dir/usr/share/${package_str}/debian
	rm -rf $deb_root_dir/usr/share/${package_str}/packages
	mkdir -p $deb_root_dir/var/tmp/sisiya-webui-php
	for d in "links" "packages" "xmlconf"
	do
		mkdir -p $deb_root_dir/var/lib/sisiya-webui-php/$d
	done
	cp $package_dir/packages/versions.xml $deb_root_dir/var/lib/sisiya-webui-php/packages/
	(cd $base_dir/deb ; tar cfz ${package_str}_${version_str}.orig.tar.gz $package_name) 
	rm -rf $deb_root_dir 
	echo "OK"
	###############################################
	### create directory structure for Arch systems
	###############################################
	###
	pacman_root_dir="$base_dir/pacman/$package_name"
	echo -n "Creating ${pacman_root_dir}.tar.gz ..."
	rm -rf $pacman_root_dir 
	cp -a $package_dir $pacman_root_dir
	(cd $base_dir/pacman ; tar cfz ${package_name}.tar.gz $package_name )
	md5sum_str=`md5sum $base_dir/pacman/${package_name}.tar.gz | cut -d " " -f 1`
	cat $source_dir/packaging/pacman/PKGBUILD-${package_str} | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/" -e "s/__MD5SUM__/${md5sum_str}/" > $base_dir/pacman/PKGBUILD-$package_name
	cp $source_dir/packaging/pacman/${package_str}.install $base_dir/pacman/
	rm -rf $pacman_root_dir &&
	echo "OK"
}

create_sisiyad()
{
	package_str="sisiyad"
	#package_name="${package_str}-${version_str}-$release_str"
	package_name="${package_str}-${version_str}"
	package_dir="$base_dir/tmp/${package_name}"

	### common package directory for all package types (rpm, deb, pacman ...)
	rm -rf $package_dir
	mkdir -p $package_dir
	cp -a ${source_dir}/doc $package_dir
	cp -a ${source_dir}/edbc $package_dir
	cp -a ${source_dir}/etc $package_dir
	for f in "AUTHORS" "ChangeLog" "COPYING" "INSTALL" "Makefile.am" "bootstrap"  "configure.ac" "NEWS" "README"
	do
		cp -a ${source_dir}/$f $package_dir
	done
	cp -a ${source_dir}/$package_str $package_dir
	echo "$version_str" > $package_dir/version.txt
	echo "$version_str" > $package_dir/edbc/version.txt
	echo "$version_str" > $package_dir/$package_str/version.txt
	mv $package_dir/$package_str/debian $package_dir
	cat ${source_dir}/$package_str/debian/copyright | sed -e "s/__YEAR__/${year_str}/"  > $package_dir/debian/copyright
	cat ${source_dir}/$package_str/debian/changelog | sed -e "s/__DISTRO_NAME__/${distro_name}/g"  > $package_dir/debian/changelog
	update_changelog
	#############################
	### create RPM source package
	#############################
	rpm_root_dir="$base_dir/rpm/$package_name"
	echo -n "Creating ${rpm_root_dir}.tar.gz ..."
	rm -rf $rpm_root_dir
	cp -a $package_dir $rpm_root_dir
	cat $source_dir/packaging/rpmspec/${package_str}.spec 	| sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/"  > $rpm_root_dir/${package_str}.spec 
	(cd $base_dir/rpm ; tar -cz -f ${package_name}.tar.gz $package_name)
	rm -rf $rpm_root_dir
	echo "OK"
	################################
	### create Debian source package
	################################
	deb_root_dir="$base_dir/deb/$package_name"
	echo -n "Creating $base_dir/deb/${package_str}_${version_str}.orig.tar.gz ..."
	rm -rf $deb_root_dir 
	cp -a $package_dir $deb_root_dir
	cp ${source_dir}/etc/systemd/sisiyad.service  $deb_root_dir/debian/
	cp ${source_dir}/etc/sisiyad_sysvinit $deb_root_dir/debian/sisiyad.init
	change_libdir $deb_root_dir/edbc
	(cd $base_dir/deb ; tar cfz ${package_str}_${version_str}.orig.tar.gz $package_name) 
	rm -rf $deb_root_dir 
	echo "OK"
	###############################################
	### create directory structure for Arch systems
	###############################################
	pacman_root_dir="$base_dir/pacman/$package_name"
	echo -n "Creating ${pacman_root_dir}.tar.gz ..."
	rm -rf $pacman_root_dir 
	cp -a $package_dir $pacman_root_dir
	(cd $base_dir/pacman ; tar cfz ${package_name}.tar.gz $package_name )
	md5sum_str=`md5sum $base_dir/pacman/${package_name}.tar.gz | cut -d " " -f 1`
	cat $source_dir/packaging/pacman/PKGBUILD-${package_str} | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/" -e "s/__MD5SUM__/${md5sum_str}/" > $base_dir/pacman/PKGBUILD-$package_name
	rm -rf $pacman_root_dir &&
	echo "OK"
}


create_edbc_libs()
{
	package_str="sisiya-edbc-libs"
	#package_name="${package_str}-${version_str}-$release_str"
	package_name="${package_str}-${version_str}"
	package_dir="$base_dir/tmp/${package_name}"

	### common package directory for all package types (rpm, deb, pacman ...)
	rm -rf $package_dir
	mkdir -p $package_dir
	cp -a ${source_dir}/edbc/* $package_dir/
	echo "$version_str" > $package_dir/version.txt
	cat ${source_dir}/edbc/debian/copyright | sed -e "s/__YEAR__/${year_str}/"  > $package_dir/debian/copyright
	cat ${source_dir}/edbc/debian/changelog | sed -e "s/__DISTRO_NAME__/${distro_name}/g"  > $package_dir/debian/changelog
	update_changelog
	#############################
	### create RPM source package
	#############################
	rpm_root_dir="$base_dir/rpm/$package_name"
	echo -n "Creating ${rpm_root_dir}.tar.gz ..."
	rm -rf $rpm_root_dir
	cp -a $package_dir $rpm_root_dir
	cat $source_dir/packaging/rpmspec/${package_str}.spec 	| sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/"  > $rpm_root_dir/${package_str}.spec 
	(cd $base_dir/rpm ; tar -cz -f ${package_name}.tar.gz $package_name)
	rm -rf $rpm_root_dir
	echo "OK"
	################################
	### create Debian source package
	################################
	deb_root_dir="$base_dir/deb/$package_name"
	echo -n "Creating $base_dir/deb/${package_str}_${version_str}.orig.tar.gz ..."
	rm -rf $deb_root_dir 
	cp -a $package_dir $deb_root_dir
	change_libdir $deb_root_dir
	(cd $base_dir/deb ; tar cfz ${package_str}_${version_str}.orig.tar.gz $package_name) 
	rm -rf $deb_root_dir 
	echo "OK"
	###############################################
	### create directory structure for Arch systems
	###############################################
	pacman_root_dir="$base_dir/pacman/$package_name"
	echo -n "Creating ${pacman_root_dir}.tar.gz ..."
	rm -rf $pacman_root_dir 
	cp -a $package_dir $pacman_root_dir
	(cd $base_dir/pacman ; tar cfz ${package_name}.tar.gz $package_name )
	md5sum_str=`md5sum $base_dir/pacman/${package_name}.tar.gz | cut -d " " -f 1`
	cat $source_dir/packaging/pacman/PKGBUILD-${package_str} | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/" -e "s/__MD5SUM__/${md5sum_str}/" > $base_dir/pacman/PKGBUILD-$package_name
	rm -rf $pacman_root_dir &&
	echo "OK"
}

create_remote_checks()
{
	package_str="sisiya-remote-checks"
	#package_name="${package_str}-${version_str}-$release_str"
	package_name="${package_str}-${version_str}"
	package_dir="$base_dir/tmp/${package_name}"

	### common package directory for all package types (rpm, deb, pacman ...)
	rm -rf $package_dir
	mkdir -p $package_dir
	cp -a ${source_dir}/$package_str/* $package_dir/
	echo "${version_str}-$release_str" > $package_dir/version.txt

	mkdir -p $package_dir/etc/cron.d
	cp ${source_dir}/etc/cron.d/$package_str $package_dir/etc/cron.d
	mkdir -p $package_dir/etc/sisiya
	cp -a ${source_dir}/etc/sisiya/$package_str $package_dir/etc/sisiya
	cat ${source_dir}/$package_str/debian/copyright | sed -e "s/__YEAR__/${year_str}/"  > $package_dir/debian/copyright
	cat ${source_dir}/$package_str/debian/changelog | sed -e "s/__DISTRO_NAME__/${distro_name}/"  > $package_dir/debian/changelog
	update_changelog

	find $package_dir/ -type d -exec chmod 755 {} \;
	find $package_dir/ -type f -exec chmod 644 {} \;
	find $package_dir/ -name "*.pl" -exec chmod 755 {} \;
	find $package_dir/ -name "*.sh" -exec chmod 755 {} \;
	find $package_dir/etc -type f -exec chmod 644 {} \;
	#############################
	### create RPM source package
	#############################
	rpm_root_dir="$base_dir/rpm/$package_name"
	echo -n "Creating ${rpm_root_dir}.tar.gz ..."
	rm -rf $rpm_root_dir
	cp -a $package_dir $rpm_root_dir
	cat $source_dir/packaging/rpmspec/${package_str}.spec 	| sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/"  > $rpm_root_dir/${package_str}.spec 
	(cd $base_dir/rpm ; tar -cz -f ${package_name}.tar.gz $package_name)
	rm -rf $rpm_root_dir
	echo "OK"
	################################
	### create Debian source package
	################################
	deb_root_dir="$base_dir/deb/$package_name"
	echo -n "Creating $base_dir/deb/${package_str}_${version_str}.orig.tar.gz ..."
	rm -rf $deb_root_dir 
	mkdir -p $deb_root_dir 
	mkdir -p $deb_root_dir/usr/share/${package_str} 
	mkdir -p $deb_root_dir/usr/share/doc/${package_str} 
	for f in etc debian version.txt
	do
		cp -a $package_dir/$f $deb_root_dir
	done
	for f in misc scripts utils
	do
		cp -a $package_dir/$f ${deb_root_dir}/usr/share/${package_str} 
	done
	(cd $base_dir/deb ; tar cfz ${package_str}_${version_str}.orig.tar.gz $package_name) 
	rm -rf $deb_root_dir 
	echo "OK"
	###############################################
	### create directory structure for Arch systems
	###############################################
	pacman_root_dir="$base_dir/pacman/$package_name"
	echo -n "Creating ${pacman_root_dir}.tar.gz ..."
	rm -rf $pacman_root_dir 
	cp -a $package_dir $pacman_root_dir
	cp -a ${package_dir}/etc $pacman_root_dir 
	(cd $base_dir/pacman ; tar cfz ${package_name}.tar.gz $package_name )
	md5sum_str=`md5sum $base_dir/pacman/${package_name}.tar.gz | cut -d " " -f 1`
	cat $source_dir/packaging/pacman/PKGBUILD-${package_str} | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/" -e "s/__MD5SUM__/${md5sum_str}/" > $base_dir/pacman/PKGBUILD-$package_name
	rm -rf $pacman_root_dir &&
	echo "OK"
}

create_client_checks()
{
	package_str="sisiya-client-checks"
	#package_name="${package_str}-${version_str}-$release_str"
	package_name="${package_str}-${version_str}"
	package_dir="$base_dir/tmp/${package_name}"

	### common package directory for all package types (rpm, deb, pacman ...)
	rm -rf $package_dir
	mkdir  $package_dir
	cp -a $source_dir/$package_str/* $package_dir/
	rm -rf $package_dir/scripts/template
	echo "${version_str}-$release_str" > $package_dir/version.txt
	mkdir -p $package_dir/etc/cron.d
	cp ${source_dir}/etc/cron.d/$package_str $package_dir/etc/cron.d/
	mkdir -p $package_dir/etc/sisiya
	cp -a ${source_dir}/etc/sisiya/$package_str/ $package_dir/etc/sisiya
	cat ${source_dir}/$package_str/debian/copyright | sed -e "s/__YEAR__/${year_str}/"  > $package_dir/debian/copyright
	cat ${source_dir}/$package_str/debian/changelog | sed -e "s/__DISTRO_NAME__/${distro_name}/"  > $package_dir/debian/changelog
	update_changelog

	find $package_dir/ -type d -exec chmod 755 {} \;
	find $package_dir/ -type f -exec chmod 644 {} \;
	find $package_dir/ -name "*.pl" -exec chmod 755 {} \;
	find $package_dir/ -name "*.sh" -exec chmod 755 {} \;
	find $package_dir/etc -type f -exec chmod 644 {} \;
	#############################
	### create RPM source package
	#############################
	rpm_root_dir="$base_dir/rpm/$package_name"
	echo -n "Creating ${rpm_root_dir}.tar.gz ..."
	rm -rf $rpm_root_dir
	cp -a $package_dir $rpm_root_dir
	cat $source_dir/packaging/rpmspec/${package_str}.spec   | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/"  > $rpm_root_dir/${package_str}.spec
	(cd $base_dir/rpm ; tar -cz -f ${package_name}.tar.gz $package_name)
	rm -rf $rpm_root_dir
	echo "OK"
	################################
	### create Debian source package
	################################
	deb_root_dir="$base_dir/deb/$package_name"
	echo -n "Creating $base_dir/deb/${package_str}_${version_str}.orig.tar.gz ..."
	rm -rf $deb_root_dir 
	mkdir -p $deb_root_dir 
	for f in etc debian version.txt
	do
		cp -a $package_dir/$f $deb_root_dir
	done
	mkdir -p $deb_root_dir/usr/share/${package_str} 
	for f in misc scripts utils
	do
		cp -a $package_dir/$f ${deb_root_dir}/usr/share/${package_str}/ 
	done

	find $deb_root_dir/ -type d -exec chmod 755 {} \;
	find $deb_root_dir/ -type f -exec chmod 644 {} \;
	find $deb_root_dir/ -name "*.pl" -exec chmod 755 {} \;
	find $deb_root_dir/ -name "*.sh" -exec chmod 755 {} \;
	find $deb_root_dir/etc -type f -exec chmod 644 {} \;

	(cd $base_dir/deb ; tar cfz ${package_str}_${version_str}.orig.tar.gz $package_name) 
	rm -rf $deb_root_dir 
	echo "OK"
	###############################################
	### create directory structure for Arch systems
	###############################################
	pacman_root_dir="$base_dir/pacman/$package_name"
	echo -n "Creating ${pacman_root_dir}.tar.gz ..."
	rm -rf $pacman_root_dir 
	cp -a $package_dir $pacman_root_dir
	cp -a ${package_dir}/etc $pacman_root_dir 
	(cd $base_dir/pacman ; tar czf ${package_name}.tar.gz $package_name )
	md5sum_str=`md5sum $base_dir/pacman/${package_name}.tar.gz | cut -d " " -f 1`
	cat $source_dir/packaging/pacman/PKGBUILD-${package_str} | sed -e "s/__VERSION__/${version_str}/" -e "s/__RELEASE__/${release_str}/" -e "s/__MD5SUM__/${md5sum_str}/" > $base_dir/pacman/PKGBUILD-$package_name
	rm -rf $pacman_root_dir &&
	echo "OK"
}

# create the SisIYA source package and put it under the $base_dir/src directory
create_source_package()
{
	source_dir=$sisiya_dir
	dst_dir="$source_package_name"
	tar_file="${source_package_name}.tar.gz"

	cd $base_dir || error "cd $base_dir"
	# create output directories
	for d in "rpm" "deb" "pacman" "src" "tmp"
	do
		mkdir -p $d || error "mkdir -p $d"
	done

	cd tmp || error "cd tmp"
	rm -rf $dst_dir || error "rm -rf $dst_dir"
	mkdir $dst_dir || error "mkdir $dst_dir"
	cp -a $source_dir/* $dst_dir/ || error "cp -a $source_dir/* $dst_dir/"

	#
	tar --exclude='.git' -cz -f $tar_file $dst_dir || error "tar -cz -f $tar_file $dst_dir"
	mv -f $tar_file $base_dir/src || error "mv -f $tar_file $base_dir/src"
}

package_building_info()
{
	echo "-----------------------------------------------------------------------------------------------------------------------"
	echo "For RPM packages    : rpmbuild -ta rpm/package.tar.gz"
	echo "For DEB packages    : Unpack the deb/package.tar.gz and run the dpkg --build package command."
	echo "For Pacman packages : Use pacman/package.tar.gz and use it together with PKGBUILD-package and/or 
			package.install files on a Pacman system (makepkg)."
	echo "-----------------------------------------------------------------------------------------------------------------------"
}

update_changelog()
{
	changelog_file=$package_dir/debian/changelog
	# sisiya-edbc-libs (0.6.77.1~0ubuntu1~ppa1~__DISTRO_NAME__1) __DISTRO_NAME__; urgency=low
	str=`head -n1 $changelog_file | cut -d "(" -f 2 | cut -d "~" -f 1`
	if [ "$str" == "${version_str}.$release_str" ]; then
		# the version in the changelog file is the same as the latest tag, which means
		# this is a clean release build
		return
	fi
	# add an automatic change log entry
	###############################################################################
	# sisiya-client-checks (0.6.77.1~0ubuntu1~ppa1~__DISTRO_NAME__1) __DISTRO_NAME__; urgency=low
	#
	#  * added device type for smart check script
	#
	#  -- Erdal Mutlu <erdal@sisiya.org>  Fri, 3 Jun 2020 14:04:00 +0300
	###############################################################################
	tmp_file=`mktemp`
	echo "$package_str (${version_str}.${release_str}~0ubuntu1~ppa1~${distro_name}1) ${distro_name}; urgency=low" > $tmp_file
	echo "" >> $tmp_file
	echo "  * automatic build" >> $tmp_file
	echo "" >> $tmp_file
	date_str=`LANG=C date -R`
	echo " -- Erdal Mutlu <erdal@sisiya.org>  $date_str" >> $tmp_file
	cat $changelog_file >> $tmp_file
	mv $tmp_file $changelog_file
}
######################################################################################################################################
trap cleanup EXIT

if [ $# -lt 1 ]; then
	echo "Usage: $0 output_dir [Ubuntu distribution code name]"
	echo "Example: $0 /home/erdalmutlu/downloads/sisiya"
	echo "Example: $0 /home/erdalmutlu/downloads/sisiya precise"
	echo "Ubuntu distribution names: focal, bionic, xenial, precise, trusty, quantal, vivid etc"
	exit 1
fi

output_dir=$1
distro_name="focal"
if [ $# -eq 2 ]; then
	distro_name="$2"
fi

sisiya_dir=`git rev-parse --show-toplevel`
if [ $? -ne 0 ]; then
	echo "$0: This script must be run inside sisiya git repo!"
	error "git rev-parse --show-toplevel"
fi

for d in $output_dir $sisiya_dir
do
	if [ ! -d $d ]; then
		error "Directory $d does not exist. Exiting..."
	fi
done

check_git_porcelain
get_version

year_str=`date +%Y` 
base_dir=$output_dir
source_package_name="sisiya-${version_str}"
source_package_file="${source_name}.tar.gz"
source_dir="$base_dir/tmp/$source_package_name"

create_source_package

# create source files for every package
create_client_checks
create_edbc_libs
create_remote_checks
create_sisiyad
create_webui_images
create_webui_php

package_building_info
