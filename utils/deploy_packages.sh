#!/usr/bin/env bash
#
# This script is used for deploying SisIYA packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
### defaults
project_name="sisiya"
git_url="https://gitlab.com/erdalmutlu/sisiya.git"
git_dir="$HOME/git"
git_branch="master"
deploy_target="rpm"	# valid values: rpm, deb, pacman
clean_git_dir="true"
project_dir="$git_dir/$project_name"
deploy_status_file_prefix="$HOME/deploy_status"
deb_deploy_prog="$project_dir/utils/deploy_deb.sh"
rpm_deploy_prog="$project_dir/utils/deploy_rpm.sh"
pacman_deploy_prog="$project_dir/utils/deploy_pacman.sh"
log_dir="$HOME"
tmp_output_dir="$HOME/tmp_sisiya_dir"
### end of defaults
#################################################################################
error()
{
	echo "$0: $1"
	exit 1
}

error_without_exit()
{
	echo "$0: $1"
}

cleanup()
{
        rm -rf "$log_file"
}

echo_log()
{
	date_str=$(date '+%Y%m%d%H%M%S')
	echo "$date_str : $1" >> "$log_file"
}

record_deploy_status()
{
	tmp_file=$(mktemp)
	if [ -f "$deploy_status_file" ]; then
		grep -v "$1" "$deploy_status_file" > "$tmp_file"
		cp -f "$tmp_file" "$deploy_status_file"
	else
		touch "$deploy_status_file"
	fi
	# mv does not work across file systems
	rm -f "$tmp_file"
	echo "${1}:$2" >> "$deploy_status_file"
}

check_deploy_status()
{
	echo "$0: Checking last deployed status for $1..."
	deploy_status_file="${deploy_status_file_prefix}_${deploy_target}.txt"
	if [ ! -f "$deploy_status_file" ]; then
		touch "$deploy_status_file"
		return
	fi
	str=$(grep "$1" "$deploy_status_file" | grep "OK")
	if [ -n "$str" ]; then
		echo "$0: Packages for $project_name $1 are already built."
		echo "$0: If you would like to rebuild, please remove the following line from $deploy_status_file file"
		echo "$1"
		exit 0
	fi
}

check_prerequisites()
{
	return
	for f in $HOME/.git-credentials
	do
		if [ ! -f "$f" ]; then
			error "File $f does not exist"
		fi
	done
}

get_version()
{
	version_str=$(git describe | tr "-" ".")
	if [ -z "$version_str" ]; then
		echo "$0: git describe returned epmty string. No tags found!"
		exit 1
	fi
}

set_deploy_prog()
{
	deploy_prog="unknown"
	case $deploy_target in
		"deb")
			deploy_prog=$deb_deploy_prog
			;;
		"pacman")
			deploy_prog=$pacman_deploy_prog
			;;
		"rpm")
			deploy_prog=$rpm_deploy_prog
			;;
	esac
}
#################################################################################
trap cleanup EXIT
log_file=$(mktemp)


if test $# -eq 1 ; then
	conf_file="$1"
	if [ -f "$conf_file" ]; then
		### source the conf file
		# shellcheck disable=SC1090
		. "$conf_file"
	fi
else
	conf_file="deploy_packages.conf"
	if [ -f "$conf_file" ]; then
		### source the conf file
		# shellcheck disable=SC1090
		. "$conf_file"
	fi
fi

check_prerequisites

mkdir -p "$git_dir"

cd "$git_dir" || error "Could not change to directory $git_dir"

# configure credential store
# git config --global credential.helper store

if [ "$clean_git_dir" == "true" ]; then
	echo_log "Removing $git_dir/$project_name ..."
	rm -rf "${git_dir:?}/${project_name}"
	echo_log "Cloning $git_url ..." >> "$log_file"
	str=$(git clone --branch $git_branch $git_url 2>&1)
	# shellcheck disable=SC2181
	if [ $? -ne 0 ]; then
		echo_log "$str"
		error "Could not clone git: git clone --branch $git_branch $git_url"
	fi
	echo_log "$str"
else
	echo_log "Skipping rm -rf $git_dir/$project_name"
fi

cd "$project_dir" || error "Could not change to directory $project_dir"
last_commit_hash=$(git rev-parse  HEAD)
#echo "Last commit : $last_commit_hash log_file: $log_file"

get_version

check_deploy_status "$last_commit_hash"

echo "$0: Deploying for $deploy_target ..."
set_deploy_prog
for f in $deploy_prog
do
	if [ ! -f "$deploy_prog" ]; then
		error "No such file $f"
	fi
done

mkdir -p "$tmp_output_dir" || error "Could not create tmp output directory $tmp_output_dir"

echo_log "Started to deploy using $deploy_prog ..."
if "$deploy_prog" "$project_dir" "$tmp_output_dir" "$version_str" >> "$log_file" 2>&1 ; then
	# archive
	record_deploy_status "$last_commit_hash" "OK"
	echo_log "Successfully deployed $project_name $last_commit_hash"
	cp -f "$log_file" "$log_dir/${project_name}_${last_commit_hash}.log"
	echo "$0: Suceessfuly deployed. For details have a look at log file: $log_dir/${project_name}_${last_commit_hash}.log"
else
	record_deploy_status "$last_commit_hash" "FAILED"
	echo_log "Could not deploy $project_name $last_commit_hash"
	cp -f "$log_file" "$log_dir/${project_name}_${last_commit_hash}.log"
	echo "$0: Failed to deploy! Please have a look at log file: $log_dir/${project_name}_${last_commit_hash}.log"
	exit 1
fi
