#!/bin/bash
#
# This script is used to build Arch linux packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#################################################################################
### defaults
package_list="sisiya-client-checks sisiya-remote-checks sisiya-webui-php sisiya-webui-images sisiya-edbc-libs sisiyad"
package_list_any="sisiya-client-checks sisiya-remote-checks sisiya-webui-php sisiya-webui-images"
package_list_binary="sisiyad sisiya-edbc-libs"
### end of defaults
###########################################################################
error()
{
	echo "$0: Error: $1"
	exit 1
}

build_pacman_package()
{
	f=$1
	echo "$0: Building $f-${version_str}.tar.gz ..."
	rm -f PKGBUILD
	ln -s PKGBUILD-$f-${version_str} PKGBUILD
	makepkg -f 
	if [ $? -ne 0 ]; then
		error "Could not build package $f"
	fi
}
###########################################################################
if [ $# -ne 3 ]; then
	echo "Usage  : sisiya_git_directory output_directory version_str "
	echo "Example: $HOME/git/sisiya $HOME/tmp_sisiya_output 0.6.78"
	exit 1
fi

git_dir=$1
output_dir=$2
version_str=$3

str=`echo $version_str | grep "-"`
if [ -n "$str" ]; then
	error "Version must not have the character - in it! Please use dor (.) instaed."
fi

if [ ! -d $git_dir ]; then
	error "SisIYA git directory $git_dir does not exist!"
fi

echo "$0: Creating output directory $output_dir ..."
mkdir -p $output_dir || error "mkdir -p $output_dir"

cd $git_dir || error "cd $git_dir"

echo "$0: Creating source packages in $output_dir ..."
$git_dir/utils/create_source_packages.sh $output_dir

echo "$0: Building pacman packages..."
cd $output_dir/pacman || error "cd $output_dir/pacman"

# In order to build sisiyad package sisiya-edbc-libs package must be installed first.
# sisiya-edbc-libs is a runtime dependency only, but pkgbuild do not have a means to define
# runtime dependency only (as far as I know). There is another way, which is to build
# sisiyad and sisiya-ebdc-libs in one source package using split packaging. But for 
# now I am going to stick with the option of building the sisiya-edbc-libs first,
# installing them and after that build the sisiyad package.
# The same for sisiya-webui-images and sisiya-webui-php.

for f in "sisiya-edbc-libs" "sisiya-webui-images"
do
	echo "$0: Building $f-${version_str}.tar.gz ..."
	build_pacman_package $f
	
	march=any
	if [ $f == "sisiya-edbc-libs" ]; then
		march=`uname -m`
	fi
	p=$f-${version_str}-1-${march}.pkg.tar.zst
	echo "$0: Installing $p ..."
	sudo pacman -U --noconfirm $p
	if [ $? -ne 0 ]; then
		error "Could not install package $p"
	fi
done

# now build the rest
package_list=`ls PKGBUILD*$version_str | grep -v "sisiya-edbc-libs" | grep -v "sisiya-webui-images" | sed -e "s/PKGBUILD-//" -e "s/-$version_str//"`
for f in $package_list
do
	build_pacman_package $f
done
