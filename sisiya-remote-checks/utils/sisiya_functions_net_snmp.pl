#!/usr/bin/perl -w
#
# Common functions
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
use strict;
use warnings;
use Net::SNMP;
use Socket;

sub get_snmp_all_oids
{
	my ($snmp_session, $oid_tables_ref, $oids_ref) = @_;
	my ($result, $h_ref);
	my @oid_list = ();
	
	$h_ref = get_snmp_table_oids($snmp_session, $oid_tables_ref);
	# print STDERR "get_snmp_all_oids: get_table:\n";
	# print_hash($h_ref);

	# print STDERR "get_snmp_all_oids: merging oids\n";
	merge_oids($oid_tables_ref, $oids_ref, \@oid_list);
	# print STDERR "get_snmp_all_oids: merging oid array:\n";
	# print_array(\@oid_list);
	if (@oid_list) {
		# print STDERR "get_snmp_all_oids: oid_list has elements\n";
		if ($snmp_session->version() == Net::SNMP::SNMP_VERSION_1) {
			# print STDERR "get_snmp_all_oids: version 1\n";
			$result = $snmp_session->get_request(-varbindlist => \@oid_list);
		} else {
			# print STDERR "get_snmp_all_oids: version > 1\n";
			# $result = $snmp_session->get_bulk_request(-varbindlist => \@oid_list);
			# ??????? could not make bulk request to work
			$result = $snmp_session->get_request(-varbindlist => \@oid_list);
		}
	       	if (defined $result) {
			# print STDERR "get_snmp_all_oids: merging hashes\n";
			%$h_ref = (%$h_ref, %$result);
		}# else {
		#	print STDERR "get_snmp_all_oids: could not get oid_list!\n";
		#	print STDERR $snmp_session->error();
		#}
	}

	# remove new line character from every value of the hash
	chomp(%$h_ref);

	# print_hash($h_ref);

	# return reference to hash
        return $h_ref;
}

sub get_snmp_table_oids
{
	my ($snmp_session, $oid_tables_ref) = @_;
	my (%h, $result);

	foreach my $key (keys %$oid_tables_ref) {
		# print STDERR "get_snmp_tables: push: key=$key value=$oid_tables_ref->{$key}\n";
		$result = $snmp_session->get_table(-baseoid => $oid_tables_ref->{$key});
        	if (!defined $result) {
			# print STDERR "get_snmp_tables: skipping $oid_tables_ref->{$key} ...\n";
                	next;
        	}
		# print STDERR "get_all_oids: adding $oid_tables_ref->{$key} to the results...\n";
		%h = (%h, %$result);
	}
	return \%h;
}

sub get_snmp_value
{
	my ($snmp_session, $oid) = @_;

	# returns a reference to hash
	my $result = $snmp_session->get_request(-varbindlist => [$oid]);
	if (!defined $result) {
		return '';
	}
	return $result->{$oid};
}

## generate a temporary file name in the $SisIYA_Remote_Config::tmp_dir directory
#sub get_temp_file_name 
#{
#	my $fh = File::Temp->new(TEMPLATE => 'tempXXXXX', DIR => $SisIYA_Remote_Config::tmp_dir, SUFFIX => '.tmp');
#
#	return $fh->filename;
#}

sub check_uptime
{
	my ($statusid_ref, $up_in_minutes, $uptime_warning, $uptime_error) = @_;
	my $s;

	if ($up_in_minutes <= $uptime_error) {
		$$statusid_ref = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: The systems was restarted ".minutes2string($up_in_minutes). " (<= ".minutes2string($uptime_error).") ago!";
	} elsif ($up_in_minutes <= $uptime_warning) {
		$$statusid_ref = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: The systems was restarted ".minutes2string($up_in_minutes). " (<= ".minutes2string($uptime_warning).") ago!";
	} else {
		$$statusid_ref = $SisIYA_Config::statusids{'ok'};
		$s = "OK: The system is up for ".minutes2string($up_in_minutes). ".";
	}
	return $s;
}

sub check_http_protocol
{
	my ($isactive, $serviceid, $expire, $system_name, $virtual_host, $index_file, $http_port, $username, $password, $ssl) = @_;

	if ($isactive eq 'f' ) {
		return '';
	}
	#print STDERR "check_http: Checking system_name=[$system_name] isactive=[$isactive] virtual_host=[$virtual_host] index_file=[$index_file] http_port=[$http_port] username=[$username] password=[$password]...\n";

	my $x_str = "<system><name>$system_name</name><message><serviceid>$serviceid</serviceid>";
	#############################################################
	#HTTP/1.1 200 OK
	#Date: Thu, 12 Dec 2013 07:24:06 GMT
	#Server: Apache
	#Last-Modified: Wed, 11 Sep 2013 14:27:45 GMT
	#ETag: "101513-ca7-4e61c6cc7b001"
	#Accept-Ranges: bytes
	#Content-Length: 3239
	#Connection: close
	#Content-Type: text/html; charset=UTF-8
	#############################################################
	my $params = '--max-time 4 --include';
	if (grep(/^HASH/, $username) == 0) {
	       $params = "$params --user \"$username:$password\"";
	}	       
	my @a;
       	if ($ssl == 1) {
		#print STDERR "$SisIYA_Remote_Config::external_progs{'curl'} $params https://$virtual_host:$http_port$index_file\n";
		#@a = `$SisIYA_Remote_Config::external_progs{'curl'} $params https://$virtual_host:$http_port$index_file 2>/dev/null`;
		$params .= ' --insecure';
		@a = `$SisIYA_Remote_Config::external_progs{'curl'} $params https://$virtual_host:$http_port$index_file 2>/dev/null`;
	}
	else {
		#print STDERR "$SisIYA_Remote_Config::external_progs{'curl'} $params http://$virtual_host:$http_port$index_file\n";
		@a = `$SisIYA_Remote_Config::external_progs{'curl'} $params http://$virtual_host:$http_port$index_file 2>/dev/null`;
	}
	my $s = '';
	my $statusid;
	my $retcode = $? >>=8;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: The service is not running! retcode=$retcode";
	}
	else {
		my $info_str = '';
		my @b = grep(/^Server:/, @a);
		if ( $#b != -1 ) {
			$info_str = (split(/:/, $b[0]))[1];
			chomp($info_str = $info_str);
			$info_str =~ s/\r//g;
			$info_str = "INFO:$info_str";
			#print STDERR "info=[$info_str]\n";
		}
		my $http_status_code = (split(/\s+/, (grep(/^HTTP\//, @a))[0]))[1];
		#if (($http_status_code >= 200) && ($http_status_code < 300)) {
		if (($http_status_code >= 200) && ($http_status_code < 400)) {
			$statusid = $SisIYA_Config::statusids{'ok'};
			$s = "OK: ".get_http_protocol_description($http_status_code);
		}
		#elsif ( ($http_status_code >= 300) && ($http_status_code < 400)) {
		#	$statusid = $SisIYA_Config::statusids{'warning'};
		#	$s = "WARNING: The service is not running! ".get_http_protocol_description($http_status_code)." retcode=$retcode";
		#}
		else {
			#$statusid = $SisIYA_Config::statusids{'error'};
			#$s = "ERROR: The service has problem ! ".get_http_protocol_description($http_status_code)." retcode=$retcode";
			$statusid = $SisIYA_Config::statusids{'warning'};
			$s = "WARNING: The service has problems! ".get_http_protocol_description($http_status_code)." retcode=$retcode";
		}
		$s .= $info_str;
	}
	$x_str .= "<statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message></system>";
	return $x_str;
}

sub check_system
{
	my ($expire, $h_ref, $uptimes_ref) = @_;

	my $s = '';
	my $statusid = check_system_status($h_ref, $uptimes_ref, \$s);
	my $serviceid = get_serviceid('system');
	if ($s eq '') {
		return '';
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_system_status
{
	my ($h_ref, $uptimes_ref, $msg_ref) = @_;

	my $sysDesc = '1.3.6.1.2.1.1.1.0';
	my $sysLocation = '1.3.6.1.2.1.1.6.0';
	my $sysUpTime = '1.3.6.1.2.1.1.3.0';

	if (!%$h_ref) {
		print STDERR "check_system_status: h_ref is uninitialized!\n";
		return '';
	}
	if (!exists($h_ref->{$sysUpTime}) || $h_ref->{$sysUpTime} eq '') {
		return '';
	}

	my $statusid;
	my $up_in_minutes = int($h_ref->{$sysUpTime} / 6000);
	$$msg_ref = check_uptime(\$statusid, $up_in_minutes, $uptimes_ref->{'warning'}, $uptimes_ref->{'error'});
	$$msg_ref .= " Description: $h_ref->{$sysDesc} Location: $h_ref->{$sysLocation}.";
	return $statusid;
}

sub check_snmp_system
{
	my ($expire, $snmp_session, $uptimes_ref) = @_;
	my $sysDesc = '1.3.6.1.2.1.1.1.0';
	my $sysLocation = '1.3.6.1.2.1.1.6.0';
	my $sysUpTime = '1.3.6.1.2.1.1.3.0';
	# my @oids = ('1.3.6.1.2.1.1.1.0', '1.3.6.1.2.1.1.6.0', '1.3.6.1.2.1.1.3.0');
	my @oids = ($sysDesc, $sysLocation, $sysUpTime);

	# returns a reference to hash
	my $h_ref = $snmp_session->get_request(-varbindlist => \@oids);

	if (!defined $h_ref) {
		# printf "check_snmp_system2: Could not get_request: %s.\n", $snmp_session->error();
		# $snmp_session->close();
		return '';
	}
	return check_system($expire, $h_ref, $uptimes_ref);
}

sub connect_to_socket_and_read_line
{
	my ($server, $port, $timeout, $proto_name) = @_;
	# create the socket, connect to the port
	if (socket(SOCKET, PF_INET, SOCK_STREAM, (getprotobyname($proto_name))[2]) == -1) {
		print STDERR "connect_to_socket_and_read_line: ERROR: Could not create socket of type $proto_name!";
		return '';
	}
	my $line;
	if (!connect( SOCKET, pack_sockaddr_in($port, inet_aton($server)))) {
		print STDERR "connect_to_socket_and_read_line: ERROR: Could not connect to $server:$port!";
		return '';
	} else {
		$line = <SOCKET>;
		close SOCKET;
		chomp($line = $line);
		$line =~ s/\r//g;
		#print STDERR "[$line]\n";
	}
	return $line;
}

sub count_keys
{
        #my %h = %{$_[0]}; # get hash by reference
	my ($h_ref, $oid) = @_;
        #my $h_ref = $_[0]; # get hash by reference
	#my $oid = $_[1];

	my $i = 0;
	do {
		$i++;
	} while (exists $h_ref->{$oid.'.'.$i}); 
	return($i - 1);
}

sub extract_keys
{
	my ($a_ref) = @_;	# get array by reference
	#my @a_ref = @{$_[0]};	# get array by reference
        my (%h, $k, $v);

        for my $s (@$a) {
                ($k, $v) = split(/ = /, $s);
                $v //= '';
                if ($v eq '') {
                        next;
                }
                chomp($v = $v);
                $h{$k} = $v;
        }
	# return hash by reference
	return %h;
}

sub get_http_protocol_description
{
	my %http_protocol_str = ( 
				200 => 'The request has succeeded.',
				201 => 'Created',
				202 => 'Accepted.',
				203 => 'Non-Authoritative Information.',
				204 => 'No content.',
				205 => 'Reset content.',
				206 => 'Partial content.',
				300 => 'Multiple choices.',
				301 => 'Moved permanently.',
				302 => 'Found.',
				303 => 'See other.',
				304 => 'Not modified.',
				305 => 'Use proxy.',
				306 => 'Unused.',
				307 => 'Temporary redirect.',
				400 => 'Client error: Bad request!',
				401 => 'Client error: Unauthorized!',
				402 => 'Client error: Payment required!',
				403 => 'Client error: Forbidden!',
				404 => 'Client error: Not found!',
				405 => 'Client error: Method not allowed!',
				406 => 'Client error: Not acceptable!',
				407 => 'Client error: Proxy authontication required!',
				408 => 'Client error: Request timeout!',
				409 => 'Client error: Conflict!',
				410 => 'Client error: Gone!',
				411 => 'Client error: Length is required!',
				412 => 'Client error: Precondicion failed!',
				413 => 'Client error: Request entity too large!',
				414 => 'Client error: Request URI too large!',
				415 => 'Client error: Unsupported media type!',
				416 => 'Client error: Requested range not satisfiable!',
				417 => 'Client error: Expectation failed!',
				500 => 'Internal server error!',
				501 => 'Server error: Not implemented!',
				502 => 'Server error: Bad gateway!',
				503 => 'Server error: Service unavailable!',
				504 => 'Server error: Gateway timeout!',
				505 => 'Server error: HTTP version not supported!'
			 );
	if ( exists($http_protocol_str{$_[0]})) {
		return "HTTP code $_[0]: $http_protocol_str{$_[0]}";
	}
	else {
		return 'Unknown HTTP status code: $_[0]';
	}
}

sub lock_check
{
	my ($check_name) = @_;

	my $script_name = $SisIYA_Remote_Config::checks{$check_name}{'script'};
	my $pid_file = "$SisIYA_Remote_Config::tmp_dir/".$script_name.".lock";
	my $ps_prog = $SisIYA_Remote_Config::external_progs{'ps'};

	return lock_using_pid_file($script_name, $pid_file, $ps_prog);
}

sub merge_oids
{
	my ($oid_tables_ref, $oids_ref, $oid_list) = @_;

	# only add those OIDs, which are not covered by OIDs from oid_tables
	my $found;
	foreach my $key (keys %$oids_ref) {
		# print STDERR "merge_oids: checking oid=$oids_ref->{$key}\n";
		$found = 0;
		foreach my $table_key (keys %$oid_tables_ref) {
			# print STDERR "merge_oids: checking table oid=$oid_tables_ref->{$table_key} with oid=$oids_ref->{$key}\n";
			if (index($oids_ref->{$key}, $oid_tables_ref->{$table_key}) != -1) {
				$found = 1;
				last;
			}
		}
		if (not $found) {
			# print STDERR "get_all_oids: adding key $oids_ref->{$key} to the oid_list\n";
			push @$oid_list, $oids_ref->{$key};
		}
	}
}

sub print_array
{
	my @a = @{$_[0]};
	my $n = @a;

	print STDERR "print_array: count= $n\n";
	foreach my $v (@a) {
		print STDERR "print_array: value=[$v]\n";
	}
}

sub print_hash
{
	my ($h_ref) = @_;

	foreach my $k (keys %$h_ref) {
		print STDERR "print_hash: key=[$k] value=[$h_ref->{$k}]\n";
	}
}


sub snmp_init 
{
	my ($hostname, $version, $community, $username, $password) = @_;

	my $h = inet_aton($hostname);
	if (not defined $h) {
		# print STDERR "snmp_init: ERROR: hostname not defined $hostname.\n";
		return 0;
	}

	if ($version eq '2c') {
		$version = '2';
	}

	# print STDERR "snmp_init: hostname = $hostname community = $community version = $version\n";
	my ($snmp_session, $error);
	my $timeout = 5;
	my $retries = 1;
	# if (defined $username and $username eq '') {
	if (grep(/^HASH/, $username) == 0) {
		($snmp_session, $error) = Net::SNMP->session(-hostname => $hostname, -version => $version, -community => $community, -username => $username, -password => $password, -timeout => $timeout, -retries => $retries);
	} else {
		($snmp_session, $error) = Net::SNMP->session(-hostname => $hostname, -version => $version, -community => $community, -timeout => $timeout, -retries => $retries);
	}

	if (!defined $snmp_session) {
		#print STDERR "snmp_init: Could not create SNMP session error = $error\n";
		return 0;
	}
	# $snmp_session->timeout([1]);
	# $snmp_session->retries([1]);
	
	# do not convert TimeTicks in a TimeFormat
	#$snmp_session->translate([ '-timeticks' => 0, '-endofmibview' => 0, '-nosuchobject' => 0, '-nosuchinstance' => 0 ]);
	$snmp_session->translate([ '-timeticks' => 0, '-nosuchobject' => 0, '-nosuchinstance' => 0]);
	
	return($snmp_session);
}

sub unlock_check
{
	my ($check_name) = @_;

	my $script_name = $SisIYA_Remote_Config::checks{$check_name}{'script'};
	my $pid_file = "$SisIYA_Remote_Config::tmp_dir/".$script_name.".lock";
	return unlock_using_pid_file($pid_file);
}

1;
