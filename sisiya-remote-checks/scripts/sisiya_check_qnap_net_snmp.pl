#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use Net::SNMP;

my $check_name = 'qnap';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}
###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %loads = ('error' => 90, 'warning' => 80);
our %oid_tables = (
	'hd'		=> '1.3.6.1.4.1.24681.1.2.11',		# System hard disk table
	'fan'		=> '1.3.6.1.4.1.24681.1.3.15',		# Fan table
	'volume'	=> '1.3.6.1.4.1.24681.1.2.17'		# Volume table
);

our %oids = (
	'cpu_temperature'		=> '1.3.6.1.4.1.24681.1.2.5.0',
	'fan_number'			=> '1.3.6.1.4.1.24681.1.3.14.0',
	'fan_index'			=> '1.3.6.1.4.1.24681.1.3.15.1.1',
	'fan_name'			=> '1.3.6.1.4.1.24681.1.3.15.1.2',
	'fan_speed'			=> '1.3.6.1.4.1.24681.1.3.15.1.3',
	'hd_number'			=> '1.3.6.1.4.1.24681.1.2.10.0',
	'hd_index'			=> '1.3.6.1.4.1.24681.1.2.11.1.1',
	'hd_descr'			=> '1.3.6.1.4.1.24681.1.2.11.1.2',
	'hd_temperature'		=> '1.3.6.1.4.1.24681.1.2.11.1.3',
	'hd_status'			=> '1.3.6.1.4.1.24681.1.2.11.1.4',
	'hd_model'			=> '1.3.6.1.4.1.24681.1.2.11.1.5',
	'hd_capacity'			=> '1.3.6.1.4.1.24681.1.2.11.1.6',
	'hd_smart_info'			=> '1.3.6.1.4.1.24681.1.2.11.1.7',
	'model_name'			=> '1.3.6.1.4.1.24681.1.2.12.0',
	'system_free_ram'		=> '1.3.6.1.4.1.24681.1.2.3.0',
	'system_total_ram'		=> '1.3.6.1.4.1.24681.1.2.2.0',
	'system_volume_number'		=> '1.3.6.1.4.1.24681.1.2.16.0',
	'system_volume_index'		=> '1.3.6.1.4.1.24681.1.2.17.1.1',
	'system_volume_descr'		=> '1.3.6.1.4.1.24681.1.2.17.1.2',
	'system_volume_fs'		=> '1.3.6.1.4.1.24681.1.2.17.1.3',
	'system_volume_total_size'	=> '1.3.6.1.4.1.24681.1.2.17.1.4',
	'system_volume_free_size'	=> '1.3.6.1.4.1.24681.1.2.17.1.5',
	'system_volume_status'		=> '1.3.6.1.4.1.24681.1.2.17.1.6',
	'system_temperature'		=> '1.3.6.1.4.1.24681.1.2.6.0',
	'system_load'			=> '1.3.6.1.4.1.24681.1.2.1.0',
	# system
	'system_description'			=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'				=> '1.3.6.1.2.1.1.3.0',
	'system_contact'			=> '1.3.6.1.2.1.1.4.0',
	'system_name'				=> '1.3.6.1.2.1.1.5.0',
	'system_location'			=> '1.3.6.1.2.1.1.6.0'
);

our %default_temperatures = ( 'warning' => 30, 'error' => 35 );
our @temperatures = (
	{ 'name' => 'CPU',	'oid' => $oids{'cpu_temperature'},	'warning' => 45, 'error' => 50},
	{ 'name' => 'System',	'oid' => $oids{'system_temperature'},	'warning' => 35, 'error' => 40}
);
# end of default values
############################################################################################################
sub check_qnap_load
{
	my ($expire, $h_ref) = @_;

	my $system_load = $h_ref->{$oids{'system_load'}};
	if ($system_load eq '') {
		return '';
	}
	$system_load =~ s/"//g;
	$system_load = trim((split(/%/, $system_load))[0]);
#	if ($system_load == 0) {
#		return '';
#	}
	my $serviceid = get_serviceid('load');
	my $statusid = $SisIYA_Config::statusids{'ok'};
	my $s = '';
	if ($system_load >= $loads{'error'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: System load is $system_load % (>= $loads{'error'})!";
	} elsif ($system_load >= $loads{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: System load is $system_load % (>= $loads{'warning'})!";
	} else {
		$s = "OK: System load is $system_load %.";
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_temperature_status
{
	my ($h_ref, $name, $oid, $error_temperature, $warning_temperature, $msg_ref) = @_;
	my $statusid;

	my $t = $h_ref->{$oid};

	$$msg_ref = '';
	if ($t eq '') {
		return -1;
	}
	$t =~ s/"//g;
	$t = trim((split(/C/, $t))[0]);
	if ($t == 0) {
		return -1;
	}
	if ($t >= $error_temperature) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$$msg_ref = "ERROR: $name temperature is $t C (>= $error_temperature)!";
	} elsif ($t >= $warning_temperature) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$$msg_ref = "WARNING: $name temperature is $t C (>= $warning_temperature)!";
	} else {
		$statusid = $SisIYA_Config::statusids{'ok'};
		$$msg_ref = "OK: $name temperature is $t C.";
	}
	return $statusid;
}

sub check_qnap_temperature
{
	my ($expire, $h_ref) = @_;
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my ($statusid, $s_id);
	my $s = '';

	$statusid = $SisIYA_Config::statusids{'ok'};
	for my $i (0..$#temperatures) {
		$s_id = check_qnap_temperature_status($h_ref, $temperatures[$i]{'name'}, $temperatures[$i]{'oid'}, $temperatures[$i]{'error'}, $temperatures[$i]{'warning'}, \$s);
		if ($s_id == -1) {
			next;
		}
		if ($s_id == $SisIYA_Config::statusids{'error'}) {
			$statusid = $s_id;
			$error_str .= $s;
		} elsif ($s_id == $SisIYA_Config::statusids{'warning'}) {
			$warning_str .= $s;
			if ($s_id > $statusid) {
				$statusid = $s_id;
			}
		} else {
			$ok_str .= $s;
		}
	}

	my $serviceid = get_serviceid('temperature');
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_raid
{
	my ($expire, $h_ref) = @_;

	my $system_volume_number = $h_ref->{$oids{'system_volume_number'}};
	if ($system_volume_number eq '') {
		return '';
	}

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $serviceid = get_serviceid('raid');
	my $statusid = $SisIYA_Config::statusids{'ok'};

	my (@a, $s, $j, $k, $oid, @oid_list);
	my ($system_volume_fs, $system_volume_total_size, $system_volume_free_size, $system_volume_descr, $system_volume_status, $index);
	for (my $i = 1; $i <= $system_volume_number; $i++) {
		$j = $h_ref->{$oids{'system_volume_index'}.'.'.$i};
		if ($j eq '') {
			next;
		}
		for ($k = 0; $k < @a; $k++) {
			$a[$k] =~ s/"//g;
		}

		#print STDERR "index: $index system_volume_status=[$h_ref->{$oids{'system_volume_status'}.'.'.$j}] system_volume_total_size=[$h_ref->{$oids{'system_volume_total_size'}.'.'.$j}] system_volume_free_size=[$h_ref->{$oids{'system_volume_free_size'}.'.'.$j}] system_volume_descr=[$h_ref->{$oids{'system_volume_descr'}.'.'.$j}] system_volume_fs=[$h_ref->{$oids{'system_volume_fs'}.'.'.$j}]\n";
		$s = "$h_ref->{$oids{'system_volume_descr'}.'.'.$j} File system: $h_ref->{$oids{'system_volume_fs'}.'.'.$j} Total size: $h_ref->{$oids{'system_volume_total_size'}.'.'.$j} Free Size: $h_ref->{$oids{'system_volume_free_size'}.'.'.$j}";
		if ($h_ref->{$oids{'system_volume_status'}.'.'.$j} eq 'Ready') {
			$ok_str .= " OK: Volume $j ($s) is OK.";
		} elsif ($h_ref->{$oids{'system_volume_status'}.'.'.$j} eq 'Rebuilding...') {
			if ($statusid < $SisIYA_Config::statusids{'warning'}) {
				$statusid = $SisIYA_Config::statusids{'warning'};
			}
			$warning_str .= " WARNING: Volume $j ($s) is rebuilding.";
		} else {
			$statusid = $SisIYA_Config::statusids{'error'};
			$error_str .= " ERROR: Volume $j ($s) has status = $h_ref->{$oids{'system_volume_status'}.'.'.$j} (!= Ready)!";
		}
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_smart
{
	my ($expire, $h_ref) = @_;

	my $hd_number = $h_ref->{$oids{'hd_number'}};
	if ($hd_number eq '') {
		return '';
	}
	# print STDERR "hd_number = [$hd_number]\n";
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $serviceid = get_serviceid('smart');
	my (@a, $s, $j, $x);
	for (my $i = 1; $i <= $hd_number; $i++) {
		$j = $h_ref->{$oids{'hd_index'}.'.'.$i};
		if ($j eq '') {
			next;
		}

		# print STDERR "index: $j hd_status=[$h_ref->{$oids{'hd_status'}.'.'.$j}] hd_capacity=[$h_ref->{$oids{'hd_capacity'}.'.'.$j}] hd_model=[$h_ref->{$oids{'hd_model'}.'.'.$j}] hd_descr=[$h_ref->{$oids{'hd_descr'}.'.'.$j}] hd_temperature=[$h_ref->{$oids{'hd_temperature'}.'.'.$j}], SMART=[$h_ref->{$oids{'hd_smart_info'}.'.'.$j}] \n";
		$s = "$h_ref->{$oids{'hd_descr'}.'.'.$j} Device Model: $h_ref->{$oids{'hd_model'}.'.'.$j} Capacity: $h_ref->{$oids{'hd_capacity'}.'.'.$j} Temperature: $h_ref->{$oids{'hd_temperature'}.'.'.$j} SMART info: $h_ref->{$oids{'hd_smart_info'}.'.'.$j}";
		if ($h_ref->{$oids{'hd_status'}.'.'.$j} == 0) {
			$ok_str .= " OK: Disk $j ($s) is OK.";
		} else {
			$error_str .= " ERROR: Disk $j ($s) has status = $h_ref->{$oids{'hd_status'}.'.'.$j} (!= 0)!";
		}
		$x = trim((split(/C/, $h_ref->{$oids{'hd_temperature'}.'.'.$j}))[0]);
		if ($x ne '--') {
			if ($x >= $default_temperatures{'error'}) {
				$error_str .= " ERROR: Disk $j temperature is $x Celsius (>= $default_temperatures{'error'})!";
			} elsif ($x >= $default_temperatures{'warning'}) {
				$warning_str .= " WARNING: Disk $j temperature is $x Celsius (>= $default_temperatures{'warning'})!";
			}
		}
	}
	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_fan
{
	my ($expire, $h_ref) = @_;
	my ($index, $fan_name, $fan_speed);
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $s = '';	
	my $n = $h_ref->{$oids{'fan_number'}};
       	my $statusid = $SisIYA_Config::statusids{'ok'};
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h_ref->{$oids{'fan_index'}.'.'.$i};
		$fan_speed = $h_ref->{$oids{'fan_speed'}.'.'.$index};
		$fan_name = $h_ref->{$oids{'fan_name'}.'.'.$index};
		$fan_name =~ s/"//g;
		if ($fan_speed == 0) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$error_str .= "ERROR: Fan $fan_name speed is $fan_speed!";
		} else {
			$ok_str .= "OK: Fan $fan_name speed is $fan_speed.";
		}
	}

	my $serviceid = get_serviceid('fanspeed');
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}


sub check_qnap_ram
{
	my ($expire, $h_ref) = @_;
	my $s;

	my $serviceid = get_serviceid('ram');
	my $statusid = $SisIYA_Config::statusids{'info'};
	$s = "INFO: Total RAM: $h_ref->{$oids{'system_total_ram'}} Free RAM: $h_ref->{$oids{'system_free_ram'}}";
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}


sub check_qnap_system
{
	my ($expire, $h_ref) = @_;
	my $s;

	my $serviceid = get_serviceid('system');
	my $statusid = check_system_status($h_ref, \%uptimes, \$s);
	$s .= " Model: $h_ref->{$oids{'model_name'}}";		
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_qnap
{
	my ($expire, $system_name, $snmp_session) = @_;

	# print STDERR "Checking system_name=[$system_name] ...\n";
	#my $s = check_snmp_system($expire, $snmp_session, \%uptimes);
	#if ($s eq '') {
	#	return '';
	#}
	my $h_ref = get_snmp_all_oids($snmp_session, \%oid_tables, \%oids);
	my $s .= check_qnap_system($expire, $h_ref);
	$s .= check_qnap_smart($expire, $h_ref);
	$s .= check_qnap_raid($expire, $h_ref);
	$s .= check_qnap_temperature($expire, $h_ref);
	$s .= check_qnap_load($expire, $h_ref);
	$s .= check_qnap_ram($expire, $h_ref);
	$s .= check_qnap_fan($expire, $h_ref);
	return "<system><name>$system_name</name>$s</system>";
}
########################################################################
my ($systems_file, $expire) = @ARGV;
my $serviceid = get_serviceid($check_name);
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file, ForceArray => ['record']);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
foreach my $h (@{$data->{'record'}}) {
	if ($h->{'isactive'} eq 'f' ) {
		next;
	}
	#print STDERR "Checking $h->{'hostname'} version: $h->{'snmp_version'} ...\n";
	$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	if ($snmp_session == 0) {
		next;
	}
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str .= check_qnap($expire, $h->{'system_name'}, $snmp_session);
	$snmp_session->close();
}
unlock_check($check_name);
print $xml_str;
