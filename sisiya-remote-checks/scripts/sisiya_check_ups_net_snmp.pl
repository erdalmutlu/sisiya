#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use Net::SNMP;

my $check_name = 'ups';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %oid_tables = (
	'bypass'	=> '1.3.6.1.2.1.33.1.5.3',
	'input'		=> '1.3.6.1.2.1.33.1.3.3',
	'output'	=> '1.3.6.1.2.1.33.1.4.4'
);
our %oids = (
	# system
	'system_description'		=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'			=> '1.3.6.1.2.1.1.3.0',
	'system_contact'		=> '1.3.6.1.2.1.1.4.0',
	'system_name'			=> '1.3.6.1.2.1.1.5.0',
	'system_location'		=> '1.3.6.1.2.1.1.6.0',
	# battery
	'battery_status'		=> '1.3.6.1.2.1.33.1.2.1.0',
	'seconds_on_battery'		=> '1.3.6.1.2.1.33.1.2.2.0',
	'estimated_minutes_remaining'	=> '1.3.6.1.2.1.33.1.2.3.0',
	'estimated_charge_remaining'	=> '1.3.6.1.2.1.33.1.2.4.0',
	'battery_voltage'		=> '1.3.6.1.2.1.33.1.2.5.0',
	'battery_current'		=> '1.3.6.1.2.1.33.1.2.6.0',
	'battery_temperature'		=> '1.3.6.1.2.1.33.1.2.7.0',
	#
	'upsmgIdentFirmwareVersion'	=> '1.3.6.1.4.1.705.1.1.4.0',
	'upsIdentAgentSoftwareVersion'	=> '1.3.6.1.2.1.33.1.1.4.0',
	# input
	'input_num_lines'		=> '1.3.6.1.2.1.33.1.3.2',
	'input_line_bads'		=> '1.3.6.1.2.1.33.1.3.1.0',
	'input_num_lines'		=> '1.3.6.1.2.1.33.1.3.2.0',
	'input_frequency'		=> '1.3.6.1.2.1.33.1.3.3.1.2',
	'input_voltage'			=> '1.3.6.1.2.1.33.1.3.3.1.3',
	'input_current'			=> '1.3.6.1.2.1.33.1.3.3.1.4',
	'input_true_power'		=> '1.3.6.1.2.1.33.1.3.3.1.5',
	# output
	'output_num_lines'		=> '1.3.6.1.2.1.33.1.4.3',
	'output_source'			=> '1.3.6.1.2.1.33.1.4.1.0',
	'output_frequency'		=> '1.3.6.1.2.1.33.1.4.2.0',
	'output_num_lines'		=> '1.3.6.1.2.1.33.1.4.3.0',
	'output_voltage'		=> '1.3.6.1.2.1.33.1.4.4.1.2',
	'output_current'		=> '1.3.6.1.2.1.33.1.4.4.1.3',
	'output_power'			=> '1.3.6.1.2.1.33.1.4.4.1.4',
	'output_percent_load'		=> '1.3.6.1.2.1.33.1.4.4.1.5',
	# bypass
	'bypass_num_lines'		=> '1.3.6.1.2.1.33.1.5.2',
	'bypass_frequency'		=> '1.3.6.1.2.1.33.1.5.1.0',
	'bypass_num_lines'		=> '1.3.6.1.2.1.33.1.5.2.0',
	'bypass_voltage'		=> '1.3.6.1.2.1.33.1.5.3.1.2',
	'bypass_current'		=> '1.3.6.1.2.1.33.1.5.3.1.3',
	'bypass_power'			=> '1.3.6.1.2.1.33.1.5.3.1.4',
	# powerware
	'xups_env_ambient_temp'		=> '1.3.6.1.4.1.534.1.6.1.0',
	'xups_env_ambient_lower_limit'	=> '1.3.6.1.4.1.534.1.6.2.0',
	'xups_env_ambient_upper_limit'	=> '1.3.6.1.4.1.534.1.6.3.0'
);
our %thresholds = (
	'battery_temperature' 		=> { 'warning' => 40,	'error' => 45 },	# in Celcius
	'input_frequency_lower'		=> { 'warning' => 47,	'error' => 40 },	# in Hz
	'input_frequency_upper'		=> { 'warning' => 53,	'error' => 60 },	# in Hz
	'output_frequency_lower'	=> { 'warning' => 47,	'error' => 40 },	# in Hz
	'output_frequency_upper'	=> { 'warning' => 53,	'error' => 60 },	# in Hz
	'output_percent_load' 		=> { 'warning' => 45,	'error' => 50 },	# in %
	'input_current_lower'		=> { 'warning' => 2,	'error' => 4 },		# in current
	'input_current_upper'		=> { 'warning' => 2,	'error' => 4 },		# in current
	'input_voltage_lower'		=> { 'warning' => 205,	'error' => 200 },	# in voltage
	'input_voltage_upper'		=> { 'warning' => 235,	'error' => 240 },	# in voltage
	'output_voltage_lower'		=> { 'warning' => 205,	'error' => 200 },	# in voltage
	'output_voltage_upper'		=> { 'warning' => 235,	'error' => 240 },	# in voltage
	'estimated_minutes_remaining'	=> { 'warning' => 60,	'error' => 30 },	# in minutes
	'estimated_charge_remaining'	=> { 'warning' => 50,	'error' => 30 },	# in percent
	'seconds_on_battery' 		=> { 'warning' => 3600,	'error' => 7200 }	# in seconds 
);
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/printer_system_$system_name.pl
# end of default values
############################################################################################################

sub check_ups_battery_status
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'battery_status'}};
	if ($x eq '') {
		return '';
	}
	if ($x == 1) {
		$msg_ref->{'error'} .= " ERROR: The battery status is unknown!";
	} elsif ($x == 2) {
		$msg_ref->{'ok'} .= " OK: The battery status is normal.";
	} elsif ($x == 3) {
		$msg_ref->{'warning'} .= " WARNING: The battery status is low!";
	} elsif ($x == 4) {
		$msg_ref->{'error'} .= " ERROR: The battery is depleted!";
	} else {
		$msg_ref->{'error'} .= " ERROR: Unknown battery status = $x!";
	}
}

sub check_ups_battery_voltage
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'battery_voltage'}};
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$msg_ref->{'info'} .= " INFO: The battery voltage is $x Volts.";
}


sub check_ups_estimated_minutes_remaining
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'estimated_minutes_remaining'}};
	if ($x eq '') {
		return '';
	}

	if ($x == 0) {
		$msg_ref->{'ok'} .= 'OK: The estimated time on battery is 0. The UPS must be online.';
	} else {
		if ($x <= $thresholds{'estimated_minutes_remaining'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The estimated time on battery is $x (<= $thresholds{'estimated_minutes_remaining'}{'error'}) minutes!";
		} elsif ($x <= $thresholds{'estimated_minutes_remaining'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The estimated time on battery is $x (<= $thresholds{'estimated_minutes_remaining'}{'warning'}) minutes!";
		} else {
			$msg_ref->{'ok'} .= "OK: The estimated time on battery is $x minutes.";
		}
	}
}

sub check_ups_estimated_charge_remaining
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'estimated_charge_remaining'}};
	if ($x eq '') {
		return '';
	}

	if ($x <= $thresholds{'estimated_charge_remaining'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The estimated battery chrage remining is $x% (<= $thresholds{'estimated_charge_remaining'}{'error'})!";
	} elsif ($x <= $thresholds{'estimated_charge_remaining'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The estimated battery chrage remining is $x% (<= $thresholds{'estimated_charge_remaining'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The estimated battery chrage remining is $x%.";
	}
}

sub check_ups_seconds_on_battery
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'seconds_on_battery'}};
	if ($x eq '') {
		return '';
	}
	if ($x == 0) {
		$msg_ref->{'ok'} .= ' OK: The time spend on battery is 0. The UPS must be online.';
	} else {
		if ($x <= $thresholds{'seconds_on_battery'}{'error'}) {
			$msg_ref->{'error'} .= " ERROR: The time spend on battery is $x (<= $thresholds{'seconds_on_battery'}{'error'})! seconds";
		} elsif ($x <= $thresholds{'seconds_on_battery'}{'warning'}) {
			$msg_ref->{'warning'} .= " WARNING: The time spend on battery is $x (<= $thresholds{'seconds_on_battery'}{'warning'}) seconds!";
		} else {
			$msg_ref->{'ok'} .= " OK: The time spend on battery is $x seconds.";
		}
	}
}


sub check_ups_battery
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_battery');

	check_ups_battery_status($h_ref, \%msg);
	check_ups_battery_voltage($h_ref, \%msg);

	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}

sub check_ups_ambient_temperature
{
	my ($expire, $h_ref) = @_;
	my $serviceid = get_serviceid('temperature');
	my $s = '';

	my $x = $h_ref->{$oids{'xups_env_ambient_temp'}};
	if ($x eq '') {
		return '';
	}
	my $lower_limit = $h_ref->{$oids{'xups_env_ambient_lower_limit'}};
	if ($lower_limit eq '') {
		return '';
	}
	my $upper_limit = $h_ref->{$oids{'xups_env_ambient_upper_limit'}};
	if ($upper_limit eq '') {
		return '';
	}
	
	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($x == 0) {
		#$s = ' OK: No temperature sensor.';
		return '';
	} else {
		if ($x >= $upper_limit) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$s = "ERROR: Ambient temperature is $x C (>= $upper_limit)!";
		} elsif ($x <= $lower_limit) {
			$statusid = $SisIYA_Config::statusids{'warning'};
			$s = "WARNING: Ambient temperature is $x C (<= $lower_limit)!";
			} else {
				$s = "OK: Ambient temperature is $x C.";
			}
		}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_ups_battery_temperature
{
	my ($expire, $h_ref) = @_;
	my $serviceid = get_serviceid('temperature');
	my $s = '';

	my $x = $h_ref->{$oids{'battery_temperature'}};
	if ($x eq '') {
		return '';
	}
	
	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($x == 0) {
		#$s = ' OK: No temperature sensor.';
		return '';
	} else {
		if ($x >= $thresholds{'battery_temperature'}{'error'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$s = "ERROR: Battery temperature is $x C (>= $thresholds{'battery_temperature'}{'error'})!";
		} elsif ($x >= $thresholds{'battery_temperature'}{'warning'}) {
			$statusid = $SisIYA_Config::statusids{'warning'};
			$s = "WARNING: Battery temperature is $x C (>= $thresholds{'battery_temperature'}{'warning'})!";
			} else {
				$s = "OK: Battery temperature is $x C.";
			}
		}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_ups_battery_times
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_timeonbattery');

	check_ups_estimated_minutes_remaining($h_ref, \%msg);
	check_ups_estimated_charge_remaining($h_ref, \%msg);
	check_ups_seconds_on_battery($h_ref, \%msg);

	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";

}

sub check_ups_bypass
{
	my ($expire, $snmp_session) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_bypass');

	check_ups_bypass_frequency($snmp_session, \%msg);

	my $nlines = get_snmp_value2($snmp_session, $oids{'bypass_num_lines'});

	if ($nlines ne '') {
		check_ups_bypass_voltage($snmp_session, \%msg, $nlines);
		check_ups_input_current($snmp_session, \%msg, $nlines);
		check_ups_input_true_power($snmp_session, \%msg, $nlines);
	}
	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}


sub check_ups_input_current
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'input_current'}.'.'.$i;
		$x = $h_ref->{$oid};

		if ($x == 0) {
			next;
		}
		if ($x >= $thresholds{'input_current_upper'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input current is ".$x."V (>= $thresholds{'input_current_upper'}{'error'})!";
		} elsif ($x <= $thresholds{'input_current_lower'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input current is ".$x."V (<= $thresholds{'input_current_lower'}{'error'})!";
		} elsif ($x >= $thresholds{'input_current_upper'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input current is ".$x."V (>= $thresholds{'input_current_upper'}{'warning'})!";
		} elsif ($x <= $thresholds{'input_current_lower'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input current is ".$x."V (<= $thresholds{'input_current_lower'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The input current is ".$x."V.";
		}
	}
}

sub check_ups_input_true_power
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'input_true_power'}.'.'.$i;
		$x = $h_ref->{$oid};

		if ($x == 0) {
			next;
		}
		if ($x >= $thresholds{'input_true_power_upper'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input true power is ".$x."V (>= $thresholds{'input_true_power_upper'}{'error'})!";
		} elsif ($x <= $thresholds{'input_true_power_lower'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input true power is ".$x."V (<= $thresholds{'input_true_power_lower'}{'error'})!";
		} elsif ($x >= $thresholds{'input_true_power_upper'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input true power is ".$x."V (>= $thresholds{'input_true_power_upper'}{'warning'})!";
		} elsif ($x <= $thresholds{'input_true_power_lower'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input true power is ".$x."V (<= $thresholds{'input_true_power_lower'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The input true power is ".$x."V.";
		}
	}
}

sub check_ups_input_voltage
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'input_voltage'}.'.'.$i;
		$x = $h_ref->{$oid};

		if ($x >= $thresholds{'input_voltage_upper'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input voltage is ".$x."V (>= $thresholds{'input_voltage_upper'}{'error'})!";
		} elsif ($x <= $thresholds{'input_voltage_lower'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input voltage is ".$x."V (<= $thresholds{'input_voltage_lower'}{'error'})!";
		} elsif ($x >= $thresholds{'input_voltage_upper'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input voltage is ".$x."V (>= $thresholds{'input_voltage_upper'}{'warning'})!";
		} elsif ($x <= $thresholds{'input_voltage_lower'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input voltage is ".$x."V (<= $thresholds{'input_voltage_lower'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The input voltage is ".$x."V.";
		}
	}
}

sub check_ups_input_frequency
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'input_frequency'}.'.'.$i;
		$x = $h_ref->{$oid};
		$x = $x / 10;
		if ($x >= $thresholds{'input_frequency_upper'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The input frequency is ".$x."Hz (>= $thresholds{'input_frequency_upper'}{'error'})!";
		} elsif ($x <= $thresholds{'input_frequency_lower'}{'error'}) {
			$msg_ref->{'error'}  .= "ERROR: The input frequency is ".$x."Hz (<= $thresholds{'input_frequency_lower'}{'error'})!";
		} elsif ($x >= $thresholds{'input_frequency_upper'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input frequency is ".$x."Hz (>= $thresholds{'input_frequency_upper'}{'warning'})!";
		} elsif ($x <= $thresholds{'input_frequency_lower'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The input frequency is ".$x."Hz (<= $thresholds{'input_frequency_lower'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The input frequency is ".$x."Hz.";
		}
	}
}

sub check_ups_input
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_input');

	my $nlines = $h_ref->{$oids{'input_num_lines'}};

	if ($nlines ne '') {
		check_ups_input_voltage($h_ref, \%msg, $nlines);
		check_ups_input_frequency($h_ref, \%msg, $nlines);
		check_ups_input_current($h_ref, \%msg, $nlines);
		check_ups_input_true_power($h_ref, \%msg, $nlines);
	}
	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}

sub check_ups_output_load
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'output_percent_load'}.'.'.$i;
		$x = $h_ref->{$oid};
		if ($x eq '') {
			return '';
		}
		if ($x >= $thresholds{'output_percent_load'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The output load is ".$x."% (>= $thresholds{'output_percent_load'}{'error'})!";
		} elsif ($x >= $thresholds{'output_percent_load'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The load is ".$x."% (>= $thresholds{'output_percent_load'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The output load is ".$x."%.";
		}
	}
}

sub check_ups_output_source
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'output_source'}};
	if ($x eq '') {
		return '';
	}

	if ($x == 1) {
		$msg_ref->{'error'} .= " ERROR: The output source is unknown!";
	} elsif ($x == 2) {
		$msg_ref->{'warning'} .= " WARNING: The output source is none!";
	} elsif ($x == 3) {
		$msg_ref->{'ok'} .= " OK: The output source is normal.";
	} elsif ($x == 4) {
		$msg_ref->{'warning'} .= " WARNING: The output source is bypass!";
	} elsif ($x == 5) {
		$msg_ref->{'warning'} .= " WARNING: The output source is booster!";
	} elsif ($x == 6) {
		$msg_ref->{'warning'} .= " WARNING: The output source is reducer!";
	} else {
		$msg_ref->{'error'} .= " ERROR: Unknown output source = $x!";
	}
}

sub check_output_current
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'output_current'}.'.'.$i;
		$x = $h_ref->{$oid};
		if ($x eq '') {
			return '';
		}
		$x = $x / 10;
		$msg_ref->{'info'} .= "INFO: The output current is ".$x." Ampers.";
	}
}

sub check_output_power
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'output_power'}.'.'.$i;
		$x = $h_ref->{$oid};
		if ($x eq '') {
			return '';
		}
		$msg_ref->{'info'} .= "INFO: The output power is ".$x."Watts.";
	}
}


sub check_output_voltage
{
	my ($h_ref, $msg_ref, $nlines) = @_;

	my ($x, $oid);
	for (my $i = 1; $i <= $nlines; $i++) {
		$oid = $oids{'output_voltage'}.'.'.$i;
		$x = $h_ref->{$oid};
		if ($x eq '') {
			return '';
		}
		if ($x >= $thresholds{'output_voltage_upper'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The output voltage is ".$x."V (>= $thresholds{'output_voltage_upper'}{'error'})!";
		} elsif ($x <= $thresholds{'output_voltage_lower'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The output voltage is ".$x."V (<= $thresholds{'output_voltage_lower'}{'error'})!";
		} elsif ($x >= $thresholds{'output_voltage_upper'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The output voltage is ".$x."V (>= $thresholds{'output_voltage_upper'}{'warning'})!";
		} elsif ($x <= $thresholds{'output_voltage_lower'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The output voltage is ".$x."V (<= $thresholds{'output_voltage_lower'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The output voltage is ".$x."V.";
		}
	}
}

sub check_ups_output_frequency
{
	my ($h_ref, $msg_ref) = @_;

	my $x = $h_ref->{$oids{'output_frequency'}};
	if ($x eq '') {
		return '';
	}

	$x = $x / 10;
	if ($x >= $thresholds{'output_frequency_upper'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output frequency is ".$x."Hz (>= $thresholds{'output_frequency_upper'}{'error'})!";
	} elsif ($x <= $thresholds{'output_frequency_lower'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output frequency is ".$x."Hz (<= $thresholds{'output_frequency_lower'}{'error'})!";
	} elsif ($x >= $thresholds{'output_frequency_upper'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The output frequency is ".$x."Hz (>= $thresholds{'output_frequency_upper'}{'warning'})!";
	} elsif ($x <= $thresholds{'output_frequency_lower'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The output frequency is ".$x."Hz (<= $thresholds{'output_frequency_lower'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The output frequency is ".$x."Hz.";
	}
}

sub check_ups_output
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_output');

	check_ups_output_source($h_ref, \%msg);
	check_ups_output_frequency($h_ref, \%msg);

	my $nlines = $h_ref->{$oids{'output_num_lines'}};
	if ($nlines ne '') {
		check_ups_output_load($h_ref, \%msg, $nlines);
		check_output_voltage($h_ref, \%msg, $nlines);
		check_output_power($h_ref, \%msg, $nlines);
		check_output_current($h_ref, \%msg, $nlines);
	}

       	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}


sub check_ups
{
	my ($expire, $system_name, $snmp_session) = @_;

	# print STDERR "Checking system_name=[$system_name] ...\n";
	my $h_ref = get_snmp_all_oids($snmp_session, \%oid_tables, \%oids);
	my $s = check_system($expire, $h_ref, \%uptimes);
	$s .= check_ups_battery($expire, $h_ref);
	$s .= check_ups_battery_times($expire, $h_ref);
	$s .= check_ups_input($expire, $h_ref);
	$s .= check_ups_output($expire, $h_ref);
	$s .= check_ups_battery_temperature($expire, $h_ref);
	$s .= check_ups_ambient_temperature($expire, $h_ref);
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file, ForceArray => ['record']);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
foreach my $h (@{$data->{'record'}}) {
	if ($h->{'isactive'} eq 'f' ) {
		next;
	}
	$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	if ($snmp_session == 0) {
		next;
	}
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str .= check_ups($expire, $h->{'system_name'}, $snmp_session);
}
unlock_check($check_name);
print $xml_str;
