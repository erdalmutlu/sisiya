#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use Time::Piece;
#use Data::Dumper;

sub get_days
{
	my ($date_str) = @_;

	my @a = split(/\s+/, $date_str);
	my $t = Time::Piece->strptime("$a[0] $a[1] $a[3]", "%b %d %Y");
	my $now = Time::Piece->localtime();
	my $diff = Time::Seconds->new($t - $now);
	return int($diff->days);
}

sub check_certificate_expire_days
{
	my ($statusid, $host, $port) = @_;

	my $error_days = 15;
	my $warn_days = 20;
	# print STDERR "check_ssl: Checking host=[$host] port=[$port] statusid=$$statusid...\n";

	#############################################################

	#############################################################
	my $params = '';
	my @a;
	#@a = `echo | openssl s_client -servername $host -connect $host:$port | openssl x509 -noout -enddate`;
	@a = `$SisIYA_Remote_Config::external_progs{'echo'} | $SisIYA_Remote_Config::external_progs{'openssl'} s_client -servername $host -connect $host:$port 2> /dev/null | $SisIYA_Remote_Config::external_progs{'openssl'} x509 -noout -enddate`;
	# print STDERR @a;
       	my $s = '';
	my $retcode = $?;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: Something went very wrong! retcode=$retcode";
	}
	else {
		# sample:
		# notAfter=Feb 12 01:15:05 2022 GMT
		# https://www.perl.com/article/59/2014/1/10/Solve-almost-any-datetime-need-with-Time--Piece/
		my @b = split(/=/, $a[0]);
		chomp($b[1]);
		my $days = get_days($b[1]);
		# print STDERR "end date: $b[1] ($days)";
		# check date and warn before the certificate expires
		if ($days <= $error_days) {
			$$statusid = $SisIYA_Config::statusids{'error'};
			# $s = " ERROR: Certificate for $host ends $b[1] ($days days) <= $error_days)!";
			$s = " ERROR: Certificate for $host ends in $days days (<= $error_days)!";
		} elsif ($days <= $warn_days) {
			if ($$statusid < $SisIYA_Config::statusids{'warning'}) {
				$$statusid = $SisIYA_Config::statusids{'warning'};
			}
			$s = " WARNING: Certificate for $host ends in $days days (<= $warn_days)!";
		} else {
			$s = " OK: Certificate for $host ends in $days days.";
		}
	}
	return $s;
}

my $check_name = 'ssl';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

my ($systems_file, $expire) = @ARGV;
my $serviceid = get_serviceid($check_name);
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
#print STDERR Dumper($data);
if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}

# build a hash and group by system name{ system_name => [ { host => 'host_name', port => port } ] }
my %y;
if (ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		if ($h->{'isactive'} eq 't') {
			push @{$y{$h->{'system_name'}}}, { host => $h->{'host'}, port=> $h->{'port'} };
		}
	}
} else {
	my $h = $data->{'record'};
	if ($h->{'isactive'} eq 't') {
		push @{$y{$h->{'system_name'}}}, { host => $h->{'host'}, port=> $h->{'port'} };
	}
}

foreach (keys %y) {
	$xml_str .= "<system><name>$_</name><message><serviceid>$serviceid</serviceid>";
	#print STDERR "system name: $_\n";
	#my $r = $y{$_};
	#my @a = @$r;
	#foreach(@a) {
	my $statusid = $SisIYA_Config::statusids{'ok'};
	my $s = '';
	foreach(@{$y{$_}}) {
		# print STDERR "host: $_->{'host'} port: $_->{'port'}\n";
		$s .= check_certificate_expire_days(\$statusid, $_->{'host'}, $_->{'port'});
	}
	$xml_str .= "<statusid>$statusid</statusid><expire>$expire</expire><data><msg>";
	$xml_str .= $s.'</msg><datamsg></datamsg></data></message></system>';
}

unlock_check($check_name);
# print STDERR $xml_str."\n";
print $xml_str;
