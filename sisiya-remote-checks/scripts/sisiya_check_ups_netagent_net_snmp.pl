#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use Net::SNMP;

my $check_name = 'ups_netagent';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %oid_tables = (
#	'ups'	=> ''
);
our %oids = (
	# system
	'system_description'			=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'				=> '1.3.6.1.2.1.1.3.0',
	'system_contact'			=> '1.3.6.1.2.1.1.4.0',
	'system_name'				=> '1.3.6.1.2.1.1.5.0',
	'system_location'			=> '1.3.6.1.2.1.1.6.0',
	#
	'battery_capacity'		=> '1.3.6.1.4.1.935.1.1.1.2.2.1.0',
	'battery_replacement_status'	=> '1.3.6.1.4.1.318.1.1.1.2.2.4.0',
	'battery_status'		=> '1.3.6.1.2.1.33.1.2.1.0',
	'estimated_time_on_battery'	=> '1.3.6.1.4.1.935.1.1.1.2.2.4.0',
	'firmware' 			=> '1.3.6.1.4.1.935.1.1.1.1.2.4.0',
	'time_spend_on_battery'		=> '1.3.6.1.4.1.935.1.1.1.2.1.2.0',
	'ups_input_ac_status'		=> '1.3.6.1.4.1.935.1.1.1.3.1.1.0',
	'ups_input_frequency'		=> '1.3.6.1.4.1.935.1.1.1.4.2.2.0',
	'ups_input_voltage'		=> '1.3.6.1.4.1.935.1.1.1.3.2.1.0',
	'ups_output_frequency'		=> '1.3.6.1.4.1.935.1.1.1.4.2.2.0',
	'ups_output_load'		=> '1.3.6.1.4.1.935.1.1.1.4.2.3.0',
	'ups_output_status'		=> '1.3.6.1.4.1.935.1.1.1.4.1.1.0',
	'ups_output_voltage'		=> '1.3.6.1.4.1.935.1.1.1.4.2.1.0',
	'ups_status'			=> '1.3.6.1.4.1.935.1.1.1.4.1.1.0'
);
our @tsensors = (
	{ 'name' => 'internal', 'warning' => 30, 'error' => 35, 'mib' => '1.3.6.1.4.1.935.1.1.1.2.1.2.0' },
	{ 'name' => 'external', 'warning' => 30, 'error' => 35, 'mib' => '1.3.6.1.4.1.318.1.1.10.2.3.2.1.4.1' }
);
our %thresholds = (
	'battery_capacity' 		=> { 'warning' => 90,	'error' => 85 },	# in capacity
	'input_frequency_lower'		=> { 'warning' => 47,	'error' => 40 },	# in Hz
	'input_frequency_upper'		=> { 'warning' => 53,	'error' => 60 },	# in Hz
	'output_frequency_lower'	=> { 'warning' => 47,	'error' => 40 },	# in Hz
	'output_frequency_upper'	=> { 'warning' => 53,	'error' => 60 },	# in Hz
	'output_load' 			=> { 'warning' => 45,	'error' => 50 },	# in %
	'input_voltage_lower'		=> { 'warning' => 205,	'error' => 200 },	# in voltage
	'input_voltage_upper'		=> { 'warning' => 235,	'error' => 240 },	# in volatge
	'output_voltage_lower'		=> { 'warning' => 205,	'error' => 200 },	# in voltage
	'output_voltage_upper'		=> { 'warning' => 235,	'error' => 240 },	# in voltage
	'estimated_time_on_battery'	=> { 'warning' => 600,	'error' => 300 },	# in minutes
	'time_spend_on_battery' 	=> { 'warning' => 600,	'error' => 300 }	# in minutes
);
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/printer_system_$system_name.pl
# end of default values
############################################################################################################
sub check_ups_battery_capacity
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'battery_capacity'}};
	if ($x eq '') {
		return '';
	}

	if ($x <= $thresholds{'battery_capacity'}{'error'}) {
		$msg_ref->{'error'}.= " ERROR: The total battery capacity is $x (<= $thresholds{'battery_capacity'}{'error'})!";
	} elsif ($x <= $thresholds{'battery_capacity'}{'warning'}) {
		$msg_ref->{'warning'} .= " WARNING: The total battery capacity is $x (<= $thresholds{'battery_capacity'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= " OK: The total battery capacity is $x.";
	}
}

sub check_ups_battery_status
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'battery_status'}};
	if ($x eq '') {
		return '';
	}
	if ($x == 1) {
		$msg_ref->{'error'} .= " ERROR: The battery status is unknown!";
	} elsif ($x == 2) {
		$msg_ref->{'ok'} .= " OK: The battery status is normal.";
	} elsif ($x == 3) {
		$msg_ref->{'warning'} .= " WARNING: The battery status is low!";
	} else {
		$msg_ref->{'error'} .= " ERROR: Unknown battery status = $x!";
	}
}
sub check_ups_battery_replacement_status
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'battery_replacement_status'}};
	if ($x eq '') {
		return '';
	}
	if ($x == 1) {
		$msg_ref->{'ok'} .= "OK: The battery does not need replacement.";
	} elsif ($x == 3) {
		$msg_ref->{'warning'} .= "WARNING: The battery needs replacement!";
	} else {
		$msg_ref->{'error'} .= "ERROR: Unknown battery replacement status = $x!";
	}
}

sub check_ups_estimated_time_on_battery
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'estimated_time_on_battery'}};
	if ($x eq '') {
		return '';
	}

	if ($x == 0) {
		$msg_ref->{'ok'} .= 'OK: The estimated time on battery is 0. The UPS must be online.';
	} else {
		if ($x <= $thresholds{'estimated_time_on_battery'}{'error'}) {
			$msg_ref->{'error'} .= "ERROR: The estimated time on battery is $x (<= $thresholds{'estimated_time_on_battery'}{'error'})!";
		} elsif ($x <= $thresholds{'estimated_time_on_battery'}{'warning'}) {
			$msg_ref->{'warning'} .= "WARNING: The estimated time on battery is $x (<= $thresholds{'estimated_time_on_battery'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= "OK: The estimated time on battery is $x.";
		}
	}
}

sub check_ups_time_spend_on_battery
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'time_spend_on_battery'}};
	if ($x eq '') {
		return '';
	}
	if ($x == 0) {
		$msg_ref->{'ok'} .= ' OK: The time spend on battery is 0. The UPS must be online.';
	} else {
		if ($x <= $thresholds{'time_spend_on_battery'}{'error'}) {
			$msg_ref->{'error'} .= " ERROR: The time spend on battery is $x (<= $thresholds{'time_spend_on_battery'}{'error'})!";
		} elsif ($x <= $thresholds{'time_spend_on_battery'}{'warning'}) {
			$msg_ref->{'warning'} .= " WARNING: The time spend on battery is $x (<= $thresholds{'time_spend_on_battery'}{'warning'})!";
		} else {
			$msg_ref->{'ok'} .= " OK: The time spend on battery is $x.";
		}
	}
}

sub check_ups_battery
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_battery');

	check_ups_battery_capacity(\%msg, $h_ref);
	check_ups_battery_status(\%msg, $h_ref);
	check_ups_battery_replacement_status(\%msg, $h_ref);

	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}

sub check_ups_battery_times
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_timeonbattery');

	check_ups_estimated_time_on_battery(\%msg, $h_ref);
	check_ups_time_spend_on_battery(\%msg, $h_ref);

	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";

}

sub check_ups_status
{
	my ($expire, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_status'}};
	if ($x eq '') {
		return '';
	}
	my $serviceid = get_serviceid('ups_status');
	my $statusid = $SisIYA_Config::statusids{'error'};
	my $s = '';
	if ($x == 1) {
		$s = "ERROR: The UPS status is unknown!";
	} elsif ($x == 2) {
		$statusid = $SisIYA_Config::statusids{'ok'};
		$s = "OK: The UPS is online.";
	} elsif ($x == 3) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: The UPS is on battery!";
	} elsif ($x == 4) {
		$s = "ERROR: The UPS is on smart boost!";
	} elsif ($x == 5) {
		$s = "ERROR: The UPS is timed sleeping!";
	} elsif ($x == 6) {
		$s = "ERROR: The UPS is on software bypass!";
	} elsif ($x == 7) {
		$s = "ERROR: The UPS is rebooting!";
	} elsif ($x == 8) {
		$s = "ERROR: The UPS is standby!";
	} elsif ($x == 9) {
		$s = "ERROR: The UPS is on buck!";
	} else {
		$s = "ERROR: The UPS status=$x is unknown!";
	}

	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_ups_input_ac_status
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_input_ac_status'}};
	if ($x eq '') {
		return '';
	}
	if ($x == 1) {
		$msg_ref->{'ok'} .= "OK: The AC status in normal.";
	} else {
		$msg_ref->{'error'} .= "ERROR: Unknown AC status = $x!";
	}
}

sub check_ups_input_voltage
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_input_voltage'}};
	if ($x eq '') {
		return '';
	}

	$x = $x / 10;
	if ($x >= $thresholds{'input_voltage_upper'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The input voltage is ".$x."V (>= $thresholds{'input_voltage_upper'}{'error'})!";
	} elsif ($x <= $thresholds{'input_voltage_lower'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The input voltage is ".$x."V (<= $thresholds{'input_voltage_lower'}{'error'})!";
	} elsif ($x >= $thresholds{'input_voltage_upper'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The input voltage is ".$x."V (>= $thresholds{'input_voltage_upper'}{'warning'})!";
	} elsif ($x <= $thresholds{'input_voltage_lower'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The input voltage is ".$x."V (<= $thresholds{'input_voltage_lower'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The input voltage is ".$x."V.";
	}
}

sub check_ups_input_frequency
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_input_frequency'}};
	if ($x eq '') {
		return '';
	}

	$x = $x / 10;
	if ($x >= $thresholds{'input_frequency_upper'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The input frequency is ".$x."Hz (>= $thresholds{'input_frequency_upper'}{'error'})!";
	} elsif ($x <= $thresholds{'input_frequency_lower'}{'error'}) {
		$msg_ref->{'error'}  .= "ERROR: The input frequency is ".$x."Hz (<= $thresholds{'input_frequency_lower'}{'error'})!";
	} elsif ($x >= $thresholds{'input_frequency_upper'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The input frequency is ".$x."Hz (>= $thresholds{'input_frequency_upper'}{'warning'})!";
	} elsif ($x <= $thresholds{'input_frequency_lower'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The input frequency is ".$x."Hz (<= $thresholds{'input_frequency_lower'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The input frequency is ".$x."Hz.";
	}
}

sub check_ups_input
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_input');

	check_ups_input_ac_status(\%msg, $h_ref);
	check_ups_input_voltage(\%msg, $h_ref);
	check_ups_input_frequency(\%msg, $h_ref);

	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}


sub check_ups_output_load
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_output_load'}};
	if ($x eq '') {
		return '';
	}
	if ($x >= $thresholds{'output_load'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output load is ".$x."% (>= $thresholds{'output_load'}{'error'})!";
	} elsif ($x >= $thresholds{'output_load'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The load is ".$x."% (>= $thresholds{'output_load'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The output load is ".$x."%.";
	}
}

sub check_ups_output_voltage
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_output_voltage'}};
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	if ($x >= $thresholds{'output_voltage_upper'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output voltage is ".$x."V (>= $thresholds{'output_voltage_upper'}{'error'})!";
	} elsif ($x <= $thresholds{'output_voltage_lower'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output voltage is ".$x."V (<= $thresholds{'output_voltage_lower'}{'error'})!";
	} elsif ($x >= $thresholds{'output_voltage_upper'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The output voltage is ".$x."V (>= $thresholds{'output_voltage_upper'}{'warning'})!";
	} elsif ($x <= $thresholds{'output_voltage_lower'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The output voltage is ".$x."V (<= $thresholds{'output_voltage_lower'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The output voltage is ".$x."V.";
	}
}

sub check_ups_output_frequency
{
	my ($msg_ref, $h_ref) = @_;

	my $x = $h_ref->{$oids{'ups_output_frequency'}};
	if ($x eq '') {
		return '';
	}

	$x = $x / 10;
	if ($x >= $thresholds{'output_frequency_upper'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output frequency is ".$x."Hz (>= $thresholds{'output_frequency_upper'}{'error'})!";
	} elsif ($x <= $thresholds{'output_frequency_lower'}{'error'}) {
		$msg_ref->{'error'} .= "ERROR: The output frequency is ".$x."Hz (<= $thresholds{'output_frequency_lower'}{'error'})!";
	} elsif ($x >= $thresholds{'output_frequency_upper'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The output frequency is ".$x."Hz (>= $thresholds{'output_frequency_upper'}{'warning'})!";
	} elsif ($x <= $thresholds{'output_frequency_lower'}{'warning'}) {
		$msg_ref->{'warning'} .= "WARNING: The output frequency is ".$x."Hz (<= $thresholds{'output_frequency_lower'}{'warning'})!";
	} else {
		$msg_ref->{'ok'} .= "OK: The output frequency is ".$x."Hz.";
	}
}

sub check_ups_output
{
	my ($expire, $h_ref) = @_;
	my %msg = ( 'error' => '', 'warning' => '', 'ok' => '', 'info' => '');
	my $serviceid = get_serviceid('ups_output');

	check_ups_output_load(\%msg, $h_ref);
	check_ups_output_voltage(\%msg, $h_ref);
	check_ups_output_frequency(\%msg, $h_ref);

       	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($msg{'error'} ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($msg{'warning'} ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$msg{'error'}$msg{'warning'}$msg{'ok'}$msg{'info'}</msg><datamsg></datamsg></data></message>";
}

sub check_ups_netagent
{
	my ($expire, $system_name, $snmp_session) = @_;

	# print STDERR "Checking system_name=[$system_name] ...\n";
	my $h_ref = get_snmp_all_oids($snmp_session, \%oid_tables, \%oids);
	
	my $s = check_system($expire, $h_ref, \%uptimes);
	$s .= check_ups_status($expire, $h_ref);
	$s .= check_ups_battery($expire, $h_ref);
	$s .= check_ups_battery_times($expire, $h_ref);
	$s .= check_ups_input($expire, $h_ref);
	$s .= check_ups_output($expire, $h_ref);
	return "<system><name>$system_name</name>$s</system>";
}
########################################################################
my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file, ForceArray => ['record']);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
foreach my $h (@{$data->{'record'}}) {
	if ($h->{'isactive'} eq 'f' ) {
		next;
	}
	$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	if ($snmp_session == 0) {
		next;
	}
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str .= check_ups_netagent($expire, $h->{'system_name'}, $snmp_session);
	$snmp_session->close();
}
unlock_check($check_name);
print $xml_str;
