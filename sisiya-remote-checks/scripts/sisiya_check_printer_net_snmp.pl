#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use Net::SNMP;

my $check_name = 'printer';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}
###########################################################################################################
# default values
# in minutes
our %uptimes = ('error' => 10, 'warning' => 15);
our %oid_tables = (
	'printer' 	=> '1.3.6.1.2.1.25.3.5',	# Printer table 
	'device' 	=> '1.3.6.1.2.1.25.3.2'		# Device tree 
);
our %oids = (
	# printer
	'printer_status'	=> '1.3.6.1.2.1.25.3.5.1.1.1',	# HOST-RESOURCES-MIB::hrPrinterStatus 
	'printer_state' 	=> '1.3.6.1.2.1.25.3.5.1.2.1',	# HOST-RESOURCES-MIB::hrPrinterDetectedERRORState 
	# device
	'device_status' 	=> '1.3.6.1.2.1.25.3.2.1.5.1',	# HOST-RESOURCES-MIB::hrDeviceStatus 
	# system
	'system_description'	=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'		=> '1.3.6.1.2.1.1.3.0',
	'system_contact'	=> '1.3.6.1.2.1.1.4.0',
	'system_name'		=> '1.3.6.1.2.1.1.5.0',
	'system_location'	=> '1.3.6.1.2.1.1.6.0'
);
our @pages = (
	{'name' => 'engine',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.5.0'},
	{'name' => 'duplex',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.22.0'},
	{'name' => 'pcl', 		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.3.3.3.5.0'},
	{'name' => 'postscript',	'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.3.3.4.5.0'},
	{'name' => 'color',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.7.0'},
	{'name' => 'mono',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.6.0'},
	{'name' => 'pagecount',		'oid' => '1.3.6.1.2.1.43.10.2.1.4.1.1'}
);
# push @pages, {'name' => 'engine',            'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.5.0'}; 
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/printer_$system_name.conf
# end of default values
############################################################################################################
my @printer_error_states = ( 'paper low', 'no paper', 'toner low', 'no toner', 'door open', 'jammed', 'offline', 'service needed' );
my @printer_statuses = ( 'other', 'unknown', 'idle', 'printing', 'warmup');

sub decode_error_state_bitstring 
{
	my @bits = split(//, unpack('B8', shift));
	
	if (scalar(@bits) != 8) {
		return '';
	}
	for (my $i = 0; $i < 8; ++$i) {
		if ($bits[$i] == 1) {
			return $printer_error_states[$i];
		}
	}
	return '';
}

sub decode_error_state 
{
	my $s = shift;

	my $str = decode_error_state_bitstring($s);	
	if (length($s) == 2) {
		my $s2 = decode_error_state_bitstring(substr($s, 1,1));
		if ($s2 ne '') {
			if ($str eq '') {
				$str = $s2;
			} else {
				$str .= ','.$s2;
			}
		}
	}
	return $str;
}

sub check_printer_device
{
	my ($expire, $h_ref) = @_; 
	
	my $printer_status = '';
	if (!exists($h_ref->{$oids{'printer_status'}}) || $h_ref->{$oids{'printer_status'}} eq '') {
		return '';
	}

	$printer_status = $h_ref->{$oids{'printer_status'}};
	my $printer_state  = $h_ref->{$oids{'printer_state'}};
	my $device_status = $h_ref->{$oids{'device_status'}};
	my $serviceid = get_serviceid('printer');
	my $statusid = $SisIYA_Config::statusids{'ok'};
	my $s = '';
	#print STDERR "device_status=[$device_status] printer_status=[$printer_status] printer_state=[$printer_state] length=".length($printer_state)."\n";
	if ($device_status == 1) { # unknown
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = 'WARNING: Device status is unknown!';
	} elsif ($device_status == 2) { # running
		$s = 'OK: Device status is '.$printer_statuses[$printer_status].'.';
	} elsif ($device_status == 3) { # warning
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: ".decode_error_state($printer_state).'!';
		$s .= ' Device status is '.$printer_statuses[$printer_status].'.';
	} elsif ($device_status == 4) { # testing
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: The device is testing! device_status=[$device_status] printer_status=[$printer_status] printer_state=[$printer_state]";
	} elsif ($device_status == 5) { # down
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: ".decode_error_state($printer_state).'!';
		$s .= ' Device status is '.$printer_statuses[$printer_status].'.';
	} else {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: Undetermined device status! device_status=[$device_status] printer_status=[$printer_status] printer_state=[$printer_state]";
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_printer_page_counts
{
	my ($expire, $h_ref) = @_; 

	if ($#pages == -1) {
		return '';
	}

	my $serviceid = get_serviceid('printer_pagecounts');
	my $statusid = $SisIYA_Config::statusids{'info'};
	my ($n, $result);
	my $s = '';

	for my $i (0..$#pages) {
		if (!exists($h_ref->{$pages[$i]{'oid'}})) {
			next;
		}
		$n = $h_ref->{$pages[$i]{'oid'}};
		if ($n eq '') {
			next;
		}

		$s .= "INFO: Total number of $pages[$i]{'name'} pages is $n."; 
	}
	if ($s eq '') {
		return '';
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_printer_page_counts2
{
	my ($expire, $snmp_session) = @_; 

	if ($#pages == -1) {
		return '';
	}

	my $serviceid = get_serviceid('printer_pagecounts');
	my $statusid = $SisIYA_Config::statusids{'info'};
	my ($n, $result);
	my $s = '';

	for my $i (0..$#pages) {
		# print STDERR "check_printer_page_counts2: Checking $pages[$i]{'oid'} ...\n";
		$n = get_snmp_value($snmp_session, $pages[$i]{'oid'});
		if ($n eq '') {
			# print STDERR "check_printer_page_counts2: No such oid: $pages[$i]{'oid'} Skipping...\n";
			next;
		}

		$s .= "INFO: Total number of $pages[$i]{'name'} pages is $n."; 
	}
	if ($s eq '') {
		return '';
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_printer
{
	my ($expire, $system_name, $snmp_session) = @_;

	# speed up checks when there are a lot of offline devices
	#my $x = $snmp_session->timeout();
	#print STDERR "check_printer: default session timeout : $x\n";
	#$snmp_session->timeout([1]);
	#$x = $snmp_session->timeout();
	#print STDERR "check_printer: session timeout : $x\n";
	#$x = $snmp_session->retries();
	#print STDERR "check_printer: default session retries : $x\n"; 
	#$snmp_session->retries([1]);
	#$x = $snmp_session->retries();
	#print STDERR "check_printer: session retries : $x\n"; 
	my $s = get_snmp_value($snmp_session, $oids{'system_uptime'});
	if ($s eq '') {
		#print STDERR "check_printer: printer $system_name is unreachable\n";
		return '';
	}
	# gets noSuchName error:
	# add oids from @pages array
	#for my $i (0..$#pages) {
	#	$oids{'page_'.$pages[$i]{'name'}} = $pages[$i]{'oid'};
	#}
	# print_hash(\%oids);
	my $h_ref = get_snmp_all_oids($snmp_session, \%oid_tables, \%oids);
	# check if h_ref is an empty hash
	# print_hash($h_ref);
	if( !keys %$h_ref ) { #if (!%$h_ref) {
		# print STDERR "check_printer: h_ref hash is empty!\n";
		return '';
	}
	# print_hash($h_ref);
	$s = check_system($expire, $h_ref, \%uptimes);
	$s .= check_printer_device($expire, $h_ref);
	#$s .= check_printer_page_counts($expire, $h_ref);
	$s .= check_printer_page_counts2($expire, $snmp_session);
	# print STDERR "check_printer: s: $s\n";
	if ($s eq '') {
		return '';
	}
	# print STDERR "check_printer: OK\n";
	return "<system><name>$system_name</name>$s</system>";
}
########################################################################
my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file, ForceArray => ['record']);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
foreach my $h (@{$data->{'record'}}) {
	if ($h->{'isactive'} eq 'f' ) {
		next;
	}
	#print STDERR "Checking $h->{'hostname'} version: $h->{'snmp_version'} ...\n";
	$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	if ($snmp_session == 0) {
		next;
	}
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str .= check_printer($expire, $h->{'system_name'}, $snmp_session);
	$snmp_session->close();
}
unlock_check($check_name);
print $xml_str;
