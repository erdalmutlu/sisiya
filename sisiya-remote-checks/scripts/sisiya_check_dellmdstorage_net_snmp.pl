#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
#use Data::Dumper;
use Net::SNMP;

my $check_name = 'dellmdstorage';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}
###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %loads = ('error' => 90, 'warning' => 80);
our %default_temperatures = ( 'warning' => 35, 'error' => 40 );
our $retries = 3;	# number of retries for the snmpwalk command
our $timeout = 15;	# timeout between retries for the snmpwalk command 
our %oids = (
	'idrac_mib'				=> '1.3.6.1.4.1.674.10892',			# whole iDRAC tree
	# system
	'mib_table_system'			=> '1.3.6.1.2.1.1',				# SNMP MIB-2 System tree
	'system_description'			=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'				=> '1.3.6.1.2.1.1.3.0',
	'system_contact'			=> '1.3.6.1.2.1.1.4.0',
	'system_name'				=> '1.3.6.1.2.1.1.5.0',
	'system_location'			=> '1.3.6.1.2.1.1.6.0',
	# CPU 
	'mib_table_cpu'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30', 	# CPU tree
	'cpu_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.1',
	'cpu_index'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.2',
	'cpu_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.3',
	'cpu_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.4',
	'cpu_status'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.5',
	'cpu_type'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.7',
	'cpu_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.8',
	'cpu_state'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.9',
	'cpu_family'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.10',
	'cpu_max_speed'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.11',
	'cpu_current_speed'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.12',
	'cpu_external_clock_speed'		=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.13',
	'cpu_voltage'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.14',
	'cpu_version_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.16',
	'cpu_core_count'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.17',
	'cpu_core_enabled_count'		=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.18',
	'cpu_thread_count'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.19',
	'cpu_brand_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.23',
	# RAM 
	'mib_table_ram'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50', 	# RAM tree
	'ram_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.1',
	'ram_index'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.2',
	'ram_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.3',
	'ram_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.4',
	'ram_status'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.5',
	'ram_type'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.7',
	'ram_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.8',
	'ram_bank_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.10',
	'ram_size'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.14',
	'ram_speed'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.15',
	'ram_manufacturer_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.21',
	'ram_part_number_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.22',
	'ram_serial_number_name'		=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.23',
	'ram_fqdd'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.26',
	# voltage probes 
	'mib_table_voltage'			=> '1.3.6.1.4.1.674.10892.5.4.600.20',		# Voltages tree
	'vp_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.1',
	'vp_index'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.2',
	'vp_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.3',
	'vp_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.4',
	'vp_status'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.5',
	'vp_reading'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.6',
	'vp_type'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.7',
	'vp_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.8',
	'vp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.9',
	'vp_upper_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.10',
	'vp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.11',
	'vp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.12',
	'vp_lower_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.13',
	'vp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.14',
	'vp_probe_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.15',
	'vp_discrete_reading'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.16',
	# intrusion 
	'mib_table_intrusion'			=> '1.3.6.1.4.1.674.10892.5.4.300.70',		# Intrusion tree
	'int_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.1',
	'int_index'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.2',
	'int_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.3',
	'int_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.4',
	'int_status'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.5',
	'int_reading'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.6',
	'int_type'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.7',
	'int_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.8',
	# temperature probe devices  
	'mib_table_temperature'			=> '1.3.6.1.4.1.674.10892.5.4.700.20',		# Temperature tree
	'tp_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.1',
	'tp_index'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.2',
	'tp_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.3',
	'tp_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.4',
	'tp_status'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.5',
	'tp_reading'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.6',
	'tp_type'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.7',
	'tp_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.8',
	'tp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.9',
	'tp_upper_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.10',
	'tp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.11',
	'tp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.12',
	'tp_lower_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.13',
	'tp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.14',
	'tp_probe_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.15',
	'tp_discrete_reading'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.16',
	# cooling unit devices  
	'mib_table_cu'				=> '1.3.6.1.4.1.674.10892.5.4.700.10',		# Cooling units tree
	'cu_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.1',
	'cu_index'				=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.2',
	'cu_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.3',
	'cu_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.4',
	'cu_redundancy_status'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.5',
	'cu_device_count_for_redundancy'	=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.6',
	'cu_name'				=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.7',
	'cu_status'				=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.8',
	# cooling devices  
	'mib_table_cd'				=> '1.3.6.1.4.1.674.10892.5.4.700.12',		# Cooling devices tree
	'cd_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.1',
	'cd_index'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.2',
	'cd_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.3',
	'cd_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.4',
	'cd_status'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.5',
	'cd_reading'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.6',
	'cd_type'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.7',
	'cd_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.8',
	'cd_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.9',
	'cd_upper_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.10',
	'cd_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.11',
	'cd_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.12',
	'cd_lower_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.13',
	'cd_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.14',
	'cd_cooling_utit_index_reference'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.15',
	'cd_sub_type'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.16',
	'cd_probe_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.17',
	'cd_discrete_reading'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.18',
	'cd_fqdd'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.19',
	# chassis  
	'mib_table_chassis'			=> '1.3.6.1.4.1.674.10892.5.4.300.10',		# Chassis tree
	'chassis_index'				=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.1',
	'chassis_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.2',
	'chassis_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.3',
	'chassis_status'			=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.4',
	'chassis_type'				=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.6',
	'chassis_name'				=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.7',
	'chassis_manufacturer_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.8',
	'chassis_model_type_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.9',
	'chassis_asset_tag_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.10',
	'chassis_service_tag_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.11',
	'chassis_system_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.15',
	# firmware  
	'mib_table_firmware'			=> '1.3.6.1.4.1.674.10892.5.4.300.60',		# Firmware tree
	'firmware_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.1',
	'firmware_index'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.2',
	'firmware_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.3',
	'firmware_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.4',
	'firmware_status'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.5',
	'firmware_size'				=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.6',
	'firmware_type'				=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.7',
	'firmware_type_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.8',
	'firmware_update_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.9',
	'firmware_version_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.11',
	# system BIOS 
	'mib_table_bios'			=> '1.3.6.1.4.1.674.10892.5.4.300.50',		# BIOS tree
	'bios_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.1',
	'bios_index'				=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.2',
	'bios_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.3',
	'bios_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.4',
	'bios_status'				=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.5',
	'bios_release_date_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.7',
	'bios_version_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.8',
	'bios_manufacturer_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.11',
	# battery
	'mib_table_battery'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15',	# Battery tree
	'battery_number'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.1',
	'battery_state'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.4',
	'battery_component_status'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.6',
	'battery_predicted_capacity'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.10',
	'battery_fqdd'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.20',
	'battery_display_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.21',
	# system battery 
	'mib_table_sysbattery'			=> '1.3.6.1.4.1.674.10892.5.4.600.50',		# System battery tree
	'sysbattery_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.1',
	'sysbattery_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.2',
	'sysbattery_state_capabilitis'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.3',
	'sysbattery_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.4',
	'sysbattery_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.5',
	'sysbattery_reading'			=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.6',
	'sysbattery_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.7',
	# physical disk
	'mib_table_physical_disk'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130',	# Physical disk tree
	'physical_disk_number'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.1',	
	'physical_disk_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.2',
	'physical_disk_size'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.11',
	'physical_disk_state'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.4',
	'physical_disk_spare_state'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.22',
	'physical_disk_media_type'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.35',
	'physical_disk_display_name'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.55',
	'physical_disk_manufacturer'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.3',
	'physical_disk_operational_state'	=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.50',
	# controller
	'mib_table_controller'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130',	# Controller tree
	'ctrl_number'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.1',
	'ctrl_name'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.2',
	'ctrl_fw_version'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.8',
	'ctrl_cache_size_in_mb'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.9',
	'ctrl_roll_up_status'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.37',
	'ctrl_component_status'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.38',
	'ctrl_driver_version'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.41',
	'ctrl_fqdd'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.78',
	'ctrl_display_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.79',
	# virtual disk
	'mib_table_virtual_disk'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.140',	# Virtual disk tree
	'virtual_disk_table_entry'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1',
	'virtual_disk_number'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.1',
	'virtual_disk_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.2',
	'virtual_disk_display_name'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.36',
	'virtual_disk_state'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.4',
	'virtual_disk_operational_state'	=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.30',
	'virtual_disk_size'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.6',
	'virtual_disk_layout'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.13',
	'virtual_disk_remaining_redundancy'	=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.34',
	# power
	'mib_table_idrac_system'		=> '1.3.6.1.4.1.674.10892.5.4.200.10',		# System tree
	'power_supply_state_details'		=> '1.3.6.1.4.1.674.10892.5.4.200.10.1.8.1',
	# amperage
	'mib_table_amperage'			=> '1.3.6.1.4.1.674.10892.5.4.600.30',		# Amparage tree
	'amperage_probe_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.1',
	'amperage_probe_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.2',
	'amperage_probe_state_capabilities'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.3',
	'amperage_probe_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.4',
	'amperage_probe_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.5',
	'amperage_probe_reading'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.6',
	'amperage_probe_type'			=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.7',
	'amperage_probe_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.8',
	'amperage_probe_upper_nonrecoverable_treshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.9',
	'amperage_probe_upper_critical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.10',
	'amperage_probe_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.11',
	'amperage_probe_lower_critical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.12',
	'amperage_probe_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.13',
	'amperage_probe_lower_nonrecoverable_treshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.14',
	'amperage_probe_probe_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.15',
	'amperage_probe_discrete_reading'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.16',
	# power unit
	'mib_table_power_unit'			=> '1.3.6.1.4.1.674.10892.5.4.600.10',		# Power unit tree
	'power_unit_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.1',
	'power_unit_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.2',
	'power_unit_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.3',
	'power_unit_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.4',
	'power_unit_redundancy_status'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.5',
	'power_supply_count_for_redundancy'	=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.6',
	'power_unit_name'			=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.7',
	'power_unit_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.8',
	# power supply
	'mib_table_power_supply'		=> '1.3.6.1.4.1.674.10892.5.4.600.12',		# Power supply tree
	'power_supply_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.1',
	'power_supply_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.2',
	'power_supply_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.5',
	'power_supply_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.8',
	'power_supply_output_watts'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.6',
	'power_supply_input_voltage'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.9',
	'power_supply_type'			=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.7',
	'power_supply_sensor_state'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.11',
);
# end of default values
############################################################################################################
sub check_md_storage
{
	my ($expire, $system_name, $snmp_session) = @_;

	#print STDERR "Checking system_name=[$system_name] ...\n";
	my $s = check_snmp_system($expire, $snmp_session, \%uptimes);
	if ($s eq '') {
		return '';
	}
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $serviceid = get_serviceid($check_name);
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file, ForceArray => ['record']);
my $xml_str = '';
my $snmp_session;
#print STDERR Dumper($data);
if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
for my $k (keys %oids) {
	$oids{$k} = '.'.$oids{$k};
}
my $module_conf_file;
foreach my $h (@{$data->{'record'}}) {
	if ($h->{'isactive'} eq 'f' ) {
		next;
	}
	$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	if ($snmp_session == 0) {
		next;
	}
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str .= check_md_storage($expire, $h->{'system_name'}, $snmp_session); 
	$snmp_session->close();
}
unlock_check($check_name);
print $xml_str;
