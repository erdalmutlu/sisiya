#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use SNMP;
#use Data::Dumper;

my $check_name = 'switch';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/switch_system_$system_name.pl
# end of default values
############################################################################################################
sub check_switch_temperature
{
	return '';
}

sub check_switch_fans
{
	return '';
}

sub check_switch_load
{
	return '';
}

sub check_switch_process_count
{
	return '';
}

sub check_switch_ram
{
	return '';
}

sub check_switch
{
	my ($expire, $system_name, $snmp_session) = @_;

	#print STDERR "Checking system_name=[$system_name] ...\n";
	my $s = check_snmp_system2($expire, $snmp_session);
	if ($s eq '') {
		return '';
	}
	#$s .= check_switch_temperature($expire, $hostname, $snmp_version, $community, $username, $password);
	#$s .= check_switch_fans($expire, $hostname, $snmp_version, $community, $username, $password);
	#$s .= check_switch_load($expire, $hostname, $snmp_version, $community, $username, $password);
	#$s .= check_switch_process_count($expire, $hostname, $snmp_version, $community, $username, $password);
	#$s .= check_switch_ram($expire, $hostname, $snmp_version, $community, $username, $password);

	if ($s eq '') {
		return '';
	}
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
my $snmp_session;
#print STDERR Dumper($data);
if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
if( ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		if ($h->{'isactive'} eq 'f' ) {
			next;
		}
		#print STDERR "Checking $h->{'hostname'} version: $h->{'snmp_version'} ...\n";
		$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
		if (!$snmp_session || $snmp_session == 0) {
			next;
		}
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		$xml_str .= check_switch($expire, $h->{'system_name'}, $snmp_session);
	}
}
else {
	if ($data->{'record'}->{'isactive'} eq 't' ) {
		$snmp_session = snmp_init($data->{'record'}->{'hostname'}, $data->{'record'}->{'snmp_version'}, $data->{'record'}->{'community'},
					$data->{'record'}->{'username'}, $data->{'record'}->{'password'});	
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$data->{'record'}->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		if ($snmp_session != 0) {
			$xml_str = check_switch($expire, $data->{'record'}->{'system_name'}, $snmp_session);
		}
	}
}
unlock_check($check_name);
print $xml_str;
