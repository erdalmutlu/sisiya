#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use SNMP;

my $check_name = 'printer';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
# in minutes
our %uptimes = ('error' => 10, 'warning' => 15);
our %oids = (
	# printer
	'mib_table_printer' 	=> '1.3.6.1.2.1.25.3.5',	# Printer tree 
	'printer_status'	=> '1.3.6.1.2.1.25.3.5.1.1.1',	# HOST-RESOURCES-MIB::hrPrinterStatus 
	'printer_state' 	=> '1.3.6.1.2.1.25.3.5.1.2.1',	# HOST-RESOURCES-MIB::hrPrinterDetectedERRORState 
	# device
	'mib_table_device' 	=> '1.3.6.1.2.1.25.3.2',	# Device tree 
	'device_status' 	=> '1.3.6.1.2.1.25.3.2.1.5.1',	# HOST-RESOURCES-MIB::hrDeviceStatus 
);
our @pages = (
	{'name' => 'engine',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.5.0'},
	{'name' => 'duplex',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.22'},
	{'name' => 'pcl', 		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.3.3.3.5'},
	{'name' => 'postscript',	'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.3.3.4.5'},
	{'name' => 'color',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.7.0'},
	{'name' => 'mono',		'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.6.0'},
	{'name' => 'pagecount',		'oid' => '1.3.6.1.2.1.43.10.2.1.4.1.1'}
);
# push @pages, {'name' => 'engine',            'oid' => '1.3.6.1.4.1.11.2.3.9.4.2.1.4.1.2.5.0'}; 
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/printer_system_$system_name.pl
# end of default values
############################################################################################################
my @printer_error_states = ( 'paper low', 'no paper', 'toner low', 'no toner', 'door open', 'jammed', 'offline', 'service needed' );
my @printer_statuses = ( 'other', 'unknown', 'idle', 'printing', 'warmup');

sub decode_error_state_bitstring 
{
	my @bits = split(//, unpack('B8', shift));
	
	if (scalar(@bits) != 8) {
		return '';
	}
	for (my $i = 0; $i < 8; ++$i) {
		if ($bits[$i] == 1) {
			return $printer_error_states[$i];
		}
	}
	return '';
}

sub decode_error_state 
{
	my $s = shift;

	my $str = decode_error_state_bitstring($s);	
	if (length($s) == 2) {
		my $s2 = decode_error_state_bitstring(substr($s, 1,1));
		if ($s2 ne '') {
			if ($str eq '') {
				$str = $s2;
			} else {
				$str .= ','.$s2;
			}
		}
	}
	return $str;
}

sub check_printer_device
{
	my ($expire, $snmp_session) = @_;
	
	my @oid_list = ($oids{'device_status'}, $oids{'printer_status'}, $oids{'printer_state'});
	my @vbs;
	foreach my $oid ( @oid_list ) {
		push @vbs, new SNMP::Varbind([$oid]);
	}
	my $vl = new SNMP::VarList(@vbs);
	my @a = $snmp_session->get($vl);
	if ($snmp_session->{ErrorNum}) {
		return '';
	}
	my $device_status  = $a[0];
	my $printer_status = $a[1];
	my $printer_state  = $a[2];

	if ($device_status eq 'NOSUCHINSTANCE') {
		return '';
	}

	my $serviceid = get_serviceid('printer');
	my $statusid = $SisIYA_Config::statusids{'ok'};
	my $s = '';
	#print STDERR "device_status=[$device_status] printer_status=[$printer_status] printer_state=[$printer_state] length=".length($printer_state)."\n";
	if ($device_status == 1) { # unknown
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = 'WARNING: Device status is unknown!';
	} elsif ($device_status == 2) { # running
		$s = 'OK: Device status is '.$printer_statuses[$printer_status].'.';
	} elsif ($device_status == 3) { # warning
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: ".decode_error_state($printer_state).'!';
		$s .= ' Device status is '.$printer_statuses[$printer_status].'.';
	} elsif ($device_status == 4) { # testing
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: The device is testing! device_status=[$device_status] printer_status=[$printer_status] printer_state=[$printer_state]";
	} elsif ($device_status == 5) { # down
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: ".decode_error_state($printer_state).'!';
		$s .= ' Device status is '.$printer_statuses[$printer_status].'.';
	} else {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: Undetermined device status! device_status=[$device_status] printer_status=[$printer_status] printer_state=[$printer_state]";
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_printer_page_counts
{
	my ($expire, $snmp_session) = @_;

	if ($#pages == -1) {
		return '';
	}

	my $serviceid = get_serviceid('printer_pagecounts');
	my $statusid = $SisIYA_Config::statusids{'info'};
	my ($vb, $n);
	my $s = '';

	for my $i (0..$#pages) {
		$vb = new SNMP::Varbind([$pages[$i]{'oid'}]);
		$n = $snmp_session->get($vb);
		if ($snmp_session->{ErrorNum}) {
			next;
		}
		if ($n eq 'NOSUCHOBJECT' || $n eq 'NOSUCHISTANCE') {
			next;
		}

		#$n = get_snmp_value('-OvQe', $hostname, $snmp_version, $community, $pages[$i]{'oid'});
		$s .= "INFO: Total number of $pages[$i]{'name'} pages is $n."; 
	}
	if ($s eq '') {
		return '';
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_printer
{
	my ($expire, $system_name, $snmp_session) = @_;

	my $s = check_snmp_system2($expire, $snmp_session);
	if ($s eq '') {
		return '';
	}
	$s .= check_printer_device($expire, $snmp_session);
	$s .= check_printer_page_counts($expire, $snmp_session);

	if ($s eq '') {
		return '';
	}
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
if( ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		if ($h->{'isactive'} eq 'f' ) {
			next;
		}
		#print STDERR "Checking $h->{'hostname'} version: $h->{'snmp_version'} ...\n";
		$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
		if ($snmp_session == 0) {
			next;
		}
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		$xml_str .= check_printer($expire, $h->{'system_name'}, $snmp_session);
	}
}
else {
	if ($data->{'record'}->{'isactive'} eq 't' ) {
		#print STDERR "Checking $data->{'record'}->{'hostname'} version: $data->{'record'}->{'snmp_version'} ...\n";
		$snmp_session = snmp_init($data->{'record'}->{'hostname'}, $data->{'record'}->{'snmp_version'}, $data->{'record'}->{'community'},
					$data->{'record'}->{'username'}, $data->{'record'}->{'password'});	
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$data->{'record'}->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		if ($snmp_session != 0) {
			$xml_str = check_printer($expire, $data->{'record'}->{'system_name'}, $snmp_session);
		}
	}
}
unlock_check($check_name);
print $xml_str;
