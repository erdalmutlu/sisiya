#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
#use Data::Dumper;

my $check_name = 'imm';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/imm_system_$system_name.pl
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %fan_percentages = ('error' => 90, 'warning' => 85);
our %oids = (
	# system
	'mib_table_system'			=> '1.3.6.1.2.1.1',			# SNMP MIB-2 System tree
	'system_description'			=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'				=> '1.3.6.1.2.1.1.3.0',
	'system_contact'			=> '1.3.6.1.2.1.1.4.0',
	'system_name'				=> '1.3.6.1.2.1.1.5.0',
	'system_location'			=> '1.3.6.1.2.1.1.6.0',
	# temperature probe devices  
	'mib_table_temperature'			=> '1.3.6.1.4.1.2.3.51.3.1.1.2',		# Temperature tree
	'tp_index'				=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.1',
	'tp_name'				=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.2',
	'tp_reading'				=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.3',
	'tp_nominal_reading'			=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.4',
	'tp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.5',
	'tp_upper_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.6',
	'tp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.7',
	'tp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.8',
	'tp_lower_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.9',
	'tp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.10',
	# fan devices  
	'mib_table_fans'			=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1',		# Fans tree
	'fan_index'				=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.1',
	'fan_name'				=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.2',
	'fan_speed'				=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.3',
	'fan_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.4',
	'fan_upper_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.5',
	'fan_upper_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.6',
	'fan_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.7',
	'fan_lower_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.8',
	'fan_lower_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.9',
	# voltages probe devices  
	'mib_table_voltage'			=> '1.3.6.1.4.1.2.3.51.3.1.2.2',		# Voltages tree
	'vp_index'				=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.1',
	'vp_name'				=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.2',
	'vp_reading'				=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.3',
	'vp_nominal_reading'			=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.4',
	'vp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.5',
	'vp_upper_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.6',
	'vp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.7',
	'vp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.8',
	'vp_lower_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.9',
	'vp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.10',
);

# end of default values
############################################################################################################
sub check_system
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $serviceid = get_serviceid('system');
	my $statusid = $SisIYA_Config::statusids{'ok'};
	#my $sys_description = $h{$oids{'system_description'}};
	#my $sys_location = $h{$oids{'system_location'}};
	#my $sys_name = $h{$oids{'system_name'}};
	my $s = $h{$oids{'system_uptime'}};
	my @a = split(/:/, $s);
	my $up_in_minutes = $a[0] * 1440  + $a[1] * 60 + $a[2];
	$s = check_uptime(\$statusid, $up_in_minutes, $uptimes{'warning'}, $uptimes{'error'});
	$s = "$s Description: $h{$oids{'system_description'}} Location: $h{$oids{'system_location'}} Name: $h{$oids{'system_name'}}";
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_temperature
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('temperature');
	my $statusid;

	my ($name_str, $index, $current_reading, $lower_critical_reading);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	# find out how many temperature probe devices are there
	my $n = count_keys(\%h, $oids{'tp_index'});
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h{$oids{'tp_index'}.'.'.$i};
		$name_str = $h{$oids{'tp_name'}.'.'.$index};
		$name_str =~ s/"//g;


		$current_reading = $h{$oids{'tp_reading'}.'.'.$index};
		$upper_critical_reading = $h{$oids{'tp_upper_critical_threshold'}.'.'.$index};
		$upper_noncritical_reading = $h{$oids{'tp_upper_noncritical_threshold'}.'.'.$index};
		$lower_critical_reading = $h{$oids{'tp_lower_critical_threshold'}.'.'.$index};
		$lower_noncritical_reading = $h{$oids{'tp_lower_noncritical_threshold'}.'.'.$index};

		if ($current_reading >= $upper_critical_reading) {
			$error_str .= "ERROR: Temperature of $name_str is $current_reading (>= $upper_critical_reading) Celcius!";
		} elsif ($current_reading >= $upper_noncritical_reading) {
			$warning_str .= "WARNING: Temperature of $name_str is $current_reading (>= $upper_noncritical_reading) Celcius!";
		} elsif ($current_reading <= $lower_critical_reading) {
			$error_str .= "ERROR: Temperature of $name_str is $current_reading (<= $lower_critical_reading) Celcius!";
		} elsif ($current_reading <= $lower_noncritical_reading) {
			$warning_str .= "WARNING: Temperature of $name_str is $current_reading (<= $lower_noncritical_reading) Celcius!";
		} else {
			$ok_str .= "OK: Temperature of $name_str is $current_reading Celcius.";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_fans
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('fanspeed');
       	my $statusid = $SisIYA_Config::statusids{'ok'};

	my ($name_str, $index, $current_reading, $lower_critical_reading);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	# find out how many temperature probe devices are there
	my $n = count_keys(\%h, $oids{'fan_index'});
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h{$oids{'fan_index'}.'.'.$i};
		$name_str = $h{$oids{'fan_name'}.'.'.$index};
		$name_str =~ s/"//g;

		$current_reading = $h{$oids{'fan_speed'}.'.'.$index};
		$current_reading =~ s/ %//;
		$current_reading =~ s/"//g;

		if ($current_reading >= $fan_percentages{'error'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$error_str .= "ERROR: Fan of $name_str is $current_reading %(>= $fan_percentages{'error'})!";
		} elsif ($current_reading >= $fan_percentages{'warning'}) {
			$statusid = $SisIYA_Config::statusids{'warning'};
			$warning_str .= "WARNING: Fan of $name_str is $current_reading %(>= $fan_percentages{'warning'})!";
		} else {
			$ok_str .= "OK: Fan of $name_str is $current_reading %.";
		}
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_voltages
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('voltages');
	my $statusid;

	my ($name_str, $index, $current_reading, $lower_critical_reading);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	my $divide_factor = 1000;
	# find out how many voltage probe devices are there
	my $n = count_keys(\%h, $oids{'vp_index'});
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h{$oids{'vp_index'}.'.'.$i};
		$name_str = $h{$oids{'vp_name'}.'.'.$index};
		$name_str =~ s/"//g;


		$current_reading = $h{$oids{'vp_reading'}.'.'.$index} / $divide_factor;
		$upper_critical_reading = $h{$oids{'vp_upper_critical_threshold'}.'.'.$index} / $divide_factor;
		$upper_noncritical_reading = $h{$oids{'vp_upper_noncritical_threshold'}.'.'.$index} / $divide_factor;
		$lower_critical_reading = $h{$oids{'vp_lower_critical_threshold'}.'.'.$index} / $divide_factor;
		$lower_noncritical_reading = $h{$oids{'vp_lower_noncritical_threshold'}.'.'.$index} / $divide_factor;

		if ($upper_critical_reading != 0 && $current_reading >= $upper_critical_reading) {
			$error_str .= "ERROR: Voltage of $name_str is $current_reading (>= $upper_critical_reading) Volts!";
		} elsif ($upper_noncritical_reading != 0 && $current_reading >= $upper_noncritical_reading) {
			$warning_str .= "WARNING: Voltage of $name_str is $current_reading (>= $upper_noncritical_reading) Volts!";
		} elsif ($lower_critical_reading != 0 && $current_reading <= $lower_critical_reading) {
			$error_str .= "ERROR: Voltage of $name_str is $current_reading (<= $lower_critical_reading) Volts!";
		} elsif ($lower_noncritical_reading != 0 && $current_reading <= $lower_noncritical_reading) {
			$warning_str .= "WARNING: Voltage of $name_str is $current_reading (<= $lower_noncritical_reading) Volts!";
		} else {
			$ok_str .= "OK: Voltage of $name_str is $current_reading Volts.";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_imm
{
	my ($isactive, $expire, $system_name, $hostname, $snmp_version, $community, $username, $password) = @_;

	if ($isactive eq 'f' ) {
		return '';
	}

	#print STDERR "Checking system_name=[$system_name] hostname=[$hostname] isactive=[$isactive] snmp_version=[$snmp_version] community=[$community] username=[$username] password=[$password]...\n";
	my $s = '';

	# get the all in an array
	#my $mib = substr $oids{'idrac_mib'}, 1, length($oids{'idrac_mib'}) - 1;
	#my @a = `$SisIYA_Remote_Config::external_progs{'snmpwalk'} -OnQ -v $snmp_version -c $community $hostname $mib 2>&1`;
	#if ($?) {
	#	$s = "<message><serviceid>".get_serviceid('system')."</serviceid><statusid>$SisIYA_Config::statusids{'error'}</statusid><expire>$expire</expire><data><msg>Could execute snmpwalk! $a[0]</msg><datamsg></datamsg></data></message>";
	#	return "<system><name>$system_name</name>$s</system>";
	#}
	my (@a, @b, $mib);
	my @mib_table_keys = grep {/mib_table_/} keys %oids;
	for (my $i = 0; $i < @mib_table_keys; $i++) {
		$mib = substr $oids{$mib_table_keys[$i]}, 1, length($oids{$mib_table_keys[$i]}) - 1;
		#print STDERR "Getting $hostname $mib ...\n";
		@b = `$SisIYA_Remote_Config::external_progs{'snmpwalk'} -OnQ -v $snmp_version -c $community $hostname $mib 2>&1`;
		if ($?) {
			$s = "<message><serviceid>".get_serviceid('system')."</serviceid><statusid>$SisIYA_Config::statusids{'error'}</statusid><expire>$expire</expire><data><msg>Could execute snmpwalk! $b[0]</msg><datamsg></datamsg></data></message>";
			return "<system><name>$system_name</name>$s</system>";
		}
		@a = (@a, @b);
	}
	my %h = extract_keys(\@a);
	# print_keys(\%h);
	$s .= check_system($expire, $system_name, $hostname, \%h);
	$s .= check_fans($expire, $system_name, $hostname, \%h);
	$s .= check_temperature($expire, $system_name, $hostname, \%h);
	$s .= check_voltages($expire, $system_name, $hostname, \%h);
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
#print STDERR Dumper($data);
if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
for my $k (keys %oids) {
	$oids{$k} = '.'.$oids{$k};
}
my $module_conf_file;
if( ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		$xml_str .= check_imm($h->{'isactive'}, $expire, $h->{'system_name'}, $h->{'hostname'}, 
					$h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	}
}
else {
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$data->{'record'}->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str = check_imm($data->{'record'}->{'isactive'}, $expire, $data->{'record'}->{'system_name'}, 
				$data->{'record'}->{'hostname'}, $data->{'record'}->{'snmp_version'}, $data->{'record'}->{'community'},
				$data->{'record'}->{'username'}, $data->{'record'}->{'password'});
}
unlock_check($check_name);
print $xml_str;
