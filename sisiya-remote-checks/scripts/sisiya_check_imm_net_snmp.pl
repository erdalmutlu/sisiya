#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
#use Data::Dumper;
use Net::SNMP;

my $check_name = 'imm';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/imm_system_$system_name.pl
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %fan_percentages = ('error' => 90, 'warning' => 85);
our %oid_tables = (
#	#'disk'		=> '1.3.6.1.4.1.2.3.51.3.1.12',		# disks table
	'temperature'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2',	# temperature probes table
	'fan'		=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1',	# fans table
	#time out problem	'memory'	=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1',	# memory table 
#	#'power'		=> '1.3.6.1.4.1.2.3.51.3.1.11',		# power table	
#	#'storage'	=> '1.3.6.1.4.1.2.3.51.3.1.13',		# local storage tree
#	#'system_health'	=> '1.3.6.1.4.1.2.3.51.3.1.4',		# system health table
	'voltage'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2'		# voltages table
);
our %oids = (
	# system
	'system_description'			=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'				=> '1.3.6.1.2.1.1.3.0',
	'system_contact'			=> '1.3.6.1.2.1.1.4.0',
	'system_name'				=> '1.3.6.1.2.1.1.5.0',
	'system_location'			=> '1.3.6.1.2.1.1.6.0',
	# temperature probe devices  
	'tp_index'				=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.1',
	'tp_name'				=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.2',
	'tp_reading'				=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.3',
	'tp_nominal_reading'			=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.4',
	'tp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.5',
	'tp_upper_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.6',
	'tp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.7',
	'tp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.8',
	'tp_lower_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.9',
	'tp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.1.2.1.10',
	# fan devices  
	'fan_index'				=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.1',
	'fan_name'				=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.2',
	'fan_speed'				=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.3',
	'fan_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.4',
	'fan_upper_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.5',
	'fan_upper_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.6',
	'fan_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.7',
	'fan_lower_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.8',
	'fan_lower_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.3.2.1.9',
	# RAM 
	'memory_index'				=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.1',
	'memory_rated_speed'			=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.10',
	'memory_description'			=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.2',
	'memory_part_number'			=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.3',
	'memory_fru_serial_number'		=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.4',
	'memory_manufacture_date'		=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.5',
	'memory_type'				=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.6',
	'memory_size'				=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.7',
	'memory_health_status'			=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.8',
	'memory_config_speed'			=> '1.3.6.1.4.1.2.3.51.3.1.5.21.1.9',
	# power supply
	#'power_number'				=> '1.3.6.1.4.1.2.3.51.3.1.11.1',
	#'power_index'				=> '1.3.6.1.4.1.2.3.51.3.1.11.2.1.1',
	# voltages probe devices  
	'vp_index'				=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.1',
	'vp_name'				=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.2',
	'vp_reading'				=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.3',
	'vp_nominal_reading'			=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.4',
	'vp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.5',
	'vp_upper_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.6',
	'vp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.7',
	'vp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.8',
	'vp_lower_critical_threshold'		=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.9',
	'vp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.2.3.51.3.1.2.2.1.10'
);

# end of default values
############################################################################################################
sub check_ram
{
	my ($expire, $h_ref) = @_;

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('ram');
	my $statusid;
	my ($ram_name, $ram_status, $ram_size, $ram_type, $ram_speed, $chassis_index, $index);
	my $divide_factor = 1048576;

	# find out how many RAMs are there
	my $n = count_keys($h_ref, $oids{'memory_index'});
#print STDERR "check_ram: total number RAMs: $n\n";
	for (my $i = 1; $i <= $n; $i++) {
		$index = $i;
		$ram_status = $h_ref->{$oids{'memory_health_status'}.'.'.$index};
		$ram_name = $h_ref->{$oids{'memory_description'}.'.'.$index};
		$ram_name =~ s/"//g;
		$ram_size = $h_ref->{$oids{'memory_size'}.'.'.$index} / $divide_factor;
		# $ram_type = get_type_name($h_ref->{$oids{'memeory_type'}.'.'.$index});
		$ram_type = $h_ref->{$oids{'memeory_type'}.'.'.$index};
		$ram_speed = $h_ref->{$oids{'memory_config_speed'}.'.'.$index};

		$ram_name = $ram_name." (Size: $ram_size GB Type: $ram_type Speed: $ram_speed MHz)";
		if ($ram_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Memory status of $ram_name on chassis $chassis_index is OK.";
		} else {
			if ($ram_status == 5) { # ObjectStatusEnum: critical
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is critical!";
			} elsif ($ram_status == 6) { # ObjectStatusEnum: nonRecoverable
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is not recoverable!";
			} elsif ($ram_status == 4) { # ObjectStatusEnum: critical
				$warning_str .= "WARNING: Memory status of $ram_name on chassis $chassis_index is not critical!";
			} elsif ($ram_status == 1) { # ObjectStatusEnum: other
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is other!";
			} else {
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is unknown!";
			}
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_temperature
{
	my ($expire, $h_ref) = @_; 

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('temperature');
	my $statusid;

	my ($name_str, $index, $current_reading, $lower_critical_reading);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	# find out how many temperature probe devices are there
	my $n = count_keys($h_ref, $oids{'tp_index'});
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h_ref->{$oids{'tp_index'}.'.'.$i};
		$name_str = $h_ref->{$oids{'tp_name'}.'.'.$index};
		$name_str =~ s/"//g;

		$current_reading = $h_ref->{$oids{'tp_reading'}.'.'.$index};
		$upper_critical_reading = $h_ref->{$oids{'tp_upper_critical_threshold'}.'.'.$index};
		$upper_noncritical_reading = $h_ref->{$oids{'tp_upper_noncritical_threshold'}.'.'.$index};
		$lower_critical_reading = $h_ref->{$oids{'tp_lower_critical_threshold'}.'.'.$index};
		$lower_noncritical_reading = $h_ref->{$oids{'tp_lower_noncritical_threshold'}.'.'.$index};

		if ($current_reading >= $upper_critical_reading) {
			$error_str .= "ERROR: Temperature of $name_str is $current_reading (>= $upper_critical_reading) Celcius!";
		} elsif ($current_reading >= $upper_noncritical_reading) {
			$warning_str .= "WARNING: Temperature of $name_str is $current_reading (>= $upper_noncritical_reading) Celcius!";
		} elsif ($current_reading <= $lower_critical_reading) {
			$error_str .= "ERROR: Temperature of $name_str is $current_reading (<= $lower_critical_reading) Celcius!";
		} elsif ($current_reading <= $lower_noncritical_reading) {
			$warning_str .= "WARNING: Temperature of $name_str is $current_reading (<= $lower_noncritical_reading) Celcius!";
		} else {
			$ok_str .= "OK: Temperature of $name_str is $current_reading Celcius.";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_fans
{
	my ($expire, $h_ref) = @_; 

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('fanspeed');
       	my $statusid = $SisIYA_Config::statusids{'ok'};

	my ($name_str, $index, $current_reading, $lower_critical_reading);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	# find out how many temperature probe devices are there
	my $n = count_keys($h_ref, $oids{'fan_index'});
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h_ref->{$oids{'fan_index'}.'.'.$i};
		$name_str = $h_ref->{$oids{'fan_name'}.'.'.$index};
		$name_str =~ s/"//g;

		$current_reading = $h_ref->{$oids{'fan_speed'}.'.'.$index};
		$current_reading =~ s/ %//;
		$current_reading =~ s/"//g;

		if ($current_reading >= $fan_percentages{'error'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$error_str .= "ERROR: Fan of $name_str is $current_reading %(>= $fan_percentages{'error'})!";
		} elsif ($current_reading >= $fan_percentages{'warning'}) {
			$statusid = $SisIYA_Config::statusids{'warning'};
			$warning_str .= "WARNING: Fan of $name_str is $current_reading %(>= $fan_percentages{'warning'})!";
		} else {
			$ok_str .= "OK: Fan of $name_str is $current_reading %.";
		}
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_voltages
{
	my ($expire, $h_ref) = @_; 

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('voltages');
	my $statusid;

	my ($name_str, $index, $current_reading, $lower_critical_reading);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	my $divide_factor = 1000;
	# find out how many voltage probe devices are there
	my $n = count_keys($h_ref, $oids{'vp_index'});
	for (my $i = 1; $i <= $n; $i++) {
		$index = $h_ref->{$oids{'vp_index'}.'.'.$i};
		$name_str = $h_ref->{$oids{'vp_name'}.'.'.$index};
		$name_str =~ s/"//g;


		$current_reading = $h_ref->{$oids{'vp_reading'}.'.'.$index} / $divide_factor;
		$upper_critical_reading = $h_ref->{$oids{'vp_upper_critical_threshold'}.'.'.$index} / $divide_factor;
		$upper_noncritical_reading = $h_ref->{$oids{'vp_upper_noncritical_threshold'}.'.'.$index} / $divide_factor;
		$lower_critical_reading = $h_ref->{$oids{'vp_lower_critical_threshold'}.'.'.$index} / $divide_factor;
		$lower_noncritical_reading = $h_ref->{$oids{'vp_lower_noncritical_threshold'}.'.'.$index} / $divide_factor;

		if ($upper_critical_reading != 0 && $current_reading >= $upper_critical_reading) {
			$error_str .= "ERROR: Voltage of $name_str is $current_reading (>= $upper_critical_reading) Volts!";
		} elsif ($upper_noncritical_reading != 0 && $current_reading >= $upper_noncritical_reading) {
			$warning_str .= "WARNING: Voltage of $name_str is $current_reading (>= $upper_noncritical_reading) Volts!";
		} elsif ($lower_critical_reading != 0 && $current_reading <= $lower_critical_reading) {
			$error_str .= "ERROR: Voltage of $name_str is $current_reading (<= $lower_critical_reading) Volts!";
		} elsif ($lower_noncritical_reading != 0 && $current_reading <= $lower_noncritical_reading) {
			$warning_str .= "WARNING: Voltage of $name_str is $current_reading (<= $lower_noncritical_reading) Volts!";
		} else {
			$ok_str .= "OK: Voltage of $name_str is $current_reading Volts.";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_imm
{
	my ($expire, $system_name, $snmp_session) = @_;

	# print STDERR "Checking system_name=[$system_name] ...\n";
	my $h_ref = get_snmp_all_oids($snmp_session, \%oid_tables, \%oids);
	my $s = check_system($expire, $h_ref, \%uptimes);
	$s .= check_fans($expire, $h_ref);
	#$s .= check_ram($expire, $h_ref);
	$s .= check_temperature($expire, $h_ref);
	$s .= check_voltages($expire, $h_ref);
	return "<system><name>$system_name</name>$s</system>";
}
########################################################################
my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file, ForceArray => ['record']);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
# ????
#for my $k (keys %oids) {
#	$oids{$k} = '.'.$oids{$k};
#}
my $module_conf_file;
foreach my $h (@{$data->{'record'}}) {
	if ($h->{'isactive'} eq 'f' ) {
		next;
	}
	#print STDERR "Checking $h->{'hostname'} community: $h->{'community'} version: $h->{'snmp_version'} ...\n";
	$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	if ($snmp_session == 0) {
		next;
	}
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str .= check_imm($expire, $h->{'system_name'}, $snmp_session); 
}
unlock_check($check_name);
print $xml_str;
