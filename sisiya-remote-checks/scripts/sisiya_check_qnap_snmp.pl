#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use SNMP;

my $check_name = 'qnap';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}
###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %loads = ('error' => 90, 'warning' => 80);
our %default_temperatures = ( 'warning' => 35, 'error' => 40 );
our %oids = (
	'mib_table_storage'		=> '1.3.6.1.4.1.24681',			# Storage tree
	'mib_table_system_hd'		=> '1.3.6.1.4.1.24681.1.2.11',		# System hd tree
	'hd_number'			=> '1.3.6.1.4.1.24681.1.2.10.0',
	'hd_index'			=> '1.3.6.1.4.1.24681.1.2.11.1.1',
	'hd_descr'			=> '1.3.6.1.4.1.24681.1.2.11.1.2',
	'hd_temperature'		=> '1.3.6.1.4.1.24681.1.2.11.1.3',
	'hd_status'			=> '1.3.6.1.4.1.24681.1.2.11.1.4',
	'hd_model'			=> '1.3.6.1.4.1.24681.1.2.11.1.5',
	'hd_capacity'			=> '1.3.6.1.4.1.24681.1.2.11.1.6',
	'hd_smart_info'			=> '1.3.6.1.4.1.24681.1.2.11.1.7',
	'mib_table_system_volume'	=> '1.3.6.1.4.1.24681.1.2.17',		# Volume tree
	'sys_volume_number'		=> '1.3.6.1.4.1.24681.1.2.16.0',
	'sys_volume_index'		=> '1.3.6.1.4.1.24681.1.2.17.1.1',
	'sys_volume_descr'		=> '1.3.6.1.4.1.24681.1.2.17.1.2',
	'sys_volume_fs'			=> '1.3.6.1.4.1.24681.1.2.17.1.3',
	'sys_volume_total_size'		=> '1.3.6.1.4.1.24681.1.2.17.1.4',
	'sys_volume_free_size'		=> '1.3.6.1.4.1.24681.1.2.17.1.5',
	'sys_volume_status'		=> '1.3.6.1.4.1.24681.1.2.17.1.6',
	'system_temperature'		=> '1.3.6.1.4.1.24681.1.2.6.0',
	'system_load'			=> '1.3.6.1.4.1.24681.1.2.1.0',
);
# end of default values
############################################################################################################
sub check_qnap_load
{
	my ($expire, $snmp_session) = @_;

	my $vb = new SNMP::Varbind([$oids{'system_load'}]);
	my $system_load = $snmp_session->get($vb);
	if ($snmp_session->{ErrorNum}) {
		return '';
	}
	$system_load =~ s/"//g;
	$system_load = trim((split(/%/, $system_load))[0]);
#	if ($system_load == 0) {
#		return '';
#	}
	my $serviceid = get_serviceid('load');
	my $statusid = $SisIYA_Config::statusids{'ok'};
	my $s = '';
	if ($system_load >= $loads{'error'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: System load is $system_load % (>= $loads{'error'})!";
	} elsif ($system_load >= $loads{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: System load is $system_load % (>= $loads{'warning'})!";
	} else {
		$s = "OK: System load is $system_load %.";
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_temperature
{
	my ($expire, $snmp_session) = @_;

	my $vb = new SNMP::Varbind([$oids{'system_temperature'}]);
	my $system_temperature = $snmp_session->get($vb);
	if ($snmp_session->{ErrorNum}) {
		return '';
	}

	$system_temperature =~ s/"//g;
	$system_temperature = trim((split(/C/, $system_temperature))[0]);
	if ($system_temperature == 0) {
		return '';
	}
	my $serviceid = get_serviceid('temperature');
	my $statusid = $SisIYA_Config::statusids{'ok'};
	my $s = '';
	if ($system_temperature >= $default_temperatures{'error'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$s = "ERROR: System temperature is $system_temperature C (>= $default_temperatures{'error'})!";
	} elsif ($system_temperature >= $default_temperatures{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$s = "WARNING: System temperature is $system_temperature C (>= $default_temperatures{'warning'})!";
	} else {
		$s = "OK: System temperature is $system_temperature C.";
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$s</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_raid
{
	my ($expire, $snmp_session) = @_;

	my $vb = new SNMP::Varbind([$oids{'sys_volume_number'}]);
	my $sys_volume_number = $snmp_session->get($vb);
	if ($snmp_session->{ErrorNum} || $sys_volume_number == 0) {
		return '';
	}

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $serviceid = get_serviceid('raid');
	my $statusid = $SisIYA_Config::statusids{'ok'};

	my (@a, $s, $j, $k, $oid, @oid_list);
	my ($sys_volume_fs, $sys_volume_total_size, $sys_volume_free_size, $sys_volume_descr, $sys_volume_status, $index);
	for (my $i = 1; $i <= $sys_volume_number; $i++) {
		$vb = new SNMP::Varbind([$oids{'sys_volume_index'}.'.'.$i]);
		$j = $snmp_session->get($vb);
		if ($snmp_session->{ErrorNum}) {
			next;
		}
		@oid_list = ($oids{'sys_volume_status'}.'.'.$j, $oids{'sys_volume_total_size'}.'.'.$j, $oids{'sys_volume_free_size'}.'.'.$j, 
				$oids{'sys_volume_descr'}.'.'.$j, $oids{'sys_volume_fs'}.'.'.$j);
		my @vbs; # do not decrale this array elswhere, because we push varbinds and it needs to be destroyed for the next run
		foreach $oid ( @oid_list ) {
			push @vbs, new SNMP::Varbind([$oid]);
		}
		$vb = new SNMP::VarList(@vbs);
		@a = $snmp_session->get($vb);
		if ($snmp_session->{ErrorNum} || $a[0] eq 'NOSUCHINSTANCE') {
			next;
		}
		for ($k = 0; $k < @a; $k++) {
			$a[$k] =~ s/"//g;
		}

		#print STDERR "index: $index sys_volume_status=[$a[0]] sys_volume_total_size=[$a[1]] sys_volume_free_size=[$a[2]] sys_volume_descr=[$a[3]] sys_volume_fs=[$a[4]]\n";
		$s = "$a[3] File system: $a[4] Total size: $a[1] Free Size: $a[2]";
		if ($a[0] eq 'Ready') {
			$ok_str .= " OK: Volume $j ($s) is OK.";
		} elsif ($a[0] eq 'Rebuilding...') {
			if ($statusid < $SisIYA_Config::statusids{'warning'}) {
				$statusid = $SisIYA_Config::statusids{'warning'};
			}
			$warning_str .= " WARNING: Volume $j ($s) is rebuilding.";
		} else {
			$statusid = $SisIYA_Config::statusids{'error'};
			$error_str .= " ERROR: Volume $j ($s) has status = $a[0] (!= Ready)!";
		}
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}

sub check_qnap_smart
{
	my ($expire, $snmp_session) = @_;
	
	my $vb = new SNMP::Varbind([$oids{'hd_number'}]);
	my $hd_number = $snmp_session->get($vb);
	if ($snmp_session->{ErrorNum} || $hd_number == 0) {
		return '';
	}

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $serviceid = get_serviceid('smart');
	my (@a, $s, $j, $k, $oid, @oid_list);
	for (my $i = 1; $i <= $hd_number; $i++) {
		
		$vb = new SNMP::Varbind([$oids{'hd_index'}.'.'.$i]);
		$j = $snmp_session->get($vb);
		if ($snmp_session->{ErrorNum}) {
			next;
		}
		@oid_list = ($oids{'hd_status'}.'.'.$j, $oids{'hd_descr'}.'.'.$j,$oids{'hd_model'}.'.'.$j, $oids{'hd_capacity'}.'.'.$j, 
				$oids{'hd_temperature'}.'.'.$j, $oids{'hd_smart_info'}.'.'.$j);
		my @vbs; # do not decrale this array elswhere, because we push varbinds and it needs to be destroyed for the next run
		foreach $oid ( @oid_list ) {
			push @vbs, new SNMP::Varbind([$oid]);
		}
		$vb = new SNMP::VarList(@vbs);
		@a = $snmp_session->get($vb);
		if ($snmp_session->{ErrorNum} || $a[0] eq 'NOSUCHINSTANCE') {
			print STDERR "got ERROR 2\n";
			next;
		}
		for ($k = 0; $k < @a; $k++) {
			$a[$k] =~ s/"//g;
			$a[$k] =~ s/\n//g;
		}

		#print STDERR "index: $j hd_status=[$a[0]] hd_capacity=[$a[3]] hd_model=[$a[2]] hd_descr=[$a[1]] hd_temperature=[$a[4]], SMART=[$a[5]] \n";
		$s = "$a[1] Device Model: $a[2] Capacity: $a[3] Temperature: $a[4] SMART info: $a[5]";
		if ($a[0] == 0) {
			$ok_str .= " OK: Disk $j ($s) is OK.";
		} else {
			$error_str .= " ERROR: Disk $j ($s) has status = $a[0] (!= 0)!";
		}
		$a[4] = trim((split(/C/, $a[4]))[0]);
		if ($a[4] ne '--') {
			if ($a[4] >= $default_temperatures{'error'}) {
				$error_str .= " ERROR: Disk $j temperature is $a[4] Celsius (>= $default_temperatures{'error'})!";
			} elsif ($a[4] >= $default_temperatures{'warning'}) {
				$warning_str .= " WARNING: Disk $j temperature is $a[4] Celsius (>= $default_temperatures{'warning'})!";
			}
		}
	}
	my $statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}

sub check_qnap
{
	my ($expire, $system_name, $snmp_session) = @_;

	#print STDERR "Checking system_name=[$system_name] ...\n";
	my $s = check_snmp_system2($expire, $snmp_session);
	if ($s eq '') {
		return '';
	}
	$s .= check_qnap_smart($expire, $snmp_session);
	$s .= check_qnap_raid($expire, $snmp_session);
	$s .= check_qnap_temperature($expire, $snmp_session);
	$s .= check_qnap_load($expire, $snmp_session);
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $serviceid = get_serviceid($check_name);
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
if (ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		if ($h->{'isactive'} eq 'f' ) {
			next;
		}
		#print STDERR "Checking $h->{'hostname'} version: $h->{'snmp_version'} ...\n";

		$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
		if ($snmp_session == 0) {
			next;
		}
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		$xml_str .= check_qnap($expire, $h->{'system_name'}, $snmp_session);
	}
}
else {
	if ($data->{'record'}->{'isactive'} eq 't' ) {
		$snmp_session = snmp_init($data->{'record'}->{'hostname'}, $data->{'record'}->{'snmp_version'}, $data->{'record'}->{'community'},
					$data->{'record'}->{'username'}, $data->{'record'}->{'password'});	
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$data->{'record'}->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		if ($snmp_session != 0) {
			$xml_str = check_qnap($expire, $data->{'record'}->{'system_name'}, $snmp_session);
		}
	}
}
unlock_check($check_name);
print $xml_str;
