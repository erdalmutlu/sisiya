#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
#use Data::Dumper;

my $check_name = 'dellmdstorage';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}
###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %loads = ('error' => 90, 'warning' => 80);
our %default_temperatures = ( 'warning' => 35, 'error' => 40 );
our $retries = 3;	# number of retries for the snmpwalk command
our $timeout = 15;	# timeout between retries for the snmpwalk command 
our %oids = (
	'idrac_mib'				=> '1.3.6.1.4.1.674.10892',			# whole iDRAC tree
	# system
	'mib_table_system'			=> '1.3.6.1.2.1.1',				# SNMP MIB-2 System tree
	'system_description'			=> '1.3.6.1.2.1.1.1.0',
	'system_uptime'				=> '1.3.6.1.2.1.1.3.0',
	'system_contact'			=> '1.3.6.1.2.1.1.4.0',
	'system_name'				=> '1.3.6.1.2.1.1.5.0',
	'system_location'			=> '1.3.6.1.2.1.1.6.0',
	# CPU 
	'mib_table_cpu'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30', 	# CPU tree
	'cpu_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.1',
	'cpu_index'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.2',
	'cpu_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.3',
	'cpu_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.4',
	'cpu_status'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.5',
	'cpu_type'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.7',
	'cpu_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.8',
	'cpu_state'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.9',
	'cpu_family'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.10',
	'cpu_max_speed'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.11',
	'cpu_current_speed'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.12',
	'cpu_external_clock_speed'		=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.13',
	'cpu_voltage'				=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.14',
	'cpu_version_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.16',
	'cpu_core_count'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.17',
	'cpu_core_enabled_count'		=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.18',
	'cpu_thread_count'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.19',
	'cpu_brand_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.30.1.23',
	# RAM 
	'mib_table_ram'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50', 	# RAM tree
	'ram_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.1',
	'ram_index'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.2',
	'ram_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.3',
	'ram_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.4',
	'ram_status'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.5',
	'ram_type'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.7',
	'ram_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.8',
	'ram_bank_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.10',
	'ram_size'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.14',
	'ram_speed'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.15',
	'ram_manufacturer_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.21',
	'ram_part_number_name'			=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.22',
	'ram_serial_number_name'		=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.23',
	'ram_fqdd'				=> '1.3.6.1.4.1.674.10892.5.4.1100.50.1.26',
	# voltage probes 
	'mib_table_voltage'			=> '1.3.6.1.4.1.674.10892.5.4.600.20',		# Voltages tree
	'vp_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.1',
	'vp_index'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.2',
	'vp_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.3',
	'vp_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.4',
	'vp_status'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.5',
	'vp_reading'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.6',
	'vp_type'				=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.7',
	'vp_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.8',
	'vp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.9',
	'vp_upper_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.10',
	'vp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.11',
	'vp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.12',
	'vp_lower_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.13',
	'vp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.14',
	'vp_probe_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.15',
	'vp_discrete_reading'			=> '1.3.6.1.4.1.674.10892.5.4.600.20.1.16',
	# intrusion 
	'mib_table_intrusion'			=> '1.3.6.1.4.1.674.10892.5.4.300.70',		# Intrusion tree
	'int_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.1',
	'int_index'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.2',
	'int_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.3',
	'int_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.4',
	'int_status'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.5',
	'int_reading'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.6',
	'int_type'				=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.7',
	'int_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.70.1.8',
	# temperature probe devices  
	'mib_table_temperature'			=> '1.3.6.1.4.1.674.10892.5.4.700.20',		# Temperature tree
	'tp_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.1',
	'tp_index'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.2',
	'tp_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.3',
	'tp_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.4',
	'tp_status'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.5',
	'tp_reading'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.6',
	'tp_type'				=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.7',
	'tp_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.8',
	'tp_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.9',
	'tp_upper_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.10',
	'tp_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.11',
	'tp_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.12',
	'tp_lower_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.13',
	'tp_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.14',
	'tp_probe_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.15',
	'tp_discrete_reading'			=> '1.3.6.1.4.1.674.10892.5.4.700.20.1.16',
	# cooling unit devices  
	'mib_table_cu'				=> '1.3.6.1.4.1.674.10892.5.4.700.10',		# Cooling units tree
	'cu_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.1',
	'cu_index'				=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.2',
	'cu_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.3',
	'cu_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.4',
	'cu_redundancy_status'			=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.5',
	'cu_device_count_for_redundancy'	=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.6',
	'cu_name'				=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.7',
	'cu_status'				=> '1.3.6.1.4.1.674.10892.5.4.700.10.1.8',
	# cooling devices  
	'mib_table_cd'				=> '1.3.6.1.4.1.674.10892.5.4.700.12',		# Cooling devices tree
	'cd_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.1',
	'cd_index'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.2',
	'cd_state_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.3',
	'cd_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.4',
	'cd_status'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.5',
	'cd_reading'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.6',
	'cd_type'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.7',
	'cd_location_name'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.8',
	'cd_upper_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.9',
	'cd_upper_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.10',
	'cd_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.11',
	'cd_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.12',
	'cd_lower_critical_threshold'		=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.13',
	'cd_lower_nonrecoverable_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.14',
	'cd_cooling_utit_index_reference'	=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.15',
	'cd_sub_type'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.16',
	'cd_probe_capabilities'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.17',
	'cd_discrete_reading'			=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.18',
	'cd_fqdd'				=> '1.3.6.1.4.1.674.10892.5.4.700.12.1.19',
	# chassis  
	'mib_table_chassis'			=> '1.3.6.1.4.1.674.10892.5.4.300.10',		# Chassis tree
	'chassis_index'				=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.1',
	'chassis_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.2',
	'chassis_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.3',
	'chassis_status'			=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.4',
	'chassis_type'				=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.6',
	'chassis_name'				=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.7',
	'chassis_manufacturer_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.8',
	'chassis_model_type_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.9',
	'chassis_asset_tag_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.10',
	'chassis_service_tag_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.11',
	'chassis_system_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.10.1.15',
	# firmware  
	'mib_table_firmware'			=> '1.3.6.1.4.1.674.10892.5.4.300.60',		# Firmware tree
	'firmware_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.1',
	'firmware_index'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.2',
	'firmware_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.3',
	'firmware_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.4',
	'firmware_status'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.5',
	'firmware_size'				=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.6',
	'firmware_type'				=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.7',
	'firmware_type_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.8',
	'firmware_update_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.9',
	'firmware_version_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.60.1.11',
	# system BIOS 
	'mib_table_bios'			=> '1.3.6.1.4.1.674.10892.5.4.300.50',		# BIOS tree
	'bios_chassis_index'			=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.1',
	'bios_index'				=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.2',
	'bios_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.3',
	'bios_state_settings'			=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.4',
	'bios_status'				=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.5',
	'bios_release_date_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.7',
	'bios_version_name'			=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.8',
	'bios_manufacturer_name'		=> '1.3.6.1.4.1.674.10892.5.4.300.50.1.11',
	# battery
	'mib_table_battery'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15',	# Battery tree
	'battery_number'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.1',
	'battery_state'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.4',
	'battery_component_status'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.6',
	'battery_predicted_capacity'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.10',
	'battery_fqdd'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.20',
	'battery_display_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.15.1.21',
	# system battery 
	'mib_table_sysbattery'			=> '1.3.6.1.4.1.674.10892.5.4.600.50',		# System battery tree
	'sysbattery_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.1',
	'sysbattery_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.2',
	'sysbattery_state_capabilitis'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.3',
	'sysbattery_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.4',
	'sysbattery_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.5',
	'sysbattery_reading'			=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.6',
	'sysbattery_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.600.50.1.7',
	# physical disk
	'mib_table_physical_disk'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130',	# Physical disk tree
	'physical_disk_number'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.1',	
	'physical_disk_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.2',
	'physical_disk_size'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.11',
	'physical_disk_state'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.4',
	'physical_disk_spare_state'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.22',
	'physical_disk_media_type'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.35',
	'physical_disk_display_name'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.55',
	'physical_disk_manufacturer'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.3',
	'physical_disk_operational_state'	=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.50',
	# controller
	'mib_table_controller'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130',	# Controller tree
	'ctrl_number'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.1',
	'ctrl_name'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.2',
	'ctrl_fw_version'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.8',
	'ctrl_cache_size_in_mb'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.9',
	'ctrl_roll_up_status'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.37',
	'ctrl_component_status'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.38',
	'ctrl_driver_version'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.41',
	'ctrl_fqdd'				=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.78',
	'ctrl_display_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.130.1.1.79',
	# virtual disk
	'mib_table_virtual_disk'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.140',	# Virtual disk tree
	'virtual_disk_table_entry'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1',
	'virtual_disk_number'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.1',
	'virtual_disk_name'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.2',
	'virtual_disk_display_name'		=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.36',
	'virtual_disk_state'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.4',
	'virtual_disk_operational_state'	=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.30',
	'virtual_disk_size'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.6',
	'virtual_disk_layout'			=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.13',
	'virtual_disk_remaining_redundancy'	=> '1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.34',
	# power
	'mib_table_idrac_system'		=> '1.3.6.1.4.1.674.10892.5.4.200.10',		# System tree
	'power_supply_state_details'		=> '1.3.6.1.4.1.674.10892.5.4.200.10.1.8.1',
	# amperage
	'mib_table_amperage'			=> '1.3.6.1.4.1.674.10892.5.4.600.30',		# Amparage tree
	'amperage_probe_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.1',
	'amperage_probe_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.2',
	'amperage_probe_state_capabilities'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.3',
	'amperage_probe_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.4',
	'amperage_probe_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.5',
	'amperage_probe_reading'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.6',
	'amperage_probe_type'			=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.7',
	'amperage_probe_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.8',
	'amperage_probe_upper_nonrecoverable_treshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.9',
	'amperage_probe_upper_critical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.10',
	'amperage_probe_upper_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.11',
	'amperage_probe_lower_critical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.12',
	'amperage_probe_lower_noncritical_threshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.13',
	'amperage_probe_lower_nonrecoverable_treshold'	=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.14',
	'amperage_probe_probe_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.15',
	'amperage_probe_discrete_reading'		=> '1.3.6.1.4.1.674.10892.5.4.600.30.1.16',
	# power unit
	'mib_table_power_unit'			=> '1.3.6.1.4.1.674.10892.5.4.600.10',		# Power unit tree
	'power_unit_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.1',
	'power_unit_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.2',
	'power_unit_state_capabilities'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.3',
	'power_unit_state_settings'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.4',
	'power_unit_redundancy_status'		=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.5',
	'power_supply_count_for_redundancy'	=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.6',
	'power_unit_name'			=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.7',
	'power_unit_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.10.1.8',
	# power supply
	'mib_table_power_supply'		=> '1.3.6.1.4.1.674.10892.5.4.600.12',		# Power supply tree
	'power_supply_chassis_index'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.1',
	'power_supply_index'			=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.2',
	'power_supply_status'			=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.5',
	'power_supply_location_name'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.8',
	'power_supply_output_watts'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.6',
	'power_supply_input_voltage'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.9',
	'power_supply_type'			=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.7',
	'power_supply_sensor_state'		=> '1.3.6.1.4.1.674.10892.5.4.600.12.1.11',
);
# end of default values
############################################################################################################
# CoolingDeviceTypeEnum: coolingDeviceTypeIsOther(1), coolingDeviceTypeIsUnknown(2), 
# 			coolingDeviceTypeIsAFan(3), coolingDeviceTypeIsABlower(4), 
# 			coolingDeviceTypeIsAChipFan(5), coolingDeviceTypeIsACabinetFan(6), 
# 			coolingDeviceTypeIsAPowerSupplyFan(7), coolingDeviceTypeIsAHeatPipe(8), 
# 			coolingDeviceTypeIsRefrigeration(9), coolingDeviceTypeIsActiveCooling(10), 
# 			coolingDeviceTypeIsPassiveCooling(11) 	 
# CoolingDeviceDiscreteReadingEnum: coolingDeviceIsGood(1), coolingDeviceIsBad(2)
# CoolingDeviceSubTypeEnum: coolingDeviceSubTypeIsOther(1), coolingDeviceSubTypeIsUnknown(2), 
# 				coolingDeviceSubTypeIsAFanThatReadsInRPM(3), 
# 				coolingDeviceSubTypeIsAFanReadsONorOFF(4), 
# 				coolingDeviceSubTypeIsAPowerSupplyFanThatReadsinRPM(5), 
# 				coolingDeviceSubTypeIsAPowerSupplyFanThatReadsONorOFF(6), 
# 				coolingDeviceSubTypeIsDiscrete(16)
# IntrusionReadingEnum: chassisNotBreached(1), chassisBreached(2), chassisBreachedPrior(3), chassisBreachSensorFailure(4) 				
# IntrusionTypeEnum: chassisBreachDetectionWhenPowerON(1), chassisBreachDetectionWhenPowerOFF(2)
# MemoryDeviceTypeEnum: deviceTypeIsOther(1), deviceTypeIsUnknown(2), deviceTypeIsDRAM(3), 
# 		deviceTypeIsEDRAM(4), deviceTypeIsVRAM(5), deviceTypeIsSRAM(6), 
# 		deviceTypeIsRAM(7), deviceTypeIsROM(8), deviceTypeIsFLASH(9), 
# 		deviceTypeIsEEPROM(10), deviceTypeIsFEPROM(11), deviceTypeIsEPROM(12), 
# 		deviceTypeIsCDRAM(13), deviceTypeIs3DRAM(14), deviceTypeIsSDRAM(15), 
# 		deviceTypeIsSGRAM(16), deviceTypeIsRDRAM(17), deviceTypeIsDDR(18), 
# 		deviceTypeIsDDR2(19), deviceTypeIsDDR2FBDIMM(20), deviceTypeIsDDR3(24), deviceTypeIsFBD2(25)
# ObjectStatusEnum: other(1), unknown(2), ok(3), nonCritical(4), critical(5), nonRecoverable(6)
# PowerStateStatusEnum: other(1), unknown(2), off(3), on(4)
# StateSettingsFlags: unknown(1), enabled(2), notReady(4), enabledAndNotReady(6)
# StateCapabilitiesFlags: unknownCapabilities(1), enableCapable(2), notReadyCapable(4), enableAndNotReadyCapable(6)
# StatusProbeEnum: other(1), unknown(2), ok(3), nonCriticalUpper(4), criticalUpper(5), nonRecoverableUpper(6), 
# 			nonCriticalLower(7), criticalLower(8), nonRecoverableLower(9), failed(10)
# StatusRedundancyEnum: other(1), unknown(2), full(3), degraded(4), lost(5), notRedundant(6), redundancyOffline(7)
# SystemBatteryReadingFlags: predictiveFailure(1), failed(2), presenceDetected(4) => Note: These values are bit masks, so combination values are possible.
# TemperatureProbeTypeEnum: temperatureProbeTypeIsOther(1), temperatureProbeTypeIsUnknown(2),
# 				temperatureProbeTypeIsAmbientESM(3), temperatureProbeTypeIsDiscrete(16)
# TemperatureDiscreteReadingEnum: temperatureIsGood(1), temperatureIsBad(2)
# VoltageDiscreteReadingEnum: voltageIsGood(1), voltageIsBad(2)
# VoltageTypeEnum: voltageProbeTypeIsOther(1), voltageProbeTypeIsUnknown(2), 
# 		voltageProbeTypeIs1Point5Volt(3), voltageProbeTypeIs3Point3Volt(4), 
# 		voltageProbeTypeIs5Volt(5), voltageProbeTypeIsMinus5Volt(6), 
# 		voltageProbeTypeIs12Volt(7), voltageProbeTypeIsMinus12Volt(8), 
# 		voltageProbeTypeIsIO(9), voltageProbeTypeIsCore(10), voltageProbeTypeIsFLEA(11), 
# 		voltageProbeTypeIsBattery(12), voltageProbeTypeIsTerminator(13), 
# 		oltageProbeTypeIs2Point5Volt(14), voltageProbeTypeIsGTL(15), 
# 		voltageProbeTypeIsDiscrete(16), voltageProbeTypeIsGenericDiscrete(17), 
# 		voltageProbeTypeIsPSVoltage(18), voltageProbeTypeIsMemoryStatus(19) 	 
#
# physical_disk_state: 1.3.6.1.4.1.674.10892.5.5.1.20.130.4.1.4
# The current state of this physical disk. Possible states: 
# 	1: The current state could not be determined. 
# 	2: The physical disk is available for use, but no RAID configuration has been assigned. 
# 	3: A RAID configuration has been assigned to the physical disk. 
# 	4: The physical disk has been moved from another controller and contains all or some portion of a virtual disk. 
# 	5: The physical disk is not available to the RAID controller. 
# 	6: The physical disk is currently blocked by controller. 
# 	7: The physical disk is not operational. 
# 	8: The physical disk is not a RAID capable disk 
# 	9: The physical disk has been removed. 
############################################################################################################
# amperageProbeType	: 1.3.6.1.4.1.674.10892.5.4.600.30.1.7
# 	amperageProbeTypeIsOther(1)
# 	amperageProbeTypeIsUnknown(2)
# 	amperageProbeTypeIs1Point5Volt(3)
# 	amperageProbeTypeIs3Point3volt(4)
# 	amperageProbeTypeIs5Volt(5)
# 	amperageProbeTypeIsMinus5Volt(6)
# 	amperageProbeTypeIs12Volt(7)
# 	amperageProbeTypeIsMinus12Volt(8)
# 	amperageProbeTypeIsIO(9)
# 	amperageProbeTypeIsCore(10)
# 	amperageProbeTypeIsFLEA(11)
# 	amperageProbeTypeIsBattery(12)
# 	amperageProbeTypeIsTerminator(13)
# 	amperageProbeTypeIs2Point5Volt(14)
# 	amperageProbeTypeIsGTL(15)
# 	amperageProbeTypeIsDiscrete(16)
# 	amperageProbeTypeIsPowerSupplyAmps(23)
# 	amperageProbeTypeIsPowerSupplyWatts(24)
# 	amperageProbeTypeIsSystemAmps(25)
# 	amperageProbeTypeIsSystemWatts(26)
#
# amperageProbeReading	: 1.3.6.1.4.1.674.10892.5.4.600.30.1.6
# This attribute defines the reading for an amperage probe of type other than amperageProbeTypeIsDiscrete. 
# When the value for amperageProbeType is amperageProbeTypeIsPowerSupplyAmps or amperageProbeTypeIsSystemAmps, 
# the value returned for this attribute is the power usage that the probe is reading in tenths of Amps. When 
# the value for amperageProbeType is amperageProbeTypeIsPowerSupplyWatts or amperageProbeTypeIsSystemWatts,
# the value returned for this attribute is the power usage that the probe is reading in Watts. When the 
# value for amperageProbeType is other than amperageProbeTypeIsDiscrete, amperageProbeTypeIsPowerSupplyAmps, 
# amperageProbeTypeIsPowerSupplyWatts, amperageProbeTypeIsSystemAmps or amperageProbeTypeIsSystemWatts, the 
# value returned for this attribute is the amperage that the probe is reading in Milliamps. When the value 
# for amperageProbeType is amperageProbeTypeIsDiscrete, a value is not returned for this attribute.
############################################################################################################

#################################################################
sub get_snmp_value_from_array
{
	my ($search_str, @a) = @_;
	
	#print STDERR "search str: $search_str\n";
	my $s = (grep(/$search_str/, @a))[0];
	#print STDERR "1 s=$s\n";
	if ($s) {
		$s = trim((split(/=/, $s))[1]);
	}
	#print STDERR "2 s=$s\n";
	return $s;
}
sub get_type_name
{
	my $k = $_[0];

	if ($k == 26) {
	      return('DDR-4');
	} elsif ($k == 25) {	      
	      return('FBD2');
	} elsif ($k == 24) {	      
	      return('DDR-3');
	} elsif ($k == 20) {	      
	      return('DDR-2 FB');
	} elsif ($k == 19) {	      
	      return('DDR-2');
	} elsif ($k == 18) {	      
	      return('DDR');
	} elsif ($k == 17) {	      
	      return('RDRAM');
	} elsif ($k == 16) {	      
	      return('SGRAM');
	} elsif ($k == 15) {	      
	      return('SDRAM');
	} elsif ($k == 14) {	      
	      return('3DRAM');
	} elsif ($k == 13) {	      
	      return('CDRAM');
	} elsif ($k == 12) {	      
	      return('EPROM');
	} elsif ($k == 11) {	      
	      return('FEPROM');
	} elsif ($k == 10) {	      
	      return('EEPROM');
	} elsif ($k == 9) {	      
	      return('FLASH');
	} elsif ($k == 8) {	      
	      return('ROM');
	} elsif ($k == 7) {	      
	      return('RAM');
	} elsif ($k == 6) {	      
	      return('SRAM');
	} elsif ($k == 5) {	      
	      return('VRAM');
	} elsif ($k == 4) {	      
	      return('EDRAM');
	} elsif ($k == 3) {	      
	      return('DRAM');
	} elsif ($k == 2) {	      
	      return('UKNOWN');
	} elsif ($k == 1) {	      
	      return('Other');
	} else {	      
	      return('Uknown');
 	}
}

sub check_cpu
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('cpu');
	my $statusid;
	my ($cpu_name, $cpu_status, $cpu_type, $chassis_index, $index);
	my ($cpu_family, $cpu_core_count, $cpu_thread_count, $cpu_brand_name);

	# find out how many CPUs are there
	my $n = count_keys(\%h, $oids{'cpu_chassis_index'}.'.1');
	for (my $i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'cpu_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'cpu_index'}.'.'.$chassis_index.'.'.$i};
		$cpu_status = $h{$oids{'cpu_status'}.'.'.$index};
		$cpu_name = $h{$oids{'cpu_location_name'}.'.'.$index};
		$cpu_name =~ s/"//g;
		$cpu_family = $h{$oids{'cpu_family'}.'.'.$index};
		$cpu_core_count = $h{$oids{'cpu_core_count'}.'.'.$index};
		$cpu_thread_count = $h{$oids{'cpu_thread_count'}.'.'.$index};
		$cpu_type = $h{$oids{'cpu_type'}.'.'.$index};
		$cpu_brand_name = $h{$oids{'cpu_brand_name'}.'.'.$index};
		$cpu_brand_name =~ s/"//g;

		$cpu_name = $cpu_name." ($cpu_brand_name Cores: $cpu_core_count Threads: $cpu_thread_count Family: $cpu_family)";
		if ($cpu_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: CPU status of $cpu_name on chassis $chassis_index is OK.";
		} else {
			if ($cpu_status == 5) { # ObjectStatusEnum: critical
				$error_str .= "ERROR: CPU status of $cpu_name on chassis $chassis_index is critical!";
			} elsif ($cpu_status == 6) { # ObjectStatusEnum: nonRecoverable
				$error_str .= "ERROR: CPU status of $cpu_name on chassis $chassis_index is not recoverable!";
			} elsif ($cpu_status == 4) { # ObjectStatusEnum: critical
				$warning_str .= "WARNING: CPU status of $cpu_name on chassis $chassis_index is not critical!";
			} elsif ($cpu_status == 1) { # ObjectStatusEnum: other
				$error_str .= "ERROR: CPU status of $cpu_name on chassis $chassis_index is other!";
			} else {
				$error_str .= "ERROR: CPU status of $cpu_name on chassis $chassis_index is unknown!";
			}
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_ram
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('ram');
	my $statusid;
	my ($ram_name, $ram_status, $ram_size, $ram_type, $ram_speed, $chassis_index, $index);
	my $divide_factor = 1048576;

	# find out how many RAMs are there
	my $n = count_keys(\%h, $oids{'ram_chassis_index'}.'.1');
	for (my $i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'ram_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'ram_index'}.'.'.$chassis_index.'.'.$i};
		$ram_status = $h{$oids{'ram_status'}.'.'.$index};
		$ram_name = $h{$oids{'ram_location_name'}.'.'.$index};
		$ram_name =~ s/"//g;
		$ram_size = $h{$oids{'ram_size'}.'.'.$index} / $divide_factor;
		$ram_type = get_type_name($h{$oids{'ram_type'}.'.'.$index});
		$ram_speed = $h{$oids{'ram_speed'}.'.'.$index};

		$ram_name = $ram_name." (Size: $ram_size GB Type: $ram_type Speed: $ram_speed MHz)";
		if ($ram_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Memory status of $ram_name on chassis $chassis_index is OK.";
		} else {
			if ($ram_status == 5) { # ObjectStatusEnum: critical
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is critical!";
			} elsif ($ram_status == 6) { # ObjectStatusEnum: nonRecoverable
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is not recoverable!";
			} elsif ($ram_status == 4) { # ObjectStatusEnum: critical
				$warning_str .= "WARNING: Memory status of $ram_name on chassis $chassis_index is not critical!";
			} elsif ($ram_status == 1) { # ObjectStatusEnum: other
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is other!";
			} else {
				$error_str .= "ERROR: Memory status of $ram_name on chassis $chassis_index is unknown!";
			}
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_voltages
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('voltages');
	my $statusid;
	my ($vp_name, $vp_status, $vp_reading, $vp_type, $reading_status, $chassis_index, $index);
	my ($divide_factor, $current_reading, $lower_critical_reading, $probe_str);
	my ($upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);

	$divide_factor = 1000;
	# find out how many temperature probe devices are there
	my $n = count_keys(\%h, $oids{'vp_chassis_index'}.'.1');
	for (my $i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'vp_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'vp_index'}.'.'.$chassis_index.'.'.$i};
		$vp_status = $h{$oids{'vp_status'}.'.'.$index};
		$vp_name = $h{$oids{'vp_location_name'}.'.'.$index};
		$vp_name =~ s/"//g;
		$vp_reading = $h{$oids{'vp_reading'}.'.'.$index};
		$vp_type = $h{$oids{'vp_type'}.'.'.$index};

		$reading_status = 0; # problem
		if ($vp_type != 16 ) { # VoltageTypeEnum: voltageProbeTypeIsDiscrete(16)

			# This attribute defines the reading for a voltage probe of type other 
			# than voltageProbeTypeIsDiscrete. When the value for voltageProbeType 
			# is other than voltageProbeTypeIsDiscrete, the value returned for this 
			# attribute is the voltage that the probe is reading in millivolts. When 
			# the value for voltageProbeType is voltageProbeTypeIsDiscrete, a value 
			# is not returned for this attribute.

			$current_reading = $h{$oids{'vp_reading'}.'.'.$index} / $divide_factor;
			if (exists $h{$oids{'vp_upper_critical_threshold'}.'.'.$index}) {
				$upper_critical_reading = $h{$oids{'vp_upper_critical_threshold'}.'.'.$index} / $divide_factor;
				$upper_noncritical_reading = $h{$oids{'vp_upper_noncritical_threshold'}.'.'.$index} / $divide_factor;
				$lower_critical_reading = $h{$oids{'vp_lower_critical_threshold'}.'.'.$index} / $divide_factor;
				$lower_noncritical_reading = $h{$oids{'vp_lower_noncritical_threshold'}.'.'.$index} / $divide_factor;
	
				if ($current_reading >= $upper_critical_reading) {
					$error_str .= "ERROR: Voltage of $vp_name on chassis $chassis_index is $current_reading (>= $upper_critical_reading) Volt!";
				} elsif ($current_reading >= $upper_noncritical_reading) {
					$warning_str .= "WARNING: Voltage of $vp_name on chassis $chassis_index is $current_reading (>= $upper_noncritical_reading) Volt!";
				} elsif ($current_reading <= $lower_critical_reading) {
					$error_str .= "ERROR: Voltage of $vp_name on chassis $chassis_index is $current_reading (<= $lower_critical_reading) Volt!";
				} elsif ($current_reading <= $lower_noncritical_reading) {
					$warning_str .= "WARNING: Voltage of $vp_name on chassis $chassis_index is $current_reading (<= $lower_noncritical_reading) Volt!";
				} else {
					$reading_status = 1; 
					$probe_str = "Reading is $current_reading Volt.";
				}
			} else {
				$reading_status = 1; 
				$probe_str = "Reading is $current_reading Volt.";
			}
		} else {	# it is a discrete probe device
			if ($h{$oids{'vp_discrete_reading'}.'.'.$index} == 1) {
				$reading_status = 1; 
				$probe_str = "Discrete reading is OK.";
			} else {
				$error_str .= "ERROR: Voltage probe device discrete reading of $vp_name on chassis $chassis_index is bad!";
			}
		}

		if ($vp_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Voltage status of $vp_name on chassis $chassis_index is OK.";
			if ($reading_status == 1) {
				$ok_str .= " $probe_str";
			}
		} else {
			if ($vp_status == 5) { # ObjectStatusEnum: critical
				$error_str .= "ERROR: Voltage status of $vp_name on chassis $chassis_index is critical!";
			} elsif ($vp_status == 6) { # ObjectStatusEnum: nonRecoverable
				$error_str .= "ERROR: Voltage status of $vp_name on chassis $chassis_index is not recoverable!";
			} elsif ($vp_status == 4) { # ObjectStatusEnum: critical
				$warning_str .= "WARNING: Voltage status of $vp_name on chassis $chassis_index is not critical!";
			} elsif ($vp_status == 1) { # ObjectStatusEnum: other
				$error_str .= "ERROR: Voltage status of $vp_name on chassis $chassis_index is other!";
			} else {
				$error_str .= "ERROR: Voltage status of $vp_name on chassis $chassis_index is unknown!";
			}
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_intrusion
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('intrusion');
	my $statusid;
	
	my ($int_name, $int_status, $int_reading, $int_type, $chassis_index, $index, $breach_str);
	# find out how many intrusion probe devices are there
	my $n = count_keys(\%h, $oids{'int_chassis_index'}.'.1');
	for (my $i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'int_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'int_index'}.'.'.$chassis_index.'.'.$i};
		$int_status = $h{$oids{'int_status'}.'.'.$index};
		$int_name = $h{$oids{'int_location_name'}.'.'.$index};
		$int_name =~ s/"//g;
		$int_reading = $h{$oids{'int_reading'}.'.'.$index};
		$int_type = $h{$oids{'int_type'}.'.'.$index};

		$breach_str = '';
		if ($int_type == 1) {		# IntrusionTypeEnum: chassisBreachDetectionWhenPowerON(1),
			$breach_str = " Breach detected when the system was powered on.";
		} elsif ($int_type == 2) {	# IntrusionTypeEnum:  chassisBreachDetectionWhenPowerOFF(2)
			$breach_str = " Breach detected when the system was powered off.";
		}

		if ($int_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Intrusion status of $int_name on chassis $chassis_index is OK.";
		} elsif ($int_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Intrusion status of $int_name on chassis $chassis_index is critical!";
		} elsif ($int_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Intrusion status of $int_name on chassis $chassis_index is not recoverable!";
		} elsif ($int_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Intrusion status of $int_name on chassis $chassis_index is not critical!";
		} elsif ($int_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Intrusion status of $int_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Intrusion status of $int_name on chassis $chassis_index is unknown!";
		}
		
		if ($int_reading == 1) {	# IntrusionReadingEnum: chassisNotBreached(1)
			$ok_str .= "OK: $int_name on chassis $chassis_index is not breached.";
		} elsif ($int_reading == 2) {	# IntrusionReadingEnum: chassisBreached(2)
			$error_str .= "ERROR: $int_name on chassis $chassis_index is breached!$breach_str";
		} elsif ($int_reading == 3) {	# IntrusionReadingEnum: chassisBreachedPrior(3)
			$error_str .= "ERROR: $int_name on chassis $chassis_index is breached prior!$breach_str";
		} elsif ($int_reading == 4) {	# IntrusionReadingEnum: chassisBreachSensorFailure(4) 
			$error_str .= "ERROR: Detected sensor failure for $int_name on chassis $chassis_index";
		} else {
			$error_str .= "ERROR: $int_name on chassis $chassis_index unknown breach reading ($int_reading)!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_temperature
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('temperature');
	my $statusid;
	
	# find out how many temperature probe devices are there
	my $number_of_tps = count_keys(\%h, $oids{'tp_chassis_index'}.'.1');

	my ($tp_name, $tp_status, $chassis_index, $index, $current_reading, $lower_critical_reading, $reading_status, $probe_str);
	my ($divide_factor, $upper_critical_reading, $lower_noncritical_reading, $upper_noncritical_reading);
	$divide_factor = 10;
	for (my $i = 1; $i <= $number_of_tps; $i++) {
		$chassis_index = $h{$oids{'tp_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'tp_index'}.'.'.$chassis_index.'.'.$i};
		$tp_status = $h{$oids{'tp_status'}.'.'.$index};
		$tp_name = $h{$oids{'tp_location_name'}.'.'.$index};
		$tp_name =~ s/"//g;


		$reading_status = 0; # problem
		if ($h{$oids{'tp_type'}.'.'.$index} != 16 ) { # temperatureProbeTypeIsDiscrete(16)
			# This attribute defines the reading for a temperature probe of type temperatureProbeTypeIsDiscrete. 
			# When the value for temperatureProbeType is other than temperatureProbeTypeIsDiscrete, a value is 
			# not returned for this attribute. When the value for temperatureProbeType is temperatureProbeTypeIsDiscrete, 
			# the value returned for this attribute is the discrete reading for the probe.
			$current_reading = $h{$oids{'tp_reading'}.'.'.$index} / $divide_factor;
			$upper_critical_reading = $h{$oids{'tp_upper_critical_threshold'}.'.'.$index} / $divide_factor;
			$upper_noncritical_reading = $h{$oids{'tp_upper_noncritical_threshold'}.'.'.$index} / $divide_factor;
			$lower_critical_reading = $h{$oids{'tp_lower_critical_threshold'}.'.'.$index} / $divide_factor;
			$lower_noncritical_reading = $h{$oids{'tp_lower_noncritical_threshold'}.'.'.$index} / $divide_factor;

			if ($current_reading >= $upper_critical_reading) {
				$error_str .= "ERROR: Temperature of $tp_name on chassis $chassis_index is $current_reading (>= $upper_critical_reading) Celcius!";
			} elsif ($current_reading >= $upper_noncritical_reading) {
				$warning_str .= "WARNING: Temperature of $tp_name on chassis $chassis_index is $current_reading (>= $upper_noncritical_reading) Celcius!";
			} elsif ($current_reading <= $lower_critical_reading) {
				$error_str .= "ERROR: Temperature of $tp_name on chassis $chassis_index is $current_reading (<= $lower_critical_reading) Celcius!";
			} elsif ($current_reading <= $lower_noncritical_reading) {
				$warning_str .= "WARNING: Temperature of $tp_name on chassis $chassis_index is $current_reading (<= $lower_noncritical_reading) Celcius!";
			} else {
				$reading_status = 1; 
				$probe_str = "Reading is $current_reading Celcius.";
			}
		} else {	# it is a discrete temperature probe device
			if ($h{$oids{'tp_discrete_reading'}.'.'.$index} == 1) {
				$reading_status = 1; 
				$probe_str = "OK: Discrete reading is OK.";
			} else {
				$error_str .= "ERROR: Temperature probe device discrete reading of $tp_name on chassis $chassis_index is bad!";
			}
		}
		if ($tp_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Temperature probe device status of $tp_name on chassis $chassis_index is OK.";
			if ($reading_status == 1) {
				$ok_str .= " $probe_str";
			}
		} elsif ($tp_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Temperature probe device status of $tp_name on chassis $chassis_index is critical!";
		} elsif ($tp_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Temperature probe device status of $tp_name on chassis $chassis_index is not recoverable!";
		} elsif ($tp_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Temperature probe device status of $tp_name on chassis $chassis_index is not critical!";
		} elsif ($tp_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Temperature probe device status of $tp_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Temperature probe device status of $tp_name on chassis $chassis_index is unknown!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_cooling_device
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('fanspeed');
	my $statusid;
	my ($cd_name, $cd_status, $chassis_index, $index, $speed_str);
	my $i;

	# find out how many cooling devices are there
	my $number_of_cds = count_keys(\%h, $oids{'cd_chassis_index'}.'.1');
	for ($i = 1; $i <= $number_of_cds; $i++) {
		$chassis_index = $h{$oids{'cd_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'cd_index'}.'.'.$chassis_index.'.'.$i};
		$cd_status = $h{$oids{'cd_status'}.'.'.$index};
		$cd_name = $h{$oids{'cd_location_name'}.'.'.$index};
		$cd_name =~ s/"//g;

		#Threshold attributes:
		#The value is an integer representing fan speed in revolutions per minute (RPM). 
		#It is not applicable to OFF/ON type cooling devices or non-cooling device types.

		$speed_str = '';
		if ($h{$oids{'cd_sub_type'}.'.'.$index} != 16 ) { # coolingDeviceSubTypeIsDiscrete(16)

			#This attribute defines the reading for a cooling device of subtype other 
			#than coolingDeviceSubTypeIsDiscrete. When the value for coolingDeviceSubType 
			#is other than coolingDeviceSubTypeIsDiscrete, the value returned for this 
			#attribute is the speed in RPM or the OFF/ON value of the cooling device. 
			#When the value for coolingDeviceSubType is coolingDeviceSubTypeIsDiscrete, 
			#a value is not returned for this attribute.
		
			$speed_str = " (Speed: $h{$oids{'cd_reading'}.'.'.$index} RPM.) ";
		} else {	# it is a discrete cooling device
			$speed_str = '';
			if ($h{$oids{'cd_discrete_reading'}.'.'.$index} == 1) {
				$ok_str .= "OK: Cooling device discrete reading of $cd_name on chassis $chassis_index is OK.";
			} else {
				$error_str .= "ERROR: Cooling device discrete reading of $cd_name on chassis $chassis_index is bad!";
			}
		}
		$cd_name = $cd_name.$speed_str;
		if ($cd_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Cooling device status of $cd_name on chassis $chassis_index is OK.";
		} elsif ($cd_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Cooling device status of $cd_name on chassis $chassis_index is critical!";
		} elsif ($cd_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Cooling device status of $cd_name on chassis $chassis_index is not recoverable!";
		} elsif ($cd_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Cooling device status of $cd_name on chassis $chassis_index is not critical!";
		} elsif ($cd_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Cooling device status of $cd_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Cooling device status of $cd_name on chassis $chassis_index is unknown!";
		}
	}

	# find out how many cooling units are there
	my $number_of_cus = count_keys(\%h, $oids{'cu_chassis_index'}.'.1');

	my ($cu_name, $cu_status, $cu_redundancy_status);
	for ($i = 1; $i <= $number_of_cus; $i++) {
		$chassis_index = $h{$oids{'cu_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'cu_index'}.'.'.$chassis_index.'.'.$i};
		$cu_status = $h{$oids{'cu_status'}.'.'.$index};
		$cu_redundancy_status = $h{$oids{'cu_redundancy_status'}.'.'.$index};
		$cu_name = $h{$oids{'cu_name'}.'.'.$index};
		$cu_name =~ s/"//g;

		if ($cu_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Cooling unit status of $cu_name on chassis $chassis_index is OK.";
		} elsif ($cu_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Cooling unit status of $cu_name on chassis $chassis_index is critical!";
		} elsif ($cu_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Cooling unit status of $cu_name on chassis $chassis_index is not recoverable!";
		} elsif ($cu_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Cooling unit status of $cu_name on chassis $chassis_index is not critical!";
		} elsif ($cu_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Cooling unit status of $cu_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Cooling unit status of $cu_name on chassis $chassis_index is unknown!";
		}

		if ($cu_redundancy_status == 3) { # StatusRedundancyEnum: full
			$ok_str .= "OK: Cooling unit redundancy status of $cu_name on chassis $chassis_index is full.";
		} elsif ($cu_redundancy_status == 5) { # StatusRedundancyEnum: lost
			$error_str .= "ERROR: Cooling unit redundancy status of $cu_name on chassis $chassis_index is lost!";
		} elsif ($cu_redundancy_status == 6) { # StatusRedundancyEnum: notRedundant 
			$info_str .= "INFO: Power unit is not redundant.";
		} elsif ($cu_redundancy_status == 7) { # StatusRedundancyEnum: redundancyOffline 
			$warning_str .= "WARNING: Cooling unit redundancy status of $cu_name on chassis $chassis_index is offline!";
		} elsif ($cu_redundancy_status == 4) { # StatusRedundancyEnum: degraded
			$warning_str .= "WARNING: Cooling unit redundancy status of $cu_name on chassis $chassis_index is degraded!";
		} elsif ($cu_redundancy_status == 1) { # StatusRedundancyEnum: other 
			$warning_str .= "WARNING: Cooling unit redundancy status of $cu_name on chassis $chassis_index is other!";
		} else {
			$warning_str .= "WARNING: Cooling unit redundancy status of $cu_name on chassis $chassis_index is unknown!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub check_system
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('system');
	my $statusid;
	my ($number_of_bioses, $bios_name, $bios_status, $bios_release_date, $bios_version, $chassis_index, $index);
	my $i;

	my $s = $h{$oids{'system_uptime'}};
	my @a = split(/:/, $s);
	my $up_in_minutes = $a[0] * 1440  + $a[1] * 60 + $a[2];
	$s = check_uptime(\$statusid, $up_in_minutes, $uptimes{'warning'}, $uptimes{'error'});
	$s = "$s Description: $h{$oids{'system_description'}} Location: $h{$oids{'system_location'}} Name: $h{$oids{'system_name'}}";
	if ($statusid == $SisIYA_Config::statusids{'error'}) {
		$error_str = $s;
	} elsif ($statusid == $SisIYA_Config::statusids{'warning'}) {
		$warning_str = $s;
	} else {
		$ok_str = $s;
	}

	# find out how many chassis are there
	my $n = count_keys(\%h, $oids{'bios_chassis_index'}.'.1');
	for ($i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'bios_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'bios_index'}.'.'.$chassis_index.'.'.$i};
		$bios_status = $h{$oids{'bios_status'}.'.'.$index};
		$bios_name = $h{$oids{'bios_manufacturer_name'}.'.'.$index};
		$bios_name =~ s/"//g;
		$bios_release_date = $h{$oids{'bios_release_date_name'}.'.'.$index};
		#$bios_release_date =~ s/"//g;
		$bios_version = $h{$oids{'bios_version_name'}.'.'.$index};
		$info_str .= "INFO: BIOS version: $bios_version release date: $bios_release_date";
		if ($bios_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: System BIOS status of $bios_name on chassis $chassis_index is OK.";
		} elsif ($bios_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: System BIOS status of $bios_name on chassis $chassis_index is critical!";
		} elsif ($bios_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: System BIOS status of $bios_name on chassis $chassis_index is not recoverable!";
		} elsif ($bios_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: System BIOS status of $bios_name on chassis $chassis_index is not critical!";
		} elsif ($bios_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: System BIOS status of $bios_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: System BIOS status of $bios_name on chassis $chassis_index is unknown!";
		}
	}


	my ($number_of_firmwares, $firmware_name, $firmware_status, $firmware_version);
	# find out how many chassis are there
	$n = count_keys(\%h, $oids{'firmware_chassis_index'}.'.1');
	for ($i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'firmware_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'firmware_index'}.'.'.$chassis_index.'.'.$i};
		$firmware_status = $h{$oids{'firmware_status'}.'.'.$index};
		$firmware_name = $h{$oids{'firmware_type_name'}.'.'.$index};
		$firmware_name =~ s/"//g;
		$firmware_version = $h{$oids{'firmware_version_name'}.'.'.$index};
		$firmware_version =~ s/"//g;
		$info_str .= "INFO: Firmware $firmware_name Version: $firmware_version";
		if ($firmware_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Firmware status of $firmware_name on chassis $chassis_index is OK.";
		} elsif ($firmware_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Firmware status of $firmware_name on chassis $chassis_index is critical!";
		} elsif ($firmware_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Firmware status of $firmware_name on chassis $chassis_index is not recoverable!";
		} elsif ($firmware_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Firmware status of $firmware_name on chassis $chassis_index is not critical!";
		} elsif ($firmware_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Firmware status of $firmware_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Firmware status of $firmware_name on chassis $chassis_index is unknown!";
		}
	}


	my ($number_of_chassis_infos, $chassis_name, $chassis_system_name, $chassis_status, $chassis_model, $chassis_service_tag, $chassis_asset_tag);
	# find out how many chassis are there
	$n = count_keys(\%h, $oids{'chassis_index'});
	for ($i = 1; $i <= $n; $i++) {
		$index = $i;
		$chassis_status = $h{$oids{'chassis_status'}.'.'.$index};
		$chassis_name = $h{$oids{'chassis_name'}.'.'.$index};
		if ($chassis_name ne '""') { 
			$chassis_name =~ s/"//g;
		}
		$chassis_model = $h{$oids{'chassis_model_type_name'}.'.'.$index};
		if ($chassis_model ne '""') { 
			$chassis_model =~ s/"//g;
		}
		$chassis_system_name = $h{$oids{'chassis_system_name'}.'.'.$index};
		$chassis_system_name =~ s/"//g;
		$chassis_service_tag = $h{$oids{'chassis_service_tag_name'}.'.'.$index};
		$chassis_service_tag =~ s/"//g;
		$chassis_asset_tag = $h{$oids{'chassis_asset_tag_name'}.'.'.$index};
		if ($chassis_asset_tag ne '""') { 
			$chassis_asset_tag =~ s/"//g;
		}
		$info_str .= "INFO: Chassis $chassis_name Model: $chassis_model Service Tag: $chassis_service_tag System Name: $chassis_system_name Asset Tag: $chassis_asset_tag";
		if ($chassis_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Chassis status of $chassis_name on chassis $i is OK.";
		} elsif ($chassis_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Chassis status of $chassis_name on chassis $i is critical!";
		} elsif ($chassis_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Chassis status of $chassis_name on chassis $i is not recoverable!";
		} elsif ($chassis_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Chassis status of $chassis_name on chassis $i is not critical!";
		} elsif ($chassis_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Chassis status of $chassis_name on chassis $i is other!";
		} else {
			$error_str .= "ERROR: Chassis status of $chassis_name on chassis $i is unknown!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_battery
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference

	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('battery');
	my $statusid;
	my ($i, $str);
	
	my ($number_of_batteries, $battery_name, $battery_reading, $battery_status, $battery_state, $index, $chassis_index);

	# find out how many system batteries are there
	my $n = count_keys(\%h, $oids{'sysbattery_chassis_index'}.'.1');
	for ($i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'sysbattery_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'sysbattery_index'}.'.'.$chassis_index.'.'.$i};
		$battery_status = $h{$oids{'sysbattery_status'}.'.'.$index};
		$battery_name = $h{$oids{'sysbattery_location_name'}.'.'.$index};
		$battery_name =~ s/"//g;
		$battery_reading = $h{$oids{'sysbattery_reading'}.'.'.$index};
		if ($battery_reading == 0) { # SystemBatteryReadingFlags: No battery presence detected
			next; # skip the rest 
		}
		if ($battery_reading == 4) { # SystemBatteryReadingFlags: presenceDetected(4) (4 = 0100)
			$ok_str .= "OK: Battery reading of $battery_name on chassis $chassis_index is OK.";
		} elsif ($battery_reading == 5) { # SystemBatteryReadingFlags: failed(2) (4 + 1 = 0110)
			$error_str .= "ERROR: Battery reading of $battery_name on chassis $chassis_index is failed!";
		} elsif ($battery_status == 6) { # SystemBatteryReadingFlags: predictiveFailure(1) (4 + 2  = 0110)
			$warning_str .= "WARNING: Battery reading of $battery_name on chassis $chassis_index is predictive failure!";
		} elsif ($battery_reading == 7) { # SystemBatteryReadingFlags: failed(2) (4 + 2 + 1 = 0111)
			$error_str .= "ERROR: Battery reading of $battery_name on chassis $chassis_index is failed and predictive failure!";
		} else {
			$error_str .= "ERROR: Battery reading of $battery_name on chassis $chassis_index is unknown (= $battery_reading)!";
		}
		#if ($battery_reading == 4) { # SystemBatteryReadingFlags: presenceDetected(4)
		#	next; # skip the rest 
		#}

		if ($battery_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Battery status of $battery_name on chassis $chassis_index is OK.";
		} elsif ($battery_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Battery status of $battery_name on chassis $chassis_index is critical!";
		} elsif ($battery_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Battery status of $battery_name on chassis $chassis_index is not recoverable!";
		} elsif ($battery_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Battery status of $battery_name on chassis $chassis_index is not critical!";
		} elsif ($battery_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Battery status of $battery_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Battery status of $battery_name on chassis $chassis_index is unknown (=$battery_status)!";
		}
	}

	# find out how many batteries are there
	$n = count_keys(\%h, $oids{'battery_number'});
	for ($i = 1; $i <= $n; $i++) {
		$index = $h{$oids{'battery_number'}.'.'.$i};
		$battery_status = $h{$oids{'battery_component_status'}.'.'.$index};
		$battery_name = $h{$oids{'battery_display_name'}.'.'.$index};
		$battery_name =~ s/"//g;
		$battery_state = $h{$oids{'battery_state'}.'.'.$index};

		$str = '';
		if ($battery_state == 2) { # 2: The battery is operating normally.
			$str = ' (state: normall)';
		} elsif ($battery_state == 1) { # 1: The current state could not be determined
			$error_str .= "ERROR: Battery state of $battery_name could not be determined!";
		} elsif ($battery_state == 3) { # 3: The battery has failed and needs to be replaced.
			$error_str .= "ERROR: Battery $battery_name failed and needs to be replaced!";
		} elsif ($battery_state == 4) { # 4: The battery temperature is high or charge level is depleting.
			$error_str .= "ERROR: Battery $battery_name temperature is high or charge level is depleting!";
		} elsif ($battery_state == 5) { # 5: The battery is missing or not detected
			$error_str .= "ERROR: Battery $battery_name battery is missing or not detected!";
		} elsif ($battery_state == 6) { # 6: The battery is undergoing the re-charge phase
			$warning_str .= "WARNING: Battery $battery_name is undergoing the re-charge phase!";
		} elsif ($battery_state == 7) { # 7: The battery voltage or charge level is below the threshold
			$error_str .= "ERROR: Battery $battery_name voltage or charge level is below the threshold!";
		} else {
			$error_str .= "ERROR: Battery $battery_name state is unknown (= $battery_state)!";
		}

		$battery_name = "$battery_name$str";
		if ($battery_status == 3) { # ObjectStatusEnum: ok
			$ok_str .= "OK: Battery status of $battery_name is OK.";
		} elsif ($battery_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Battery status of $battery_name is critical!";
		} elsif ($battery_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Battery status of $battery_name is not recoverable!";
		} elsif ($battery_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Battery status of $battery_name is not critical!";
		} elsif ($battery_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Battery status of $battery_name is other!";
		} else {
			$error_str .= "ERROR: Battery status of $battery_name is unknown!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_md_storage_power_supply
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
	my $serviceid = get_serviceid('powersupply');
	my $statusid;
	my $i;
	
	# find out how many power supplies are there
	my $number_of_pss = count_keys(\%h, $oids{'power_supply_chassis_index'}.'.1');

	my @power_supply_states = (0, 0); # all two power supplies are enabled by default
	my ($power_consumption, $power_warning_threshold, $power_error_threshold, $power_supply_state_details);
	$power_supply_state_details = $h{$oids{'power_supply_state_details'}};
	if (defined $power_supply_state_details) {
		$power_supply_state_details =~ s/"//g;
		@power_supply_states = (1, 1); # all two power supplies are enabled by default
		if ($power_supply_state_details eq "00 02 ") {
			$power_supply_states[0] = 0;
		} elsif ($power_supply_state_details eq "02 00 ") {
			$power_supply_states[1] = 0;
		}
		
# 
# Rewrite power consumption part of this script
#
#	$power_consumption = $h{$oids{'power_consumption'}} ;
#	$power_warning_threshold = $h{$oids{'power_consumption_warning_threshold'}} ;
#	$power_error_threshold = $h{$oids{'power_consumption_error_threshold'}} ;
#	if ($power_consumption >= $power_error_threshold) {
#		$error_str .= "ERROR: Power consumption is $power_consumption (>= $power_error_threshold) watts.";
#	} elsif ($power_consumption >= $power_warning_threshold) {
#		$error_str .= "ERROR: Power consumption is $power_consumption (>= $power_warning_threshold) watts!";
#	} else {
#		$ok_str .= "OK: Power consumption is $power_consumption watts.";
#	}
}
	my ($chassis_index, $index, $ps_input_voltage, $ps_name, $ps_status, $ps_sensor_state, $ps_output_watts, $pu_redundancy_status);
	# find out how many power units are there
	my $n = count_keys(\%h, $oids{'power_unit_chassis_index'}.'.1');
	for (my $i = 1; $i <= $n; $i++) {
		$chassis_index = $h{$oids{'power_unit_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'power_unit_index'}.'.'.$chassis_index.'.'.$i};
		$pu_redundancy_status = $h{$oids{'power_unit_redundancy_status'}.'.'.$index};
		if ($pu_redundancy_status == 3) { # StatusRedundancyEnum: full
			$ok_str .= "OK: Power unit redundancy is full.";
		} elsif ($pu_redundancy_status == 5) { # StatusRedundancyEnum: lost
			$error_str .= "ERROR: Power unit redundancy is lost!";
		} elsif ($pu_redundancy_status == 6) { # StatusRedundancyEnum: notRedundant 
			$info_str .= "INFO: Power unit is not redundant.";
		} elsif ($pu_redundancy_status == 7) { # StatusRedundancyEnum: redundancyOffline 
			$warning_str .= "WARNING: Power unit redundancy is offline!";
		} elsif ($pu_redundancy_status == 4) { # StatusRedundancyEnum: degraded
			$warning_str .= "WARNING: Power unit redundancy is degraded!";
		} elsif ($pu_redundancy_status == 1) { # StatusRedundancyEnum: other 
			$warning_str .= "WARNING: Power unit redundancy is other!";
		} else {
			$warning_str .= "WARNING: Power unit redundancy is unknown!";
		}
	}

	$n = count_keys(\%h, $oids{'power_supply_chassis_index'}.'.1');
	for ($i = 1; $i <= $n; $i++) {
		if ($power_supply_states[$i - 1] == 0) {
			$error_str .= "ERROR: Power supply $i is absent!";
			next;
		}
		$chassis_index = $h{$oids{'power_supply_chassis_index'}.'.1.'.$i};
		$index = $chassis_index.'.'.$h{$oids{'power_supply_index'}.'.'.$chassis_index.'.'.$i};
		$ps_status = $h{$oids{'power_supply_status'}.'.'.$index};
		$ps_name = $h{$oids{'power_supply_location_name'}.'.'.$index};
		$ps_name =~ s/"//g;
		$ps_output_watts = $h{$oids{'power_supply_output_watts'}.'.'.$index} / 10;
		$ps_sensor_state = $h{$oids{'power_supply_sensor_state'}.'.'.$index};
		$ps_input_voltage = $h{$oids{'power_supply_input_voltage'}.'.'.$index};
		if ($ps_status == 3) { # ObjectStatusEnum: ok
			if ($ps_sensor_state == 1) { # 
				$ok_str .= "OK: Power supply senosr state of $ps_name on chassis $chassis_index is OK. Input voltage: $ps_input_voltage Output watts: $ps_output_watts";
			} else {
				$error_str .= "ERROR: Power supply status of $ps_name on chassis $chassis_index is not OK (state=$ps_sensor_state != 1)!";
			}
		} elsif ($ps_status == 5) { # ObjectStatusEnum: critical
			$error_str .= "ERROR: Power supply status of $ps_name on chassis $chassis_index is critical!";
		} elsif ($ps_status == 6) { # ObjectStatusEnum: nonRecoverable
			$error_str .= "ERROR: Power supply status of $ps_name on chassis $chassis_index is not recoverable!";
		} elsif ($ps_status == 4) { # ObjectStatusEnum: critical
			$warning_str .= "WARNING: Power supply status of $ps_name on chassis $chassis_index is not critical!";
		} elsif ($ps_status == 1) { # ObjectStatusEnum: other
			$error_str .= "ERROR: Power supply status of $ps_name on chassis $chassis_index is other!";
		} else {
			$error_str .= "ERROR: Power supply status of $ps_name on chassis $chassis_index is unknown!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}

sub get_operational_state
{
	my $k = $_[0];

	if ($k == 1) {
		return('no active operation');
	} elsif ($k == 2) {
		return('reconstructing');
	} elsif ($k == 3) {
		return('rsyncing');
	} elsif ($k == 4) {
		return('initializing');
	} elsif ($k == 5) {
		return('background init');
	} else {
		return('unknown');
	}
}

sub check_md_storage_raid
{
	my $expire = shift;
	my $system_name = shift;
	my $hostname = shift;
        my %h = %{$_[0]}; # get hash by reference
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $serviceid = get_serviceid('raid');
	my $statusid;
	my $i;
	my ($index, $vd_name, $vd_desc, $vd_manufacturer, $vd_remaining_redundancy, $vd_op_state, $vd_state, $vd_size);
	my ($ctrl_name, $ctrl_roll_up_status, $ctrl_cache_size, $ctrl_fw_version, $ctrl_display_name);

	# controllers
	my $n = count_keys(\%h, $oids{'ctrl_number'});
	for ($i = 1; $i <= $n; $i++) {
		$index = $h{$oids{'ctrl_number'}.'.'.$i};
		$ctrl_name = $h{$oids{'ctrl_name'}.'.'.$index};
		$ctrl_name =~ s/"//g;
		$ctrl_roll_up_status = $h{$oids{'ctrl_roll_up_status'}.'.'.$index};
		$ctrl_fw_version = $h{$oids{'ctrl_fw_version'}.'.'.$index};
		$ctrl_fw_version =~ s/"//g;
		$ctrl_display_name = $h{$oids{'ctrl_display_name'}.'.'.$index};
		$ctrl_display_name =~ s/"//g;
		$ctrl_cache_size = $h{$oids{'ctrl_cache_size_in_mb'}.'.'.$index};

		$ctrl_name = "$ctrl_name (Description: $ctrl_display_name Firmware: $ctrl_fw_version Cache: $ctrl_cache_size MB)";
		# This is the combined status of the controller and its components. Possible values: 1: Other. 2: Unknown. 3: OK 4: Non-critical 5: Critical. 6: Non-recoverable. 
		if ($ctrl_roll_up_status == 3) { # ok
			$ok_str .= "OK: Controller $ctrl_name status is OK.";
		} elsif ($ctrl_roll_up_status == 4) { # Non-critical
			$warning_str .= "WARNING: Controller $ctrl_name status is non-critical!";
		} elsif ($ctrl_roll_up_status == 5) { # critical
			$error_str .= "ERROR: Controller $ctrl_name status is critical!";
		} elsif ($ctrl_roll_up_status == 6) { # non-recoverable
			$error_str .= "ERROR: Controller $ctrl_name status is non-recoverable!";
		} elsif ($ctrl_roll_up_status == 2) { # unknown
			$error_str .= "ERROR: Controller $ctrl_name status is unknown!";
		} elsif ($ctrl_roll_up_status == 1) { # other
			$error_str .= "ERROR: Controller $ctrl_name status is other!";
		} else {
			$error_str .= "ERROR: Controller $ctrl_name status is Unknown!";
		}
	}

	# logical drives
	$n = count_keys(\%h, $oids{'virtual_disk_number'});
	for ($i = 1; $i <= $n; $i++) {
		$index = $h{$oids{'virtual_disk_number'}.'.'.$i};
		$vd_name = $h{$oids{'virtual_disk_name'}.'.'.$index};
		$vd_name =~ s/"//g;
		$vd_state = $h{$oids{'virtual_disk_state'}.'.'.$index};
		$vd_op_state = get_operational_state($h{$oids{'virtual_disk_operational_state'}.'.'.$index});
		$vd_size = $h{$oids{'virtual_disk_size'}.'.'.$index} / 1024;	# convert to GB
		$vd_remaining_redundancy = $h{$oids{'virtual_disk_remaining_redundancy'}.'.'.$index};
		$vd_desc = $h{$oids{'virtual_disk_display_name'}.'.'.$index};
		$vd_desc =~ s/"//g;
		$vd_manufacturer = $h{$oids{'virtual_disk_layout'}.'.'.$index};
		$vd_desc = "$vd_desc (name: $vd_name, capacity: $vd_size GB, layout: $vd_manufacturer, state: $vd_op_state, remaining redundancy: $vd_remaining_redundancy)";
		if ($vd_state == 2) { # online
			$ok_str .= "OK: Logical drive $vd_desc status is OK.";
		} else {
			$error_str .= "ERROR: Logical drive $vd_desc status is not OK (state=$vd_state != 2)!";
		}
	}

	# physical disks
	my $number_of_physical_disks = count_keys(\%h, $oids{'physical_disk_number'});

	my ($d_name, $d_desc, $d_manufacturer, $d_remaining_redundancy, $d_op_state, $d_spare_state, $d_state, $d_size);
	my $spare_str;
	for ($i = 1; $i <= $number_of_physical_disks; $i++) {
		$index = $h{$oids{'physical_disk_number'}.'.'.$i};
		$d_name = $h{$oids{'physical_disk_name'}.'.'.$index};
		$d_name =~ s/"//g;
		$d_state = $h{$oids{'physical_disk_state'}.'.'.$index};
		$d_spare_state = $h{$oids{'physical_disk_spare_state'}.'.'.$index};
		$d_op_state = get_operational_state($h{$oids{'physical_disk_operational_state'}.'.'.$index});
		$d_size = $h{$oids{'physical_disk_size'}.'.'.$index} / 1024;	# convert to GB
		$d_desc = $h{$oids{'physical_disk_display_name'}.'.'.$index};
		$d_desc =~ s/"//g;
		$d_manufacturer = $h{$oids{'physical_disk_manufacturer'}.'.'.$index};
		$d_manufacturer =~ s/"//g;
		if ($d_spare_state == 1) {
			$spare_str = 'spare: no';
		} elsif ($d_spare_state == 2) {
			$spare_str = 'spare: dedicated';
		} elsif ($d_spare_state == 3) {
			$spare_str = 'spare: global';
		} else {
			$spare_str = "spare: unknown, spare state code: $d_spare_state";
		}
			
		$d_desc = "$d_desc (name: $d_name, state: $d_state, $spare_str, capacity: $d_size GB, manufacturer: $d_manufacturer, state: $d_op_state)";
		if ($d_state == 3 || $d_state == 2) { # 2=online, 3=ready
			$ok_str .= "OK: Disk $d_desc status is OK.";
		} elsif ($d_state == 4) { 
			$warning_str .= "WARNING: Disk $d_desc has been moved from another controller and contains all or some portion of a virtual disk!";
		} elsif ($d_state == 5) { 
			$warning_str .= "WARNING: Disk $d_desc is not available to the RAID controller!";
		} elsif ($d_state == 6) { 
			$warning_str .= "WARNING: Disk $d_desc is currently blocked by controller!";
		} elsif ($d_state == 7) { 
			$error_str .= "ERROR: Disk $d_desc is not operational!";
		} elsif ($d_state == 8) { 
			$error_str .= "ERROR: Disk $d_desc is not a RAID capable disk!";
		} elsif ($d_state == 9) { 
			$error_str .= "ERROR: Disk $d_desc has been removed!";
		} else {
			$error_str .= "ERROR: Disk $d_desc status is not OK (state=$d_state)!";
		}
	}

       	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str</msg><datamsg></datamsg></data></message>";
}

sub check_md_storage
{
	my ($isactive, $expire, $system_name, $hostname, $snmp_version, $community, $username, $password) = @_;

	if ($isactive eq 'f' ) {
		return '';
	}

	#print STDERR "Checking system_name=[$system_name] hostname=[$hostname] isactive=[$isactive] snmp_version=[$snmp_version] community=[$community] username=[$username] password=[$password]...\n";
	my $s = '';

	# get the all in an array
	#my $mib = substr $oids{'idrac_mib'}, 1, length($oids{'idrac_mib'}) - 1;
	#my @a = `$SisIYA_Remote_Config::external_progs{'snmpwalk'} -OnQ -v $snmp_version -c $community $hostname $mib 2>&1`;
	#if ($?) {
	#	$s = "<message><serviceid>".get_serviceid('system')."</serviceid><statusid>$SisIYA_Config::statusids{'error'}</statusid><expire>$expire</expire><data><msg>Could execute snmpwalk! $a[0]</msg><datamsg></datamsg></data></message>";
	#	return "<system><name>$system_name</name>$s</system>";
	#}
	my (@a, @b, $mib);
	my @mib_table_keys = grep {/mib_table_/} keys %oids;
	for (my $i = 0; $i < @mib_table_keys; $i++) {
		$mib = substr $oids{$mib_table_keys[$i]}, 1, length($oids{$mib_table_keys[$i]}) - 1;
		#print STDERR "Getting $hostname $mib ...\n";
		@b = `$SisIYA_Remote_Config::external_progs{'snmpwalk'} -r $retries -t $timeout -OnQ -v $snmp_version -c $community $hostname $mib 2>&1`;
		if ($?) {
			$s = "<message><serviceid>".get_serviceid('system')."</serviceid><statusid>$SisIYA_Config::statusids{'error'}</statusid><expire>$expire</expire><data><msg>Could execute snmpwalk! $b[0]</msg><datamsg></datamsg></data></message>";
			return "<system><name>$system_name</name>$s</system>";
		}
		@a = (@a, @b);
	}
	my %h = extract_keys(\@a);
	# print_keys(\%h);
	$s .= check_system($expire, $system_name, $hostname, \%h);
	$s .= check_md_storage_raid($expire, $system_name, $hostname, \%h);
	$s .= check_md_storage_power_supply($expire, $system_name, $hostname, \%h);
	$s .= check_battery($expire, $system_name, $hostname, \%h);
	$s .= check_cooling_device($expire, $system_name, $hostname, \%h);
	$s .= check_temperature($expire, $system_name, $hostname, \%h);
	$s .= check_intrusion($expire, $system_name, $hostname, \%h);
	$s .= check_voltages($expire, $system_name, $hostname, \%h);
	$s .= check_ram($expire, $system_name, $hostname, \%h);
	$s .= check_cpu($expire, $system_name, $hostname, \%h);
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $serviceid = get_serviceid($check_name);
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
#print STDERR Dumper($data);
if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
for my $k (keys %oids) {
	$oids{$k} = '.'.$oids{$k};
}
my $module_conf_file;
if( ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		$xml_str .= check_md_storage($h->{'isactive'}, $expire, $h->{'system_name'}, $h->{'hostname'}, 
					$h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
	}
}
else {
	### override defaults if there is a corresponding conf file
	$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$data->{'record'}->{'system_name'}.conf";
	if (-f $module_conf_file) {
		require $module_conf_file;
	}
	$xml_str = check_md_storage($data->{'record'}->{'isactive'}, $expire, $data->{'record'}->{'system_name'}, 
				$data->{'record'}->{'hostname'}, $data->{'record'}->{'snmp_version'}, $data->{'record'}->{'community'},
				$data->{'record'}->{'username'}, $data->{'record'}->{'password'});
}
unlock_check($check_name);
print $xml_str;
