#!/usr/bin/perl -w 
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;
use SisIYA_Remote_Config;
use XML::Simple;
use SNMP;

my $check_name = 'carel';

if( $#ARGV != 1 ) {
	print "Usage : $0 ".$check_name."_systems.xml expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

if(-f $SisIYA_Remote_Config::local_conf) {
	require $SisIYA_Remote_Config::local_conf;
}
#if(-f $SisIYA_Remote_Config::client_conf) {
#	require $SisIYA_Remote_Config::client_conf;
#}
if(-f $SisIYA_Remote_Config::client_local_conf) {
	require $SisIYA_Remote_Config::client_local_conf;
}
if(-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
if(-f $SisIYA_Remote_Config::functions) {
	require $SisIYA_Remote_Config::functions;
}

###########################################################################################################
# default values
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %oids = (
	'unit_status'				=> '1.3.6.1.4.1.9839.2.1.2.16.0',
	# temperature
	'setpoint_temperature'			=> '1.3.6.1.4.1.9839.2.1.2.10.0',
	'room_temperature'			=> '1.3.6.1.4.1.9839.2.1.2.1.0',
	#'outdoor_temperature'			=> '1.3.6.1.4.1.9839.2.1.2.2.0',
	'supply_temperature'			=> '1.3.6.1.4.1.9839.2.1.2.3.0',
	#'chilled_water_temperature'		=> '1.3.6.1.4.1.9839.2.1.2.4.0',
	'outdoor_temperature'			=> '1.3.6.1.4.1.9839.2.1.2.4.0',
	'hot_water_temperature'			=> '1.3.6.1.4.1.9839.2.1.2.5.0',
	# temperature thresholds
	'room_temperature_high_threshold'	=> '1.3.6.1.4.1.9839.2.1.2.26.0',
	'room_temperature_low_threshold'	=> '1.3.6.1.4.1.9839.2.1.2.27.0',
	# valve
	'valve_heat_percent'			=> '1.3.6.1.4.1.9839.2.1.2.26.0',
	'valve_cool_percent'			=> '1.3.6.1.4.1.9839.2.1.2.27.0',
	'valve_reheat_percent'			=> '1.3.6.1.4.1.9839.2.1.2.32.0',
	# humidity
	'setpoint_humidity'			=> '1.3.6.1.4.1.9839.2.1.2.44.0',
	'outdoor_humidity'			=> '1.3.6.1.4.1.9839.2.1.2.34.0',
	'room_humidity'				=> '1.3.6.1.4.1.9839.2.1.2.40.0',
	# fan speeds
	'setpoint_fanspeed'			=> '1.3.6.1.4.1.9839.2.1.3.50.0',
	'suction_fanspeed'			=> '1.3.6.1.4.1.9839.2.1.3.58.0',
	'supply_fanspeed'			=> '1.3.6.1.4.1.9839.2.1.3.59.0',
	#
	'fresh_air'				=> '1.3.6.1.4.1.9839.2.1.2.18.0',
	'recirculation'				=> '1.3.6.1.4.1.9839.2.1.2.12.0',
);
our @tsensors = (
	{ 'name' => 'internal', 'warning' => 30, 'error' => 35, 'mib' => '1.3.6.1.4.1.935.1.1.1.2.1.2.0' },
	{ 'name' => 'external', 'warning' => 30, 'error' => 35, 'mib' => '1.3.6.1.4.1.318.1.1.10.2.3.2.1.4.1' }
);
our %thresholds = (
	'battery_capacity' 		=> { 'warning' => 90,	'error' => 85 },	# in capacity
	'input_frequency_lower'		=> { 'warning' => 47,	'error' => 40 },	# in Hz
	'input_frequency_upper'		=> { 'warning' => 53,	'error' => 60 },	# in Hz
	'output_frequency_lower'	=> { 'warning' => 47,	'error' => 40 },	# in Hz
	'output_frequency_upper'	=> { 'warning' => 53,	'error' => 60 },	# in Hz
	'output_load' 			=> { 'warning' => 45,	'error' => 50 },	# in %
	'input_voltage_lower'		=> { 'warning' => 205,	'error' => 200 },	# in voltage
	'input_voltage_upper'		=> { 'warning' => 235,	'error' => 240 },	# in volatge
	'output_voltage_lower'		=> { 'warning' => 205,	'error' => 200 },	# in voltage
	'output_voltage_upper'		=> { 'warning' => 235,	'error' => 240 },	# in voltage
	'estimated_time_on_battery'	=> { 'warning' => 600,	'error' => 300 },	# in minutes
	'time_spend_on_battery' 	=> { 'warning' => 600,	'error' => 300 }	# in minutes
);
# One can override there default values in the $SisIYA_RemoteConfig::conf_dir/printer_system_$system_name.pl
# end of default values
############################################################################################################
sub check_carel_fans
{
	my ($expire, $snmp_session) = @_;
	my $serviceid = get_serviceid('fanspeed');
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
       	my $statusid;

	my $x = get_snmp_value2($snmp_session, $oids{'supply_fanspeed'});
	if ($x eq '') {
		return '';
	}
	$x = $x * 100;
	$ok_str .= "OK: Supply fanspeed is $x m3/h";	

	$x = get_snmp_value2($snmp_session, $oids{'suction_fanspeed'});
	if ($x eq '') {
		return '';
	}
	$x = $x * 100;
	$ok_str .= "OK: Suction fanspeed is $x m3/h";	

	$x = get_snmp_value2($snmp_session, $oids{'setpoint_fanspeed'});
	if ($x eq '') {
		return '';
	}
	$x = $x * 100;
	$info_str .= "INFO: Set point fanspeed is $x m3/h";	

	$x = get_snmp_value2($snmp_session, $oids{'fresh_air'});
	if ($x eq '') {
		return '';
	}
	$info_str .= "INFO: Fresh air is $x%";	

	$x = get_snmp_value2($snmp_session, $oids{'recirculation'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Recirculation is $x%";	

	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_carel_humidity
{
	my ($expire, $snmp_session) = @_;
	my $serviceid = get_serviceid('humidity');
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
       	my $statusid;

	my $x = get_snmp_value2($snmp_session, $oids{'room_humidity'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$ok_str .= "OK: Room humidity is $x%.";	

	$x = get_snmp_value2($snmp_session, $oids{'outdoor_humidity'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$ok_str .= "OK: Outdoor humidity is $x%.";	

	$x = get_snmp_value2($snmp_session, $oids{'setpoint_humidity'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Set point humidity is $x%.";	

	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_carel_temperature
{
	my ($expire, $snmp_session) = @_;
	my $serviceid = get_serviceid('temperature');
	my $error_str = '';
	my $warning_str = '';
	my $ok_str = '';
	my $info_str = '';
       	my $statusid;

	my $x = get_snmp_value2($snmp_session, $oids{'room_temperature'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;

	my $error_temp = get_snmp_value2($snmp_session, $oids{'room_temperature_high_threshold'});
	if ($error_temp eq '') {
		return '';
	}
	$error_temp = $error_temp / 10;

	my $warning_temp = get_snmp_value2($snmp_session, $oids{'room_temperature_low_threshold'});
	if ($warning_temp eq '') {
		return '';
	}
	$warning_temp = $warning_temp / 10;

	$ok_str .= "OK: Room temperature is $x Celcius.";	

	$x = get_snmp_value2($snmp_session, $oids{'outdoor_temperature'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$ok_str .= "OK: Outdoor temperature is $x Celcius.";	

	$x = get_snmp_value2($snmp_session, $oids{'supply_temperature'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$ok_str .= "OK: Supply temperature is $x Celcius.";	

	#$x = get_snmp_value2($snmp_session, $oids{'chilled_water_temperature'});
	#if ($x eq '') {
	#	return '';
	#}
	#$x = $x / 10;
	#$ok_str .= "OK: Chilled water temperature is $x Celcius.";	

	#$x = get_snmp_value2($snmp_session, $oids{'hot_water_temperature'});
	#if ($x eq '') {
	#	return '';
	#}
	#$x = $x / 10;
	#$ok_str .= "OK: Hot water temperature is $x Celcius.";	

	$x = get_snmp_value2($snmp_session, $oids{'unit_status'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Unit status code is $x.";	

	$x = get_snmp_value2($snmp_session, $oids{'setpoint_temperature'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Set point temperature is $x Celcius.";	

	$x = get_snmp_value2($snmp_session, $oids{'valve_heat_percent'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Valve heat is $x%.";	

	$x = get_snmp_value2($snmp_session, $oids{'valve_reheat_percent'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Valve reheat is $x%.";	

	$x = get_snmp_value2($snmp_session, $oids{'valve_cool_percent'});
	if ($x eq '') {
		return '';
	}
	$x = $x / 10;
	$info_str .= "INFO: Valve cool is $x%.";	

	$statusid = $SisIYA_Config::statusids{'ok'};
	if ($error_str ne '') {
		$statusid = $SisIYA_Config::statusids{'error'};
	} elsif ($warning_str ne '') {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}
	return "<message><serviceid>$serviceid</serviceid><statusid>$statusid</statusid><expire>$expire</expire><data><msg>$error_str$warning_str$ok_str$info_str</msg><datamsg></datamsg></data></message>";
}


sub check_carel
{
	my ($expire, $system_name, $snmp_session) = @_;

	#print STDERR "Checking system_name=[$system_name] ...\n";
	my $s = check_snmp_system2($expire, $snmp_session);
	if ($s eq '') {
		return '';
	}
	$s .= check_carel_temperature($expire, $snmp_session);
	$s .= check_carel_humidity($expire, $snmp_session);
	$s .= check_carel_fans($expire, $snmp_session);
	return "<system><name>$system_name</name>$s</system>";
}

my ($systems_file, $expire) = @ARGV;
my $xml = new XML::Simple;
my $data = $xml->XMLin($systems_file);
my $xml_str = '';
my $snmp_session;

if (lock_check($check_name) == 0) {
	print STDERR "Could not get lock for $check_name! The script must be running!\n";
	exit 1;
}
my $module_conf_file;
if (ref($data->{'record'}) eq 'ARRAY' ) {
	foreach my $h (@{$data->{'record'}}) {
		if ($h->{'isactive'} eq 'f' ) {
			next;
		}
		$snmp_session = snmp_init($h->{'hostname'}, $h->{'snmp_version'}, $h->{'community'}, $h->{'username'}, $h->{'password'});
		if ($snmp_session == 0) {
			next;
		}
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$h->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		$xml_str .= check_carel($expire, $h->{'system_name'}, $snmp_session);
	}
}
else {
	if ($data->{'record'}->{'isactive'} eq 't' ) {
		$snmp_session = snmp_init($data->{'record'}->{'hostname'}, $data->{'record'}->{'snmp_version'}, $data->{'record'}->{'community'},
					$data->{'record'}->{'username'}, $data->{'record'}->{'password'});	
		### override defaults if there is a corresponding conf file
		$module_conf_file = "$SisIYA_Remote_Config::conf_d_dir/${check_name}_$data->{'record'}->{'system_name'}.conf";
		if (-f $module_conf_file) {
			require $module_conf_file;
		}
		if ($snmp_session != 0) {
			$xml_str = check_carel($expire, $data->{'record'}->{'system_name'}, $snmp_session);
		}
	}
}
unlock_check($check_name);
print $xml_str;
