##################################################################################################
### These are the default values for this script. Uncomment and change
### them according to your needs. This is a powershell code.
##################################################################################################
#$ok_list = @("Normal")
#$warning_list = @("DoorOpen", "Jammed", "LowPaper", "NoPaper", "PaperOut", "TonerLow", "Other", "Unknown")
#$error_list = @("Offline", "Service Requested")
##################################################################################################
