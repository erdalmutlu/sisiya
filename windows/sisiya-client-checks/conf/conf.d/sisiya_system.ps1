##################################################################################################
### These are the default values for this script. Uncomment and change
### them according to your needs. This is a powershell code.
##################################################################################################
### The format of error and warning uptimes is
### 1) If the value is a number, then it is the number of days.
### 2) If the value is of the form d:hh:mm, then it is d days hh hours mm minutes.
### 3) warning_uptime must be greater than error_uptime
##
## If $max_uptimes{'error'} is > 0 then check is reversed so that max
## up time is checked insted of up times. In short servers should not be
## restarted often, but client computers (desktops & laptops) should not be running
## too much.
############################################################################################################
### the default values
#$error_uptime = "1"
#$warning_uptime = "3"
#$max_error_uptime = "0"
#$max_warning_uptime = "0"
##################################################################################################
