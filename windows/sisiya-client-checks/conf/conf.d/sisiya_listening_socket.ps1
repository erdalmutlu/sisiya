##################################################################################################
### These are the default values for this script. Uncomment and change
### them according to your needs. This is a powershell code.
##################################################################################################
### the default values
# There are no default values for this check! Uncomment and specify sockets that should 
# be check in the sockets array bellow :
#
#$sockets = @( 	
#	@{ 'progname' = 'myserver1'; 'description' = 'My special server1'; 'onerror' = 'error'; 'port' = 45566; 'protocol' = 'tcp'; 'interface' = '0.0.0.0' },
#	@{ 'progname' = 'myserver2'; 'description' = 'My special server2'; 'onerror' = 'warn';  'port' = 45567; 'protocol' = 'udp'; 'interface' = '0.0.0.0' }
#	)
### end of the default values
############################################################################################################
