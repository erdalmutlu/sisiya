############################################################################################################
#
#    Copyright (C) 2003 - 2014  Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
############################################################################################################
$prog_name = $MyInvocation.MyCommand.Name
if ($Args.Length -lt 2) {
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire" 
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire output_file" 
	Write-Host "The expire parameter must be given in minutes."
	exit
} 

$conf_file = $Args[0]
$expire = $Args[1]
if ([System.IO.File]::Exists($conf_file) -eq $False) {
	Write-Host $prog_name ": SisIYA configuration file " $conf_file " does not exist!"
	exit
}
[string]$output_file = ""
if ($Args.Length -eq 3) {
	$output_file = $Args[2]
}
### get configuration file included
. $conf_file 

if([System.IO.File]::Exists($local_conf_file) -eq $False) {
	Write-Output "SisIYA local configurations file " $local_conf_file " does not exist!" | eventlog_error
	exit
}
### get SisIYA local configurations file included
. $local_conf_file 

if ([System.IO.File]::Exists($sisiya_functions) -eq $False) {
#if(test-path $conf_file -eq $False) {
	Write-Output "SisIYA functions file " $sisiya_functions " does not exist!" | eventlog_error
	exit
}
### get common functions
. $sisiya_functions
### Module configuration file name. It has the same name as the script, because of powershell's include system, but 
### it is located under the $conf_d_dir directory.
$module_conf_file = $conf_d_dir + "\" + $prog_name
$data_message_str = ''
############################################################################################################
############################################################################################################
###
function getAdditionalInfo()
{
	$s = ""
	if (getAntivirusInfoForSymantec ([ref]$s)) {
		return $s
	}
	if (getAntivirusInfoForMSForefront ([ref]$s)) {
		return $s
	}
	if (getAntivirusInfoForMSSecurityClient ([ref]$s)) {
		return $s
	}
	if (getAntivirusInfoForAvira ([ref]$s)) {
		return $s
	}
	return ""
}

function getAntivirusInfoForMSForefront
{
	param([ref]$info_str)

	# Namespace
	#EE98922AA7EA8F240A0CC999FC6B44BF
	
	$a = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Microsoft Forefront\Client Security\1.0\AM" -ErrorAction "SilentlyContinue"
	if(! $a) {
		return $false
	}

	$b = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Microsoft Forefront\Client Security\1.0\AM\Signature Updates" 2> $null
	if(! $b) {
		return $false
	}
	$s = "INFO: Engine version "+ $b.EngineVersion
	$s += ". Antivirus signature version " + $b.AVSignatureVersion
	$s += ". Antispyware signature version " + $b.ASSignatureVersion

	if ($a.ProductUpdateAvailable -eq "") { 
		$s += ". No available product update."
	}
	else {
		$s += ". This product needs update!"
	}
	$a = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\UserData\S-1-5-18\Products\EE98922AA7EA8F240A0CC999FC6B44BF\InstallProperties"
	if ($a) {
		$s = $a.DisplayName + " : Client version " + $a.DisplayVersion + " " + $s
	}
	$info_str.value += $s
	#write-host "info_str=$info_str"
	return $true
}

function getAntivirusInfoForMSSecurityClient
{
	param([ref]$info_str)

	$a = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Microsoft Antimalware\Signature Updates" 2> $null
	if(! $a) {
		return $false
	}
	$info_str.value += "INFO: Engine version: " + $a.EngineVersion + " Antivirus signature: " +  $a.AVSignatureVersion 
	$info_str.value += ". Antispyware signature: " +  $a.ASSignatureVersion 
	$info_str.value += ". Network Inspection System Engine: " + $a.NISEngineVersion + " Network Inspection System Signature: " + $a.NISSignatureVersion 

	#write-host "info_str=$info_str"
	return $true
	
}

function getAntivirusInfoForAvira
{
	param([ref]$info_str)

	# Namespace
	#305CA226-D286-468e-B848-2B2E8E697B74
	### 32 bit systems
	$a = Get-ItemProperty "HKLM:\SOFTWARE\Avira\AntiVir Desktop" -ErrorAction "SilentlyContinue"
	if (! $a) {
		# check the 32 bit software on 64 bit system
		$a = Get-ItemProperty "HKLM:\SOFTWARE\Wow6432Node\Avira\AntiVir Desktop" -ErrorAction "SilentlyContinue"
		if (! $a) {
			return $false
		}
	}
	$info_str.value += "INFO: Engine version " + $a.EngineVersion + ". Antivirus signature version " + $a.VdfVersion
	$info_str.value += ". Update available " + $a.ProductUpdateAvailable + "."
	#write-host $info_str
	return $true
	
}

function getAntivirusInfoForSymantec
{
	param([ref]$info_str)

	$a = Get-ItemProperty "HKLM:\SOFTWARE\Symantec\Symantec Endpoint Protection\CurrentVersion" -ErrorAction "SilentlyContinue"
	if (! $a) {
		return $false
	}
	$s = "INFO: " + $a.ProductName
	$a = Get-ItemProperty "HKLM:\SOFTWARE\Symantec\Symantec Endpoint Protection\CurrentVersion\public-opstate" -ErrorAction "SilentlyContinue"
	if (! $a) {
		return $false
	}
	$s += " : Client version " + $a.DeployRunningVersion
	$s +=". Latest virus definitions date is " + $a.LatestVirusDefsDate
	$s += " and revision is " + $a.LatestVirusDefsRevision
	$s += ". Antivirus running status is " + $a.AVRunningStatus
	$s += ". Antispyware running status is " + $a.ASRunningStatus
	$s += ". Content download health is " + $a.ContentDownloadHealth
	$s += ". Infected status is " + $a.Infected
	$s += ". Last successful scan date and time is " + $a.LastSuccessfulScanDateTime + "."
	$info_str.value = $s
	return $true
}

function checkSymantec
{
	param(
		[ref]$info_str,
		[ref]$ok_str,
		[ref]$warning_str,
		[ref]$error_str
	)
	
	$a = Get-ItemProperty -Path "HKLM:\SOFTWARE\Symantec\Symantec Endpoint Protection\CurrentVersion\public-opstate" -ErrorAction "SilentlyContinue"
	if ($a) {
		$today = Get-Date
		$n = (new-timespan -start $a.LatestVirusDefsDate -end $today).days
		if ($n -lt 3) {
			$ok_str.value += "OK: The antivirus software is uptodate."
		} else {
			$error_str.value += "ERROR: The antivirus software is out of date!"
		}
		if ($a.AVRunningStatus -eq 1) {
			$ok_str.value += "OK: Antivirus running status is OK."
		} else {
			$error_str.value += "ERROR: Antivirus running status is not OK! Staus = " + $a.AVRunningStatus
		}
		if ($a.ASRunningStatus -eq 1) {
			$ok_str.value += "OK: Antispyware running status is OK."
		} else {
			$error_str.value += "ERROR: Antispyware running status is not OK! Staus = " + $a.ASRunningStatus
		}
		if ($a.ContentDownloadHealth -eq 1) {
			$ok_str.value += "OK: Content download health status is OK."
		} else {
			$error_str.value += "ERROR: Content download health status is not OK! Staus = " + $a.ContentDownloadHealth
		}
		if ($a.Infected -eq 0) {
			$ok_str.value += "OK: The system is not infected."
		} else {
			$error_str.value = "ERROR: The system is infected! Staus = " + $a.Infected
		}
		if ($n -eq 1) {
			$info_str.value += "INFO: Virus definitions are 1 day old."
		}
		if ($n -gt 1) {
			$info_str.value += "INFO: Virus definitions are " + $n + " days old."
		}
	}
}

function getStatusFromSecurityCenter
{
	param(
		[ref]$info_str, 
		[ref]$ok_str, 
		[ref]$warning_str, 
		[ref]$error_str
	)

	$a = Get-WmiObject -NameSpace root\SecurityCenter -Class AntivirusProduct 2> $null
	if (! $a) {
		return $false
	}
	if ($a.productUptoDate -eq $false) {
		$error_str.value += "ERROR: The antivirus software is not uptodate!"
	}
	else { 
		$ok_str.value += "OK: The antivirus software is uptodate."
	}
	if ($a.onAccessScanningEnabled -eq "True") {
		$ok_str.value += "OK: On access scan is enabled."
	} else {
		$error_str.value += "ERROR: On access scan is disabled!"
		return $false
	}
	return $true
} 

function getStatusFromSecurityCenter2
{
	param(
		[ref]$info_str, 
		[ref]$ok_str, 
		[ref]$warning_str, 
		[ref]$error_str
	)

	$a = Get-WmiObject -NameSpace root\SecurityCenter2 -Class AntivirusProduct 2> $null
	if (! $a) {
		return $false
	}
	if ($a -is [system.array]) {
		# exclude Windows Defender 
		if ($a[0].displayName -eq "Windows Defender") {
			$b = $a[1]
		} else {
			$b = $a[0]
		}
		$a = $b
	}
	$up2date_status = $False	# up to date
	$rp_status = $False		# realtime protection
		
	switch ($a.productState) {
		"262144"	{$up2date_status = $True;	$rp_status = $False	}
		"262160"	{$up2date_status = $False;	$rp_status = $False	}
		"266240"	{$up2date_status = $True;	$rp_status = $True	}
		"266256"	{$up2date_status = $False;	$rp_status = $True	}
		"393216"	{$up2date_status = $True;	$rp_status = $False	}
		"393232"	{$up2date_status = $False;	$rp_status = $False	}
		"393472"	{$up2date_status = $True;	$rp_status = $True	}
		"393488"	{$up2date_status = $False;	$rp_status = $False	}
		"397312"	{$up2date_status = $True;	$rp_status = $True	}
		"397328"	{$up2date_status = $False;	$rp_status = $True	}
		"397584"	{$up2date_status = $False;	$rp_status = $True	}
		"462848"	{$up2date_status = $True;	$rp_status = $True	}
		"462864"	{$up2date_status = $True;	$rp_status = $True	}
		default		{$up2date_status = $False;	$rp_status = $False	}
	}
	if ($up2date_status -eq $True) {
		$ok_str.value += "OK: The antivirus software is uptodate."
	} else {
		$error_str.value += "ERROR: The antivirus software is not uptodate!"
	}

	if ($rp_status -eq $True) {
		$ok_str.value += "OK: The realtime protection is enabled."
	} else {
		$error_str.value += "ERROR: The realtime protection is disabled!"
	}
	return $true
} 
############################################################################################################
$service_name = "antivirus"
############################################################################################################
### the default values
### end of the default values
############################################################################################################
### If there is a module conf file then override these default values
if ([System.IO.File]::Exists($module_conf_file) -eq $True) {
	. $module_conf_file
}
###############################################################################################################################################
$info_str = ""
$ok_str = ""
$error_str = ""
$warning_str = ""

if (getStatusFromSecurityCenter ([ref]$info_str)  ([ref]$ok_str)  ([ref]$warning_str)  ([ref]$error_str)) {
	$info_str += getAdditionalInfo
} elseif (getStatusFromSecurityCenter2 ([ref]$info_str) ([ref]$ok_str) ([ref]$warning_str) ([ref]$error_str)) {
	$info_str += getAdditionalInfo
} else {
	# we are on MS Windows server system, therefore check registry directly
	if (getAntivirusInfoForSymantec ([ref]$info_str)) {
		checkSymantec ([ref]$info_str) ([ref]$ok_str)  ([ref]$warning_str)  ([ref]$error_str)
	} else {
		$info_str = "INFO: This is not a supported antivirus program."
	}
}

$statusid = $statusids.Item("info")
if ($error_str -ne "") {
	$statusid = $statusids.Item("error")
	$message_str = $error_str
}
if ($warning_str -ne "") {
	if ($statusid -lt $statusids.Item("warning")) {
		$statusid = $statusids.Item("warning")
	}
	$message_str += $warning_str
}
if ($ok_str -ne "") {
	if ($statusid -lt $statusids.Item("ok")) {
		$statusid = $statusids.Item("ok")
	}
	$message_str += $ok_str
}
if ($info_str -ne "") {
	$message_str += $info_str
}
	
$data_str = '<entries>'
if ($statusid -gt $statusids.Item("ok")) {
	$data_str += '<entry name="protected" type="boolean">0</entry>'
} else {
	$data_str += '<entry name="is_uptodate" type="boolean">1</entry>'
}
$data_str += '</entries>'

###############################################################################################################################################
print_and_exit "$FS" "$service_name" $statusid "$message_str" "$data_str"
###############################################################################################################################################
