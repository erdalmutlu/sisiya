############################################################################################################
#
#    Copyright (C) 2003 - 2014  Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
############################################################################################################
$prog_name = $MyInvocation.MyCommand.Name
if ($Args.Length -lt 2) {
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire" 
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire output_file" 
	Write-Host "The expire parameter must be given in minutes."
	exit
} 

$conf_file = $Args[0]
$expire = $Args[1]
if ([System.IO.File]::Exists($conf_file) -eq $False) {
	Write-Host $prog_name ": SisIYA configuration file " $conf_file " does not exist!"
	exit
}
[string]$output_file = ""
if ($Args.Length -eq 3) {
	$output_file = $Args[2]
}
### get configuration file included
. $conf_file 

if([System.IO.File]::Exists($local_conf_file) -eq $False) {
	Write-Output "SisIYA local configurations file " $local_conf_file " does not exist!" | eventlog_error
	exit
}
### get SisIYA local configurations file included
. $local_conf_file 

if ([System.IO.File]::Exists($sisiya_functions) -eq $False) {
#if(test-path $conf_file -eq $False) {
	Write-Output "SisIYA functions file " $sisiya_functions " does not exist!" | eventlog_error
	exit
}
### get common functions
. $sisiya_functions
### Module configuration file name. It has the same name as the script, because of powershell's include system, but 
### it is located under the $conf_d_dir directory.
$module_conf_file = $conf_d_dir + "\" + $prog_name
$data_message_str = ''
############################################################################################################
############################################################################################################
$service_name = "battery"
############################################################################################################
### the default values
### end of the default values
############################################################################################################
### If there is a module conf file then override these default values
if ([System.IO.File]::Exists($module_conf_file) -eq $True) {
#if(test-path $module_conf_file -eq $True) {
	. $module_conf_file
}
###############################################################################################################################################
$statusid = $statusids.Item("ok")
$message_str = ''

$info_message_str = ''
$ok_message_str = ''
$warning_message_str = ''
$error_message_str = ''
		
$Availabilities = @{
	1 = "Other"
	2 = "Unknown"
	3 = "Running or Full Power"
	4 = "Warning"
	5 = "In Test"
	6 = "Not Applicable"
	7 = "Power Off"
	8 = "Off Line"
	9 = "Off Duty"
	10 = "Degraded"
	11 = "Not Installed"
	12 = "Install Error"
	13 = "Power Save - Unknown"
	14 = "Power Save - Low Power Mode"
	15 = "Power Save - Standby"
	16 = "Power Cycle"
	17 = "Power Save - Warning"
}

$BatteryStatuses = @{
	1 = "Discharging"
	2 = "On A/C"
	3 = "Fully Charged"
	4 = "Low"
	5 = "Critical"
	6 = "Charging"
	7 = "Charging High"
	8 = "Charging Low"
	9 = "Charging Critical"
	10 = "Undefined"
	11 = "Partially Charged"
}

$Chemistries = @{
	1 = "Other"
	2 = "Unknown"
	3 = "Lead Acid"
	4 = "Nickel Cadmium"
	5 = "Nickel Metal Hydride"
	6 = "Lithium-ion"
	7 = "Zinc air"
	8 = "Lithium Polimer"
}

function getHashString
{
	Param ([hashtable]$h, [int]$x)

	if ($x -gt $h.Count) {
		$str = 'Undefined'
	} else {
		$str = $h[$x]
	}
	Write-Output $str
}
##################################################################################
$batteries = Get-WmiObject win32_battery 2>$null #| select Availability,EstimatedRunTime,EstimatedChargeRemaining,BatteryStatus
if (-Not $batteries) {
	### do nothing, this system does not support ACPI batteries
	Exit
	#$error_message_str = "ERROR: Could not get battery information!"
} else {
	$data_str = ''
	foreach ($b in $batteries) {
		$availability = getHashString $Availabilites $b.Availability
		$battery_status = getHashString $BatteryStatuses $b.BatteryStatus
		$chemistry = getHashString $Chemistries $b.Chemistry

		$s = ' Availability: ' + $availability
		$s += ', battery status : ' + $battery_status
		$s += ', estimated charge remaining: %' + $b.EstimatedChargeRemaining 
		$s += ', chemistry: ' + $chemistry 
		$s += ', estimated run time : ' + $b.EstimatedRunTime + ' minutes' 

		# Status:
		#OK ("OK")
		#Error ("Error")
		#Degraded ("Degraded")
		#Unknown ("Unknown")
		#Pred Fail ("Pred Fail")
		#Starting ("Starting")
		#Stopping ("Stopping")
		#Service ("Service")
		#Stressed ("Stressed")
		#NonRecover ("NonRecover")
		#No Contact ("No Contact")
		#Lost Comm ("Lost Comm")
	
		if ($b.Status -eq 'OK') {
			$ok_message_str += 'OK: ' + $b.Caption + ' status is OK.' + $s
		} else {
			$error_message_str += 'ERROR: ' + $b.Caption + ' status is ' + $b.Status + '(!=OK)!' + $s
		}

		$data_str += '<battery>'
		$data_str += '<name>' + $b.Capiton + '</name>'
		$data_str += '<status>' + $b.Status + '</status>'
		$data_str += '<battery_status>' + $battery_status + '</battery_status>'
		$data_str += '<chemistry>' + $chemistry + '</chemistry>'
		$data_str += '<charge_remaining>' + $b.EstimatedChargeRemaining + '</charge_remaining>'
		$data_str += '<estimated_time>' + $b.EstimatedRunTime + '</estimated_time>'
		$data_str += '<availability>' + $availability + '</availability>'
		$data_str += '</battery>'
	}
}

if ($data_str -ne '') {
	$data_str = '<entries>' + $data_str + '</entries>'
}

$statusid = $statusids.Item("ok")
if ($error_message_str.Length -gt 0) {
	$statusid = $statusids.Item("error")
}
elseif ($warning_message_str.Length -gt 0) {
	$statusid = $statusids.Item("warning")
}
$error_message_str = $error_message_str.Trim()
$warning_message_str = $warning_message_str.Trim()
$ok_message_str = $ok_message_str.Trim()
$info_message_str = $info_message_str.Trim()
$message_str = $error_message_str + " " + $warning_message_str + " " + $ok_message_str + " " + $info_message_str
#######################################################################################

#######################################################################################
# Get_WmiObject win32_battery
#######################################################################################
#
#__GENUS                     : 2
#__CLASS                     : Win32_Battery
#__SUPERCLASS                : CIM_Battery
#__DYNASTY                   : CIM_ManagedSystemElement
#__RELPATH                   : Win32_Battery.DeviceID="3658QCOMPAL PA3465U "
#__PROPERTY_COUNT            : 33
#__DERIVATION                : {CIM_Battery, CIM_LogicalDevice, CIM_LogicalElement, CIM_ManagedSystemElement}
#__SERVER                    : WALT04-DU
#__NAMESPACE                 : root\cimv2
#__PATH                      : \\WALT04-DU\root\cimv2:Win32_Battery.DeviceID="3658QCOMPAL PA3465U "
#Availability                : 2
#BatteryRechargeTime         :
#BatteryStatus               : 2
#Caption                     : Ýç Pil
#Chemistry                   : 2
#ConfigManagerErrorCode      :
#ConfigManagerUserConfig     :
#CreationClassName           : Win32_Battery
#Description                 : Ýç Pil
#DesignCapacity              :
#DesignVoltage               : 11100
#DeviceID                    : 3658QCOMPAL PA3465U
#ErrorCleared                :
#ErrorDescription            :
#EstimatedChargeRemaining    : 100
#EstimatedRunTime            : 71582788
#ExpectedBatteryLife         :
#ExpectedLife                :
#FullChargeCapacity          :
#InstallDate                 :
#LastErrorCode               :
#MaxRechargeTime             :
#Name                        : PA3465U ?h¦
#PNPDeviceID                 :
#PowerManagementCapabilities : {1}
#PowerManagementSupported    : False
#SmartBatteryVersion         :
#Status                      : OK
#StatusInfo                  :
#SystemCreationClassName     : Win32_ComputerSystem
#SystemName                  : WALT04-DU
#TimeOnBattery               :
#TimeToFullCharge            :
#######################################################################################
#Get-WmiObject -NameSpace "root\WMI" -Class "BatteryStatus"
#######################################################################################
# 
#
# __GENUS            : 2
# __CLASS            : BatteryStatus
# __SUPERCLASS       : MSBatteryClass
# __DYNASTY          : CIM_StatisticalInformation
# __RELPATH          : BatteryStatus.InstanceName="ACPI\\PNP0C0A\\1_0"
# __PROPERTY_COUNT   : 20
# __DERIVATION       : {MSBatteryClass, Win32_PerfRawData, Win32_Perf, CIM_StatisticalInformation}
# __SERVER           : WALT04-DU
# __NAMESPACE        : root\WMI
# __PATH             : \\WALT04-DU\root\WMI:BatteryStatus.InstanceName="ACPI\\PNP0C0A\\1_0"
# Active             : True
# Caption            :
# ChargeRate         : 0
# Charging           : True
# Critical           : False
# Description        :
# DischargeRate      : 0
# Discharging        : False
# Frequency_Object   :
# Frequency_PerfTime :
# Frequency_Sys100NS :
# InstanceName       : ACPI\PNP0C0A\1_0
# Name               :
# PowerOnline        : True
# RemainingCapacity  : 56044
# Tag                : 1
# Timestamp_Object   :
# Timestamp_PerfTime :
# Timestamp_Sys100NS :
# Voltage            : 11100
#
#######################################################################################

#######################################################################################
#Get-WmiObject -NameSpace "root\WMI" -class "BatteryFullChargedCapacity"
#######################################################################################
#
#
#__GENUS             : 2
#__CLASS             : BatteryFullChargedCapacity
#__SUPERCLASS        : MSBatteryClass
#__DYNASTY           : CIM_StatisticalInformation
#__RELPATH           : BatteryFullChargedCapacity.InstanceName="ACPI\\PNP0C0A\\1_0"
#__PROPERTY_COUNT    : 13
#__DERIVATION        : {MSBatteryClass, Win32_PerfRawData, Win32_Perf, CIM_StatisticalInformation}
#__SERVER            : WALT04-DU
#__NAMESPACE         : root\WMI
#__PATH              : \\WALT04-DU\root\WMI:BatteryFullChargedCapacity.InstanceName="ACPI\\PNP0C0A\\1_0"
#Active              : True
#Caption             :
#Description         :
#Frequency_Object    :
#Frequency_PerfTime  :
#Frequency_Sys100NS  :
#FullChargedCapacity : 56610
#InstanceName        : ACPI\PNP0C0A\1_0
#Name                :
#Tag                 : 1
#Timestamp_Object    :
#Timestamp_PerfTime  :
#Timestamp_Sys100NS  :
#
#######################################################################################
###############################################################################################################################################
print_and_exit "$FS" "$service_name" $statusid "$message_str" "$data_str"
###############################################################################################################################################
