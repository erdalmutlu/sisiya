############################################################################################################
#
#    Copyright (C) 2003 - 2014  Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
############################################################################################################
$prog_name = $MyInvocation.MyCommand.Name
if ($Args.Length -lt 2) {
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire" 
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire output_file" 
	Write-Host "The expire parameter must be given in minutes."
	exit
} 

$conf_file = $Args[0]
$expire = $Args[1]
if ([System.IO.File]::Exists($conf_file) -eq $False) {
	Write-Host $prog_name ": SisIYA configuration file " $conf_file " does not exist!"
	exit
}
[string]$output_file = ""
if ($Args.Length -eq 3) {
	$output_file = $Args[2]
}
### get configuration file included
. $conf_file 

if([System.IO.File]::Exists($local_conf_file) -eq $False) {
	Write-Output "SisIYA local configurations file " $local_conf_file " does not exist!" | eventlog_error
	exit
}
### get SisIYA local configurations file included
. $local_conf_file 

if ([System.IO.File]::Exists($sisiya_functions) -eq $False) {
#if(test-path $conf_file -eq $False) {
	Write-Output "SisIYA functions file " $sisiya_functions " does not exist!" | eventlog_error
	exit
}
### get common functions
. $sisiya_functions
### Module configuration file name. It has the same name as the script, because of powershell's include system, but 
### it is located under the $conf_d_dir directory.
$module_conf_file = $conf_d_dir + "\" + $prog_name
$data_message_str = ''
############################################################################################################
############################################################################################################
$service_name = "users"
############################################################################################################
### the default values
$exceptions = @("SYSTEM", "Local Service", "LOCAL SERVICE", "NETWORK SERVICE", "ANONYMOUS LOGON")
#$exceptions=""
### end of the default values
############################################################################################################
### If there is a module conf file then override these default values
if ([System.IO.File]::Exists($module_conf_file) -eq $True) {
	. $module_conf_file
}
###############################################################################################################################################
function isException()
{
	Param([String]$service_str)
	
	for($i = 0; $i -lt $exceptions.Count; $i++) {
		if($service_str -eq $exceptions[$i]) {
			return $True
		}
	}
	return $False
}

$statusid = $statusids.Item("info")

#write-output "exceptions : count " + $exceptions.count + " exceptions = " + $exceptions

### get workgroup name
#$workgroup = (Get-WmiObject -Class Win32_ComputerSystem).workgroup
$workgroup = (Get-WmiObject -Class Win32_ComputerSystem).domain
#$username = [Environment]::UserName

$users = ''
$other_users =''
$a = Get-WmiObject Win32_LoggedOnUser | Select Antecedent -Unique | findstr "\\"

$data_str = '' 
for ($i = 0; $i -lt $a.Count; $i++) {
	$user = $a[$i].Split('"')[3]
	if ($exceptions -cnotcontains $user) {
		if($users.Length -eq 0) {
			$users = $user
		}
		else {
			$users += ', ' + $user
			$data_str += '<entry name="username" type="string">' + $user + '</entry>'
		}
	} else {
		if($other_users.Length -eq 0) {
			$other_users = $user
		}
		else {
			$other_users += ', ' + $user
		}
	}	
}
if ($data_str -ne '') {
	$data_str = '<entries>' + $data_str + '</entries>'
}

if ($users -ne '') {
	$message_str = "INFO: Users: " + $users
}
if ($other_users -ne '') {
	$message_str += "INFO: Other users: " + $other_users
}
$message_str += "INFO: Workgroup: " + $workgroup
###############################################################################################################################################
print_and_exit "$FS" "$service_name" $statusid "$message_str" "$data_str"
###############################################################################################################################################
