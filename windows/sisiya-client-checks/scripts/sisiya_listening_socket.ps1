############################################################################################################
#
#    Copyright (C) 2003 - 2014  Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
############################################################################################################
$prog_name = $MyInvocation.MyCommand.Name
if ($Args.Length -lt 2) {
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire" 
	Write-Host "Usage: " $prog_name " SisIYA_Config.ps1 expire output_file" 
	Write-Host "The expire parameter must be given in minutes."
	exit
} 

$conf_file = $Args[0]
$expire = $Args[1]
if ([System.IO.File]::Exists($conf_file) -eq $False) {
	Write-Host $prog_name ": SisIYA configuration file " $conf_file " does not exist!"
	exit
}
[string]$output_file = ""
if ($Args.Length -eq 3) {
	$output_file = $Args[2]
}
### get configuration file included
. $conf_file 

if([System.IO.File]::Exists($local_conf_file) -eq $False) {
	Write-Output "SisIYA local configurations file " $local_conf_file " does not exist!" | eventlog_error
	exit
}
### get SisIYA local configurations file included
. $local_conf_file 

if ([System.IO.File]::Exists($sisiya_functions) -eq $False) {
#if(test-path $conf_file -eq $False) {
	Write-Output "SisIYA functions file " $sisiya_functions " does not exist!" | eventlog_error
	exit
}
### get common functions
. $sisiya_functions
### Module configuration file name. It has the same name as the script, because of powershell's include system, but 
### it is located under the $conf_d_dir directory.
$module_conf_file = $conf_d_dir + "\" + $prog_name
$data_message_str = ''
############################################################################################################
############################################################################################################
$service_name = "listening_socket"
############################################################################################################
## There are no default values for this check! Uncomment and specify sockets that should 
# be check in the sockets array bellow :
#
$sockets = @()
#$sockets = @( 	
#	@{ 'progname' = 'myserver1'; 'description' = 'My special server1'; 'onerror' = 'error'; 'port' = 45566; 'protocol' = 'tcp'; 'interface' = '0.0.0.0' },
#	@{ 'progname' = 'myserver2'; 'description' = 'My special server2'; 'onerror' = 'warn';  'port' = 45567; 'protocol' = 'udp'; 'interface' = '0.0.0.0' }
#	)
## the default values
### end of the default values
############################################################################################################
### If there is a module conf file then override these default values
if([System.IO.File]::Exists($module_conf_file) -eq $True) {
#if(test-path $module_conf_file -eq $True) {
	. $module_conf_file
}
###############################################################################################################################################
$message_str = ""
$error_message_str = ""
$warning_message_str = ""
$ok_message_str = ""
$info_message_str = ""

###############################################################
#Get-NetTCPConnection -State Listen
#netstat -ntao 
# (Get-Process -Id $item[-1] -ErrorAction SilentlyContinue).Name 
Function Get-ListeningTCPConnections 
{            
	[cmdletbinding()]            
	param()            
            
	try {            
		$TCPProperties = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()            
		$Connections = $TCPProperties.GetActiveTcpListeners()            
		foreach($Connection in $Connections) {            
      		  	if($Connection.address.AddressFamily -eq "InterNetwork" ) { 
				$IPType = "IPv4" 
			} else { 
				$IPType = "IPv6" 
			}            
                    
			$OutputObj = New-Object -TypeName PSobject            
			$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalAddress" -Value $connection.Address            
			$OutputObj | Add-Member -MemberType NoteProperty -Name "ListeningPort" -Value $Connection.Port            
			$OutputObj | Add-Member -MemberType NoteProperty -Name "IPV4Or6" -Value $IPType            
			$OutputObj            
		}
	} catch {            
    		Write-Error "Failed to get listening connections. $_"            
	}           
}

function Get-NetworkStatistics 
{ 
	$properties = 'Protocol','LocalAddress','LocalPort' 
	$properties += 'RemoteAddress','RemotePort','State','ProcessName','PID'

	netstat -ano | Select-String -Pattern '\s+(TCP|UDP)' | ForEach-Object {
		$item = $_.line.split(" ", [System.StringSplitOptions]::RemoveEmptyEntries)

		if($item[1] -notmatch '^\[::') {            
			if (($la = $item[1] -as [ipaddress]).AddressFamily -eq 'InterNetworkV6') { 
				$localAddress = $la.IPAddressToString 
				$localPort = $item[1].split('\]:')[-1] 
			} else { 
				$localAddress = $item[1].split(':')[0] 
				$localPort = $item[1].split(':')[-1] 
			} 

			if (($ra = $item[2] -as [ipaddress]).AddressFamily -eq 'InterNetworkV6') { 
				$remoteAddress = $ra.IPAddressToString 
				$remotePort = $item[2].split('\]:')[-1] 
			} else { 
				$remoteAddress = $item[2].split(':')[0] 
				$remotePort = $item[2].split(':')[-1] 
			} 

			New-Object PSObject -Property @{ 
				PID = $item[-1] 
				ProcessName = (Get-Process -Id $item[-1] -ErrorAction SilentlyContinue).Name 
				Protocol = $item[0] 
				LocalAddress = $localAddress 
				LocalPort = $localPort 
				RemoteAddress =$remoteAddress 
				RemotePort = $remotePort 
				State = if ($item[0] -eq 'tcp') { $item[3]} else {$null} 
			} | Select-Object -Property $properties 
		} 
	} 
}

function Get-ListeningSockets
{ 
	$properties = 'protocol', 'interface', 'port', 'progname'

	netstat -ano | Select-String -Pattern '\s+(TCP|UDP)' | ForEach-Object {
		$item = $_.line.split(" ", [System.StringSplitOptions]::RemoveEmptyEntries)
#$item

		if($item[1] -NotMatch '^\[::') {            
			$la = $item[1] -as [ipaddress] 
#			write-host "local address : " $la
			if ($la.AddressFamily -eq 'InterNetworkV6') { 
				$localAddress = $la.IPAddressToString 
				$localPort = $item[1].split('\]:')[-1] 
			} else { 
				$localAddress = $item[1].split(':')[0] 
				$localPort = $item[1].split(':')[-1] 
			} 

			New-Object PSObject -Property @{ 
				progname = (Get-Process -Id $item[-1] -ErrorAction SilentlyContinue).Name 
				protocol = $item[0] 
				interface = $localAddress 
				port = $localPort 
			} | Select-Object -Property $properties 
		} 
	} 
}

###############################################################
#Get-NetworkStatistics | Format-Table
#$a | get-member
#$a = netstat -nao 
#
if ($sockets.Count -eq 0) {
	### do nothing, no sockets to check
	Exit
}

$data_str = '<entries>'
$a = Get-ListeningSockets
for ($i = 0; $i -lt $sockets.Count; $i++) {
	#write-host "Checking for " $sockets[$i].'progname'
	$found = $False
	for ($j = 0; $j -lt $a.Count; $j++) {
		if($a[$j].'progname' -eq $sockets[$i].'progname' `
			-And $a[$j].'protocol'	-eq ($sockets[$i].'protocol').ToUpper() `
			-And $a[$j].'interface' -eq $sockets[$i].'interface' `
			-And $a[$j].'port'	-eq $sockets[$i].'port' `
		) {
			$found = $True
			break
		}
	}
	if($found) {
		#write-host "FOUND " $sockets[$i].'progname'
		if ($ok_message_str.Length -eq 0) {
			$ok_message_str = $sockets[$i].'description'
		} else {
			$ok_message_str += ", " + $sockets[$i].'description'
		}
	} else {
		#write-host "NOT FOUND " $sockets[$i].'progname'
		if($sockets[$i].'onerror' -eq 'warn') {
			if ($warning_message_str.Length -eq 0) {
				$warning_message_str = $sockets[$i].'description' 
			} else {
				$warning_message_str += ", " + $sockets[$i].'description' 
			}
		} else {
			if ($error_message_str.Length -eq 0) {
				$error_message_str = $sockets[$i].'description' 
			} else {
				$error_message_str += ", " + $sockets[$i].'description' 
			}
		}
	}
	#$data_str += '<entry name="' + $progs[$i]{'description'}.'" type="boolean">'.$flag.'</entry>'
}

$data_str += "</entries>"

$statusid = $statusids.Item("ok")
if ($error_message_str.Length -gt 0) {
	$statusid = $statusids.Item("error")
	$error_message_str = "ERROR: "+ $error_message_str + "!"
}
if ($warning_message_str.Length -gt 0) {
	if($statusid -lt $statusids.Item("warning")) {
		$statusid = $statusids.Item("warning")
	}
	$warning_message_str = "WARNING: "+ $warning_message_str + "!"
}
if ($ok_message_str.Length -gt 0) {
	$ok_message_str = "OK: "+$ok_message_str + "."
}
$message_str = $error_message_str + ' ' + $warning_message_str + ' '+ $ok_message_str
$message_str = $message_str.Trim()
###############################################################################################################################################
print_and_exit "$FS" "$service_name" $statusid "$message_str" "$data_str"
###############################################################################################################################################
