#!/bin/bash
#
#    Copyright (C) 2003 - __YEAR__  Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#
#################################################################################
if test $# -ne 2 ; then
	echo "Usage  : $0 target user_name"
	echo "Example: $0 10.10.10.1 5 [10.11.0.0/24] user1"
	exit 1
fi

target="$1"
user_name="$2"

dir_str="/var/lib/sisiya-webui-php/autodiscover"

#nmap_prog="./nmap_scan_info.sh"
nmap_prog="/usr/share/sisiya-webui-php/utils/nmap_scan_info.php"
pid_file="discover_${user_name}_pid.txt"
target_file="discover_${user_name}_target.txt"
results_file="discover_${user_name}_results.xml"
tmp_file="discover_${user_name}_tmp.xml"
error_file="error.log"

# echo "target=$target user_name=$user_name"

if [ ! -f $nmap_prog ]; then
	echo "$0: No such file $nmap_prog!"
	exit 1
fi

if [ ! -d $dir_str ]; then
	mkdir -p $dir_str
fi

cd $dir_str
if [ $? -ne 0 ]; then
	echo "Could not change into $dir_str!"
	exit 1
fi

touch $error_file

### save pid
echo $$ > $pid_file

### run scan
echo "Starting scan of $target for user $user_name in background. For error check the error log file $erro_file"

echo "$target" > $target_file
$nmap_prog "$target" 2>> $error_file > $tmp_file &

### wait for scan to finish
wait $!

mv $tmp_file $results_file
#cat $results_file
cat $results_file > a.xml

### remove pid file
rm -f $pid_file $target_file

if test ! -s $results_file ; then
	rm -f $results_file
fi
