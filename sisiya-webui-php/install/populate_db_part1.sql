insert into strkeys values(1,'sisiya.records.languages.english','English language');
insert into strkeys values(2,'sisiya.records.languages.turkish','Turkish language');

insert into languages values(0,'sisiya.records.languages.english','en','utf-8');
insert into languages values(1,'sisiya.records.languages.turkish','tr','utf-8');

insert into interface values(0,1,'English');
insert into interface values(1,1,'İngilizce');
insert into interface values(0,2,'Turkish');
insert into interface values(1,2,'Türkçe');

