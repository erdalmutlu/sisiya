#!/usr/bin/env bash
#
# This script is used to be run within Jenkins job to deploy SisIYA packages.
#
#################################################################################
### defaults
project_name="SisIYA"
deploy_status_file_prefix="jenkins/deploy_status"
target_systems="deb rpm pacman"
### end of defaults
#################################################################################
error()
{
	echo "$0: $1"
	exit 1
}

error_without_exit()
{
	echo "$0: $1"
}

cleanup()
{
	# rm -f 
	echo ""
}

check_build_status()
{
	if [ ! -f $build_status_file ]; then
		touch $build_status_file
		return
	fi
	str=`cat $build_status_file | grep "$1" | grep "OK"`
	if [ -n "$str" ]; then
		echo "Docker images for $project_name $1 are already built."
		echo "If you would like to rebuild, please remove the following line from $build_status_file file"
		echo "$1"
		exit 0
	fi
}

record_deploy_status()
{
	tmp_file=`mktemp`
	if [ -f $status_file ]; then
		grep -v "$1" $status_file > $tmp_file
		cp -f $tmp_file $status_file
	else
		touch $status_file
	fi
	# mv does not work across file systems
	rm -f $tmp_file
	echo "${1}:$2" >> $status_file
}

check_deploy_status()
{
	echo "$0: Checking last deploy status for $1..."
	if [ ! -f $status_file ]; then
		touch $status_file
		return
	fi
	str=`cat $status_file | grep "$1" | grep "OK"`
	if [ -n "$str" ]; then
		echo "$0: Packages for $project_name $1 are already deployed."
		echo "$0: If you would like to redeploy, please remove the following line from $status_file file"
		echo "$1"
		return 0
	fi
	return 1
}

deploy()
{
	cd vagrant/deploy || error "cd vagrant/deploy"
	echo "target_systems=$1" > vagrant_deploy.conf
	./vagrant_deploy.sh
}
#################################################################################
trap cleanup EXIT

if [ $# -lt 1 ]; then
	echo "Usage : $0 conf_file [deb|rpm|pacman]"
	exit 1
fi
conf_file=$1
if [ -f "$conf_file" ]; then
	### source the conf file
	. $conf_file
fi
if [ $# -eq 2 ]; then
	target_systems=$2
fi

pwd_dir=`pwd`
last_commit_hash=`git rev-parse  HEAD`
for t in $target_systems
do
	status_file=${deploy_status_file_prefix}_${t}.txt
	check_deploy_status $last_commit_hash
	if [ $? -ne 0 ]; then
		deploy $t
	fi

	retcode=$?
	cd $pwd_dir || error "Could not change into directory: $pwd_dir!"
	if [ $retcode -ne 0 ]; then
		record_deploy_status $last_commit_hash $t "FAILED"
		error "Build failed!"
	else
		record_deploy_status $last_commit_hash $t "OK"
	fi
done
