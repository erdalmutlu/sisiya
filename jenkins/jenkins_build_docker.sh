#!/usr/bin/env bash
#
# This script is used to be run within Jenkins job to build docker images.
#
#################################################################################
### defaults
project_name="sisiya"
number_of_images=2
image_names[0]="${project_name}-api"
image_versions[0]="__FROM_GIT__"
image_names[1]="${project_name}-ui"
image_versions[1]="__FROM_GIT__"
build_status_file="jenkins/build_status.txt"
### end of defaults
#################################################################################
error()
{
	echo "$0: $1"
	exit 1
}

error_without_exit()
{
	echo "$0: $1"
}

cleanup()
{
	# rm -f 
	echo ""
}

check_build_status()
{
	if [ ! -f $build_status_file ]; then
		touch $build_status_file
		return
	fi
	str=`cat $build_status_file | grep "$1" | grep "OK"`
	if [ -n "$str" ]; then
		echo "Docker images for $project_name $1 are already built."
		echo "If you would like to rebuild, please remove the following line from $build_status_file file"
		echo "$1"
		exit 0
	fi
}

get_docker_images()
{
	version_str=`git describe`
	declare -i i=0
	while [ $i -lt $number_of_images ]
	do
		v="${image_versions[$i]}"
		if [ "$v" = "__FROM_GIT__" ]; then
			v=$version_str
		fi
		f="${image_names[$i]}-${v}.tar" 
		echo "$0: Copying image file $f from guest..."
		vagrant scp :$f .
		i=i+1
	done
}

record_build_status()
{
	tmp_file=`mktemp`
	if [ -f $build_status_file ]; then
		grep -v "$1" $build_status_file > $tmp_file
		cp -f $tmp_file $build_status_file
	else
		touch $build_status_file
	fi
	# mv does not work across file systems
	rm -f $tmp_file
	echo "${1}:$2" >> $build_status_file
}

check_build_status()
{
	echo "$0: Checking last build status for $1..."
	if [ ! -f $build_status_file ]; then
		touch $build_status_file
		return
	fi
	str=`cat $build_status_file | grep "$1" | grep "OK"`
	if [ -n "$str" ]; then
		echo "$0: Docker images for $project_name $1 are already built."
		echo "$0: If you would like to rebuild, please remove the following line from $build_status_file file"
		echo "$1"
		exit 0
	fi
}

build_docker()
{
	cd docker || error "Could not change into directory: docker!"
	./build.sh
}
#################################################################################
trap cleanup EXIT

if [ $# -ne 1 ]; then
	echo "Usage : $0 conf_file"
	exit 1
fi
conf_file=$1
if [ -f "$conf_file" ]; then
	### source the conf file
	. $conf_file
fi

pwd_dir=`pwd`
last_commit_hash=`git rev-parse  HEAD`
check_build_status $last_commit_hash
build_docker
retcode=$?
cd $pwd_dir || error "Could not change into directory: $pwd_dir!"
if [ $retcode -ne 0 ]; then
	record_build_status $last_commit_hash "FAILED"
	error "Build failed!"
else
	record_build_status $last_commit_hash "OK"
fi
