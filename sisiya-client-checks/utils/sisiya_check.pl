#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use IO::Socket;
use XML::Simple;
# use Data::Dumper;
use HTTP::Request ();
use MIME::Base64;
use LWP::UserAgent ();
use JSON ();
use SisIYA_Config;
#use diagnostics;

if ( ($#ARGV < 0) || ($#ARGV > 1) ) {
	print "Usage : $0 expire\n";
	print "Usage : $0 check_script expire\n";
	print "The expire parameter must be given in minutes.\n";
	print "When run without check_script parameter all checks which are";
	print "set auto mode in the SisIYA_Config are excecuted.";
	exit 1;
}

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}

# Parameter	: script name, expire
# Return	: xml message string
sub run_script
{
	my ($script_file, $expire) = @_;

	#print STDERR "[$_[0]] ...\n";
	chomp(my $s = `/usr/bin/perl -I$SisIYA_Config::conf_dir $script_file`);
	my $status_id = $? >> 8;
	my @a = split(/$SisIYA_Config::FS/, $s);
	my $service_id = $SisIYA_Config::serviceids{$a[0]};
	# replace ' with \', because it is a problem in the SQL statemnet
	$s = $a[1];
	$s =~ s/'/\\\'/g;
	$s = '<message><serviceid>'.$service_id.'</serviceid><statusid>'.$status_id.'</statusid><expire>'.$expire.'</expire><data>'.$s.'</data></message>';
	#print STDERR "statusid = $status_id serviceid = $service_id message=$s\n";
	return $s;	
}

# Parameter	: Script directory
# Return	: XML string
sub process_checks
{
	my ($expire)  = @_;
	my $s = '';

	foreach my $check_name (keys %SisIYA_Config::checks) {	
		if( $SisIYA_Config::checks{$check_name}{'auto'} == 0 ) {
			next;
		}
		#print STDERR "Checking $check_name ...\n";
		$s .= run_script("$SisIYA_Config::scripts_dir/$SisIYA_Config::checks{$check_name}{'script'}", $expire); 
	}
	return $s;
}

# record the start time
my $date_str = get_timestamp();
my $expire;
my $xml_s_str = '';

if ($#ARGV == 1) {
	$expire = $ARGV[1];
	####$xml_s_str = run_script($ARGV[0], $expire);
	$xml_s_str = run_script("$SisIYA_Config::scripts_dir/$SisIYA_Config::checks{$ARGV[0]}{'script'}", $expire);
} else {
	$expire = $ARGV[0];
	$xml_s_str = process_checks($expire);
}

if ($xml_s_str eq '') {
	print STDERR "There is no SisIYA message to be send!\n";
	exit 1;
}

my $xml_str = '<?xml version="1.0" encoding="utf-8"?>';
$xml_str .= '<sisiya_messages><timestamp>'.$date_str.'</timestamp>';
$xml_str .= '<system><name>'.$SisIYA_Config::hostname.'</name>';
$xml_str .= $xml_s_str;
$xml_str .= '</system></sisiya_messages>';

# replace all occurences of \' with space
$xml_str =~ s/\\'/ /g;

# replace all dupplicate spaces, tabs new line character with a single space character
$xml_str =~ s/[\n\r\s]+/ /g;

#print STDERR $xml_str;
if ($SisIYA_Config::export_to_xml == 1) {
	if (open (my $file, '>', $SisIYA_Config::export_xml_file)) {
		print { $file } $xml_str;
		close($file);
	}
}

if ($SisIYA_Config::send_to_server == 1) {
	send_message_data($xml_str);
}

if ($SisIYA_Config::send_to_api == 1) {
	send_to_api(convert_to_sisiya_json($xml_str));
}

exit 0;
