#!/bin/bash
#
# This script is used to get the temperature from the temperature
# sensors of a raspberry pi system.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
conf_file="/etc/sisiya/sisiya-client-checks/conf.d/raspberry_get_temperature.conf"

#######################################################################################
### default values
prog_str="/usr/share/sisiya-client-checks/utils/raspian_get_humidity_and_temperature.sh"
FS="~"			# field seperator
number_of_sensors=0	# number of sensors
sensor_type[0]="22"	# sensor type
sensor_gpio[0]="2"	# general purpose input output number
### end of default values
#######################################################################################
if  test -f $conf_file ; then
	. $conf_file
fi
if test ! -f $prog_str ; then
	echo "$0: Program $prog_str does not exist!"
	exit 1
fi

declare -i i=0
while test $i -lt $number_of_sensors
do
	stype=${sensor_type[${i}]}
	gpio=${sensor_gpio[${i}]}

	#echo "$prog_str $stype $gpio"
	str=`$prog_str $stype $gpio`
	retcode=$?
	if test $retcode -ne 0 ; then
		echo "$0: Error occured during executing of $prog_str! Error code=$retcode" 1>&2 
		#exit 2
		echo "-273.15"	# return absolute zero in case of error
	else
		#echo "str=$str"
		t=`echo $str | cut -d "$FS" -f 1`
		echo $t
	fi
	i=i+1
done
