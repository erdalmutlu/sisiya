#!/bin/bash
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#       VirusEvent COMMAND
#              Execute a command when a virus is found. In the command string %v will be replaced with the virus name. Additionally, two  envi‐
#              ronment variables will be defined: $CLAM_VIRUSEVENT_FILENAME and $CLAM_VIRUSEVENT_VIRUSNAME.
#              Default: disabled

base_dir="/usr/share/sisiya-client-checks"
conf_dir="/etc/sisiya/sisiya-client-checks"
send_message_prog="$base_dir/utils/sisiya_send_message.pl"
expire="0"
service_str="antivirus"

if test ! -f $send_message_prog ; then
	echo "$0 : SisIYA send message program $send_message_prog does not exist!"
	exit 1
fi

status_str="error"
message_str="ClamAV detected virus! Virus: $CLAM_VIRUSEVENT_VIRUSNAME File: $CLAM_VIRUSEVENT_FILENAME"
data_message_str=""
###################################################################################################
perl -I$conf_dir ${send_message_prog} $service_str $status_str $expire "<msg>$message_str</msg><datamsg>$data_message_str</datamsg>"
sleep 1
exit 0
