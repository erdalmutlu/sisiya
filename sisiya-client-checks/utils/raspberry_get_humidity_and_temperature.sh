#!/bin/bash
#
# This script is used to get the humidity and temperature from the temperature
# sensors of a raspberry pi system.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
conf_file="/etc/sisiya/sisiya-client-checks/conf.d/raspberry_get_humidity_and_temperature.conf"

#######################################################################################
### default values
prog_str="/root/git/Adafruit_Python_DHT/examples/AdafruitDHT.py"
FS="~"			# field seperator
### end of default values
#######################################################################################
if test $# -ne 2 ; then
	echo "Usage  : $0 sensor_type gpio_number" 
	echo "Examle : $0 22 2"
	exit 1
fi
stype=$1
gpio=$2

if  test -f $conf_file ; then
	. $conf_file
fi

if test ! -f $prog_str ; then
	echo "$0: Program $prog_str does not exist!"
	exit 1
fi

# Temp=29.8*  Humidity=19.9%
str=`$prog_str $stype $gpio`
retcode=$?
if test $retcode -ne 0 ; then
	echo "$0: Error occured during executing of $prog_str! Error code=$retcode" 1>&2 
	#exit 2
	echo "0${FS}0"	# return zero in case of error
else
	#echo "str=$str"
	t=`echo $str | cut -d " " -f 1 | cut -d "=" -f 2 | tr -d '*'`
	h=`echo $str | cut -d " " -f 2 | cut -d "=" -f 2 | tr -d '%'`
	echo "$h$FS$t"
fi
