#!/bin/bash
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#
#######################################################################################
#if test $# -ne 3 ; then
#	echo "Usage : $0 expire status message"
#	echo "expire must be specified in minutes. expire=0 means not to expire"
#	echo "status=error,warning,ok,info"
#	exit 1
#fi

base_dir="/usr/share/sisiya-client-checks"
conf_dir="/etc/sisiya/sisiya-client-checks"
send_message_prog="$base_dir/utils/sisiya_send_message.pl"
expire="0"
service_str="netbackup_jobs"

if test "$NV_STATUS" == "SUCCEEDED" ; then
	status_str="ok"
	org_message="Finished Job id: $NV_JOBID title: $NV_JOBTITLE"
else
	status_str="error"
	org_message="Failed Job id: $NV_JOBID title: $NV_JOBTITLE!"
fi
if test "$status_str" == "ok" && test "$NV_JOB_WARNINGS" == "TRUE" ; then
	status_str="warning"
	org_message="Finished Job id: $NV_JOBID title: $NV_JOBTITLE with warnings!"
fi

if test ! -f $send_message_prog ; then
	echo "$0 : SisIYA send message program $send_message_prog does not exist!"
	exit 1
fi

message_str="$org_message"
data_message_str=""
###################################################################################################
perl -I$conf_dir ${send_message_prog} $service_str $status_str $expire "<msg>$message_str</msg><datamsg>$data_message_str</datamsg>"
sleep 1
exit 0
###################################################################################################

###################################################################################################
# NETVAULTCLIACCOUNT
# 	
# Specifies the NetVault Backup user name. The specified account must have privileges to use the CLI utility.
# NETVAULTCLIACCOUNT=<User Account Name>
# The variable must be included in the script to access the CLI utility.
# NETVAULTCLIPASSWORD
# 	
# Specifies the password for the NetVault Backup user account.
# NETVAULTCLIPASSWORD=<Password>
# This variable must be included in the script to specify the password for the user account.
# NV_HOME
# 	
# Returns the NetVault Backup installation directory.
# NV_JOBCLIENT
# 	
# Specifies the target client for a job.
# NV_JOBCLIENT=<Name of the NetVault Backup Client>
# NV_JOBID
# 	
# Specifies the Job ID.
# NV_JOBID=<Job ID>
# NV_JOBTITLE
# 	
# Specifies the Job Title.
# NV_JOBTITLE=<Job title>
# NV_JOB_WARNINGS
# 	
# Returns TRUE if a job completes with warnings, else FALSE.
# •
# 	
# If a backup completes with warnings:
# NV_JOB_WARNINGS=TRUE
# •
# 	
# If a backup completes successfully:
# NV_JOB_WARNINGS=FALSE
# This variable can only be used in a post script. It is currently used by mail scripts, but has general applicability.
# If a backup completes with warnings, the NV_STATUS variable will return SUCCEEDED, while the NV_JOB_WARNINGS variable will return TRUE.
# NV_OUTPUT_FILE
# 	
# Returns the user-defined output file for reports.
# NV_SERVERNAME
# 	
# Specifies the NetVault Backup Server Name.
# NV_SERVERNAME=<Name of the NetVault Backup Server>
# NV_SESSIONID
# 	
# Specifies the Session ID of a job.
# NV_SESSIONID=<Session ID>
# NV_STATUS
# 	
# Returns the exit status of a job. It returns either SUCCEEDED or FAILED.
# •
# 	
# If a backup job completes successfully or completes with warnings:
# NV_STATUS=SUCCEEDED
# •
# 	
# If a backup job fails:
# NV_STATUS=FAILED
# This variable can only be used in a post script. The return value is not localized; it is SUCCEEDED or FAILED in English.
# NV_USER_ARG
# 	
# Specifies the arguments passed with the pre or postscripts.
