#!/bin/bash
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
export NETVAULTCLIACCOUNT=admin
export NETVAULTCLIPASSWORD=S1nC2r3

export PATH=$PATH:/usr/netvault/bin:/usr/netvault/util
# ID~Title~Type~Plugin~Client~Selection~Selection Options~Schedule~Source~Target~Advanced Option~Run Status~Next Run Time
nvjoblist -delimiter "~" -runinfo -noheader | while read line
do
	echo "line=[$line]"
	jobid=`echo $line		| cut -d "~" -f 1`
	title=`echo $line		| cut -d "~" -f 2`
	type_str=`echo $line		| cut -d "~" -f 3`	# B: Backup R:Restore
	plugin_str=`echo $line		| cut -d "~" -f 4`
	client_str=`echo $line		| cut -d "~" -f 5`
	selection_str=`echo $line	| cut -d "~" -f 6`
	selops_str=`echo $line		| cut -d "~" -f 7`
	schedule_str=`echo $line	| cut -d "~" -f 8`
	source_str=`echo $line		| cut -d "~" -f 9`
	target_str=`echo $line		| cut -d "~" -f 10`
	advopt_str=`echo $line		| cut -d "~" -f 11`
	status_str=`echo $line		| cut -d "~" -f 12`	# Scheduled, Running
	next_str=`echo $line		| cut -d "~" -f 13`

	echo "jobid=[$jobid] title=[$title] type=[$type_str] status=[$status_str] plugin=[$plugin_str] client=[$client_str] schedule=[$schedule_str] target=[$target_str] selection=[$selection_str] selops=[$selops_str] source=[$source_str] advanced options=[$advopt_str] next execution=[$next_str]"
	#nvreport -class "Job History" -include "%ID = $jobid"
done


exit


#nvlicenseinfo
echo "-----------------------------------"
nvreport -class "Job History" -delimiter "~" -sort "%JOBDEFINITIONID+ %STARTDATE"
#nvreport -class "Job History" -delimiter "~" -sort "%JOBDEFINITIONID+"
echo "-----------------------------------"
echo "-----------------------------------"
	nvreport -class "Job History" -include "%JOBDEFINITIONID = 77"
exit
echo "-----------------------------------"
