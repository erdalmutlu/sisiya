#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use IO::Socket;
use SisIYA_Config;
#use diagnostics;

if ( $#ARGV != 4 ) {
	#print "Usage : $0 system_name service_id status_id expire message_str\n";
	print "Usage : $0 expire\n";
	print "The expire parameter must be given in minutes.\n";
	exit 1;
}

my $system_name	= $ARGV[0];
my $serviceid	= $ARGV[1];
my $statusid	= $ARGV[2];
my $expire	= $ARGV[3];
my $message_str = $ARGV[4];
#print STDERR "statusid_str=$statusid_str serviceid_str=$serviceid_str expire=$expire message=$message_str\n";

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}

my $date_str = get_timestamp();

my $xml_str = '<?xml version="1.0" encoding="utf-8"?>';
$xml_str .= '<sisiya_messages><timestamp>'.$date_str.'</timestamp>';
$xml_str .= '<system><name>'.$system_name.'</name>';
$xml_str .= "<message><serviceid>".$serviceid."</serviceid><statusid>".$statusid."</statusid><expire>".$expire."</expire><data>".$message_str."</data></message>";
$xml_str .= '</system></sisiya_messages>';

#print STDERR $xml_str;
send_message_data($xml_str);
exit 0;
