#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
my %days = ('warning' => 15, 'error' => 20);
our @certs;
# our @certs = (
#		{
#			'file' 	=> '/etc/ssl/certficates/example.com.crt', 
#			'name'	=> 'example.com'
#		},
#		{
#			'file' 	=> '/etc/ssl/certficates/example.org.crt', 
#			'name'	=> 'example.org'
#		},
#	);
#
#push @certs , { 'file' => '/etc/ssl/certficates/example.com.crt', 'description' => 'example.com'};
#push @certs , { 'file' => '/etc/ssl/certficates/example.org.crt', 'description' => 'example.org'};
#### end of the default values
#######################################################################################
my $service_name = 'ssl';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
################################################################################
################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';

use Time::Piece;

################################################################################
sub get_days
{
	my ($date_str) = @_;

	my @a = split(/\s+/, $date_str);
	my $t = Time::Piece->strptime("$a[0] $a[1] $a[3]", "%b %d %Y");
	my $now = Time::Piece->localtime();
	my $diff = Time::Seconds->new($t - $now);
	return int($diff->days);
}

sub check_certificate_expire_days
{
	my ($file, $name) = @_;

	# print STDERR "check_certificate_expire_days: Checking file=[$file] name=[$name]...\n";
	#############################################################
	my $params = '';
	my @a;
	@a = `$SisIYA_Config::external_progs{'openssl'} x509 -noout -enddate -in $file`;
	# print STDERR @a;
       	my $s = '';
	my $retcode = $?;
	if ($retcode != 0) {
		$error_str .= "ERROR: Something went very wrong! retcode=$retcode";
	}
	else {
		# sample:
		# notAfter=Feb 12 01:15:05 2022 GMT
		# https://www.perl.com/article/59/2014/1/10/Solve-almost-any-datetime-need-with-Time--Piece/
		my @b = split(/=/, $a[0]);
		chomp($b[1]);
		my $days = get_days($b[1]);
		# print STDERR "end date: $b[1] ($days)";
		# check date and warn before the certificate expires
		if ($days <= $days{'error'}) {
			$$statusid = $SisIYA_Config::statusids{'error'};
			$error_str .= " ERROR: Certificate file $file for $name ends in $days days (<= $days{'error'})!";
		} elsif ($days <= $days{'warning'}) {
			$warning_str .= " WARNING: Certificate file $file for $name ends in $days days (<= $days{'warning'})!";
		} else {
			$ok_str .= " OK: Certificate file $file for $name ends in $days days.";
		}
	}
}
################################################################################

if (! -f $SisIYA_Config::external_progs{'openssl'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'openssl'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

for my $i (0..$#certs) {
	check_certificate_expire_days($certs[$i]{'file'}, $certs[$i]{'name'});
}
$data_str = '';

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "$error_str";
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= "$warning_str";
}
if ($ok_str ne '') {
	$message_str .= "$ok_str";
}
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
