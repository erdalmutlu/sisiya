#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
our $mapr_userticket = '/opt/mapr/conf/mapruserticket';
#### end of the default values
################################################################################
my $service_name = 'mapr_alarm';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my $info_str = '';

use JSON;

if (! -f $SisIYA_Config::external_progs{'maprcli'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'maprcli'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

$ENV{'MAPR_TICKETFILE_LOCATION'} = $mapr_userticket;

my @a = `$SisIYA_Config::external_progs{'maprcli'} alarm list -type node -json 2>&1`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: $SisIYA_Config::external_progs{'maprcli'} alarm list -type node -json error=$retcode message: @a!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

chomp(@a = @a);
my $json_str = join(' ', @a);

my $r = decode_json $json_str;

if ($r->{'status'} eq 'OK') {
	my $state;
	foreach (@{$r->{'data'}}) {
		if ($_->{'entity'} eq $SisIYA_Config::hostname) {
			if ($_->{'alarm state'} == 0) {
				$state = 'clear';
			} elsif ($_->{'alarm state'} == 1) {
				$state = 'raised';
			} else {
				$state = 'unknown';
			}
			$error_str .= "ERROR: $_->{'alarm name'} state: $state group: $_->{'alarm group'} $_->{'description'}\n";
		}
	}
} elsif ($r->{'status'} eq 'ERROR') {
	#foreach (@{$r->{'errors'}}) {
	#	$error_str .= "ERROR: id: $_->{'id'} $_->{'desc'}";
	#}
	$error_str .= "ERROR: Problem occured!";
} else {
	$error_str .= "ERROR: Unknown error status: $r->{'status'}";
}

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = $error_str;
} else {
	$ok_str = "OK: No alarm for this node."
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= $warning_str;
}
if ($ok_str ne '') {
	$message_str .= $ok_str;
}
if ($info_str ne '') {
	$message_str .= $info_str;
}
##################################################################################
# maprcli alarm list -type node -json
##################################################################################
#{
#        "timestamp":1656581182199,
#        "timeofday":"2022-06-30 11:26:22.199 GMT+0200 AM",
#        "status":"OK",
#        "total":16,
#        "data":[
#                {
#                        "entity":"server72.example.com",
#                        "alarm name":"NODE_ALARM_CORE_PRESENT",
#                        "alarm state":"1",
#                        "alarm statechange time":1655792492093,
#                        "description":"Node has core file(s). Look into /opt/cores for core files.",
#                        "alarm group":"cldb.alarm.group.error"
#                },
#                {
#                        "entity":"server74.example.com",
#                        "alarm name":"NODE_ALARM_HB_PROCESSING_SLOW",
#                        "alarm state":"1",
#                        "alarm statechange time":1656376498025,
#                        "description":"Heartbeat processing is slow. MFS Heartbeat processing at 5 seconds and CLDB Heartbeat processing at 1 seconds",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server01.example.com",
#                        "alarm name":"NODE_ALARM_HB_PROCESSING_SLOW",
#                        "alarm state":"1",
#                        "alarm statechange time":1656459165136,
#                        "description":"Heartbeat processing is slow. MFS Heartbeat processing at 5 seconds and CLDB Heartbeat processing at 1 seconds",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server72.example.com",
#                        "alarm name":"NODE_ALARM_HB_PROCESSING_SLOW",
#                        "alarm state":"1",
#                        "alarm statechange time":1656460998086,
#                        "description":"Heartbeat processing is slow. MFS Heartbeat processing at 5 seconds and CLDB Heartbeat processing at 1 seconds",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server73.example.com",
#                        "alarm name":"NODE_ALARM_HB_PROCESSING_SLOW",
#                        "alarm state":"1",
#                        "alarm statechange time":1656376497021,
#                        "description":"Heartbeat processing is slow. MFS Heartbeat processing at 4 seconds and CLDB Heartbeat processing at 5 seconds",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server09.example.com",
#                        "alarm name":"NODE_ALARM_HB_PROCESSING_SLOW",
#                        "alarm state":"1",
#                        "alarm statechange time":1656459164284,
#                        "description":"Heartbeat processing is slow. MFS Heartbeat processing at 5 seconds and CLDB Heartbeat processing at 1 seconds",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server73.example.com",
#                        "alarm name":"NODE_ALARM_SERVICE_DATA-ACCESS-GATEWAY_DOWN",
#                        "alarm state":"1",
#                        "alarm statechange time":1656579618834,
#                        "description":"Can not determine if service: data-access-gateway is running. Check logs at: /opt/mapr/data-access-gateway/logs/data-access-gateway.log",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server13.example.com",
#                        "alarm name":"NODE_ALARM_SERVICE_NODEMANAGER_DOWN",
#                        "alarm state":"1",
#                        "alarm statechange time":1656579849755,
#                        "description":"Can not determine if service: nodemanager is running. Check logs at: /opt/mapr/hadoop/hadoop-2.7.0/logs/",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"server01.example.com",
#                        "alarm name":"NODE_ALARM_VERSION_MISMATCH",
#                        "alarm state":"1",
#                        "alarm statechange time":1655792492880,
#                        "description":"FileServer process has mismatch of patch version: $Id: mapr-version: 6.1.1.20210324131839.GA 310e6838405b9f6b32adc6ed66b36e9f0a6a2a97 $ expected version $Id: mapr-version: 6.1.1.20210324131839.GA 9757f12fe2b8aa43fcb8bb7ed6296a72b3a32425 $",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"airflow_logs_e2e",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077275,
#                        "description":"Volume desired replication:nsreplication is 2:3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"test_permissions",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077264,
#                        "description":"Volume desired replication is 3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"user_statter",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077254,
#                        "description":"Volume desired replication:nsreplication is 2:3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"backup_test_OBSOLETE",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077223,
#                        "description":"Volume desired replication is 3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"airflow_logs",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077195,
#                        "description":"Volume desired replication:nsreplication is 2:3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"data_dasv",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077216,
#                        "description":"Volume desired replication is 3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                },
#                {
#                        "entity":"test",
#                        "alarm name":"VOLUME_ALARM_DATA_UNDER_REPLICATED",
#                        "alarm state":"1",
#                        "alarm statechange time":1656580077209,
#                        "description":"Volume desired replication is 3, current replication is 2",
#                        "alarm group":"cldb.alarm.group.warn"
#                }
#        ]
#}
#######################################################################
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
