#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
## the default values
# uptimes are given in minutes
our %uptimes = ('error' => 1440, 'warning' => 4320);
our %max_uptimes = ('error' => 0, 'warning' => 0);
### to get information about the server
our $info_prog = '';
##our $info_prog="$SisIYA_Config::base_dir/special/system_info_hpasm.sh"
our $version_file = "/usr/share/doc/sisiya-client-checks/version.txt";
#### end of the default values
################################################################################
my $service_name = 'system';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
################################################################################
my $message_str;
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};

sub get_os_info
{
	my ($file, $s, $x);

	if ($SisIYA_Config::osname eq 'HP-UX') {
		chomp($x = `$SisIYA_Config::external_progs{'uname'} -srm`);
	}
	else {
		chomp($x = `$SisIYA_Config::external_progs{'uname'} -srmp`);
	}
	$s = $x;
	# add OS version
	if ($SisIYA_Config::osname eq 'Linux') {
		$x = '';
		if (open($file, '<', '/etc/system-release')) {
			my @a = <$file>;
			close($file);
			$x = $a[0];
		} elsif (open($file, '<', '/etc/os-release')) {
			my @a = <$file>;
			close($file);
			$x = (split(/"/, (grep(/PRETTY_NAME=/, @a))[0]))[1];
		} elsif (open($file, '<', '/etc/redhat-release')) {
			$x = <$file>;
			close($file);
			chomp($x);
		} else {
			if (open($file, '<', '/etc/issue.net')) {
				$x = <$file>;
				close($file);
				chomp($x);
			}
		}
		$s .= " OS: $x";
	} elsif ($SisIYA_Config::osname eq 'HP-UX') {
		chomp(my @a = `$SisIYA_Config::external_progs{'swlist'}`);
		#$x = (split(/\s+/, (grep(/HPUXBase64/, @a))[0]))[1];
		$x = (grep(/HPUXBase64/, @a))[0];
		$s .= " OS: $x";
	}
	return $s;
}

sub get_system_info
{
	if (-x $SisIYA_Config::external_progs{'dmidecode'}) {
		return get_system_info_dmidecode();
	} elsif (-x $SisIYA_Config::external_progs{'system_profiler'}) {
		return get_system_info_system_profiler();
	} 
	return '';
}

sub get_system_info_system_profiler
{
	my ($retcode, $s, $x);

	my @a = `$SisIYA_Config::external_progs{'system_profiler'} SPHardwareDataType`;
	$retcode = $? >>=8;
	if ($retcode ne 0) {
		return '';
	}

	$x = (split(/:/, (grep(/Boot ROM Version/, @a))[0] ))[1];
	chomp($x = $x);
	$s = "Boot ROM Verison:$x";

	$x = (split(/:/, (grep(/SMC Version/, @a))[0] ))[1];
	chomp($x = $x);
	$s .= " SMC Version:$x";

	$x = (split(/:/, (grep(/Model Identifier/, @a))[0] ))[1];
	chomp($x = $x);
	$s .= " Product:$x";

	$x = (split(/:/, (grep(/Serial Number/, @a))[0] ))[1];
	chomp($x = $x);
	$s .= " SN:$x";

	return $s;
}

sub get_system_info_dmidecode
{
	my ($retcode, $s, $x);

	$s = '';
	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s bios-vendor`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		if ($x eq '') {
			return $s;
		}
		$s = 'BIOS: '.$x;
	}

	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s bios-version`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		$s .= ' version: '.$x;
	}
	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s bios-release-date`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		$s .= ' release date: '.$x;
	}
	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s chassis-type`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		$s .= ' Product: '.$x;
	}
	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s system-product-name`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		$s .= ' '.$x;
	}
	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s system-serial-number`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		$s .= ' SN: '.$x;
	}
	$x = `$SisIYA_Config::external_progs{'dmidecode'} -s chassis-asset-tag`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		chomp($x = $x);
		if ($x ne 'Not Specified') {
			$s .= ' Asset Tag: '.$x;
		}
	}

	return $s;
}

# get IP information
sub get_ip
{
# on HP-UX
# # netstat -in
# Name      Mtu  Network         Address         Ipkts   Ierrs Opkts   Oerrs Coll
# lan0      1500 172.19.0.0      172.19.1.13     455753100 0     2330880542 0     0
# lo0       4136 127.0.0.0       127.0.0.1       47757088 0     47757091 0     0

	my $cmd_str;
	my $s = '';
	my @a;
	my $retcode;
	if ($SisIYA_Config::osname eq 'Darwin') {
		$cmd_str = "$SisIYA_Config::external_progs{'ifconfig'} -u -a inet";
	} elsif ($SisIYA_Config::osname eq 'HP-UX') {
		$cmd_str = "$SisIYA_Config::external_progs{'netstat'} -in";
		$s = '';
		@a = `$cmd_str`;
		$retcode = $? >>=8;
		if ($retcode == 0) {
			@a = grep(/lan0/, @a);
			foreach (@a) {
				$_ = (split(/\s+/, $_))[3];
			}
			#print STDERR "@a\n";
			#chomp(@a = @a);
			$s = "@a";
			return $s;
		}
	} else {
		$cmd_str = "$SisIYA_Config::external_progs{'ip'} -4 a";
	}
	@a = `$cmd_str`;
	$retcode = $? >>=8;
	if ($retcode == 0) {
		@a = grep(/inet/, @a);
		foreach (@a) {
			$_ = (split(/\s+/, $_))[2];
		}
		#print STDERR "@a\n";
		#chomp(@a = @a);
		$s = "@a";
	}
	return $s;
}

# get information via an external info
sub get_additional_info
{
	my $s = '';

	if ($info_prog ne '') {
		chomp($s = `$info_prog`);
	}
	return $s;
}

sub get_SisIYA_version
{
	my $s = '';

	if (open(my $file, '<', $version_file)) {
		$s = <$file>;
		chomp($s);
		close($file);
	}
	return $s;
}

sub get_info()
{
	my ($s, $x);

	$s = '';
	$x = get_os_info();
	if ($x ne '') {
		$s .= 'Info: '.$x;
	}
	$x = get_system_info();
	if ($x ne '') {
		$s .= ' System: '.$x;
	}
	$x = get_SisIYA_version();
	if ($x ne '') {
		$s .= ' SisIYA: '.$x;
	}
	$x = get_ip();
	if ($x ne '') {
		$s .= ' IP: '.$x;
	}
	$x = get_additional_info();
	if ($x ne '') {
		$s .= ' Details: '.$x;
	}
	return $s;
}

sub get_uptime_in_minutes
{
	my $x;
	my $uptime_in_minutes = 0;

	if ($SisIYA_Config::osname eq 'Linux') {
		my $file;
		open($file, '<', '/proc/uptime') || die "$0: Could not open file /proc/uptime! $!";
		$x = <$file>;
		close $file;
		#chomp($x);
			#my @a = split(/\./, $x); 
			#$uptime_in_minutes = int($a[0] / 60);
		$uptime_in_minutes = int( (split(/\./, $x))[0] / 60 ); 
	} elsif ($SisIYA_Config::osname eq 'SunOS') {
		#uptime   
		# 11:52am  up  1 user,  load average: 0.04, 0.02, 0.04
		if (! -f $SisIYA_Config::external_progs{'uptime'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: External program $SisIYA_Config::external_progs{'uptime'} does not exist!";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		my @a = `$SisIYA_Config::external_progs{'uptime'}`;
		my $retcode = $? >>=8;
		if ($retcode != 0) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: Error executing the uptime command $SisIYA_Config::external_progs{'uptime'}! retcode=$retcode";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		else {
			$x = (split(/m/, $a[0]))[0];
			my $days = 0;
			my $s = (split(/\s+/, $x))[1];
			$s = (split(/[a,p]/, $s))[0];
			$uptime_in_minutes = 24 * (split(/:/, $s))[0] + (split(/:/, $s))[1];
			#print STDERR "x=[$x] s=[$s]\n";
		}
	} elsif ($SisIYA_Config::osname eq 'HP-UX') {
		# uptime
		# 2:56pm  up 618 days,  2:23,  3 users,  load average: 0.00, 0.00, 0.01
		if (! -f $SisIYA_Config::external_progs{'uptime'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: External program $SisIYA_Config::external_progs{'uptime'} does not exist!";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		my @a = `$SisIYA_Config::external_progs{'uptime'}`;
		my $retcode = $? >>=8;
		if ($retcode != 0) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: Error executing the uptime command $SisIYA_Config::external_progs{'uptime'}! retcode=$retcode";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		} else {
			my $d = 0;
			my $h = 0;
			my $m = 0;
			my $s;

			$a[0] = trim($a[0]);
			my @b = split(/,/, $a[0]);

			if (grep(/day/, $a[0]) gt 0) {
				$d = (split(/\s+/, $b[0]))[2];
				$b[1] = trim($b[1]);
				$h = (split(/:/, $b[1]))[0];
				$m = (split(/:/, $b[1]))[1];
			} elsif (grep(/hrs/, $a[0]) gt 0) {
				$s = (split(/\s+/, $b[0]))[2];
				$h = (split(/\s+/, $s))[0];
			} else {
				$s = (split(/\s+/, $b[0]))[2];
				$h = (split(/:/, $s))[0];
				$m = (split(/:/, $s))[1];
			}
			$uptime_in_minutes = 60 * ( 24 * $d + $h) + $m;
			#print STDERR "x=[$x] s=[$s]\n";
		}
	} elsif ($SisIYA_Config::osname eq 'Darwin') {
		#
		# system_profiler SPSoftwareDataType 
		#
		#uptime   
		# 13:34  up  3:18, 3 users, load averages: 1.35 1.36 1.34
		#  0:15  up 16:46, 2 users, load averages: 1.87 1.89 1.85
		#  9:48  up 1 day,  2:19, 2 users, load averages: 1.32 1.40 1.39
		if (! -f $SisIYA_Config::external_progs{'uptime'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: External program $SisIYA_Config::external_progs{'uptime'} does not exist!";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		my @a = `$SisIYA_Config::external_progs{'uptime'}`;
		my $retcode = $? >>=8;
		if ($retcode != 0) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: Error executing the uptime command $SisIYA_Config::external_progs{'uptime'}! retcode=$retcode";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		else {
			my $d = 0;
			my $h = 0;
			my $m = 0;
			my $s;

			$a[0] = trim($a[0]);
			my @b = split(/,/, $a[0]);

			if (grep(/day/, $a[0]) gt 0) {
				$d = (split(/\s+/, $b[0]))[2];
				$b[1] = trim($b[1]);
				$h = (split(/:/, $b[1]))[0];
				$m = (split(/:/, $b[1]))[1];
			} elsif (grep(/hrs/, $a[0]) gt 0) {
				$s = (split(/\s+/, $b[0]))[2];
				$h = (split(/\s+/, $s))[0];
			} else {
				$s = (split(/\s+/, $b[0]))[2];
				$h = (split(/:/, $s))[0];
				$m = (split(/:/, $s))[1];
			}
			$uptime_in_minutes = 60 * ( 24 * $d + $h) + $m;
			#print STDERR "x=[$x] s=[$s]\n";
		}

	}

	return $uptime_in_minutes;
}

###############################################################################
my $uptime_in_minutes = get_uptime_in_minutes;

$message_str = 'OK:The system is up for '.minutes2string($uptime_in_minutes);
if ($max_uptimes{'error'} > 0) {
	if ($uptime_in_minutes > $max_uptimes{'error'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = 'ERROR:The system is up since '.minutes2string($uptime_in_minutes).' (&gt; '.minutes2string($max_uptimes{'error'}).')!';
	}
	elsif ($uptime_in_minutes > $max_uptimes{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$message_str = 'WARNING:The system is up since '.minutes2string($uptime_in_minutes).' (&gt; '.minutes2string($max_uptimes{'warning'}).')!';
	}
} else {
	if ($uptime_in_minutes < $uptimes{'error'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = 'ERROR:The system was restarted '.minutes2string($uptime_in_minutes).' (&lt; '.minutes2string($uptimes{'error'}).') ago!';
	}
	elsif ($uptime_in_minutes < $uptimes{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
		$message_str = 'WARNING:The system was restarted '.minutes2string($uptime_in_minutes).' (&lt; '.minutes2string($uptimes{'warning'}).') ago!';
	}
}

# add  info
$message_str .= ' '.get_info();

$data_str = '<entries>';
$data_str .= '<entry name="uptime" type="numeric">'.$uptime_in_minutes.'</entry>';
$data_str .= '</entries>';
$data_str = '';
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
