#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
#### end of the default values
################################################################################
################################################################################
my $service_name = 'isuptodate';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}


################################################################################
my $statusid = $SisIYA_Config::statusids{'ok'};
my $message_str = '';
my $data_str = '';
my $n = -1;
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
################################################################################

sub use_pacman
{
	`$SisIYA_Config::external_progs{'pacman'} --sync --refresh >/dev/null`;
	chomp(my @a = `$SisIYA_Config::external_progs{'pacman'} --query --upgrades`);
	return @a;
}

sub use_apt_check
{
	chomp(my $s =`$SisIYA_Config::external_progs{'apt-check'} 2>&1`);
	return (split(/;/, $s))[0] + (split(/;/, $s))[1];
}

sub use_apt_get
{
	chomp(my @a =`$SisIYA_Config::external_progs{'apt-get'} -s dist-upgrade 2>&1`);
	@a = grep(/^Inst/, @a);
	return @a;
}

sub use_yum
{
	chomp(my @a = `$SisIYA_Config::external_progs{'yum'} -q list updates`);
	@a = grep(!/^Updated Packages/, grep(!/^Updated Packages/, @a));
	return @a;
}

sub use_zypper
{
	my $n;
	chomp($n = `$SisIYA_Config::external_progs{'zypper'} --non-interactive list-updates | grep "^v |" |  wc -l`);

	my @a = `$SisIYA_Config::external_progs{'zypper'} --non-interactive list-patches --category security --cve | grep "^cve"`;

	# security patch details by category
	my %h = (
		'critical'	=> { 'count' => 0, 'list' => ''},
		'important'	=> { 'count' => 0, 'list' => ''},
		'low'		=> { 'count' => 0, 'list' => ''},
		'moderate'	=> { 'count' => 0, 'list' => ''}
	);

	for my $k (keys %h) {
		my @b = grep(/\|\s+$k\s+\|/, @a);
		$h{$k}{'count'} = @b;
		if ($h{$k}{'count'} > 0) {
                        # @b = map { (split(/\|/, $_))[1] } @b;
                        # $h{$k}{'list'} = join(',', map { s/CVE-//g; s/\s+//g; $_ } @b);
			# $error_str .= "ERROR: There are $h{$k}{'count'} $k security updates! CVEs: $h{$k}{'list'}"
			# The list can get very big on some not frequently updated systems. Do not show it for now.
			$error_str .= "ERROR: There are $h{$k}{'count'} $k security updates!"
		}
	}
# zypper list-patches --category security --cve | grep ^cve
#cve | CVE-2022-22942   | CL-SUSE-SLE-SERVER-12-SP5-2022-364 | security | critical  | reboot      | needed | critical: Security update for the Linux Kernel
#cve | CVE-2021-45486   | CL-SUSE-SLE-SERVER-12-SP5-2022-68  | security | important | reboot      | needed | important: Security update for the Linux Kernel 
#cve | CVE-2021-45944   | CL-SUSE-SLE-SERVER-12-SP5-2022-81  | security | moderate  |
#cve | CVE-2021-20193   | CL-SUSE-SLE-SERVER-12-SP5-2021-975 | security | low       | ---         | needed | low: Security update for tar

	return $n;
}

sub use_softwareupdate
{
	my $n;
	my @a = `$SisIYA_Config::external_progs{'softwareupdate'} --list 2>/dev/null`;
	if (grep(/No new software available/, @a) eq 0) {
		$n = 0;
	} else {
		$n = 1;
	}
	return $n;
}

################################################################################
if (-x $SisIYA_Config::external_progs{'yum'}) {
	$n = use_yum();
} elsif (-x $SisIYA_Config::external_progs{'apt-get'}) {
	$n = use_apt_get();
} elsif (-x $SisIYA_Config::external_progs{'apt-check'}) {
	$n = use_apt_check();
} elsif (-x $SisIYA_Config::external_progs{'pacman'}) {
	$n = use_pacman();
} elsif (-x $SisIYA_Config::external_progs{'zypper'}) {
	$n = use_zypper();
} elsif (-x $SisIYA_Config::external_progs{'softwareupdate'}) {
	$n = use_softwareupdate();
}

if ($n > 0) {
	$warning_str .= "WARNING: The system is out of date! There are $n available package updates.";
} elsif ($n == 0) {
	$ok_str = "OK: The system is uptodate.";
}	
#$data_str = '<entries>';
#if ($n == 0) {
#	$data_str .= '<entry name="is_uptodate" type="boolean">1</entry>';
#} else {
#	$data_str .= '<entry name="is_uptodate" type="boolean">0</entry>';
#}
#$data_str .= '<entry name="number_of_packages" type="numeric">'.$n.'</entry>';
#$data_str .= '</entries>';
$data_str = '';

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str .= $error_str;
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= $warning_str;
}
if ($ok_str ne '' and $statusid == $SisIYA_Config::statusids{'ok'}) {
	$message_str = $ok_str;
}

if ($message_str eq '') {
	$statusid = $SisIYA_Config::statusids{'info'};
	$message_str = "INFO: Unsupported system for uptodate checking.";
}
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
