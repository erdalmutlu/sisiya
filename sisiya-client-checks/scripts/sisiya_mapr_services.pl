#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
our $mapr_userticket = '/opt/mapr/conf/mapruserticket';
#### end of the default values
################################################################################
my $service_name = 'mapr_services';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my $info_str = '';

use JSON;

if (! -f $SisIYA_Config::external_progs{'maprcli'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'maprcli'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

$ENV{'MAPR_TICKETFILE_LOCATION'} = $mapr_userticket;

my @a = `$SisIYA_Config::external_progs{'maprcli'} service list -node $SisIYA_Config::hostname -json 2>&1`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: $SisIYA_Config::external_progs{'maprcli'} service list -node $SisIYA_Config::hostname -json error=$retcode message: @a!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

chomp(@a = @a);
my $json_str = join(' ', @a);

my $r = decode_json $json_str;

my $log_info;
if ($r->{'status'} eq 'OK') {
	foreach (@{$r->{'data'}}) {
		$log_info = '';
		if (defined($_->{'logpath'})) {
			$log_info = " log path: $_->{'logpath'}";
		}
		if ($_->{'state'} == 0) {
			$info_str .= "INFO: $_->{'name'} is not configured.$log_info\n";
		} elsif ($_->{'state'} == 1) {
			$info_str .= "INFO: $_->{'name'} is configured.$log_info\n";
		} elsif ($_->{'state'} == 2) {
			$ok_str .= "OK: $_->{'name'} is running.$log_info\n";
		} elsif ($_->{'state'} == 3) {
			$warning_str .= "WARNING: $_->{'name'} is stopped!$log_info\n";
		} elsif ($_->{'state'} == 4) {
			$error_str .= "ERROR: $_->{'name'} is failed!$log_info\n";
		} elsif ($_->{'state'} == 5) {
			$ok_str .= "OK: $_->{'name'} is stand by.$log_info\n";
		} else {
			$warning_str .= "WARNING: $_->{'name'} is state is $_->{'state'} unknown!$log_info\n";
		}
	}
} elsif ($r->{'status'} eq 'ERROR') {
	#foreach (@{$r->{'errors'}}) {
	#	$error_str .= "ERROR: id: $_->{'id'} $_->{'desc'}";
	#}
	$error_str .= "ERROR: Problem occured!";
} else {
	$error_str .= "ERROR: Unknown error status: $r->{'status'}";
}

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = $error_str;
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= $warning_str;
}
if ($ok_str ne '') {
	$message_str .= $ok_str;
}
if ($info_str ne '') {
	$message_str .= $info_str;
}
##################################################################################
# maprcli dump containerinfo -ids 1 -json
##################################################################################
#{
#        "timestamp":1656526322144,
#        "timeofday":"2022-06-29 08:12:02.144 GMT+0200 PM",
#        "status":"OK",
#        "total":22,
#        "data":[
#                {
#                        "name":"hbasethrift",
#                        "state":5,
#                        "logpath":"/opt/mapr/hbase/hbase-1.1.13/logs",
#                        "displayname":"HBaseThriftServer"
#                },
#                {
#                        "name":"httpfs",
#                        "state":0,
#                        "logpath":"/opt/mapr/httpfs/httpfs-1.0/logs",
#                        "displayname":"Httpfs"
#                },
#                {
#                        "name":"spark-historyserver",
#                        "state":2,
#                        "memallocated":"Auto",
#                        "logpath":"/opt/mapr/spark/spark-2.4.4/logs/",
#                        "displayname":"SparkHistoryServer"
#                },
#                {
#                        "name":"oozie",
#                        "state":0
#                },
#                {
#                        "name":"cldb",
#                        "state":0,
#                        "logpath":"/opt/mapr/logs/cldb.log",
#                        "displayname":"CLDB"
#                },
#                {
#                        "name":"nfs4",
#                        "state":0,
#                        "logpath":"/opt/mapr/logs/nfs4/nfs4server.log",
#                        "displayname":"NFS4 Gateway"
#                },
#                {
#                        "name":"collectd",
#                        "state":2,
#                        "memallocated":"220.0",
#                        "logpath":"/opt/mapr/collectd/collectd-5.8.1/var/log/collectd",
#                        "displayname":"CollectD"
#                },
#                {
#                        "name":"hoststats",
#                        "state":2,
#                        "memallocated":"Auto",
#                        "logpath":"/opt/mapr/logs/hoststats.log",
#                        "displayname":"HostStats"
#                },
#                {
#                        "name":"fluentd",
#                        "state":0
#                },
#                {
#                        "name":"hbaserest",
#                        "state":2,
#                        "memallocated":"Auto",
#                        "logpath":"/opt/mapr/hbase/hbase-1.1.13/logs",
#                        "displayname":"HBaseRestServer"
#                },
#                {
#                        "name":"elasticsearch",
#                        "state":0
#                },
#                {
#                        "name":"fileserver",
#                        "state":2,
#                        "memallocated":"90074.0",
#                        "logpath":"/opt/mapr/logs/mfs.log",
#                        "displayname":"FileServer"
#                },
#                {
#                        "name":"grafana",
#                        "state":0,
#                        "logpath":"/opt/mapr/grafana/grafana-6.7.4/var/log/grafana",
#                        "displayname":"Grafana"
#                },
#                {
#                        "name":"historyserver",
#                        "state":2,
#                        "memallocated":"750.0",
#                        "logpath":"/opt/mapr/hadoop/hadoop-2.7.0/logs",
#                        "displayname":"JobHistoryServer"
#                },
#                {
#                        "name":"resourcemanager",
#                        "state":2,
#                        "memallocated":"4096.0",
#                        "logpath":"/opt/mapr/hadoop/hadoop-2.7.0/logs",
#                        "displayname":"ResourceManager"
#                },
#                {
#                        "name":"nfs",
#                        "state":1,
#                        "logpath":"/opt/mapr/logs/nfsserver.log",
#                        "displayname":"NFS Gateway"
#                },
#                {
#                        "name":"mastgateway",
#                        "state":3
#                },
#                {
#                        "name":"nodemanager",
#                        "state":4,
#                        "logpath":"/opt/mapr/hadoop/hadoop-2.7.0/logs",
#                        "displayname":"NodeManager"
#                },
#                {
#                        "name":"kibana",
#                        "state":6
#                },
#                {
#                        "name":"opentsdb",
#                        "state":2,
#                        "memallocated":"6144.0",
#                        "logpath":"/opt/mapr/opentsdb/opentsdb-2.4.0/var/log/opentsdb",
#                        "displayname":"OpenTsdb"
#                },
#                {
#                        "name":"gateway",
#                        "state":0
#                },
#                {
#                        "name":"apiserver",
#                        "state":2,
#                        "memallocated":"1000.0",
#                        "logpath":"/opt/mapr/apiserver/logs/apiserver.log",
#                        "displayname":"APIServer"
#                }
#        ]
#}
#######################################################################
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
