#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
our $unavailable_temperature = -273.15;	# the temperature value which is considered as unavialable, instead of error
our %default_temperatures = ( 
	'upper_warning' => 30, 'upper_error' => 35, 
	'lower_warning' => 20, 'lower_error' => 15, 
);
our @sensor_names;	# array of sensor names, $sensor_names[0] = 'indoor', $sensor_names[1] = 'outdoor';
our %temperatures;	# sensor specific temperature limits 
# can be defined as follows:
# $temperatures{'sensor_name'}{'upper_warning'} = 28;
# $temperatures{'sensor_name'}{'upper_error'}   = 33;
# $temperatures{'sensor_name'}{'lower_warning'} = 18
# $temperatures{'sensor_name'}{'lower_error'}   = 14;
#
#### end of the default values
#######################################################################################
my $service_name = 'temperature';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_raspberry_$service_name.conf";
chomp($module_conf_file);
if (-f $module_conf_file) {
	require $module_conf_file;
}
#######################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $unavailable_str = '';
my $info_str = '';
my $ok_str = '';
my $warning_str = '';

#######################################################################################
if (! -f $SisIYA_Config::external_progs{'raspberry_temperature'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'raspberry_temperature'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
my @a = `$SisIYA_Config::external_progs{'raspberry_temperature'}`;
my $retcode = $? >>=8;
if ($retcode == 0) {
	chomp(@a = @a);
	my ($s, $sensor_number, $sensor_name, $sensor_temperature, $sensor_threshold);
	my ($upper_warning_temperature, $upper_error_temperature, $lower_warning_temperature, $lower_error_temperature);
	my $i = 0;
	$data_str = '<entries>';
       	foreach (@a) {
		$sensor_temperature = $_;
		$sensor_number = $i + 1;
		$sensor_name = "sensor_$sensor_number";
		if (defined $sensor_names[$i]) {
			$sensor_name = $sensor_names[$i];
		}
			
#print STDERR "sensor=$sensor_name t=$sensor_temperature\n";

		# set default limits	
		$upper_warning_temperature = $default_temperatures{'upper_warning'};
		$upper_error_temperature  = $default_temperatures{'upper_error'};
		$lower_warning_temperature = $default_temperatures{'lower_warning'};
		$lower_error_temperature  = $default_temperatures{'lower_error'};

		# if defined overwrite sensor specific limits
		if (defined $temperatures{"$sensor_name"}{'upper_warning'}) {
			$upper_warning_temperature = $temperatures{"$sensor_name"}{'upper_warning'};
		}
		if (defined $temperatures{"$sensor_name"}{'upper_error'}) {
			$upper_warning_temperature = $temperatures{"$sensor_name"}{'upper_error'};
		}
		if (defined $temperatures{"$sensor_name"}{'lower_warning'}) {
			$lower_warning_temperature = $temperatures{"$sensor_name"}{'lower_warning'};
		}
		if (defined $temperatures{"$sensor_name"}{'lower_error'}) {
			$lower_warning_temperature = $temperatures{"$sensor_name"}{'lower_error'};
		}

		$s = "The temperature for the ".$sensor_number."th sensor $sensor_name is $sensor_temperature";
		if ($sensor_temperature == $unavailable_temperature) {
			$unavailable_str .= " UNAVAILABLE: The temperature for the ".$sensor_number."th sensor $sensor_name is not available!"
		} elsif ($sensor_temperature >= $upper_error_temperature) {
			$error_str .= " ERROR: $s (>= $upper_error_temperature) Grad Celcius!"
		} elsif ($sensor_temperature >= $upper_warning_temperature) {
			$warning_str .= " WARNING: $s (>= $upper_warning_temperature) Grad Celcius!"
		} elsif ($sensor_temperature <= $lower_error_temperature) {
			$error_str .= " ERROR: $s (<= $lower_error_temperature) Grad Celcius!"
		} elsif ($sensor_temperature <= $lower_warning_temperature) {
			$warning_str .= " WARNING: $s (<= $lower_warning_temperature) Grad Celcius!"
		} else {
			$ok_str .= " OK: $s Grad Celcius."
		}
		$data_str .= '<entry name="'.$sensor_number.'_'.$sensor_name.'" type="numeric" unit="C">'.$sensor_temperature.'</entry>';
		$i = $i + 1;
	}
	$data_str .= '</entries>';
}

if ($unavailable_str ne '') {
	$statusid = $SisIYA_Config::statusids{'unavailable'};
	$message_str = "$unavailable_str";
}
if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "$error_str";
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= "$warning_str";
}
if ($ok_str ne '') {
	$message_str .= "$ok_str";
}
if ($info_str ne '') {
	$message_str .= "$info_str";
}
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
