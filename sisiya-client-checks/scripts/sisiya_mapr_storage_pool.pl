#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
our %percents = ('warning' => 85, 'error' => 90); 
#### end of the default values
################################################################################
################################################################################
my $service_name = 'mapr_storage_pool';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my $percent_error;
my $percent_warning;
my %h;
my $size_unit;

if (! -f $SisIYA_Config::external_progs{'mrconfig'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'mrconfig'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

my @a = `$SisIYA_Config::external_progs{'mrconfig'} sp list -v 2>&1`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: $SisIYA_Config::external_progs{'mrconfig'} sp list -v error=$retcode message: @a!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

@a = grep(/^SP /, @a);
foreach my $s (@a) {
	chomp($s);
	# print STDERR "s=[$s]\n";
	# SP 18: name SP19, Online, size 32937453 MB, free 5638772 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e54e7a0000041661b2f711, log 200 MB, port 5664, guid f1f500c3db084ea30061b9b2f609cace, clusterUuid -5962651683986299599--4612594969491257974, disks /dev/disk/by-id/dm-uuid-mpath-3600a098000e54e7a0000041661b2f711 /dev/disk/by-id/dm-uuid-mpath-3600a098000e54e7a0000041961b2f730 /dev/disk/by-id/dm-uuid-mpath-3600a098000e546000000036a61b6fef5
	############
	# without -v
	############
	# SP 0: name SP1, Online, size 27438366 MB, free 15131496 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5413c000001cc5f5ee8c7
	# delimeter is double colon, comma and space at the same time
	# 0 SP
	# 1 0
	# 2 name
	# 3 SP1
	# 4 Online
	# 5 size
	# 6 27438366
	# 7 MB
	# 8 free
	# 9 14145167
	# 10 MB
	# 11 path
	# 12 /dev/disk/by-id/dm-uuid-mpath-3600a098000e5413c000001cc5f5ee8c7
	my @b = split(/[:,\s]+/, $s);
	my $k = $b[1];
	# foreach (@b) { printf STDERR "$_\n"; }
	$h{$k}{'name'} = $b[3];
	$h{$k}{'state'} = $b[4];
	$h{$k}{'size'} = $b[6];
	$h{$k}{'size_unit'} = $b[7];
	$h{$k}{'free'} = $b[9];
	$h{$k}{'free_unit'} = $b[10];
	$h{$k}{'capacity'} = int((100 * ($h{$b[1]}{'size'} - $h{$b[1]}{'free'})) / $h{$b[1]}{'size'});
	
	@b = split(/,/, $s);
	$h{$k}{'disks'} = $b[9];
}
my %totals = ( 'free' => 0, 'capacity' =>0, 'size' => 0, 'used' => 0 );
for my $k (sort keys %h) {
	# print STDERR "-----> : key=[$k] name=[$h{$k}{'name'}] state=[$h{$k}{'state'}] size=[$h{$k}{'size'}] size_unit=[$h{$k}{'size_unit'}] free=[$h{$k}{'free'}] free_unit=[$h{$k}{'free_unit'}] [$h{$k}{'disks'}]\n";
	$percent_error = $percents{'error'};
	$percent_warning = $percents{'warning'};
	$size_unit = $h{$k}{'size_unit'};
	$totals{'size'} += $h{$k}{'size'};
	$totals{'free'} += $h{$k}{'free'};
	$totals{'used'} += $h{$k}{'size'} - $h{$k}{'free'};
	if ($h{$k}{'capacity'} >= $percent_error) {
		$error_str .= " ERROR: Storage pool $h{$k}{'name'} id: $k ( state: $h{$k}{'state'} $h{$k}{'disks'}) $h{$k}{'capacity'}% (>= $percent_error) of $h{$k}{'size'} $h{$k}{'size_unit'} is full!";
	} elsif ($h{$k}{'capacity'} >= $percent_warning) {
		$warning_str .= " WARNING: Storage pool $h{$k}{'name'} id: $k (state: $h{$k}{'state'} $h{$k}{'disks'}) $h{$k}{'capacity'}% (>= $percent_warning)) of $h{$k}{'size'} $h{$k}{'size_unit'} is full!";
	} else {
		$ok_str .= " OK: Storage pool $h{$k}{'name'} $k (size: id: $h{$k}{'size'} $h{$k}{'size_unit'} free: $h{$k}{'free'} $h{$k}{'free_unit'} usage: $h{$k}{'capacity'}% $h{$k}{'disks'}) is $h{$k}{'state'}.";
	}
	if ($h{$k}{'state'} ne 'Online') {
		$error_str .= " ERROR: Storage pool $h{$k}{'name'} $k size: $h{$k}{'size'} $h{$k}{'size_unit'} free: $h{$k}{'free'} $h{$k}{'free_unit'} capacity: $h{$k}{'capacity'}% $h{$k}{'disks'} is $h{$k}{'state'} (!= Online)!";
	}
}


if ($totals{'size'} > 0) {
        $totals{'capacity'} = int(100 * $totals{'used'} / $totals{'size'});
	if ($totals{'capacity'} >= $percent_error) {
		$error_str .= " ERROR: $totals{'capacity'}% (>= $percent_error) of $totals{'size'} $size_unit is full!";
	} elsif ($totals{'capacity'} >= $percent_warning) {
		$warning_str .= " WARNING: $totals{'capacity'}% (>= $percent_warning) of $totals{'size'} $size_unit is full!";
	} else {
                $ok_str .= " OK: $totals{'capacity'}% of total $totals{'size'} $size_unit is used.";
        }
}
$data_str = '';

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = $error_str;
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= $warning_str;
}
if ($ok_str ne '') {
	$message_str .= $ok_str;
}
##################################################################################
#$MAPR_HOME/server/mrconfig sp list -v |grep "SP "
# SP 18: name SP19, Online, size 32937453 MB, free 5638772 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e54e7a0000041661b2f711, log 200 MB, port 5664, guid f1f500c3db084ea30061b9b2f609cace, clusterUuid -5962651683986299599--4612594969491257974, disks /dev/disk/by-id/dm-uuid-mpath-3600a098000e54e7a0000041661b2f711 /dev/disk/by-id/dm-uuid-mpath-3600a098000e54e7a0000041961b2f730 /dev/disk/by-id/dm-uuid-mpath-3600a098000e546000000036a61b6fef5
##################################################################################
#$MAPR_HOME/server/mrconfig sp list|grep "SP "
#######################################################################
#
#############
# example 1:
#############
#
#ListSPs resp: status 0:6
#No. of SPs (6), totalsize 164630201 MB, totalfree 93181113 MB
#
#SP 0: name SP1, Online, size 27438366 MB, free 15131496 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5413c000001cc5f5ee8c7
#SP 1: name SP3, Online, size 27438366 MB, free 21097829 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000001f55f5b430c
#SP 2: name SP4, Online, size 27438366 MB, free 15659600 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000002065f5ee8a5
#SP 3: name SP5, Online, size 27438366 MB, free 14875547 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000002475ff84dd3
#SP 4: name SP6, Online, size 27438366 MB, free 14236271 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000002415ff84db7
#SP 5: name SP7, Online, size 27438366 MB, free 12180367 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5413c0000032f614d7bfd
#
#############
# example 1:
#############
#
#ListSPs resp: status 0:6
#No. of SPs (6), totalsize 164630201 MB, totalfree 83296485 MB
#
#SP 0: name SP1, Online, size 27438366 MB, free 14145167 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5413c000001cc5f5ee8c7
#SP 1: name SP3, Online, size 27438366 MB, free 13577647 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000001f55f5b430c
#SP 2: name SP4, Online, size 27438366 MB, free 14578183 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000002065f5ee8a5
#SP 3: name SP5, Online, size 27438366 MB, free 13800014 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000002475ff84dd3
#SP 4: name SP6, Online, size 27438366 MB, free 13944905 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5418c000002415ff84db7
#SP 5: name SP7, Online, size 27438366 MB, free 13250567 MB, path /dev/disk/by-id/dm-uuid-mpath-3600a098000e5413c0000032f614d7bfd
#######################################################################

######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
