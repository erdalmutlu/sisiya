#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
#### end of the default values
#######################################################################################
my $service_name = 'zpool';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
#######################################################################################
sub get_array_list
{
	my @a;
	my $i = 0;
	foreach (@_) {
		$a[$i] = (split(/\s+/, $_))[1];
		#print STDERR "$a[$i]\n";
		$i++;
	}
	return @a;
}
#######################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my @zpool_lists;
my @a;

if (! -f $SisIYA_Config::external_progs{'zpool'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'zpool'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
@a = `$SisIYA_Config::external_progs{'zpool'} list -H`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: Error executing the $SisIYA_Config::external_progs{'zpool'} command! retcode=$retcode";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
my $i = 0;
foreach (@a) {
	$zpool_lists[$i++] = (split(/\s+/, $_))[0];
}
my ($zpool_name, $zpool_health, $zpool_size, $zpool_free, $zpool_alloc, $zpool_frag, $zpool_cap, $zpool_dedup, $zpool_expandsz, $zpool_altroot, $zpool_ckpoint);
$data_str = '<entries>';
my @b;
for $i (0..$#zpool_lists) {
	@b = split(/\s+/, $a[$i]);
	$zpool_name	= $b[0];
	$zpool_size	= $b[1];
	$zpool_alloc	= $b[2];
	$zpool_free	= $b[3];
	$zpool_ckpoint	= $b[4];
	$zpool_expandsz = $b[5];
	$zpool_frag	= $b[6];
	$zpool_cap	= $b[7];
	$zpool_dedup	= $b[8];
	$zpool_health	= $b[9];
	$zpool_altroot	= $b[10];
	#print STDERR "$state\n";

	if ($zpool_health eq 'ONLINE') {
		$ok_str .= " OK: Pool $zpool_name health is $zpool_health (size=$zpool_size, alloc=$zpool_alloc, free=$zpool_free, ckpoint=$zpool_ckpoint, expandsz=$zpool_expandsz, frag=$zpool_frag, cap=$zpool_cap, dedup=$zpool_dedup, altroot=$zpool_altroot).";
	} else {
		$error_str .= " ERROR: Pool $zpool_name health is $zpool_health != ONLINE (size=$zpool_size, alloc=$zpool_alloc, free=$zpool_free, ckpoint=$zpool_ckpoint, expandsz=$zpool_expandsz, frag=$zpool_frag, cap=$zpool_cap, dedup=$zpool_dedup, altroot=$zpool_altroot)!";
	}
}
$data_str .= '</entries>';
$data_str = '';

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "$error_str";
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= "$warning_str";
}
if ($ok_str ne '') {
	$message_str .= "$ok_str";
}

# zpool list
# NAME   SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
# data  16.4T   312G  16.1T        -         -     0%     1%  1.00x    ONLINE  -
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
