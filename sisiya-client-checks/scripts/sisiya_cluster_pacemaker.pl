#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
#### end of the default values
#######################################################################################
my $service_name = 'cluster_pacemaker';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
#######################################################################################
sub get_array_list
{
	my @a;
	my $i = 0;
	foreach (@_) {
		$a[$i] = (split(/\s+/, $_))[1];
		#print STDERR "$a[$i]\n";
		$i++;
	}
	return @a;
}
#######################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my @a;

if (! -f $SisIYA_Config::external_progs{'crm_mon'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'crm_mon'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
@a = `$SisIYA_Config::external_progs{'crm_mon'} -s`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: Error executing the $SisIYA_Config::external_progs{'crm_mon'} command! retcode=$retcode";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
my $i = 0;
my ($state, $state_info);
#######################################################################
### crm_mon -s example output:
### CLUSTER OK: 2 nodes online, 7 resources configured
#######################################################################
@a = `$SisIYA_Config::external_progs{'crm_mon'} -s`;
$retcode = $? >>=8;
if ($retcode != 0) {
	$error_str .= " ERROR: Could not get info about!";
}
else {
	chomp(@a = @a);
	$state_info = trim(( split(/:/, ( grep(/CLUSTER /, @a) )[0]) )[1]);
	$state      = trim(( split(/:/, ( grep(/CLUSTER /, @a) )[0]) )[0]);
	$state =~ s/CLUSTER //g; 
	#print STDERR "$state\n";

	if ( ($state eq 'OK') || ($state eq 'Ok') ) {
		$ok_str .= " OK: $state_info";
	} elsif (($state eq 'WARNING') || ($state eq 'Warning)')) {
		$warning_str .= " WARNING: Cluster is not OK! $state_info";
	} else {
		$error_str .= " ERROR: Cluster failed! $state_info";
	}
}

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "$error_str";
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= "$warning_str";
}
if ($ok_str ne '') {
	$message_str .= "$ok_str";
}
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
