#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
#### end of the default values
#######################################################################################
my $service_name = 'zookeeper';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
#######################################################################################
sub get_array_list
{
	my @a;
	my $i = 0;
	foreach (@_) {
		$a[$i] = (split(/\s+/, $_))[1];
		#print STDERR "$a[$i]\n";
		$i++;
	}
	return @a;
}

sub parse_line
{
		my @b = split(/\s+/, $_[0]);
		# resource
		$_[1] = $b[0];
}
#######################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my @zpool_lists;
my @a;

if (! -f $SisIYA_Config::external_progs{'zkServer.sh'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'zkServer.sh'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
@a = `$SisIYA_Config::external_progs{'zkServer.sh'} status 2>&1`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: Error executing the $SisIYA_Config::external_progs{'zkServer.sh'} command! retcode=$retcode";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

# Using config: /opt/mapr/zookeeper/zookeeper-3.4.11/bin/../conf/zoo.cfg
# zookeeper running as process 2372.
my $zookeeper_status = (grep(/^zookeeper/, @a))[0];

@a = `$SisIYA_Config::external_progs{'zkServer.sh'} qstatus 2>&1`;
$retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: Error executing the $SisIYA_Config::external_progs{'zkServer.sh'} command! retcode=$retcode";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

# Using config: /opt/mapr/zookeeper/zookeeper-3.4.11/bin/../conf/zoo.cfg
# Mode: leader
my $zookeeper_qstatus = (grep(/^Mode/, @a))[0];

$ok_str = "OK: $zookeeper_status $zookeeper_qstatus";

$data_str = '';

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "$error_str";
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= "$warning_str";
}
if ($ok_str ne '') {
	$message_str .= "$ok_str";
}
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
