#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
#### end of the default values
#######################################################################################
my $service_name = 'docker';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
#######################################################################################
sub parse_line
{
	# kaya-api~kaya-api:live~Up 7 hours
	# kaya-api~kaya-api:live~Created
	# kaya-api~kaya-api:live~Up 2 hourse (health: starting)
	# kaya-api~kaya-api:live~Up 3 days (healthy)
	my @b = split(/~/, $_[0]);
	# name
	$_[2] = $b[0];
	# image
	$_[3] = $b[1];
	# status
	my @a = split(/\s+/, $b[2]);
	#$_[1] = (split(/\s+/, $b[2]))[0];
	$_[1] = $a[0];
	# uptime
	$_[4] = $b[2];
}
#######################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my @raid_arrays;
my @a;

if (! -f $SisIYA_Config::external_progs{'docker'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'docker'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}
#######################################################################
### docker ps --format '{{.Names}}~{{.Image}}~{{.Status}}'
# kaya-api~kaya-api:live~Up 7 hours
# kaya-db~postgres:11-alpine~Up 7 hours
# kaya-redis~redis:5-alpine~Up 7 hours
#######################################################################
@a = `$SisIYA_Config::external_progs{'docker'} ps --all --format '{{.Names}}~{{.Image}}~{{.Status}}' 2>/dev/null`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$error_str .= " ERROR: Could not get info about docker!";
}
else {
	my ($status, $name, $image, $uptime);
	foreach my $s (@a) {
		parse_line($s, $status, $name, $image, $uptime);
		# print STDERR "name=$name status=$status image=$image uptime=$uptime\n";
		if ($status eq 'Up') {
			$ok_str .= "OK: Container $name (image: $image) status is $status. Uptime: $uptime";
		} elsif ($status eq 'Created') {
			$warning_str .= "WARNING: Container $name (image: $image) status is $status (!=Up)!";
		} else {
			$error_str .= "ERROR: Container $name (image: $image) status is $status (!=Up)!";
		}
	}
	if (!@a) {
		$ok_str .= "OK: There are no containers running.";
	}
}
if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "$error_str";
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= "$warning_str";
}
if ($ok_str ne '') {
	$message_str .= "$ok_str";
}

##################################################################################
# docker ps --all --format '{{.Names}}~{{.Image}}~{{.Status}}'
##################################################################################
# sisiya-api-redis~docker.io/library/redis:7.2.5-alpine3.20~Up 13 hours
# sisiya-backend-redis~docker.io/library/redis:7.2.5-alpine3.20~Up 13 hours
# sisiya-php-fpm~localhost/php:8.1-fpm-alpine3.18-sisiya~Up 13 hours
# sisiya-backend-db~docker.io/library/postgres:16.4-alpine3.20~Up 13 hours
# sisiya-db~docker.io/library/postgres:14.9-alpine3.18~Up 13 hours
# sisiya-api~docker.io/erdalmutlu/sisiya-api:latest~Up 13 hours (healthy)
# sisiya-backend~docker.io/erdalmutlu/sisiya-backend:latest~Up 13 hours (healthy)
# nginx~docker.io/library/nginx:1.26.2-alpine~Up 13 hours
# sisiya-ui~docker.io/erdalmutlu/sisiya-ui:latest~Up 13 hours
##################################################################################

######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
