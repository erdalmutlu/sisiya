#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
our %swap_percents = ( 'warning' => 30, 'error' => 50);
#### end of the default values
#######################################################################################
my $service_name = 'swap';
## override defaults if there is a corresponfing conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
chomp($module_conf_file);
if (-f $module_conf_file) {
	require $module_conf_file;
}
#######################################################################################
# 1 parameter is the search string
# 2 parameter is an array to cat /proc/meminfo
#
#sub get_meminfo
#{
#	my ($str, @lines) = @_;
#
#	my @a = grep(/$str/, @lines); 
#		#print STDERR "1 - memtotal = $a[0]\n";
#	@a = split(/:/, $a[0]);
#		#print STDERR "2 - memtotal = $a[1]\n";
#	@a = split(/k/, $a[1]);
#		#print STDERR "3 - memtotal = $a[0]\n";
#	return $a[0];
#}

# This function extracts the swap value in KB from given string in form SSSSSS.SS[G|M]
sub get_swap_value
{
	my $x = $_[0];
	my $k = 1;
	if ( grep(/M/, $x) gt 0) {
		#print STDERR "MB\n";
		$k = 1024;
	} elsif( grep(/G/, $x) gt 0) {
		#print STDERR "GB\n";
		$k = 1024 * 1024;
	}
	$x = (split(/\./, $x))[0];
	$x =~ s/\s+//g;

	return $k * $x;
}

my $statusid;
my $message_str;
my $data_str = '';
my $retcode;
my ($ram_free, $ram_total, $ram_used, $ram_percent);
my ($swap_free, $swap_total, $swap_used, $swap_percent);
if ($SisIYA_Config::osname eq 'Linux') {
	my $file;
	open($file, '<', '/proc/meminfo') || die "$0: Could not open file /proc/meminfo! $!";
	my @lines = <$file>;
	close $file;
	chomp(@lines);
#	foreach (@lines) {
#		print STDERR "$_\n";
#	}

		#$ram_total = get_meminfo('MemTotal:', @lines);
		#$ram_free = get_meminfo('MemFree:', @lines);
	$ram_total = (split(/k/, (split(/:/, (grep(/MemTotal:/, @lines))[0]))[1]))[0];
	$ram_free = (split(/k/, (split(/:/, (grep(/MemFree:/, @lines))[0]))[1]))[0];
	$ram_used = $ram_total - $ram_free;
		#$swap_total = get_meminfo('SwapTotal:', @lines);
		#$swap_free = get_meminfo('SwapFree:', @lines);
	$swap_total = (split(/k/, (split(/:/, (grep(/SwapTotal:/, @lines))[0]))[1]))[0];
	$swap_free = (split(/k/, (split(/:/, (grep(/SwapFree:/, @lines))[0]))[1]))[0];
	$swap_used = $swap_total - $swap_free;
#	print STDERR "SWAP: total=$swap_total free=$swap_free used=$swap_total\n";
#	print STDERR "RAM: total=$ram_total free=$ram_free used=$ram_total\n";
#	print STDERR "formated RAM total=".get_size_k($ram_total)."\n";
} elsif ($SisIYA_Config::osname eq 'SunOS') {
	if (! -f $SisIYA_Config::external_progs{'swap'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = "ERROR: External program $SisIYA_Config::external_progs{'swap'} does not exist!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	}
	my @a = `$SisIYA_Config::external_progs{'swap'} -s`;
	$retcode = $? >>=8;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = " ERROR: Could not execute swap command $SisIYA_Config::external_progs{'swap'}!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	}
	else {
		$swap_total = (split(/k/, (split(/,/, $a[0]))[1]))[0];
		$swap_used = (split(/k/, (split(/=/, $a[0]))[1]))[0];
		$swap_free = $swap_total - $swap_used;
	}
	if (! -f $SisIYA_Config::external_progs{'prtconf'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = "ERROR: External program $SisIYA_Config::external_progs{'prtconf'} does not exist!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	}
	@a = `$SisIYA_Config::external_progs{'prtconf'}`;
	$retcode = $? >>=8;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = " ERROR: Could not execute prtconf command $SisIYA_Config::external_progs{'prtconf'}!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	}
	else {
		my $s = (grep(/^Memory size:/, @a))[0];
		$ram_total = (split(/\s+/, (split(/:/, $s))[1]))[1];
		$ram_total = 1024 * $ram_total;
		$ram_free = 0;
		if (! -f $SisIYA_Config::external_progs{'vmstat'}) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = "ERROR: External program $SisIYA_Config::external_progs{'vmstat'} does not exist!";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		@a = `$SisIYA_Config::external_progs{'vmstat'} 1 2`;
		$retcode = $? >>=8;
		if ($retcode != 0) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = " ERROR: Could not execute vmstat command $SisIYA_Config::external_progs{'vmstat'}!";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		}
		else {
			$s = $a[3];
			$ram_free = (split(/\s+/, $s))[5];
		}
		$ram_used = $ram_total - $ram_free;
	}
} elsif ($SisIYA_Config::osname eq 'HP-UX') {
	# swapinfo -atm
	#             Mb      Mb      Mb   PCT  START/      Mb
	#TYPE      AVAIL    USED    FREE  USED   LIMIT RESERVE  PRI  NAME
	#dev        2048       0    2048    0%       0       -    1  /dev/vg00/lvol2
	#dev        2048     228    1820   11%       0       -    0  /dev/vg00/lvswap
	#reserve       -    2146   -2146
	#memory     3229    2535     694   79%
	#total      7325    4909    2416   67%       -       0    -
	################################################################################
	## swapinfo
	#             Kb      Kb      Kb   PCT  START/      Kb
	#TYPE      AVAIL    USED    FREE  USED   LIMIT RESERVE  PRI  NAME
	#dev     2097152       0 2097152    0%       0       -    1  /dev/vg00/lvol2
	#dev     2097152  233180 1863972   11%       0       -    0  /dev/vg00/lvswap
	#reserve       - 2197388 -2197388
	#memory  3306596 2595884  710712   79%


	if (! -f $SisIYA_Config::external_progs{'swapinfo'}) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = "ERROR: External program $SisIYA_Config::external_progs{'swapinfo'} does not exist!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	}
	my @a = `$SisIYA_Config::external_progs{'swapinfo'}`;
	$retcode = $? >>=8;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = " ERROR: Could not execute swapinfo command $SisIYA_Config::external_progs{'swapinfo'}!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	}
	else {
		my $s = (grep(/^memory/, @a))[0];
		$ram_total = (split(/\s+/, $s))[1];
		$ram_used = (split(/\s+/, $s))[2];
		$ram_free = $ram_total - $ram_used;
		
		$swap_total = 0;
		$swap_used = 0;
		my @b = grep(/^dev/, @a);
		foreach (@b) {
			$swap_total += (split(/\s+/, $_))[1];
			$swap_used += (split(/\s+/, $_))[2];
		}
		$swap_free = $swap_total - $swap_used;
	}
} elsif ($SisIYA_Config::osname eq 'Darwin') {
	# sysctl vm.swapusage
	# vm.swapusage: total = 1024.00M  used = 460.25M  free = 563.75M  (encrypted)


	my $s = `$SisIYA_Config::external_progs{'sysctl'} vm.swapusage`;
	$retcode = $? >>=8;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = " ERROR: Could not execute sysctl command $SisIYA_Config::external_progs{'sysctl'}!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	} else {

		my $x = (split(/\s+/, (split(/=/, $s))[1] ))[1];
		# print STDERR "x=[$x]\n";
		$swap_total = get_swap_value("$x");
		$x = (split(/\s+/, (split(/=/, $s))[3] ))[1];
		#  print STDERR "x=[$x]\n";
		$swap_used = get_swap_value("$x");
		$swap_free = $swap_total - $swap_used;
	}

	$s = `$SisIYA_Config::external_progs{'sysctl'} hw.memsize`;
	$retcode = $? >>=8;
	if ($retcode != 0) {
		$statusid = $SisIYA_Config::statusids{'error'};
		$message_str = " ERROR: Could not execute sysctl command $SisIYA_Config::external_progs{'sysctl'}!";
		print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
	} else {
		my $x = (split(/:/, $s))[1];
		$x =~ s/\s+//g;

		$ram_total = int($x / 1024);

		$ram_free = 0;
		my @a = `$SisIYA_Config::external_progs{'vm_stat'}`;
		$retcode = $? >>=8;
		if ($retcode != 0) {
			$statusid = $SisIYA_Config::statusids{'error'};
			$message_str = " ERROR: Could not execute vm_stat command $SisIYA_Config::external_progs{'vm_stat'}!";
			print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
		} else {
			$x = (split(/:/, (grep(/Pages free:/, @a))[0]))[1];
			$x = (split(/\./, $x))[0];
			$x =~ s/\s+//g;

			$ram_free = int($x * 4096 / 1024);
		}
		$ram_used = $ram_total - $ram_free;
	}
}

$swap_percent = 0;
if ($swap_total != 0) {
	$swap_percent = int(100 * $swap_used / $swap_total);
}
### only for info
$ram_percent = 0;
if ($ram_total != 0) {
	$ram_percent = int(100 * $ram_used / $ram_total);
}
my $s = "SWAP: total=".get_size_k($swap_total)." used=".get_size_k($swap_used)." free=".get_size_k($swap_free).". RAM: total=".get_size_k($ram_total)." used=".get_size_k($ram_used)." free=".get_size_k($ram_free)." usage=".int($ram_percent).'%.';
if ($swap_percent >= $swap_percents{'error'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: Swap usage is ".$swap_percent."% (>= ".$swap_percents{'error'}.")! $s";
	#	SWAP: total=".get_size_k($swap_total)." used=".get_size_k($swap_used)." free=".get_size_k($swap_free).". RAM: total=".get_size_k($ram_total)." used=".get_size_k($ram_used)." free=".get_size_k($ram_free)." usage=".int($ram_percent).'%.';
}
elsif ($swap_percent >= $swap_percents{'warning'}) {
	$statusid = $SisIYA_Config::statusids{'warning'};
	$message_str = "WARNING: Swap usage is ".$swap_percent."% (>= ".$swap_percents{'warning'}.")! $s";
	#	RAM: total=".get_size_k($ram_total)." used=".get_size_k($ram_used)." free=".get_size_k($ram_free)." usage=".int($ram_percent).'%.';
}
else {
	$statusid = $SisIYA_Config::statusids{'ok'};
	$message_str = "OK: Swap usage is ".$swap_percent."%. $s";
	#RAM: total=".get_size_k($ram_total)." used=".get_size_k($ram_used)." free=".get_size_k($ram_free)." usage=".int($ram_percent).'%.';
}
#	
$data_str = '<entries>';
$data_str .= '<entry name="swap_total" type="numeric">'.trim($swap_total).'</entry>';
$data_str .= '<entry name="swap_free" type="numeric">'.trim($swap_free).'</entry>';
$data_str .= '<entry name="swap_used" type="numeric">'.trim($swap_used).'</entry>';
$data_str .= '<entry name="swap_usage_percent" type="numeric">'.trim($swap_percent).'</entry>';
$data_str .= '<entry name="ram_total" type="numeric">'.trim($ram_total).'</entry>';
$data_str .= '<entry name="ram_free" type="numeric">'.trim($ram_free).'</entry>';
$data_str .= '<entry name="ram_used" type="numeric">'.trim($ram_used).'</entry>';
$data_str .= '<entry name="ram_usage_percent" type="numeric">'.trim($ram_percent).'</entry>';
$data_str .= '</entries>';
$data_str = '';

# cat /proc/meminfo
# cat /proc/meminfo 
# MemTotal:        4019664 kB
# MemFree:         2802780 kB
# Buffers:          423664 kB
# Cached:           504420 kB
# SwapCached:          248 kB
# Active:           840476 kB
# Inactive:         248416 kB
# Active(anon):     160156 kB
# Inactive(anon):    35536 kB
# Active(file):     680320 kB
# Inactive(file):   212880 kB
# Unevictable:           0 kB
# Mlocked:    0 kB
# HighTotal:       3286984 kB
# HighFree:        2627932 kB
# LowTotal:         732680 kB
# LowFree:          174848 kB
# SwapTotal:       4095992 kB
# SwapFree:        4094600 kB
# Dirty:    260 kB
# Writeback: 12 kB
# AnonPages:        160724 kB
# Mapped: 37404 kB
# Shmem:  34884 kB
# Slab:  101232 kB
# SReclaimable:      90776 kB
# SUnreclaim:        10456 kB
# KernelStack:        1360 kB
# PageTables:         3776 kB
# NFS_Unstable:          0 kB
# Bounce:     0 kB
# WritebackTmp:          0 kB
# CommitLimit:     6105824 kB
# Committed_AS:     467648 kB
# VmallocTotal:     122880 kB
# VmallocUsed:        5360 kB
# VmallocChunk:     109400 kB
# HugePages_Total:       0
# HugePages_Free:        0
# HugePages_Rsvd:        0
# HugePages_Surp:        0
# Hugepagesize:       2048 kB
# DirectMap4k:       10232 kB
# DirectMap2M:      897024 kB
## #############################################################
# on a SunOS system
## #############################################################
#swap -s
#total: 276788k bytes allocated + 103884k reserved = 380672k used, 4552520k available
#####
# prtconf
# System Configuration:  Oracle Corporation  i86pc
# Memory size: 2048 Megabytes
# System Peripherals (Software Nodes):
#
# i86pc
#     scsi_vhci, instance #0
#         pci, instance #0
#      pci8086,1237 (driver not attached)
#   isa, instance #0
#        i8042, instance #0
#  keyboard, instance #0
#           mouse, instance #0
# lp, instance #0 (driver not attached)
#      pit_beep, instance #0
#   pci-ide, instance #0
#    ide, instance #0
#  cmdk, instance #0
#   ide (driver not attached)
#    display, instance #0
# pci8086,1e, instance #0
#         pci80ee,cafe, instance #0
#      pci8086,7113 (driver not attached)
#   fw, instance #0
#           cpu (driver not attached)
#        sb, instance #1
# used-resources (driver not attached)
#     fcoe, instance #0
#  iscsi, instance #0
#      agpgart, instance #0 (driver not attached)
#          options, instance #0
#   pseudo, instance #0
#        vga_arbiter, instance #0 (driver not attached)
#   xsvc, instance #0
#
######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
