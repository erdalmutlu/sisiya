#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use SisIYA_Config;

if (-f $SisIYA_Config::local_conf) {
	require $SisIYA_Config::local_conf;
}
if (-f $SisIYA_Config::functions) {
	require $SisIYA_Config::functions;
}
#######################################################################################
#### the default values
our $mapr_userticket = '/opt/mapr/conf/mapruserticket';
#### end of the default values
################################################################################
sub parse_cldb {
	my ($rtr, $ip_ref, $port_ref, $epoch_ref, $rtatus_ref) = @_;
	
	# 53.55.88.155:5660--2960-VALID
	# print STDERR "parse_cldb: str: $rtr\n";
	my @a = split(/:/, $rtr);
	my @b = split(/-/, $a[1]);
	
	$$ip_ref = $a[0];
	$$port_ref = $b[0];
	$$epoch_ref = $b[2];
	$$rtatus_ref = $b[3];
}
################################################################################
my $service_name = 'mapr_cldb';
## override defaults if there is a corresponding conf file
my $module_conf_file = "$SisIYA_Config::conf_d_dir/sisiya_$service_name.conf";
if (-f $module_conf_file) {
	require $module_conf_file;
}
################################################################################
my $message_str = '';
my $data_str = '';
my $statusid = $SisIYA_Config::statusids{'ok'};
my $error_str = '';
my $ok_str = '';
my $warning_str = '';
my $info_str = '';

use JSON;

if (! -f $SisIYA_Config::external_progs{'maprcli'}) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: External program $SisIYA_Config::external_progs{'maprcli'} does not exist!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

$ENV{'MAPR_TICKETFILE_LOCATION'} = $mapr_userticket;

my @a = `$SisIYA_Config::external_progs{'maprcli'} dump containerinfo -ids 1 -json 2>&1`;
my $retcode = $? >>=8;
if ($retcode != 0) {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = "ERROR: $SisIYA_Config::external_progs{'maprcli'} dump containerinfo -ids 1 -json error=$retcode message: @a!";
	print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
}

chomp(@a = @a);
my $json_str = join(' ', @a);

my $r = decode_json $json_str;

if ($r->{'status'} eq 'OK') {
        $info_str .= "INFO: VolumeReplication: $r->{'data'}[0]{'VolumeReplication'}\n";
        $info_str .= "INFO: NameSpaceReplication: $r->{'data'}[0]{'NameSpaceReplication'}\n";

        my ($ip, $epoch, $port, $rtatus);
        parse_cldb($r->{'data'}[0]{'Master'}, \$ip, \$port, \$epoch, \$rtatus);
	$ok_str .= "OK: MapR CLDB Master: ip: $ip port: $port epoch: $epoch status: $rtatus\n";

	my $master_server_ip = $ip;	
	if (%{$r->{'data'}[0]{'ActiveServers'}}) {
		foreach (@{$r->{'data'}[0]{'ActiveServers'}{'IP'}}) {
			parse_cldb($_, \$ip, \$port, \$epoch, \$rtatus);
			if ($master_server_ip ne $ip) {
				$ok_str .= "OK: MapR CLDB active server ip: $ip port: $port epoch: $epoch status: $rtatus\n";
			}
		}
	}
	
	if (%{$r->{'data'}[0]{'InactiveServers'}}) {
		$warning_str .= "WARNING: There are inactive MapR CLDB servers!\n";
	}
	
	if (%{$r->{'data'}[0]{'UnusedServers'}}) {
		$warning_str .= "WARNING: There are unused MapR CLDB servers!\n";
	}

} elsif ($r->{'status'} eq 'ERROR') {
	foreach (@{$r->{'errors'}}) {
		$error_str .= "ERROR: id: $_->{'id'} $_->{'desc'}";
	}
} else {
	$error_str .= "ERROR: Unknown error status: $r->{'status'}";
}

if ($error_str ne '') {
	$statusid = $SisIYA_Config::statusids{'error'};
	$message_str = $error_str;
}
if ($warning_str ne '') {
	if ($statusid < $SisIYA_Config::statusids{'warning'}) {
		$statusid = $SisIYA_Config::statusids{'warning'};
	}	
	$message_str .= $warning_str;
}
if ($ok_str ne '') {
	$message_str .= $ok_str;
}
if ($info_str ne '') {
	$message_str .= $info_str;
}
##################################################################################
# maprcli dump containerinfo -ids 1 -json
##################################################################################
#{
#        "timestamp":1655563606047,
#        "timeofday":"2022-06-18 04:46:46.047 GMT+0200 PM",
#        "status":"OK",
#        "total":1,
#        "data":[
#                {
#                        "ContainerId":1,
#                        "Epoch":2960,
#                        "Master":"53.55.88.153:5660--2960-VALID",
#                        "ActiveServers":{
#                                "IP":[
#                                        "53.55.88.153:5660--2960-VALID",
#                                        "53.55.88.155:5660--2960-VALID",
#                                        "53.55.88.154:5660--2960-VALID"
#                                ]
#                        },
#                        "InactiveServers":{
#
#                        },
#                        "UnusedServers":{
#
#                        },
#                        "OwnedSizeMB":"0 MB",
#                        "SharedSizeMB":"0 MB",
#                        "LogicalSizeMB":"0 MB",
#                        "TotalSizeMB":"0 MB",
#                        "NameContainer":"true",
#                        "CreatorContainerId":0,
#                        "CreatorVolumeUuid":"",
#                        "UseActualCreatorId":false,
#                        "VolumeName":"mapr.cldb.internal",
#                        "VolumeId":1,
#                        "VolumeReplication":3,
#                        "NameSpaceReplication":3,
#                        "VolumeMounted":false,
#                        "AccessTime":"June 6, 2019"
#                }
#        ]
#}
#######################################################################
# example for status: ERROR
#{
#        "timestamp":1655567585241,
#        "timeofday":"2022-06-18 05:53:05.241 GMT+0200 PM",
#        "status":"ERROR",
#        "errors":[
#                {
#                        "id":10009,
#                        "desc":"Couldn't connect to the CLDB service"
#                }
#        ]
#}
#######################################################################

######################################################################################
print_and_exit($SisIYA_Config::FS, $service_name, $statusid, $message_str, $data_str);
######################################################################################
