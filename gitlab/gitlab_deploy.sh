#!/usr/bin/env bash
#
# This script is used to deploy packages using gitlab ci cd pipeline.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#################################################################################
# Usage:
# gitlab_deploy.sh target_system
#
# target_system: deb, rpm or pacman
#
# End of Usage
#################################################################################
### defaults
deploy_prog_prefix=deploy_packages
### end of defaults
#################################################################################
#
# Display usage information about the script but outputing
# the description at the top of this file
#
function usage() {
	local message="$1"

	echo -e "$message\n"

	# Display text from line 3 until End Of Usage and remove first two columns
	sed -e '3,/# End of Usage/!d' -e 's/^.\{1,2\}//g' -e '/^End of Usage/d' "${BASH_SOURCE[0]}"
	exit 1
}

deploy_packages()
{
	conf_file="$tmp_dir/${deploy_prog_prefix}.conf"
	script_file="$git_dir/utils/${deploy_prog_prefix}.sh"

	branch_name=$(get_branch_name)

	# the script expect git_dir to be parent dir
	parent_dir="$(dirname "$git_dir")"
	touch "$conf_file"
	echo "$0: Deploying packages for branch [$branch_name]..."
	{
		echo "git_branch=\"$branch_name\""
		echo "deploy_target=$target_system"
		echo "log_dir=/tmp"
		echo "tmp_output_dir=\"$tmp_dir\""
		echo "git_dir=\"$parent_dir\""
		echo "project_dir=\"$git_dir\""
		echo "deb_deploy_prog=\"$git_dir/utils/deploy_deb.sh\""
		echo "rpm_deploy_prog=\"$git_dir/utils/deploy_rpm.sh\""
		echo "pacman_deploy_prog=\"$git_dir/utils/deploy_pacman.sh\""
		echo "clean_git_dir=false"
		echo "deploy_status_file_prefix=/tmp/deploy_status"
	} >> "$conf_file"
	cat "$conf_file"
	echo "$script_file $conf_file"
	$script_file "$conf_file"
}

check_git_porcelain()
{
	str=$(git status --porcelain)
	if [ -n "$str" ]; then
		echo "$0: The working tree is not clean!"
		git status --porcelain
		error "Please commit your changes or disgard them and try againi!"
	fi
}

check_prerequisites()
{
	if [[ "$target_system" == "deb" ]] ; then
		# check for launchpad setup files
		for f in "$HOME/.jenkins_sisiya_private.key" "$HOME/.dput.cfg" ; do
			if [ ! -f "$f" ] ; then
				echo "check_prerequisites: File $f does not exists!"
				exit 1
			fi
		done
	fi

	if [[ "$target_system" == "rpm" ]] ; then
		# check for osc setup files
		for f in "$HOME/prj" "$HOME/.config/osc/oscrc" ; do
			if [ ! -e "$f" ] ; then
				echo "check_prerequisites: $f does not exists!"
				exit 1
			fi
		done
	fi
}

cleanup()
{
	rm -rf "$tmp_dir"
}

error()
{
	echo "$0: $1"
	exit 1
}

error_without_exit()
{
	echo "$0: $1"
}

get_branch_name()
{
	if [ -n "$CI_COMMIT_BRANCH" ]; then
		echo "$CI_COMMIT_BRANCH"
	else
		git branch -v | grep "\*" | cut -d " " -f 2
	fi
}

initialize()
{
	cleanup
	mkdir -p "$tmp_dir"
}
#################################################################################
trap cleanup EXIT

if [ $# -ne 1 ]; then
	usage "Please specify needed parameters!"
fi

target_system="$1"
git_dir="$CI_PROJECT_DIR"
tmp_dir="/tmp/sisiya_gitlab_tmp_$target_system"

initialize
check_prerequisites
check_git_porcelain
deploy_packages
