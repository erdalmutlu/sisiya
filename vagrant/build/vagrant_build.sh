#!/usr/bin/env bash
#
# This script is used to run vagrant boxes and build packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#################################################################################
### defaults
deb_build_dir="tmp_sisiya_dir/deb"		# directory where DEB packages are stored after building
rpm_build_dir="rpmbuild/RPMS"			# directory where RPM packages are stored after building
pacman_build_dir="tmp_sisiya_dir/pacman"	# directory where pacman packages are stored after building
build_prog_prefix=build_packages
poweroff_after_build="true"
number_of_systems=10
system_names[0]="centos6"
system_types[0]="rpm"
system_archs[0]="x86_64"
system_names[1]="centos7"
system_types[1]="rpm"
system_archs[1]="x86_64"
system_names[2]="centos8"
system_types[2]="rpm"
system_archs[2]="x86_64"
system_names[3]="ubuntu1204"
system_types[3]="deb"
system_archs[3]="x86_64"
system_names[4]="ubuntu1604"
system_types[4]="deb"
system_archs[4]="x86_64"
system_names[5]="ubuntu1804"
system_types[5]="deb"
system_archs[5]="x86_64"
system_names[6]="ubuntu2004"
system_types[6]="deb"
system_archs[6]="x86_64"
system_names[7]="archlinux"
system_types[7]="pacman"
system_archs[7]="x86_64"
system_names[8]="fedora32"
system_types[8]="rpm"
system_archs[8]="x86_64"
system_names[9]="fedora31"
system_types[9]="rpm"
system_archs[9]="x86_64"
### end of defaults
#################################################################################
build()
{
	declare -i i=0
	while [ $i -lt $number_of_systems ] ; do
		system_name="${system_names[$i]}"
		system_type="${system_types[$i]}"
		arch_str="${system_archs[$i]}"

		cd "boxes/$system_name" || error "cd boxes/$system_name"
		build_packages
		get_packages

		if [[ "$poweroff_after_build" == "true" ]]; then
			echo "$0: Powering down vagrant box $system_name ..."
			vagrant halt || error "vagrant halt"
		fi

		cd ../..
		i=$((i + 1))
	done
}

build_packages() {
	conf_file=${build_prog_prefix}.conf
	script_file=${build_prog_prefix}.sh
	
	branch_name=$(get_branch_name)

	check_vagrant_box

	echo "git_branch=$branch_name" > "$conf_file"

	echo "$0: Copying $conf_file into vagrand box..."
	vagrant scp "$conf_file" "$conf_file"

	echo "$0: Copying $script_file into vagrand box..."
	vagrant scp "../../../../utils/$script_file" "$script_file"

	echo "$0: Building packages inside vagrant box $system_name for barnch [$branch_name]..."
	vagrant ssh -c "./$script_file $conf_file"
}

# check_build_status() {
# 	if [[ ! -f "$build_status_file" ]]; then
# 		touch "$build_status_file"
# 		return
# 	fi
# 	str=$(grep "$1" "$build_status_file" | grep "OK")
# 	if [[ -n "$str" ]]; then
# 		echo "Packages for $1 are already built."
# 		echo "If you would like to rebuild, please remove the following line from $build_status_file file"
# 		echo "$1"
# 		exit 0
# 	fi
# }

check_git_porcelain() {
	str=$(git status --porcelain)
	if [[ -n "$str" ]]; then
		echo "$0: The working tree is not clean!"
		git status --porcelain
		error "Please commit your changes or disgard them and try againi!"
	fi
}

check_prerequisites() {
	for f in vagrant ansible git ; do
		if ! "$f" --version >/dev/null 2>&1 ; then
			error "$f is not isntalled!"
		fi	
	done

	f="vagrant-scp"
	echo "$0: Checking for vagrant plugin $f..."
	str=$(vagrant plugin list | grep $f)
	if [[ -z "$str" ]]; then
		echo "vagrant pluging $f is not installed! Trying to install..."
		vagrant plugin install $f || error "Could not install vagrant plugin $f!"
	fi

	echo "$0: Updating vagrant plugins..."
	vagrant plugin update
}

check_vagrant_box() {
	str=$(vagrant status --machine-readable | grep state,running)
	if [[ -z "$str" ]]; then
		echo "$0: Vagrant box is not running. Trying to power up..."
		vagrant up
	fi
	str=$(vagrant status --machine-readable | grep state,running)
	if [[ -z "$str" ]]; then
		error "I am sorry! Could not power up the vagrant box!"
	fi
	echo "$0: Vagrant box is up and running. Trying to provision..."
	vagrant provision
	echo "$0: Vagrant box is ready."
}

cleanup() {
	rm -f git_branch.conf
}

error() {
	echo "$0: $1"
	exit 1
}

error_without_exit() {
	echo "$0: $1"
}

get_branch_name() {
	# shellcheck disable=SC2063
	git branch -v | grep '*' | cut -d " " -f 2
}

get_packages() {
	echo "$0: Getting packages from $system_name ($arch_str)..."
	case "$system_type" in
		"deb")
			vagrant scp :"$deb_build_dir"/*.deb .
			;;
		"rpm")
			vagrant scp :"$rpm_build_dir"/noarch/*.rpm .
			vagrant scp :"$rpm_build_dir"/"$arch_str"/*.rpm .
			;;
		"pacman")
			vagrant scp :"$pacman_build_dir"/*.pkg.tar.zst .
			;;
	esac
}

initialize() {
	if [[ $# -eq 1 ]]; then
		conf_file="$1"
		if [[ -f "$conf_file" ]]; then
			### source the conf file
			# shellcheck disable=SC1090
			source "$conf_file"
		fi
	else
		conf_file="vagrant_build.conf"
		if [[ -f $conf_file ]]; then
			### source the conf file
			# shellcheck disable=SC1090
			source "$conf_file"
		fi
	fi
}
#################################################################################
trap cleanup EXIT

initialize "$@"
check_prerequisites
check_git_porcelain
build
