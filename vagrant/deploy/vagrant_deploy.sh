#!/usr/bin/env bash
#
# This script is used to run vagrant boxes and build packages.
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#################################################################################
### defaults
deploy_prog_prefix=deploy_packages
poweroff_after_deploy="true"
target_systems="deb rpm pacman"
rpm_deploy_system="ubuntu2004"		# OpenSUSE Build System is deployed via this system
deb_deploy_system="ubuntu2004"		# launchpad is deployed via this system
pacman_deploy_system="archlinux"	# pacman packages are build and deployed via this system
### end of defaults
#################################################################################
deploy()
{
	target_system=$1
	case $target_system in
		"deb" | "rpm")
			system_name=$deb_deploy_system
			;;
		"pacman")
			system_name=$pacman_deploy_system
			;;
	esac
	
	cd boxes/$system_name || error "cd boxes/$system_name"	
	deploy_packages $target_system
	retcode=$?

	if [ "$poweroff_after_deploy" == "true" ]; then
		echo "$0: Powering down vagrant box $system_name ..."
		vagrant halt || error "vagrant halt"
	fi
	cd ../../
	return $retcode
}

deploy_packages()
{
	target_system=$1
	conf_file=${deploy_prog_prefix}.conf
	script_file=${deploy_prog_prefix}.sh
	
	branch_name=$(get_branch_name)

	echo "$0: Deploying packages withing vagrant box $system_name for branch [$branch_name]..."
	check_vagrant_box
	echo "git_branch=$branch_name"		> $conf_file
	echo "deploy_target=$target_system"	>> $conf_file
	vagrant scp $conf_file $conf_file
	vagrant scp ../../../../utils/$script_file $script_file
	vagrant ssh -c "./$script_file $conf_file"
}

check_git_porcelain()
{
	str=`git status --porcelain`
	if [ -n "$str" ]; then
		echo "$0: The working tree is not clean!"
		git status --porcelain
		error "Please commit your changes or disgard them and try againi!"
	fi
}

check_prerequisites()
{
	for f in vagrant ansible git
	do
		$f --version >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			error "$f is not isntalled!"
		fi	
	done

	for f in "vagrant-scp"
	do
		echo "$0: Checking for vagrant plugin $f..."
		str=`vagrant plugin list | grep $f`
		if [ -z "$str" ]; then
			echo "vagrant pluging $f is not installed! Trying to install..."
			vagrant plugin install $f || error "Could not install vagrant plugin $f!"
		fi
	done

	echo "$0: Updating vagrant plugins..."
	vagrant plugin update
}

check_vagrant_box()
{
	str=`vagrant status --machine-readable | grep state,running`
	if [ -z "$str" ]; then
		echo "$0: Vagrant box is not running. Trying to power up..."
		vagrant up --provider virtualbox
	fi
	str=`vagrant status --machine-readable | grep state,running`
	if [ -z "$str" ]; then
		error "I am sorry! Could not power up the vagrant box!"
	fi
	echo "$0: Vagrant box is up and running. Trying to provision..."
	vagrant provision
	echo "$0: Vagrant box is ready."
}

cleanup()
{
	rm -f cicd_branch.conf
}

error()
{
	echo "$0: $1"
	exit 1
}

error_without_exit()
{
	echo "$0: $1"
}

get_branch_name()
{
	git branch -v | grep '*' | cut -d " " -f 2
}

initialize()
{
	if test $# -eq 1 ; then
		conf_file=$1
		if [ -f "$conf_file" ]; then
			### source the conf file
			. $conf_file
		fi
	else
		conf_file="vagrant_deploy.conf"
		if [ -f $conf_file ]; then
			### source the conf file
			. $conf_file
		fi
	fi
}
#################################################################################
trap cleanup EXIT

target_system=""
if [ $# -eq 1 ]; then
	target_system=$1
fi

initialize
check_prerequisites
check_git_porcelain

if [ -n "$target_system" ]; then
	deploy $target_system
else
	for t in $target_systems
	do
		deploy $t
		if [ $? -ne 0 ]; then
			error "Could not deploy for $t!"
		fi
	done
fi
