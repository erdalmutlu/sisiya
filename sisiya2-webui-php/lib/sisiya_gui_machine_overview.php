<?php
/*
    Copyright (C) 2003 - 2012 Erdal Mutlu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
error_reporting(E_ALL);
###########################################################
### begin of functions
###########################################################
###########################################################
### end of functions
###########################################################
$html='';

###################################################################################################################################################
	$sql_str="select a.*";
	$sql_str.="from machines a";
	#$sql_str.=" where a.statusid=b.id and a.systemid=c.id and c.active='t' and c.systemtypeid=e.id";
debug('sql_str='.$sql_str);
$result=$db->query($sql_str);
if(!$result)
	errorRecord('select');
else {
	$nrows=$db->getRowCount($result);
	if($nrows > 0) {
		$h->addHeadContent('<meta http-equiv="cache-control" content="no-cache" />');
		$h->addHeadContent('<meta http-equiv="refresh" content="180" />');
		$nsystems=0;
		$flag=TRUE;
		$row_index=0;
		$html.='<table class="system_overview">'."\n";
		$html.='<tr class="header"><td colspan="'.$ncolumns.'">';
		$html.='Örme Makinaları</td></tr>'."\n";
		$j = 0;
		while($flag ==  TRUE) {
			for($i=0;$i<$ncolumns && $flag == TRUE;$i++) {
				if($row_index >= $nrows) {
					$flag=FALSE;
					break;
				}
				$row=$db->fetchRow($result,$row_index);
				$row_index++;
				if($i == 0) 
					$html.='<tr class="row">'."\n";
				$html.='<td>';
				$html.='<a href="'.$mainProg.'"';
				$image_str = '';
				$mstatus = '';
					# mcomstatus : 0 no problem, > 0 number of failed connection attempts (max 25)
					if ($row[2] != 0) {
						$mstatus = 'ulaşılamıyor';
						$image_str.='<img src="./images/sisiya/orme_question6_mid.png" alt="UNAVAILABLE" />';
					} else {
						# mstatus: 	0 : power down
						#		1 : running
						#		2 : no bag
						#		3 : stopped
						#		4 : alarm 
						# machine alarms: 	word, each bit is a alarm
						#  :
						if ($row[6] == 0) {	# power down
							$mstatus = 'güç kapalı';
							$image_str.='<img src="./images/sisiya/orme_shutdown2_mid.png" alt="güç kapalı" />';
						} elseif ($row[6] == 1) {	# running
							$mstatus = 'çalışıyor';
							$image_str.='<img src="./images/sisiya/orme_ok3_mid.png" alt="çalışıyor" />';
						} elseif ($row[6] == 2) { # no bag
							$mstatus = 'torba yok';
							$image_str.='<img src="./images/sisiya/orme_bag2_mid.png" alt="torba yok" />';
						} elseif ($row[6] == 3) { # stoped
							$mstatus = 'durdu';
							$image_str.='<img src="./images/sisiya/orme_pause2_blue_mid.png" alt="durdu" />';
						} elseif ($row[6] == 4) { # alarm
							$mstatus = 'alarm';
							$image_str.='<img src="./images/sisiya/orme_alert_red3_mid.png" alt="alarm" />';
						} else { # unknown 
							$mstatus = 'bilinmeyen kod='.$row[6].'!';
							$image_str.='<img src="./images/sisiya/orme_question5_mid.png" alt="bilinmeyen kod" />';
						}
					}
					$html.=' title="'.$row[1].'('.$row[0].'): '.$mstatus.', çorap sayısı: '.$row[11].'/'.$row[12].', Hız:'.$row[7].'">';
					$html.= $image_str;
					$html.='</a></td>'."\n";
				#}
				$nsystems++;
			}
			$html.='</tr>'."\n";
			$j++;
			if ($j == 8) { # put an empty row after every 8 rows
				$html.='<tr class="header"><td colspan="'.$ncolumns.'">';
				$html.='&nbsp;</td></tr>'."\n";
				$j = 0;
			}
		}
		$html.='</tr>'."\n";
		$html.='<tr class="footer"><td colspan="'.$ncolumns.'">'.$lrb['sisiya_gui.label.TotalNumberOfSystems'].' : '.$nsystems;
		$html.='</td></tr>'."\n";
		$html.="</table>\n";
	}
	$db->freeResult($result);
}
$h->addContent($html);
?>
