#!/usr/bin/php
<?php
/*
    Copyright (C) 2003 - __YEAR__  Erdal Mutlu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

*/
error_reporting(E_ALL);

if(count($argv) != 3) {
	echo 'Usage  : '.$argv[0]." web_root_dir sisiya_switch_host\n";
	echo 'Example: '.$argv[0]." /usr/share/sisiya-webui-php swtalt03\n";
	exit(1);
}

global $web_root_dir, $prog_name, $sisiya_host;

$prog_name = $argv[0];
$web_root_dir = $argv[1];
$sisiya_host = $argv[2];

include_once($web_root_dir.'/config.php');
include_once(CONF_DIR.'/sisiya_common_conf.php');
#include_once(CONF_DIR.'/sisiya_gui_conf.php');

function getSwitchFromConfXML($conf_file_name, $host, &$isactive, &$hostname, &$snmp_version, &$community)
{
	$conf_file = BASE_DIR.'/xmlconf/'.$conf_file_name;
	if (file_exists($conf_file)) {
		$xml = simplexml_load_file($conf_file);
		if ($xml) {
			$i = 0;
			foreach ($xml->children() as $record) {
				if ($xml->record[$i]->system_name == $host) {
					#echo 'Found :'.$i." \n";
					#echo 'system_name	: '.$xml->record[$i]->system_name."\n";
					#echo 'isactive		: '.$xml->record[$i]->isactive."\n";
					#echo 'hostname		: '.$xml->record[$i]->hostname."\n";
					#echo 'snmp_version	: '.$xml->record[$i]->snmp_version."\n";
					#echo 'community		: '.$xml->record[$i]->community."\n";
					#echo 'username		: '.$xml->record[$i]->username."\n";
					#echo 'password		: '.$xml->record[$i]->password."\n";
					$isactive = $xml->record[$i]->isactive;
					$hostname = $xml->record[$i]->hostname;
					$snmp_version = $xml->record[$i]->snmp_version;
					$community = $xml->record[$i]->community;
					return;
				}
				$i++;
			}
		}
	}
}

function getSisIYASystemID($sisiya_host)
{
	global $db;
}

#####################################################################################################################
echo "Checking switch : ".$sisiya_host."...\n";

getSwitchFromConfXML('switch_systems.xml', $sisiya_host, $isactive, $hostname, $snmp_version, $community);

echo 'isactive : '.$isactive."\n";
echo 'hostname : '.$hostname."\n";
echo 'snmp_version : '.$snmp_version."\n";
echo 'community : '.$community."\n";

if ($isactive == 'f') {
	echo "The switch is not active. Exiting...\n";
	exit;
}
if ($snmp_version == '1') {
	$contact = snmpget($hostname, $community, "system.sysContact.0");
	$location = snmpget($hostname, $community, "system.sysLocation.0");
	$description = snmpget($hostname, $community, "system.sysDescr.0");
} elseif ($snmp_version == '2c') {
	#echo "name: ".snmp2_get($hostname, $community, "system.sysName.0") ."\n";
	$contact = snmp2_get($hostname, $community, "system.sysContact.0");
	$location = snmp2_get($hostname, $community, "system.sysLocation.0");
	$description = snmp2_get($hostname, $community, "system.sysDescr.0");
} #else {
#	$contact = snmp3_get($hostname, $community, "system.sysContact.0");
#	}

echo "location : ".$location."\n";
echo "contact : ".$contact."\n";
echo "description : ".$description."\n";

$sisiya_system_id = getSisIYASystemID($sisiya_host);
?>
