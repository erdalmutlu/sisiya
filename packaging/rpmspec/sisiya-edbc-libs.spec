#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

%define is_fedora %(test -e /etc/fedora-release && echo 1 || echo 0)
%define is_redhat %(test -e /etc/redhat-release && echo 1 || echo 0)
%define is_suse %(test -e /etc/SuSE-release && echo 1 || echo 0)
# do not create debuginfo rpm package
%global debug_package %{nil}


Name: sisiya-edbc-libs
Url: http://www.sisiya.org
Summary: Libraries written in C++ for Database connectivity like JDBC
BuildRoot: %{_builddir}/%{name}-root
Version: __VERSION__
Release: 0
Source0: http://sourceforge.net/projects/sisiya/files/sisiya/%{version}/rpm/%{name}-%{version}.tar.gz
License: GPL-2.0+
Vendor: Erdal Mutlu
Group: System Environment/Daemons
Packager: Erdal Mutlu <erdal@sisiya.org>
BuildRequires: autoconf automake make gcc-c++ postgresql-devel
Requires: postgresql-libs

# for RedHat / CentOS / Scientific systems
%if 0%{?rhel_version} || 0%{?centos_ver} || 0%{?centos_version} || 0%{?scientificlinux_version}
%if 0%{?rhel_version} >= 800 || 0%{?centos_ver} >= 8 || 0%{?centos_version} >= 800 || 0%{?scientificlinux_version} >= 800
BuildRequires: mariadb-connector-c-devel
Requires: mariadb-connector-c-devel
%else
%if 0%{?rhel_version} == 700 || 0%{?centos_ver} == 700 || 0%{?centos_version} == 700 || 0%{?scientificlinux_version} == 700
BuildRequires: mariadb-devel
Requires: mariadb-libs
%else
BuildRequires: mysql-devel
Requires: mysql-libs
%endif
%endif
%endif

# for Fedora linux systems
%if 0%{?fedora}
%if 0%{?fedora} < 28
BuildRequires: mariadb-devel
Requires: mariadb-libs
%else
BuildRequires: mariadb-connector-c-devel
Requires: mariadb-connector-c-devel
%endif
%endif

# for SUSE systems
%if 0%{?suse_version}
BuildRequires: mysql-devel
%endif

AutoReqProv: no
%description 
Libraries written in C++ for Database connectivity like JDBC used
by the SisIYA daemon.

%prep 
%setup -n %{name}-%{version}

%build
./bootstrap create
./configure --prefix=
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

make DESTDIR=%{buildroot} LIBDIR=%_libdir install 

%post

%postun

%clean 
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%_libdir/*.so*

%changelog
