#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# do not create debuginfo rpm package
%global debug_package %{nil}

Name: sisiyad
Summary: SisIYA daemon
Url: http://www.sisiya.org
Version: __VERSION__
Release: 0
Source0: http://sourceforge.net/projects/sisiya/files/sisiya/%{version}/rpm/%{name}-%{version}.tar.gz
License: GPL-2.0+
Vendor: Erdal Mutlu
Group: System Environment/Daemons
Packager: Erdal Mutlu <erdal@sisiya.org>
BuildRoot:%{_tmppath}/%{name}-root
BuildRequires: autoconf automake make gcc-c++ postgresql-devel
Requires: sisiya-edbc-libs

####################################
# Always check if defined first!!!
####################################

# for RedHat / CentOS / Scientific systems
%if 0%{?rhel_version} || 0%{?centos_ver} || 0%{?centos_version} || 0%{?scientificlinux_version}
%if 0%{?centos_ver} >= 8 || 0%{?rhel_version} >= 800 || 0%{?centos_version} >= 800 || 0%{?scientificlinux_version} >= 800
BuildRequires: mariadb-connector-c-devel
%else
%if 0%{?centos_ver} == 7 || 0%{?rhel_version} == 700 || 0%{?centos_version} == 700 || 0%{?scientificlinux_version} == 700
BuildRequires: mariadb-devel
%else
BuildRequires: mysql-devel
%endif
%endif
%endif

# for Fedora linux systems
%if 0%{?fedora}
%if 0%{?fedora} < 28
BuildRequires: mariadb-devel
%else
BuildRequires: mariadb-connector-c-devel
%endif
%endif

# for SUSE systems
%if 0%{?suse_version} 
BuildRequires: mysql-devel
%endif

# RedHat / CentOS / Scientific / Fedora systems
#%if (0%{?rhel_version} && 0%{?rhel_version} >= 700) || (0%{?centos_ver} && 0%{?centos_ver} >= 7) || (0%{?centos_version} && 0%{?centos_version} >= 700) || (0%{?scientific_version} && 0%{?scientific_version} >= 700) || (0%{?fedora} && 0%{?fedora} >= 14)
%if 0%{?rhel_version} || 0%{?centos_version} || 0%{?centos_ver} || 0%{?scientific_version} || 0%{?fedora}
%if 0%{?rhel_version} >= 700 || 0%{?centos_version} >= 700 || 0%{?centos_ver} >= 7 || 0%{?scientific_version} >= 700 || 0%{?fedora} >= 14
%define has_systemd 1
%endif
%endif

# openSUSE systems
%if 0%{?is_opensuse} && 0%{?suse_version} >= 1140
%define has_systemd 1
%endif

# SLE systems
%if !0%{?is_opensuse} && 0%{?sle_version} >= 120000
%define has_systemd 1
%endif

# SLE backports systems
%if 0%{?is_backports} && 0%{?sle_version} >= 120000
%define has_systemd 1
%endif

%if 0%{?has_systemd}
BuildRequires: systemd
Requires: systemd
%define sisiyad_service_root_dir /usr/lib/systemd/system
%define sisiyad_service_dst_dir /usr/lib/systemd/system
%define sisiyad_service_src_file systemd/sisiyad.service
%define sisiyad_service_dst_file sisiyad.service
%else
%define sisiyad_service_root_dir /etc
%define sisiyad_service_dst_dir /etc/init.d
%define sisiyad_service_src_file sisiyad_sysvinit
%define sisiyad_service_dst_file sisiyad
%endif

%description
The SisIYA daemon is a program which receives incomming SisIYA messages and records them
in a database system.

%prep 
%setup -n %{name}-%{version}

%build
./bootstrap create
./configure --prefix=/
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
make "DESTDIR=%{buildroot}" install 

mkdir -p %{buildroot}%{sisiyad_service_dst_dir}
cp etc/%{sisiyad_service_src_file} %{buildroot}%{sisiyad_service_dst_dir}/%{sisiyad_service_dst_file}

%if !0%{?has_systemd}
chmod 755 %{buildroot}%{sisiyad_service_dst_dir}/%{sisiyad_service_dst_file}
%endif

%post
#if test $1 -ne 2 ; then
#	exit 0
#fi

#%if 0%{?has_systemd}
#systemctl restart sisiyad
#%else
#service sisiyad restart > /dev/null 2>&1
#%endif

%preun 
### if update
#if test $1 -eq 1 ; then
#	exit 0
#fi

#%if 0%{?has_systemd}
#systemctl stop sisiyad
#systemctl disable sisiyad
#%else
#service sisiyad stop > /dev/null 2>&1
#chkconfig --del sisiyad
#%endif

%clean 
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
#%attr(0644,root,root) %doc AUTHORS ChangeLog NEWS README
%dir /etc/sisiya
%dir /etc/sisiya/sisiyad
%{sisiyad_service_dst_dir}/%{sisiyad_service_dst_file}
%attr(0600,root,root) %config(noreplace) /etc/sisiya/sisiyad/sisiyad.conf
%attr(0755,root,root) /usr/bin/sisiyad
/usr/share/man/man5/sisiyad.conf.5.gz
/usr/share/man/man8/sisiyad.8.gz

%changelog
