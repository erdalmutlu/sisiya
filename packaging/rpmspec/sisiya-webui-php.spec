#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA


%if 0%{?rhel_version} || 0%{?centos_version} || 0%{?fedora_version}
	%define www_user  apache
	%define www_group apache
%else
	%if 0%{?suse_version} || 0%{?sle_version}
		%define www_user  root
		%define www_group root
	%else
		%define www_user  apache
		%define www_group apache
	%endif
%endif


Name: sisiya-webui-php
%define install_dir /usr/share/%{name}
# some suse distros define shared state directory as /usr/com, which breaks building rpms. For now:
%define shared_state_dir /var/lib
Summary: PHP web UI for SisIYA
Url: http://www.sisiya.org
BuildArch: noarch
BuildRoot: %{_builddir}/%{name}-root
Version: __VERSION__
Release: 0
Source0: http://sourceforge.net/projects/sisiya/files/sisiya/%{version}/rpm/%{name}-%{version}.tar.gz
License: GPL-2.0+
Vendor: Erdal Mutlu
Group: System Environment/Tools
Packager: Erdal Mutlu <erdal@sisiya.org>
Requires: bc, php, php-snmp, php-pgsql, php-gd, php-mbstring, php-xml, nmap, sisiya-client-checks, sisiya-remote-checks, sisiya-webui-images

AutoReqProv: no
%description 
PHP web user and administration interface for SisIYA.

%prep 
%setup -n %{name}-%{version}

%install
make DESTDIR=%{buildroot} INSTALL_DIR=%{install_dir} DOC_DIR=%{_docdir} MAN_DIR=%{_mandir} SHARED_STATE_DIR=%{shared_state_dir} SYSCONF_DIR=%{_sysconfdir}  install 

%post
	if test ! -h %{install_dir}/index.php ; then
		ln -s %{install_dir}/sisiya_gui.php %{install_dir}/index.php
	fi
	if test ! -h %{install_dir}/images/links ; then
		mkdir -p %{shared_state_dir}/%{name}/links
		ln -s %{shared_state_dir}/%{name}/links %{install_dir}/images/links
	fi
	if test ! -h %{install_dir}/images/systems ; then
		ln -s %{shared_state_dir}/sisiya-webui-images %{install_dir}/images/systems
	fi
	if test ! -h %{install_dir}/images/tmp ; then
		mkdir -p /var/tmp/%{name}
		ln -s /var/tmp/%{name} %{install_dir}/images/tmp
	fi
	if test ! -h %{install_dir}/packages ; then
		ln -s %{shared_state_dir}/%{name}/packages %{install_dir}/packages
	fi
	if test ! -h %{install_dir}/xmlconf ; then
		ln -s /etc/sisiya/sisiya-remote-checks/conf.d %{install_dir}/xmlconf
	fi
	chown -R %{www_user}:%{www_group} %{install_dir}
	chown -R %{www_user}:%{www_group} %{shared_state_dir}/%{name}
	chown -R %{www_user}:%{www_group} /var/tmp/%{name}

%preun
# initial installation is 1
# uninstallation is 0
if test "$1" == "0" ; then
	for f in %{install_dir}/index.php %{install_dir}/images/links %{install_dir}/images/systems %{install_dir}/images/tmp %{install_dir}/packages %{install_dir}/xmlconf
	do
		rm -f $f
	done
	for d in /var/tmp/%{name} %{shared_state_dir}/%{name}/links
	do
		rm -rf $d
	done
fi

%build

%files
%attr(0755,root,root) %dir %{_sysconfdir}/sisiya
%defattr(-,%{www_user},%{www_group})
%dir %{_sysconfdir}/sisiya/%{name}
%dir %{install_dir}
%dir %{install_dir}/javascript
%dir %{install_dir}/lib
%dir %{install_dir}/style
%dir %{install_dir}/images
%dir %{install_dir}/images/sisiya
%dir %{install_dir}/install
%dir %{install_dir}/XMPPHP
%dir %{shared_state_dir}/%{name}
%dir %{shared_state_dir}/%{name}/packages
%dir %{install_dir}/utils
# for openSUSE factory
%if 0%{?suse_version} > 1500
%dir %{_sysconfdir}/cron.d
%endif
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/cron.d/sisiya-alerts
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/cron.d/sisiya-archive
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/cron.d/sisiya-check-expired
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/cron.d/sisiya-rss
%config(noreplace) %{_sysconfdir}/sisiya/%{name}/*.php
%config(noreplace) %{_sysconfdir}/sisiya/%{name}/*.conf
%{install_dir}/*.*
%{install_dir}/javascript/*.js
%{install_dir}/lib/*.php
%{install_dir}/style/*.css
%{install_dir}/images/sisiya/*.*
%{install_dir}/install/*
%{shared_state_dir}/%{name}/packages/*.*
%{install_dir}/XMPPHP/*.php
%{install_dir}/utils/*.php
%{install_dir}/utils/*.sh

%changelog
