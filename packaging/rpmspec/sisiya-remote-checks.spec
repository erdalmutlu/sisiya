#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

%define www_user  apache
%define www_group apache

%if 0%{?suse_version} || 0%{?sle_version}
%define www_user  wwwrun
%define www_group root
%endif

Name: sisiya-remote-checks
Summary: SisIYA remote check programs that are run from a central server 
Url: http://www.sisiya.org
%define install_dir /usr/share/%{name}
Version: __VERSION__
Release: 0
BuildArch: noarch
BuildRoot: %{_builddir}/%{name}-root
Source0: http://sourceforge.net/projects/sisiya/files/sisiya/%{version}/rpm/%{name}-%{version}.tar.gz
License: GPL-2.0+
Vendor: Erdal Mutlu
Group: System Environment/Daemons
Packager: Erdal Mutlu <erdal@sisiya.org>
Requires: bash, bind-utils, curl, ftp, iputils, net-snmp, net-snmp-utils, perl, perl-XML-Simple, perl-Net-SNMP, perl-Socket6, samba-client, sisiya-client-checks
AutoReqProv: no
%description 
Summary: The SisIYA server / remote check programs that are run from a central server. This is normally the server where SisIYA daemon runs.

%prep 
%setup -n %{name}-%{version}

%install
make DESTDIR=%{buildroot} INSTALL_DIR=%{install_dir} DOC_DIR=%{_docdir} MAN_DIR=%{_mandir} SYSCONF_DIR=%{_sysconfdir}  install 

%post
# change ownership of some files and directories so that sisiya-webui package can access them in order to change conf files
#chgrp    %{www_group}	%{_sysconfdir}/sisiya/sisiya-remote-checks
#chgrp -R %{www_group}	%{_sysconfdir}/sisiya/sisiya-remote-checks/conf.d

%files
%defattr(-,root,root)
%dir %{_sysconfdir}/sisiya
%dir %{_sysconfdir}/sisiya/%{name}
%dir %{_sysconfdir}/sisiya/%{name}/conf.d
%dir %{install_dir}
%dir %{install_dir}/lib
%dir %{install_dir}/misc
%dir %{install_dir}/misc/sample_command_outputs
%dir %{install_dir}/scripts
%dir %{install_dir}/src
%dir %{install_dir}/utils
# for openSUSE factory
%if 0%{?suse_version} > 1500
%dir %{_sysconfdir}/cron.d
%endif
%config(noreplace) %{_sysconfdir}/cron.d/sisiya-remote-checks
%config(noreplace) %{_sysconfdir}/sisiya/%{name}/conf.d/*
%config(noreplace) %{_sysconfdir}/sisiya/%{name}/SisIYA_Remote_Config.pm
%config(noreplace) %{_sysconfdir}/sisiya/%{name}/SisIYA_Remote_Config_local.conf
%{install_dir}/lib/*.*
%{install_dir}/misc/*.*
%{install_dir}/misc/sample_command_outputs/*.*
%{install_dir}/scripts/*.*
%{install_dir}/src/*.*
%{install_dir}/utils/*.*
%dir %{_docdir}/%{name}
%{_docdir}/%{name}/*
